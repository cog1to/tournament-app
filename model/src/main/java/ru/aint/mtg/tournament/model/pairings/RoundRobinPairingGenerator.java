package ru.aint.mtg.tournament.model.pairings;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.DummyPlayer;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Round Robin Pairing Generator.
 */
public class RoundRobinPairingGenerator extends PairingGenerator {

    @Override
    public List<Match> getPairings(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }

        TournamentState state = stateOpt.some();
        if (state.getFormat() != TournamentFormat.RoundRobin && state.getFormat() != TournamentFormat.DoubleRoundRobin) {
            throw new IllegalArgumentException("Tournament format is not Round Robin");
        }

        // If last round is not finished, don't generate any new pairs.
        Option<Round> lastRound = tournament.lastEventOfType(Round.class);
        if (lastRound.isSome() && !lastRound.some().isFinished()) {
            return List.list();
        }

        TournamentConfig config = state.getConfig();
        final Random random = new Random(new Date().getTime());

        // Registration event.
        Registration registration = tournament.lastEventOfType(Registration.class).some();

        // Get initial grid
        List<Player> players = registration.getPlayers();
        if (players.length() % 2 != 0) {
            Player byePlayer = new DummyPlayer();
            players = players.snoc(byePlayer);
        }

        // Replace players that left with dummies.
        players = players.map(player -> {
            if (player instanceof DummyPlayer) {
                return player;
            }

            PlayerState currentPlayerState = state.getPlayerStates().find(ps -> ps.getPlayer().equals(player)).some();
            if (currentPlayerState.getStatus() == PlayerStatus.Active) {
                return player;
            } else {
                return new DummyPlayer();
            }
        });

        // Rotate the moving players list while keeping first player 'fixed'.
        List<Player> rotationList = players.tail();
        int roundsPlayed = tournament.eventsSince(registration).filter(e -> e instanceof Round).length();
        rotationList = Utils.rotate(rotationList, roundsPlayed);

        // Generate new pairs.
        List<List<Player>> pairs = Utils.takeAll(rotationList.cons(players.head()), list -> {
            return List.list(list.head(), list.last());
        });

        // Remove dummy players.
        List<List<Player>> pairsWithByes = pairs.map(pair -> pair.filter(p -> !(p instanceof DummyPlayer)));
        pairsWithByes = pairsWithByes.filter(pair -> pair.length() > 0);

        // Get our matches.
        List<Match> matches = pairsWithByes.map(list -> new Match(list));
        matches.filter(m -> m.getScores().size() == 1).foreach(m -> {
            m.setScore(m.getScores().keys().head(), Option.some(config.getWinsPerMatch()));
            return Unit.unit();
        });

        /*
        List<PlayerState> activeStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
        List<List<PlayerState>> pairs = Utils.takeAll(activeStates, (List<PlayerState> list) -> {

            final PlayerState nextState = list.index(random.nextInt(list.length()));

            List<PlayerState> statesLeft = list.filter(ps -> !ps.equals(nextState) && !ps.playedAgainst(nextState.getPlayer()));
            if (statesLeft.length() > 0) {
                return List.list(nextState, statesLeft.index(random.nextInt(statesLeft.length())));
            }

            // If there is no pair at all, we have to give it a bye.
            return List.list(nextState);
        });

        // Set byes.
        List<Match> matches = pairs.map(list -> new Match(list.map(ps -> ps.getPlayer())));
        matches.filter(m -> m.getScores().size() == 1).foreach(m -> {
            m.setScore(m.getScores().keys().head(), Option.some(config.getWinsPerMatch()));
            return Unit.unit();
        });
        */

        return matches;
    }
}
