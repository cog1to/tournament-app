package ru.aint.mtg.tournament.model.brackets;

import fj.P2;
import fj.data.List;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.promises.MatchPromise;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Bracket generator for single elimination events.
 */
public class SingleEliminationBracketGenerator implements IBracketGenerator {
    @Override
    public List<Event> getBracket(List<Player> players, IStringsProvider stringsProvider) {
        if (players == null) {
            throw new IllegalArgumentException("List of players for bracket generator cannot be null");
        }

        if (players.length() < 2) {
            throw new IllegalArgumentException("List of players for bracket generator cannot be less than 2");
        }

        if (stringsProvider == null) {
            throw new IllegalArgumentException("Localized strings provider is not provided");
        }

        List<Event> rounds = List.list();

        // Minus first round.
        long numberOfByes = Utils.nextHighestPowerOf(2, players.length()) - players.length();
        if (numberOfByes != 0) {
            // Split all players into byes and pairs.
            P2<List<Player>, List<Player>> split = Utils.take(players, l -> l.take((int)numberOfByes));

            if (split._1().length() > split._2().length()) {
                // Generate paired matches. Pair first with last.
                List<List<Player>> pairs = Utils.takeAll(split._2(), l -> List.list(l.head(), l.last()));
                List<Match> matches = pairs.map(pls -> {
                    Match m = new Match(pls);
                    m.setVersion("2.0");
                    return m;
                });
                Round zeroRound = new Round(matches);
                zeroRound.setVersion("2.0");
                zeroRound.setName(stringsProvider.getRoundString(1));

                for (int idx = 0; idx < zeroRound.getMatches().length(); idx++) {
                    zeroRound.getMatches().index(idx).setName(stringsProvider.getMatchString(idx + 1));
                }

                rounds = rounds.cons(zeroRound);

                // Pair rest of players with winners of zero round.
                List<Match> round2Matches = List.list();
                for (int index = 0; index < matches.length(); index++) {
                    Player promise = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(),  zeroRound.getName(), matches.index(index).getName()), matches.index(index).getId(), false);
                    Match byedMatch = new Match(List.list(split._1().index(index), promise));
                    byedMatch.setVersion("2.0");
                    byedMatch.setName(stringsProvider.getMatchString(index + 1));
                    round2Matches = round2Matches.cons(byedMatch);
                }

                // Pair rest of the players with each other.
                for (int index = matches.length(); index < split._1().length(); index = index + 2) {
                    Player promise1 = split._1().index(index);
                    Player promise2 = split._1().index(index+1);
                    Match unpairedMatch = new Match(List.list(promise1, promise2));
                    unpairedMatch.setName(stringsProvider.getMatchString(index + 1));
                    unpairedMatch.setVersion("2.0");
                    round2Matches = round2Matches.cons(unpairedMatch);
                }

                Round firstRound = new Round(round2Matches);
                firstRound.setVersion("2.0");
                firstRound.setName(stringsProvider.getRoundString(2));
                rounds = rounds.cons(firstRound);
            } else {
                // Generate paired matches. Pair first with last.
                List<List<Player>> pairs = Utils.takeAll(split._2(), l -> List.list(l.head(), l.last()));
                List<Match> matches = pairs.map(pls -> {
                    Match m = new Match(pls);
                    m.setVersion("2.0");
                    return m;
                });
                Round zeroRound = new Round(matches);
                zeroRound.setVersion("2.0");
                zeroRound.setName(stringsProvider.getRoundString(1));

                for (int idx = 0; idx < zeroRound.getMatches().length(); idx++) {
                    zeroRound.getMatches().index(idx).setName(stringsProvider.getMatchString(idx + 1));
                }

                rounds = rounds.cons(zeroRound);

                // Pair rest of players with winners of zero round.
                List<Match> round2Matches = List.list();
                if (split._1().length() > matches.length()) {
                    for (int index = 0; index < matches.length(); index++) {
                        Player promise = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), zeroRound.getName(), matches.index(index).getName()), matches.index(index).getId(), false);
                        Match byedMatch = new Match(List.list(split._1().index(index), promise));
                        byedMatch.setVersion("2.0");
                        byedMatch.setName(stringsProvider.getMatchString(index + 1));
                        round2Matches = round2Matches.cons(byedMatch);
                    }

                    // Pair rest of the players with each other.
                    for (int index = matches.length(); index < split._1().length(); index = index + 2) {
                        Player player1 = split._1().index(index);
                        Player player2 = split._1().index(index + 1);
                        Match unpairedMatch = new Match(List.list(player1, player2));
                        unpairedMatch.setVersion("2.0");
                        unpairedMatch.setName(stringsProvider.getMatchString(index + 1));
                        round2Matches = round2Matches.cons(unpairedMatch);
                    }

                    Round firstRound = new Round(round2Matches);
                    firstRound.setVersion("2.0");
                    firstRound.setName(stringsProvider.getRoundString(2));
                    rounds = rounds.cons(firstRound);
                } else {
                    for (int index = 0; index < split._1().length(); index++) {
                        Player promise = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), zeroRound.getName(), matches.index(index).getName()), matches.index(index).getId(), false);
                        Match byedMatch = new Match(List.list(split._1().index(index), promise));
                        byedMatch.setName(stringsProvider.getMatchString(index + 1));
                        byedMatch.setVersion("2.0");
                        round2Matches = round2Matches.cons(byedMatch);
                    }

                    // Pair rest of the players with each other.
                    for (int index = split._1().length(); index < matches.length(); index = index + 2) {
                        Player promise1 = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), zeroRound.getName(), matches.index(index).getName()), matches.index(index).getId(), false);
                        Player promise2 = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), zeroRound.getName(), matches.index(index+1).getName()), matches.index(index+1).getId(), false);
                        Match unpairedMatch = new Match(List.list(promise1, promise2));
                        unpairedMatch.setVersion("2.0");
                        unpairedMatch.setName(stringsProvider.getMatchString(index + 1));
                        round2Matches = round2Matches.cons(unpairedMatch);
                    }

                    Round firstRound = new Round(round2Matches);
                    firstRound.setVersion("2.0");
                    firstRound.setName(stringsProvider.getRoundString(2));
                    rounds = rounds.cons(firstRound);
                }
            }
        } else {
            // Generate paired matches. Pair first with last.
            List<List<Player>> pairs = Utils.takeAll(players, l -> List.list(l.head(), l.last()));
            List<Match> matches = pairs.map(pls -> {
                Match m = new Match(pls);
                m.setVersion("2.0");
                return m;
            });
            Round firstRound = new Round(matches);
            firstRound.setName(stringsProvider.getRoundString(1));
            firstRound.setVersion("2.0");

            for (int idx = 0; idx < firstRound.getMatches().length(); idx++) {
                firstRound.getMatches().index(idx).setName(stringsProvider.getMatchString(idx + 1));
            }

            rounds = rounds.cons(firstRound);
        }

        // Continue until last round has only one match.
        int currentIndex = rounds.length() + 1;
        while (((Round)rounds.head()).getMatches().length() > 1) {
            List<Match> matches = List.list();
            for (int index = 0; index < ((Round)rounds.head()).getMatches().length(); index = index + 2) {
                Round round = (Round)rounds.head();
                Match match1 = round.getMatches().index(index);
                Match match2 = round.getMatches().index(index+1);

                Player promise1 = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), round.getName(), match1.getName()), match1.getId(), false);
                Player promise2 = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), round.getName(), match2.getName()), match2.getId(), false);
                Match match = new Match(List.list(promise1, promise2));
                match.setVersion("2.0");
                match.setName(stringsProvider.getMatchString(index + 1));
                matches = matches.cons(match);
            }
            Round nextRound = new Round(matches);
            nextRound.setVersion("2.0");
            nextRound.setName(stringsProvider.getRoundString(currentIndex++));
            rounds = rounds.cons(nextRound);
        }

        return rounds.reverse();
    }
}