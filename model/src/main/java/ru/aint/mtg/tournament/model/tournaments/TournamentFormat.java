package ru.aint.mtg.tournament.model.tournaments;

/**
 * Tournament format.
 */
public enum TournamentFormat {
    SingleElimination,
    Swiss,
    RoundRobin,
    DoubleRoundRobin,
    DoubleElimination
}
