package ru.aint.mtg.tournament.model.events;

import fj.P;
import fj.P2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Round event holding matches.
 */
public class Round extends Event {

    /**
     * List of matches.
     */
    List<Match> matches;

    /**
     * Returns new Round object with given values.
     *
     * @param timestamp Timestamp.
     * @param matches List of matches.
     */
    public Round(long timestamp, List<Match> matches) {
        super(timestamp);
        this.matches = matches;
    }

    /**
     * Returns new Round instance with given list of matches and current timestamp.
     *
     * @param matches List of matches.
     */
    public Round(List<Match> matches) {
        super();
        this.matches = matches;
    }

    /**
     * Returns list of matches.
     *
     * @return List of matches.
     */
    public List<Match> getMatches() {
        return matches;
    }

    /**
     * Sets new list of matches.
     *
     * @param matches List of matches.
     */
    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    @Override
    public boolean isFinished() {
        return !this.matches.exists(m -> !m.isFinished());
    }

    @Override
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (state.isNone()) {
            return Option.none();
        }

        if (!isFinished()) {
            return state;
        }

        // Update match results
        Option<TournamentState> stateAfterMatches = this.matches.foldLeft((s, m) -> m.transformState(s), state);

        // Update OMWP and OGWP for each player. We can't calculate it earlier, because it depends on all match results.
        List<PlayerState> updatedPlayerStates = stateAfterMatches.some().getPlayerStates();

        if (state.some().getFormat() == TournamentFormat.SingleElimination) {
            // Eliminated/dropped players.
            List<PlayerState> other = updatedPlayerStates.filter(s -> s.getStatus() != PlayerStatus.Active && s.getStatus() != PlayerStatus.Loser);

            // Advancing players in order of matches.
            List<PlayerState> advancing = updatedPlayerStates.filter(s -> s.getStatus() == PlayerStatus.Active);

            return Option.some(new TournamentState(state.some().getFormat(), advancing.append(other), state.some().getConfig()));
        } else if (state.some().getFormat() == TournamentFormat.Swiss) {
            return Option.some(new TournamentState(state.some().getFormat(), updatedPlayerStates, state.some().getConfig()));
        } else if (state.some().getFormat() == TournamentFormat.RoundRobin) {
            return Option.some(new TournamentState(state.some().getFormat(), updatedPlayerStates, state.some().getConfig()));
        } else if (state.some().getFormat() == TournamentFormat.DoubleRoundRobin) {
            return Option.some(new TournamentState(state.some().getFormat(), updatedPlayerStates, state.some().getConfig()));
        } else if (state.some().getFormat() == TournamentFormat.DoubleElimination) {
            // Eliminated/dropped players.
            List<PlayerState> other = updatedPlayerStates.filter(s -> s.getStatus() != PlayerStatus.Active && s.getStatus() != PlayerStatus.Loser);

            // Advancing players in order of matches.
            List<PlayerState> advancing = updatedPlayerStates.filter(s -> s.getStatus() == PlayerStatus.Active);
            List<PlayerState> advOrdered = this.matches.foldLeft((List<PlayerState> list, Match m) -> {
                Player player = m.winner().some();
                Option<PlayerState> playerState = advancing.find(ps -> ps.getPlayer().equals(player));
                if (playerState.isSome()) {
                    return list.cons(playerState.some());
                } else {
                    return list;
                }
            }, List.list());
            List<PlayerState> advancingOrdered = advOrdered.append(advancing.filter(ps -> !advOrdered.exists(ps1 -> ps1.getPlayer().equals(ps.getPlayer()))));

            // Losers.
            List<PlayerState> losers = updatedPlayerStates.filter(s -> s.getStatus() == PlayerStatus.Loser);
            List<PlayerState> oldLosers = losers.filter(ps -> state.some().getPlayerStates().find(oldPs -> oldPs.getPlayer().equals(ps.getPlayer())).some().getStatus() == PlayerStatus.Loser);

            // First order new losers in order of matches.
            List<PlayerState> newLosers = losers.filter(ps -> state.some().getPlayerStates().find(oldPs -> oldPs.getPlayer().equals(ps.getPlayer())).some().getStatus() == PlayerStatus.Active);
            List<PlayerState> newLosOrdered = this.matches.foldLeft((List<PlayerState> list, Match match) -> {
                Player winner = match.winner().some();
                if (match.isBye()) {
                    return list;
                }

                Player player = match.getScores().keys().filter(p -> !p.equals(winner)).index(0);
                Option<PlayerState> playerState = newLosers.find(ps -> ps.getPlayer().equals(player));
                if (playerState.isSome()) {
                    return list.cons(playerState.some());
                } else {
                    return list;
                }
            }, List.list());
            List<PlayerState> newLosersOrdered = newLosOrdered.append(newLosers.filter(ps -> !newLosOrdered.exists(ps1 -> ps1.getPlayer().equals(ps.getPlayer()))));

            // Then insert new losers into bracket between old losers.
            List<PlayerState> zipped = List.list();
            List<PlayerState> newLosersUnzipped = newLosersOrdered;

            if ((advancing.length() > 0) && (advancing.index(0).getMatchHistory().length() % 2 == 0)) {
                newLosersUnzipped = newLosersUnzipped.drop(newLosersUnzipped.length() / 2).append(newLosersUnzipped.take(newLosersUnzipped.length() / 2));
            }

            for (int idx = 0; idx < oldLosers.length(); idx++) {
                PlayerState old = oldLosers.index(idx);
                zipped = zipped.cons(old);

                if (idx < newLosersUnzipped.length()) {
                    zipped = zipped.cons(newLosersUnzipped.head());
                    newLosersUnzipped = newLosersUnzipped.tail();
                }
            }

            // Anyone left will be inserted into the end of list.
            if (newLosersUnzipped.length() > 0) {
                zipped = zipped.append(newLosersUnzipped);
            }

            return Option.some(new TournamentState(state.some().getFormat(), advancingOrdered.append(zipped).append(other), state.some().getConfig()));
        } else {
            throw new IllegalArgumentException("Current tournament format is not supported");
        }
    }

    public boolean hasReadyMatches() {
        return matches.exists(match -> match.isReady());
    }

    @Override
    public String toString() {
        String toString = "Round{\n";
        List<Match> matches = this.matches;
        for (Match m: matches) {
            toString = toString + m.toString();
        }
        toString += "}\n";
        return toString;
    }
}
