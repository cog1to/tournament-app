package ru.aint.mtg.tournament.model.tiebreakers;

import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Match points tie-breaker.
 */
public class MatchPointsTieBreaker extends TieBreaker {
    @Override
    public int compare(PlayerState state1, PlayerState state2, TournamentState state) {
        if (state1.getMatchPoints(state.getConfig()) > state2.getMatchPoints(state.getConfig())) {
            return GREATER_THAN;
        } else if (state1.getMatchPoints(state.getConfig()) < state2.getMatchPoints(state.getConfig())) {
            return LESS_THAN;
        }

        return TIED;
    }

    @Override
    public double getValue(PlayerState state, TournamentState tournamentState) {
        return state.getMatchPoints(tournamentState.getConfig());
    }
}
