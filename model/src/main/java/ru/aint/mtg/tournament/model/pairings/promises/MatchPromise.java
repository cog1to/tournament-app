package ru.aint.mtg.tournament.model.pairings.promises;

import ru.aint.mtg.tournament.model.players.Player;

/**
 * Promise object.
 */

public class MatchPromise extends Player {
    /**
     * Match ID.
     */
    final String matchId;

    /**
     * Flag indicating whether promise wants a looser of a match. If false, it will expect a winner.
     */
    final boolean looser;

    /**
     * Returns new instance of a Promise.
     *
     * @param name Promise name.
     * @param matchId Match ID.
     * @param looser Looser flag.
     */
    public MatchPromise(String name, String matchId, boolean looser) {
        super(name);
        this.matchId = matchId;
        this.looser = looser;
    }

    /**
     * Returns new instance of a Promise.
     *
     * @param matchId Match ID.
     * @param looser Looser flag.
     */
    public MatchPromise(String matchId, boolean looser) {
        super((looser ? "Looser" : "Winner") + " of Match '" + matchId + "'");
        this.matchId = matchId;
        this.looser = looser;
    }

    /**
     * @return Match ID.
     */
    public String getMatchId() {
        return this.matchId;
    }

    /**
     * @return Looser flag.
     */
    public boolean getLooser() {
        return this.looser;
    }
}
