package ru.aint.mtg.tournament.model.pairings;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Double round robin pairings generator.
 */
public class DoubleRoundRobinPairingGenerator extends PairingGenerator {
    @Override
    public List<Match> getPairings(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }

        TournamentState state = stateOpt.some();
        if (state.getFormat() != TournamentFormat.DoubleRoundRobin) {
            throw new IllegalArgumentException("Tournament format is not Double Round Robin");
        }

        // If last round is not finished, don't generate any new pairs.
        Option<Round> lastRound = tournament.lastEventOfType(Round.class);
        if (lastRound.isSome() && !lastRound.some().isFinished()) {
            return List.list();
        }

        int numberOfRounds = tournament.getEvents().filter(e -> e instanceof Round).length();
        int numberOfPlayers = state.getPlayerStates().length();

        // First pass - use standard round robin.
        if (numberOfRounds < (numberOfPlayers - 1)) {
            RoundRobinPairingGenerator generator = new RoundRobinPairingGenerator();
            return generator.getPairings(tournament);
        }

        // Second pass - repeat first pass.
        int currentRoundInSecondPass = numberOfRounds - (numberOfPlayers - 1);

        // Get all pairings from corresponding round.
        Round previousRound = (Round)tournament.getEvents().reverse().filter(e -> e instanceof Round).index(currentRoundInSecondPass);
        List<List<Player>> previousPairings = previousRound.getMatches().map(m -> m.getScores().keys());

        // Adjust list so all dropped players are removed from the list.
        previousPairings = previousPairings.map(l -> {
            return l.foldLeft((list, player) -> {
                PlayerState playerState = state.getPlayerStates().filter(ps -> ps.getPlayer().equals(player)).index(0);
                if (playerState.getStatus() == PlayerStatus.Active) {
                    return list.cons(player);
                }

                return list;
            }, List.list());
        });

        // Set byes.
        List<Match> matches = previousPairings.filter(list -> list.length() > 0).map(list -> new Match(list));
        matches.filter(m -> m.getScores().size() == 1).foreach(m -> {
            m.setScore(m.getScores().keys().head(), Option.some(state.getConfig().getWinsPerMatch()));
            return Unit.unit();
        });

        return matches;
    }
}
