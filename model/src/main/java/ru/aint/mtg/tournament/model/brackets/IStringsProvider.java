package ru.aint.mtg.tournament.model.brackets;

/**
 * Strings provider interface.
 */
public interface IStringsProvider {
    String getMatchString(int matchIndex);
    String getRoundString(int roundIndex);
    String getLosersRoundString(int roundIndex);
    String getFinalsString();
    String getRematchString();
    String getLoserOfString();
    String getWinnerOfString();
    String getWinnersBracketString();
    String getLosersBracketString();
}
