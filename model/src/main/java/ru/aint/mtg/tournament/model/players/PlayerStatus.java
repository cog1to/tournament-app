package ru.aint.mtg.tournament.model.players;

/**
 * Player status enumeration.
 */
public enum PlayerStatus {
    Active,
    Dropped,
    Eliminated,
    Loser,
    Disqualified
}
