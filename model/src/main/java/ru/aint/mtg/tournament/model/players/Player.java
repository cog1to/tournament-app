package ru.aint.mtg.tournament.model.players;

import ru.aint.mtg.tournament.model.base.Persistable;

/**
 * Player.
 */
public class Player extends Persistable implements Comparable<Player> {

    /**
     * Player's full name.
     */
    final String fullName;

    /**
     * Returns new instance of a player object.
     *
     * @param firstName First name.
     * @param lastName Last name.
     */
    public Player(String firstName, String lastName) {
        super();

        if (firstName == null && lastName == null) {
            throw new IllegalArgumentException("Both first and last name cannot be null simultaneously");
        }

        String fullName = firstName;

        if (lastName != null) {
            if (firstName == null) {
                fullName = lastName;
            } else {
                fullName += " " + lastName;
            }
        }

        this.fullName = fullName;
    }

    /**
     * Returns new instance of a player object.
     *
     * @param name Player's name.
     */
    public Player(String name) {
        super();

        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }

        this.fullName = name;
    }

    /**
     * Returns player's name.
     *
     * @return Player's name.
     */
    public String getFullName() {
        return fullName;
    }

    @Override
    public String toString() {
        return "Player{" + '\'' +
                fullName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return fullName.equals(player.fullName);
    }

    @Override
    public int hashCode() {
        return fullName.hashCode();
    }

    @Override
    public int compareTo(Player o) {
        return this.getFullName().compareTo(o.getFullName());
    }
}
