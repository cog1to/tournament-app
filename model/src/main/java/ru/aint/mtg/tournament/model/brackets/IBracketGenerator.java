package ru.aint.mtg.tournament.model.brackets;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.players.Player;

/**
 * Bracket generator.
 */
public interface IBracketGenerator {
    /**
     * Returns a full bracket with all rounds and matches filled in with promises.
     *
     * @param players List of participants.
     * @param stringsProvider Localized strings provider.
     *
     * @return List of rounds.
     */
    List<Event> getBracket(List<Player> players, IStringsProvider stringsProvider);
}
