package ru.aint.mtg.tournament.model.tiebreakers;

/**
 * TieBreaker Factory.
 */
public final class TieBreakerFactory {

    /**
     * Returns a tie-breaker instance corresponding to a given tie-breaker type.
     *
     * @param type Tie-breaker type.
     *
     * @return Tie-breaker instance corresponding to a given tie-breaker type.
     */
    public static TieBreaker getTieBreaker(TieBreakerType type) {
        switch (type) {
            case MatchPoints:
                return new MatchPointsTieBreaker();
            case GameWinPercentage:
                return new GameWinPercentageTieBreaker();
            case OpponentsGameWinPercentage:
                return new OpponentsGameWinPercentageTieBreaker();
            case OpponentsMatchWinPercentage:
                return new OpponentsMatchWinPercentageTieBreaker();
            default:
                throw new IllegalArgumentException("Not supported tie-breaker type");
        }
    }
}
