package ru.aint.mtg.tournament.model.events;

import java.util.Date;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Bracket object.
 */
public class Bracket extends Event {
    public static final int BRACKET_TYPE_WINNERS = 0;
    public static final int BRACKET_TYPE_LOSERS = 1;

    private int bracketType;

    private List<Round> rounds;

    public Bracket(long timestamp, List<Round> rounds, int type) {
        super("2.0", timestamp);
        this.rounds = rounds;
        this.bracketType = type;
    }

    public Bracket(long timestamp, List<Round> rounds) {
        this(timestamp, rounds, BRACKET_TYPE_WINNERS);
    }

    public Bracket(List<Round> rounds) {
        this(new Date().getTime(), rounds);
    }

    @Override
    public boolean isFinished() {
        return (this.rounds == null || this.rounds.length() == 0 || !this.rounds.exists(round -> !round.isFinished()));
    }

    @Override
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (this.rounds == null || this.rounds.length() == 0) {
            return state;
        }

        return this.rounds.foldLeft((tmpState, round) -> round.transformState(tmpState), state);
    }

    public List<Round> getRounds() {
        return this.rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public int getType() {
        return bracketType;
    }
}
