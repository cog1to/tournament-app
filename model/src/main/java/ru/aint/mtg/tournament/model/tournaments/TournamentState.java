package ru.aint.mtg.tournament.model.tournaments;

import java.util.Collections;

import fj.Ord;
import fj.Ordering;
import fj.data.List;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreaker;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerFactory;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerType;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Tournament state.
 */
public class TournamentState {

    /**
     * Tournament format.
     */
    final TournamentFormat format;

    /**
     * List of player states.
     */
    final List<PlayerState> playerStates;

    /**
     * Active tournament config.
     */
    final TournamentConfig config;

    /**
     * Archived flag.
     */
    final boolean archived;

    /**
     * Returns new instance of Tournament State object.
     *
     * @param format Tournament format.
     * @param playerStates List of player states.
     * @param config Tournament config.
     * @param archived Archived indicator.
     */
    public TournamentState(TournamentFormat format, List<PlayerState> playerStates, TournamentConfig config, boolean archived) {
        this.format = format;
        this.playerStates = playerStates;
        this.config = config;
        this.archived = archived;
    }

    /**
     * Returns new instance of tournament state.
     *
     * @param format Format.
     * @param playerStates Player states.
     * @param config Config.
     */
    public TournamentState(TournamentFormat format, List<PlayerState> playerStates, TournamentConfig config) {
        this(format, playerStates, config, false);
    }

    /**
     * Returns current tournament format.
     * @return
     */
    public TournamentFormat getFormat() {
        return format;
    }

    /**
     * Returns states of all players.
     *
     * @return States of all players.
     */
    public List<PlayerState> getPlayerStates() {
        return playerStates;
    }

    /**
     * Returns current config.
     *
     * @return
     */
    public TournamentConfig getConfig() {
        return config;
    }

    /**
     * Checks if tournament is finished.
     * @return
     */
    public boolean isFinished() {
        if (format == TournamentFormat.SingleElimination) {
            int maxMatches = playerStates.length() > 0 ? Collections.max(playerStates.map(ps -> ps.getMatchHistory().length()).toJavaList()) : 0;
            return maxMatches > 0 && playerStates.length() > 1 && playerStates.filter(ps -> ps.getStatus() == PlayerStatus.Active).length() <= 1;
        } else if (format == TournamentFormat.Swiss) {
            List<PlayerState> activeStates = playerStates.filter(ps -> ps.getStatus() == PlayerStatus.Active);

            int maxMatches = playerStates.length() > 0 ? Collections.max(playerStates.map(ps -> ps.getMatchHistory().length()).toJavaList()) : 0;
            if (maxMatches == 0) {
                return false;
            }

            long power = Utils.nextHighestOrEqualPowerValueOf(2, playerStates.length());
            return activeStates.length() > 1 && maxMatches >= power;
        } else if (format == TournamentFormat.RoundRobin) {
            int maxMatches = playerStates.length() > 0 ? Collections.max(playerStates.map(ps -> ps.getMatchHistory().length()).toJavaList()) : 0;
            return (maxMatches == ((playerStates.length() + (playerStates.length() > 0 ? (playerStates.length() % 2) : 0)) - 1));
        } else if (format == TournamentFormat.DoubleRoundRobin) {
            int maxMatches = playerStates.length() > 0 ? Collections.max(playerStates.map(ps -> ps.getMatchHistory().length()).toJavaList()) : 0;
            return (maxMatches == ((playerStates.length() + (playerStates.length() > 0 ? (playerStates.length() % 2) : 0)) - 1) * 2);
        } else if (format == TournamentFormat.DoubleElimination) {
            int maxMatches = playerStates.length() > 0 ? Collections.max(playerStates.map(ps -> ps.getMatchHistory().length()).toJavaList()) : 0;
            return maxMatches > 0 && playerStates.length() > 1 && (playerStates.filter(ps -> ps.getStatus() == PlayerStatus.Active).length() + playerStates.filter(ps -> ps.getStatus() == PlayerStatus.Loser).length()) <= 1;
        }

        throw new IllegalStateException("Not supported tournament format: " + format.toString());
    }

    /**
     * Returns current standings sorted.
     *
     * @return List of players sorted by match results and tiebreakers.
     */
    public List<PlayerState> getSortedStandings() {
        return this.getPlayerStates().sort(Ord.ord(ps1 -> ps2 -> {
            PlayerState state1 = (PlayerState)ps1;
            PlayerState state2 = (PlayerState)ps2;

            if (this.format == TournamentFormat.DoubleElimination) {
                if (state2.getMatchesWon() > state1.getMatchesWon()) {
                    return Ordering.GT;
                } else if (state2.getMatchesWon() < state1.getMatchesWon()) {
                    return Ordering.LT;
                }

                if (state2.getStatus() == PlayerStatus.Active && state1.getStatus() != PlayerStatus.Active) {
                    return Ordering.GT;
                } else if (state2.getStatus() != PlayerStatus.Active && state1.getStatus() == PlayerStatus.Active) {
                    return Ordering.LT;
                } else if (state2.getStatus() == PlayerStatus.Loser && state1.getStatus() == PlayerStatus.Eliminated) {
                    return Ordering.GT;
                } else if (state2.getStatus() == PlayerStatus.Eliminated && state1.getStatus() == PlayerStatus.Loser) {
                    return Ordering.LT;
                }

                return Ord.stringOrd.compare(state1.getPlayer().getFullName(), state2.getPlayer().getFullName());
            }

            if (this.format == TournamentFormat.SingleElimination) {
                if (state2.getMatchesWon() > state1.getMatchesWon()) {
                    return Ordering.GT;
                } else if (state2.getMatchesWon() < state1.getMatchesWon()) {
                    return Ordering.LT;
                }

                if (state2.getStatus() == PlayerStatus.Active && state1.getStatus() != PlayerStatus.Active) {
                    return Ordering.GT;
                } else if (state2.getStatus() != PlayerStatus.Active && state1.getStatus() == PlayerStatus.Active) {
                    return Ordering.LT;
                }

                return Ord.stringOrd.compare(state1.getPlayer().getFullName(), state2.getPlayer().getFullName());
            }

            for (TieBreakerType type : this.getConfig().tieBreakers) {
                int result = TieBreakerFactory.getTieBreaker(type).compare(state2, state1, this);

                if (result == TieBreaker.GREATER_THAN) {
                    return Ordering.GT;
                }

                if (result == TieBreaker.LESS_THAN) {
                    return Ordering.LT;
                }

                if (result == TieBreaker.TIED) {
                    continue;
                }
            }

            return Ord.stringOrd.compare(state1.getPlayer().getFullName(), state2.getPlayer().getFullName());
        }));
    }

    /**
     * Returns 'archived' value.
     *
     * @return <b>true</b> if tournament was archived, <b>false</b> otherwise.
     */
    public boolean isArchived() {
        return this.archived;
    }
}
