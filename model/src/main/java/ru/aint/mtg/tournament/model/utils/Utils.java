package ru.aint.mtg.tournament.model.utils;

import fj.Equal;
import fj.F;
import fj.P2;
import fj.data.List;

import static fj.P.p;

/**
 * Utilities.
 */
public final class Utils {
    private Utils() { }

    /**
     * Rotates the input list, left to right given amount of times.
     *
     * @param list Input list.
     * @param times Number of times to apply rotation.
     * @param <T> List type.
     *
     * @return New list produced by rotating input list given amount of times.
     */
    public static <T> List<T> rotate(List<T> list, int times) {
        List<T> rotated = list;
        for (int i = 0; i < times; i++) {
            rotated = rotate(rotated);
        }

        return rotated;
    }

    /**
     * Rotates the input list left to right, pushing last element to the head of the list.
     *
     * @param list Input list.
     * @param <T> List type.
     *
     * @return New list with last element of the original list removed and pushed to the beginning.
     */
    public static <T> List<T> rotate(List<T> list) {
        if (list.isEmpty()) {
            return List.list();
        }

        T last = list.last();
        List<T> withoutLast = list.take(list.length() - 1);
        return withoutLast.cons(last);
    }

    /**
     * Takes some elements from a list and returns a pair of lists where the first element
     * is a list of taken items and the second element is a list of items left on original
     * list.
     *
     * @param in Input list
     * @param taker Function that takes some elements from a list.
     * @param <T> Type of items in the list.
     *
     * @return A pair of lists where the first element
     * is a list of taken items and the second element is a list of items left on original
     * list.
     */
    public static <T> P2<List<T>, List<T>> take(List<T> in, F<List<T>, List<T>> taker) {
        List<T> taken = taker.f(in);
        if (taken.filter(t -> !in.exists(et -> et == t)).length() > 0) {
            throw new IllegalStateException("Taker function generated a list containing element not present in original list");
        }

        List<T> left = in.filter(elem -> !taken.exists(t -> t == elem));
        return p(taken, left);
    }

    /**
     * Takes all available combinations of items produced by given taker function.
     *
     * @param in Input list.
     * @param taker Function that takes some elements from a list.
     * @param <T> Type of items in the list.
     *
     * @return A list containing lists of items produced by applying taker function
     * continuously over given list.
     */
    public static <T> List<List<T>> takeAll(List<T> in, F<List<T>, List<T>> taker) {
        P2<List<T>, List<T>> takeResult = take(in, taker);
        if (takeResult._1().isEmpty()) {
            List<T> empty = List.list();
            List<List<T>> result = List.list();
            return result.cons(empty);
        } else if (takeResult._2().isEmpty()) {
            List<List<T>> result = List.list();
            return result.cons(takeResult._1());
        } else {
            return takeAll(takeResult._2(), taker).cons(takeResult._1());
        }
    }

    /**
     * Replaces all elements in a list that satisfy the condition with given new element.
     *
     * @param in Input list.
     * @param condition Condition to match elements against.
     * @param <T> Type of items in the list.
     *
     * @return New list with all matching elemnts replaced with given new element.
     */
    public static <T> List<T> replace(List<T> in, F<T, Boolean> condition, T newElem) {
        if (in.isEmpty()) {
            return in;
        }

        if (condition.f(in.head())) {
            return replace(in.tail(), condition, newElem).cons(newElem);
        } else {
            return replace(in.tail(), condition, newElem).cons(in.head());
        }
    }

    /**
     * Replaces all elements in a list that satisfy the condition with given new element
     * rr adds new element to the head of the list if none match.
     *
     * @param in Input list.
     * @param condition Condition to match elements against.
     * @param <T> Type of items in the list.
     *
     * @return New list with all matching elemnts replaced with given new element.
     */
    public static <T> List<T> addOrReplace(List<T> in, F<T, Boolean> condition, T newElem) {
        if (in.isEmpty()) {
            return in.cons(newElem);
        }

        if (in.exists(condition)) {
            return replace(in, condition, newElem);
        }

        return in.cons(newElem);
    }

    /**
     * Returns the number that is the closest highest power of base that exceeds target number.
     *
     * @param base Base number to compute powers of.
     * @param target Target number to exceed.
     *
     * @return Nearest number that is a power of base number that is higher than or equal to a target number.
     */
    public static long nextHighestPowerOf(long base, long target) {
        if (base <= 1) {
            throw new IllegalArgumentException("Base number must be a positive integer greater than 1");
        }

        if (target <= 0) {
            throw new IllegalArgumentException("Target number must be a positive integer");
        }

        long pow = 0;
        long res = base;
        while (res < target) {
            res = (long)Math.pow(base, pow);
            pow += 1;
        }
        return res;
    }

    /**
     * Returns the power value that given base number must be raised to to be equal or higher than target number.
     *
     * @param base Base number to compute powers of.
     * @param target Target number to exceed.
     *
     * @return The power value that given base number must be raised to to be equal or higher than target number.
     */
    public static long nextHighestOrEqualPowerValueOf(long base, long target) {
        if (base <= 1) {
            throw new IllegalArgumentException("Base number must be a positive integer greater than 1");
        }

        if (target <= 0) {
            throw new IllegalArgumentException("Target number must be a positive integer");
        }

        long pow = 0;
        long res = (long)Math.pow(base, pow);
        while (res < target) {
            pow += 1;
            res = (long)Math.pow(base, pow);
        }
        return pow;
    }

    /**
     * Returns the number that is the closest lowest power of the base that is lower than a target number.
     *
     * @param base Base number to compute powers of.
     * @param target Target number to compare to.
     *
     * @return Number that is a power of the base number and is nearest to and lower than the target number.
     */
    public static long nextLowestPowerOf(long base, long target) {
        if (base <= 1) {
            throw new IllegalArgumentException("Base number must be a positive integer greater than 1");
        }

        if (target < base) {
            throw new IllegalArgumentException("Target number must be greater than base number");
        }

        long pow = 0;
        long res = base;
        while (res < target) {
            res = (long)Math.pow(base, pow);
            pow += 1;
        }
        return (long)Math.pow(base, pow - 2);
    }
}
