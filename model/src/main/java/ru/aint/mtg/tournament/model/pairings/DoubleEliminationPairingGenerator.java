package ru.aint.mtg.tournament.model.pairings;

import java.util.Date;

import fj.Ord;
import fj.P2;
import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Double elimination pairings generator.
 */
public class DoubleEliminationPairingGenerator extends PairingGenerator {
    @Override
    public List<Match> getPairings(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }

        TournamentState state = stateOpt.some();
        if (state.getFormat() != TournamentFormat.DoubleElimination) {
            throw new IllegalArgumentException("Tournament format is not Double Elimination");
        }

        TournamentConfig config = state.getConfig();

        // Special treatment for the first match. We must calculate byes and pair players without byes.
        if (tournament.lastEventOfType(Round.class).isNone()) {
            // Get all player states.
            List<PlayerState> playerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
            List<Player> players = playerStates.map(ps -> ps.getPlayer());

            long numberOfByes = Utils.nextHighestPowerOf(2, playerStates.length()) - playerStates.length();

            // Split all players into byes and pairs.
            P2<List<Player>, List<Player>> split = Utils.take(players, l -> l.take((int)numberOfByes));

            // Generate bye matches.
            Option<Integer> byeScore = Option.some(state.getConfig().getWinsPerMatch());
            List<Match> byes = split._1().map(p -> new Match(Map.from(((List<Player>) List.list(p)).zip(List.list(Option.some(byeScore))))));

            // Generate paired matches. Pair first with last.
            List<List<Player>> pairs = Utils.takeAll(split._2(), l -> List.list(l.head(), l.last()));
            List<Match> matches = pairs.map(pls -> new Match(pls));

            return byes.append(matches);
        }

        // If first round was preliminary (i.e. included byes), the next round would be the REAL first round
        // so we need to pair players first-to-last again.
        Option<Registration> lastRegistration = tournament.lastEventOfType(Registration.class);
        List<Event> pastRounds = tournament.eventsSince(lastRegistration.some()).filter(e -> e instanceof Round);
        if ((pastRounds.length() == 1) && ((Round)pastRounds.head()).getMatches().filter(m -> m.isBye()).length() > 0) {
            List<PlayerState> playerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
            List<Player> players = playerStates.map(ps -> ps.getPlayer());

            // Generate paired matches. Pair first with last.
            List<List<Player>> pairs = Utils.takeAll(players, l -> List.list(l.head(), l.last()));
            List<Match> matches = pairs.map(pls -> new Match(pls));

            return matches;
        }

        // For all other rounds, pair closest active players.
        List<PlayerState> activePlayerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
        List<PlayerState> losersPlayerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser);

        // If there is exactly one Active and one Loser player, pair them together
        if (activePlayerStates.length() == 1 && losersPlayerStates.length() == 1) {
            return List.list(new Match(List.list(activePlayerStates.index(0).getPlayer(), losersPlayerStates.index(0).getPlayer())));
            // If there are two losers, pair them together
        } else if (activePlayerStates.length() == 0 && losersPlayerStates.length() == 2) {
            return List.list(new Match(losersPlayerStates.map(ps -> ps.getPlayer())));
            // If there is exactly one active player or exactly one loser, then the tournament is finished.
        } else if (activePlayerStates.length() + losersPlayerStates.length() == 1) {
            return List.list();
        }

        // Otherwise continue with normal pairings.
        List<Match> activeMatches = List.list();

        // Winners bracket matches.
        List<Match> currentWinnerMatches = (tournament.getEvents().filter(e -> e instanceof Round).bind(e -> {
            List<Match> matches = ((Round)e).getMatches();
            TournamentState stateBeforeRound = tournament.stateBeforeEvent(e).some();

            List<Match> winnerMatches = matches.filter(m -> m.getScores().keys().forall(player -> {
                PlayerState playerState = stateBeforeRound.getPlayerStates().find(ps -> ps.getPlayer().equals(player)).some();
                return playerState.getStatus() == PlayerStatus.Active;
            }));

            return winnerMatches;
        }));

        if (currentWinnerMatches.length() == 0 || currentWinnerMatches.forall(m -> m.isFinished())) {
            List<List<Player>> pairs = Utils.takeAll(activePlayerStates.map(ps -> ps.getPlayer()), l -> l.take(2));
            activeMatches = (activePlayerStates.length() > 0) ? pairs.map(p -> new Match(p)).filter(m -> m.getScores().size() > 1) : List.list();
        }

        // Losers bracket matches.
        List<Match> currentLosersMatches = (tournament.getEvents().filter(e -> e instanceof Round).bind(e -> {
            List<Match> matches = ((Round) e).getMatches();
            TournamentState stateBeforeRound = tournament.stateBeforeEvent(e).some();

            List<Match> losersMatches = matches.filter(m -> m.getScores().keys().forall(player -> {
                PlayerState playerState = stateBeforeRound.getPlayerStates().find(ps -> ps.getPlayer().equals(player)).some();
                return playerState.getStatus() == PlayerStatus.Loser;
            }));

            return losersMatches;
        }));

        List<Match> losersMatches = List.list();
        if (losersPlayerStates.length() > 0 && currentLosersMatches.forall(m -> m.isFinished()) && losersPlayerStates.length() > 1) {
            final int minMatchHistoryLength = losersPlayerStates.map(ps -> ps.getMatchHistory().length()).minimum(Ord.intOrd);
            List<Player> loserPlayers = losersPlayerStates.filter(ps1 -> ps1.getMatchHistory().length() == minMatchHistoryLength).map(ps -> ps.getPlayer());

            List<List<Player>> losersPairs = Utils.takeAll(loserPlayers, (List<Player> list) -> list.take(2));
            losersMatches = (loserPlayers.length() > 0) ? losersPairs.map(list -> new Match(list)) : List.list();

            // Set byes.
            losersMatches.filter(m -> m.getScores().size() == 1).foreach(m -> {
                m.setScore(m.getScores().keys().head(), Option.some(config.getWinsPerMatch()));
                return Unit.unit();
            });
        }

        List<Match> matches = activeMatches.append(losersMatches);
        return matches;
    }

    /**
     * Returns matches wrapped in Round objects.
     *
     * @param tournament Tournament to generate new rounds for.
     *
     * @return List or rounds.
     */
    @Override
    public List<Round> getRounds(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }
        TournamentState state = stateOpt.some();

        List<Match> matches = this.getPairings(tournament);
        List<Round> rounds = List.list();
        long timestamp = new Date().getTime();

        // Get losers matches.
        List<Match> losersMatches = matches.filter(m -> m.getScores().keys().forall(player -> {
            PlayerState playerState = state.getPlayerStates().find(ps -> ps.getPlayer().equals(player)).some();
            return playerState.getStatus() == PlayerStatus.Loser;
        }));

        if (losersMatches.length() > 0) {
            rounds = rounds.cons(new Round(timestamp, losersMatches));
        }

        // Get winner matches.
        List<Match> winnersMatches = matches.filter(m -> m.getScores().keys().forall(player -> {
            PlayerState playerState = state.getPlayerStates().find(ps -> ps.getPlayer().equals(player)).some();
            return playerState.getStatus() == PlayerStatus.Active;
        }));

        if (winnersMatches.length() > 0) {
            rounds = rounds.cons(new Round(timestamp, winnersMatches));
        }

        // Get all matches that are not in winners or losers bracket. This should usually happen only on finals.
        List<Match> matchesLeft = matches.filter(m -> !losersMatches.exists(m1 -> m1.equals(m)) && !winnersMatches.exists(m2 -> m2.equals(m)));
        if (matchesLeft.length() > 0) {
            rounds = rounds.cons(new Round(timestamp, matchesLeft));
        }

        return rounds;
    }
}