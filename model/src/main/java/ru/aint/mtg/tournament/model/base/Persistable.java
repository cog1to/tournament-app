package ru.aint.mtg.tournament.model.base;

import java.util.UUID;

/**
 * Base persistable object.
 */
public abstract class Persistable {
    /**
     * Version string. Will be used for differentiating between model versions.
     */
    protected String version;

    /**
     * ID string.
     */
    protected String id;

    /**
     * Returns new instance of Persistable object with random ID and default version.
     */
    protected Persistable() {
        id = UUID.randomUUID().toString();
    }

    /**
     * Returns new instance of Persistable object with given values.
     *
     * @param version Model version.
     * @param id ID string.
     */
    protected Persistable(String version, String id) {
        this.id = id;
        this.version = version;
    }

    /**
     * Returns new instance of Persistable object with given model version.
     *
     * @param version Model version.
     */
    protected Persistable(String version) {
        this();
        this.version = version;
    }

    /**
     * @return Object's model version.
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        }
        return version;
    }

    /**
     * Sets object's model version.
     *
     * @param newVersion New value for model version.
     */
    public void setVersion(String newVersion) {
        this.version = newVersion;
    }

    /**
     * @return Object's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets new ID.
     *
     * @param newId New ID value.
     */
    public void setId(String newId) {
        this.id = newId;
    }
}
