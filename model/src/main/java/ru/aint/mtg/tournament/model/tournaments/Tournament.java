package ru.aint.mtg.tournament.model.tournaments;

import fj.F2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.base.Persistable;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;

/**
 * Tournament data holder.
 */
public class Tournament extends Persistable {

    /**
     * Tournament name.
     */
    final String name;

    /**
     * Timestamp.
     */
    final long timestamp;

    /**
     * List of events.
     */
    private List<Event> events;

    /**
     * Creates new instance of a tournament object.
     *
     * @param id ID.
     * @param name Tournament name.
     * @param timestamp Timestamp.
     */
    public Tournament(String id, String name, long timestamp) {
        super();
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.events = List.list();
    }

    /**
     * Creates new instance of a tournament object.
     *
     * @param id ID.
     * @param name Tournament name.
     * @param timestamp Timestamp.
     * @param events List of events.
     */
    public Tournament(String id, String name, long timestamp, List<Event> events) {
        super();
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.events = events;
    }

    /**
     * Returns tournament name.
     *
     * @return Tournament name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns timestamp.
     *
     * @return Timestamp.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns list of events.
     *
     * @return List of events.
     */
    public List<Event> getEvents() {
        return events;
    }

    /**
     * Sets events.
     *
     * @param events New list of events.
     */
    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tournament that = (Tournament) o;

        if (id != that.id) return false;
        if (timestamp != that.timestamp) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    /**
     * Returns tournament's current state.
     *
     * @return Tournament's current state.
     */
    public Option<TournamentState> currentState() {
        Option<TournamentState> initial = Option.none();
        return this.events.foldRight((event, state) -> event.transformState(state), initial);
    }

    /**
     * Returns a state after given event.
     *
     * @param event Event.
     *
     * @return Tournaments state after given event.
     */
    public Option<TournamentState> stateAfterEvent(Event event) {
        F2<Option<TournamentState>, List<Event>, Option<TournamentState>> getState = new F2<Option<TournamentState>, List<Event>, Option<TournamentState>>() {
            @Override
            public Option<TournamentState> f(Option<TournamentState> state, List<Event> events) {
                if (events.isEmpty()) {
                    return state;
                }

                Event head = events.head();
                if (head == event) {
                    return head.transformState(state);
                }

                return this.f(head.transformState(state), events.tail());
            }
        };

        Option<TournamentState> initial = Option.none();
        return getState.f(initial, events.reverse());
    }

    /**
     * Returns state before given event.
     *
     * @param event Event.
     *
     * @return Tournament state before given event.
     */
    public Option<TournamentState> stateBeforeEvent(Event event) {
        F2<Option<TournamentState>, List<Event>, Option<TournamentState>> getState = new F2<Option<TournamentState>, List<Event>, Option<TournamentState>>() {
            @Override
            public Option<TournamentState> f(Option<TournamentState> state, List<Event> events) {
                if (events.isEmpty()) {
                    return state;
                }

                Event head = events.head();
                if (head == event) {
                    return state;
                }

                return this.f(head.transformState(state), events.tail());
            }
        };

        Option<TournamentState> initial = Option.none();
        return getState.f(initial, events.reverse());
    }

    /**
     * Returns state after event at given index.
     *
     * @param index Index of the target event.
     *
     * @return Tournament state after given event.
     */
    public Option<TournamentState> stateAfterEvent(int index) {
        return stateAfterEvent(this.events.reverse().index(index));
    }

    /**
     * Returns all events since given event.
     *
     * @param event Event.
     *
     * @return List of events that happened since given event.
     */
    public List<Event> eventsSince(Event event) {
        F2<List<Event>, Event, List<Event>> findEvent = new F2<List<Event>, Event, List<Event>>() {
            @Override
            public List<Event> f(List<Event> events, Event event) {
                if (events.isEmpty()) {
                    return events;
                } else if (events.head() != event) {
                    return this.f(events.tail(), event).cons(events.head());
                } else {
                    return List.list();
                }
            }
        };

        return findEvent.f(this.events, event);
    }

    /**
     * Returns last event of given type.
     *
     * @param type Event type.
     * @param <T> Event type.
     *
     * @return Last event of given type.
     */
    public <T extends Event> Option<T> lastEventOfType(Class<T> type) {
        F2<List<Event>, Class<T>, Option<T>> findEvent = new F2<List<Event>, Class<T>, Option<T>>() {
            @Override
            public Option<T> f(List<Event> events, Class<T> tClass) {
                if (events.isEmpty()) {
                    return Option.none();
                } else if (events.head().getClass() == type) {
                    return Option.some((T)events.head());
                } else {
                    return this.f(events.tail(), tClass);
                }
            }
        };

        return findEvent.f(this.events, type);
    }

    /**
     * Returns last event of given type that happened before given event.
     *
     * @param type Event type to look for.
     * @param event Target event.
     * @param <T>
     *
     * @return Last event of given type that happened before given event.
     */
    public <T extends Event> Option<T> lastEventOfTypeBeforeEvent(Class<T> type, Event event) {
        List<Event> eventsBefore = this.events.dropWhile(ev -> { return ev != event; }).tail();

        F2<List<Event>, Class<T>, Option<T>> findEvent = new F2<List<Event>, Class<T>, Option<T>>() {
            @Override
            public Option<T> f(List<Event> events, Class<T> tClass) {
                if (events.isEmpty()) {
                    return Option.none();
                } else if (events.head().getClass() == type) {
                    return Option.some((T)events.head());
                } else {
                    return this.f(events.tail(), tClass);
                }
            }
        };

        return findEvent.f(eventsBefore, type);
    }

    public boolean isStarted() {
        return events.exists(e -> !(e instanceof Registration));
    }
}
