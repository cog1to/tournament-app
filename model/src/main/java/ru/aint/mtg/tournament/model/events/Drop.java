package ru.aint.mtg.tournament.model.events;

import fj.F2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Player drop event.
 */
public class Drop extends Event {

    /**
     * Dropped players.
     */
    private List<Player> players;

    /**
     * Transforms given state.
     *
     * @param state Original state.
     *
     * @return New state transformed by the receiver.
     */
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (state.isNone()) {
            return state;
        }

        F2<TournamentState, PlayerState, PlayerState> updateState = (tournamentState, playerState) -> {
            PlayerState.Builder builder = new PlayerState.Builder(playerState);

            final Player player = playerState.getPlayer();
            if (this.players.exists(pl -> pl.equals(player))) {
                builder.status(PlayerStatus.Dropped);
            }

            return builder.build();
        };

        List<PlayerState> playerStates = state.some().getPlayerStates();
        List<PlayerState> updatedStates = playerStates.map(s -> updateState.f(state.some(), s));
        return Option.some(new TournamentState(state.some().getFormat(), updatedStates, state.some().getConfig()));
    }

    /**
     * Returns new Drop event instance.
     */
    public Drop() {
        super();
        this.players = List.list();
    }

    /**
     * Returns new Drop event with given timestamp and players.
     *
     * @param timestamp Event's timestamp.
     * @param players List of players.
     */
    public Drop(long timestamp, List<Player> players) {
        super(timestamp);
        this.players = players;
    }

    /**
     * Returns new Drop event with given players and current timestamp.
     *
     * @param players List of dropped players.
     */
    public Drop(List<Player> players) {
        super();
        this.players = players;
    }

    /**
     * Returns list of dropped players.
     *
     * @return List of dropped players.
     */
    public List<Player> getPlayers() {
        return this.players;
    }

    /**
     * Sets a list of dropped players.
     *
     * @param players List of dropped players.
     */
    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
