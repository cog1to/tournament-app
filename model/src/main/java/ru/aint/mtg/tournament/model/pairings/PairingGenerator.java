package ru.aint.mtg.tournament.model.pairings;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.tournaments.Tournament;

/**
 * Base abstract class for various pairing systems.
 */
public abstract class PairingGenerator {

    /**
     * Returns list of new matches to finish given current tournament state.
     *
     * @param tournament Tournament.
     *
     * @return List of matches.
     */
    public abstract List<Match> getPairings(Tournament tournament);

    /**
     * Returns list of new rounds to finish given current tournament state.
     *
     * @param tournament Tournament.
     *
     * @return List of rounds.
     */
    public List<Round> getRounds(Tournament tournament) {
        List<Match> pairings = this.getPairings(tournament);

        if (pairings.length() > 0) {
            return List.list(new Round(this.getPairings(tournament)));
        }

        return List.nil();
    }
}
