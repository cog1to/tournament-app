package ru.aint.mtg.tournament.model.tiebreakers;

import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Tie-breaker for game win percentage value.
 */
public class GameWinPercentageTieBreaker extends TieBreaker {
    @Override
    public int compare(PlayerState state1, PlayerState state2, TournamentState state) {
        if (state1.getGameWinPercentage(state.getConfig()) > state2.getGameWinPercentage(state.getConfig())) {
            return GREATER_THAN;
        } else if (state1.getGameWinPercentage(state.getConfig()) < state2.getGameWinPercentage(state.getConfig())) {
            return LESS_THAN;
        }

        return TIED;
    }

    @Override
    public double getValue(PlayerState state, TournamentState tournamentState) {
        return state.getGameWinPercentage(tournamentState.getConfig());
    }
}
