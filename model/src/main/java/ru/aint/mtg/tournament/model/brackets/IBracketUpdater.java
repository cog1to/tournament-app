package ru.aint.mtg.tournament.model.brackets;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Round;

/**
 * Bracket updater.
 */
public interface IBracketUpdater {
    /**
     * Updates bracket with match results.
     *
     * @param rounds
     */
    void updateBracket(List<Event> rounds);
}
