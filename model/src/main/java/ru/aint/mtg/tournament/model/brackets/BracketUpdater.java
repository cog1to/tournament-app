package ru.aint.mtg.tournament.model.brackets;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Bracket;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.promises.MatchPromise;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;

/**
 * Bracket updater implementation.
 */
public class BracketUpdater implements IBracketUpdater {

    @Override
    public void updateBracket(List<Event> events) {
        for (int eventIndex = 0; eventIndex < events.length(); eventIndex++) {
            Event event = events.index(eventIndex);
            if (event instanceof Bracket) {
                for (Round round :
                        ((Bracket) event).getRounds()) {
                    this.updateRound(round, events);
                }
            } else if (event instanceof Round) {
                Round round = (Round) event;
                this.updateRound(round, events);
            }
        }
    }

    private void updateRound(Round round, List<Event> events) {
        for (int matchIndex = 0; matchIndex < round.getMatches().length(); matchIndex++) {
            Match match = round.getMatches().index(matchIndex);

            Map<Player, Option<Integer>> scores = match.getScores();
            scores = scores.map(score -> {
                Player player = score._1();
                if (player instanceof MatchPromise) {
                    MatchPromise promise = (MatchPromise)player;
                    Match promisedMatch = findMatch(promise.getMatchId(), events);
                    if (promisedMatch.isFinished()) {
                        List<Player> loosers = promisedMatch.losers();
                        Player winner = promisedMatch.winner().some();
                        Player promisedPlayer = promise.getLooser() ? loosers.index(0) : winner;
                        return p(promisedPlayer, Option.none());
                    } else {
                        return score;
                    }
                } else {
                    return score;
                }
            });

            match.setScores(scores);
        }
    }

    Match findMatch(String matchId, List<Event> events) {
        for (int eventIndex = 0; eventIndex < events.length(); eventIndex++) {
            Event event = events.index(eventIndex);
            if (event instanceof Round) {
                Round round = (Round) event;
                Option<Match> match = round.getMatches().find(m -> m.getId().equals(matchId));
                if (match.isSome()) {
                    return match.some();
                }
            } else if (event instanceof Bracket) {
                Match found = this.findMatch(matchId, ((Bracket) event).getRounds().map(r -> (Event)r));
                if (found != null) {
                    return found;
                }
            }
        }

        return null;
    }
}
