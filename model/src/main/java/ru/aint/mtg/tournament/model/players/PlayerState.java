package ru.aint.mtg.tournament.model.players;

import fj.P2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

import static fj.data.List.list;
import static fj.function.Integers.add;

/**
 * Player state.
 */
public class PlayerState {

    /**
     * Player instance.
     */
    final Player player;

    /**
     * Player status.
     */
    final PlayerStatus status;

    /**
     * Match history.
     */
    final List<Match> matchHistory;

    /**
     * Returns new instance of player state object.
     *
     * @param player Player.
     * @param status Current status.
     * @param matchHistory Match history.
     */
    public PlayerState(Player player, PlayerStatus status, List<Match> matchHistory) {
        this.player = player;
        this.status = status;
        this.matchHistory = matchHistory;
    }

    /**
     * Returns new instance of player state.
     *
     * @param player Player instance.
     */
    public PlayerState(Player player) {
        this(player, PlayerStatus.Active, list());
    }

    /**
     * Checks if player has a bye in his match history.
     *
     * @return <b>true</b> if player had a bye, <b>false</b> otherwise.
     */
    public boolean hasBye() {
        return this.matchHistory.exists(m -> m.isBye());
    }

    /**
     * Checks if receiver player played against given player.
     *
     * @param player Player to check against.
     *
     * @return <b>true</b> if match history contains a match with given player, <b>false</b> otherwise.
     */
    public boolean playedAgainst(Player player) {
        return this.matchHistory.exists(m -> m.getScores().contains(player));
    }

    @Override
    public String toString() {
        return "PlayerState{" +
                "player=" + player +
                ", status=" + status +
                '}';
    }

    /**
     * Returns associated Player object.
     *
     * @return Associated player object.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Returns current player status.
     *
     * @return Current player status.
     */
    public PlayerStatus getStatus() {
        return status;
    }

    /**
     * Returns match history.
     *
     * @return Match hisotry.
     */
    public List<Match> getMatchHistory() {
        return matchHistory;
    }

    /**
     * Returns number of matches won.
     *
     * @return Number of matches won by this player.
     */
    public int getMatchesWon() {
        return this.matchHistory.filter(match -> {
            return (match.winner().isSome() && match.winner().some().equals(this.getPlayer()));
        }).length();
    }

    /**
     * Return number of games won.
     *
     * @return Number of games won by this player.
     */
    public int getGamesWon() {
        List<P2<Player, Option<Integer>>> scores = matchHistory.bind(m -> m.getScores().toList()).filter(score -> score._1().equals(this.getPlayer()));
        return scores.map(score -> score._2().some()).foldLeft(add, 0);
    }

    /**
     * Returns number of games lost.
     *
     * @return Number of games lost by this player.
     */
    public int getGamesLost() {
        List<P2<Player, Option<Integer>>> scores = matchHistory.bind(m -> m.getScores().toList()).filter(score -> !score._1().equals(this.getPlayer()));
        return scores.map(score -> score._2().some()).foldLeft(add, 0);
    }

    /**
     * Returns number of games ended in a draw.
     *
     * @return Number of ended in a draw by this player.
     */
    public int getGamesDrew() {
        return matchHistory.foldLeft((sum, m) -> sum + m.getDraws(), 0);
    }


    /**
     * Returns match points value according to given config.
     *
     * @param config Tournament config.
     *
     * @return Number of match points earned.
     */
    public double getMatchPoints(TournamentConfig config) {
        return this.matchHistory.map(match -> {
            if (match.isDraw()) {
                return config.getPointPerMatchDraw();
            } else if (match.winner().isSome() && match.winner().some().equals(this.getPlayer())) {
                return config.getPointsPerGameWin();
            }

            return 0;
        }).foldLeft(add, 0);
    }

    /**
     * Returns game points value according to given config.
     *
     * @param config Tournament config.
     *
     * @return Number of game points earned.
     */
    public double getGamePoints(final TournamentConfig config) {
        return this.matchHistory.map(match -> {
            Option<Integer> score = match.getScores().get(this.getPlayer()).some();
            return (score.some() * config.getPointsPerGameWin()) + (match.getDraws() * config.getPointsPerGameDraw());
        }).foldLeft(add, 0);
    }

    /**
     * Returns match win percentage.
     *
     * @param config Tournament config.
     *
     * @return Match win percentage.
     */
    public double getMatchWinPercentage(TournamentConfig config) {
        if (this.matchHistory.length() == 0) {
            return 0;
        }

        return Math.max(this.getMatchPoints(config) / ((float)this.matchHistory.length() * config.getPointsPerMatchWin()), 0.33);
    }

    /**
     * Returns games win percentage.
     *
     * @param config Tournament config.
     *
     * @return Games win percentage.
     */
    public double getGameWinPercentage(TournamentConfig config) {
        int numberOfGamesPlayed = this.matchHistory.map(match -> {
            List<Integer> scores = match.getScores().values().filter(opt -> opt.isSome()).map(opt -> opt.some());
            return scores.foldLeft(add, 0) + match.getDraws();
        }).foldLeft(add, 0);

        if (numberOfGamesPlayed == 0) {
            return 0;
        }

        return Math.max(getGamePoints(config) / (numberOfGamesPlayed * config.getPointsPerGameWin()), 0.33);
    }

    /**
     * Returns opponents' match win percentage.
     *
     * @param state Tournament state.
     *
     * @return Opponents' match win percentage.
     */
    public double getOpponentsMatchWinPercentage(TournamentState state) {
        if (this.matchHistory.length() == 0) {
            return 0;
        }

        List<Player> opponents = this.matchHistory.bind(m -> m.getScores().keys()).filter(p -> !p.equals(this.getPlayer()));
        if (opponents.length() == 0) {
            return 0.0f;
        } else {
            List<PlayerState> opponentsStates = state.getPlayerStates().filter(s -> opponents.exists(p -> p.equals(s.getPlayer())));
            return opponentsStates.map(s -> s.getMatchWinPercentage(state.getConfig())).foldLeft((sum, val) -> sum + val, 0.0d) / opponents.length();
        }
    }

    /**
     * Returns opponents' games win percentage.
     *
     * @param state Tournament state.
     *
     * @return Opponents' games win percentage.
     */
    public double getOpponentsGameWinPercentage(TournamentState state) {
        if (this.matchHistory.length() == 0) {
            return 0;
        }

        List<Player> opponents = this.matchHistory.bind(m -> m.getScores().keys()).filter(p -> !p.equals(this.getPlayer()));
        if (opponents.length() == 0) {
            return 0.0f;
        } else {
            List<PlayerState> opponentsStates = state.getPlayerStates().filter(s -> opponents.exists(p -> p.equals(s.getPlayer())));
            return opponentsStates.map(s -> s.getGameWinPercentage(state.getConfig())).foldLeft((sum, val) -> sum + val, 0.0d) / opponents.length();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerState)) return false;

        PlayerState that = (PlayerState) o;

        if (matchHistory != null ? !matchHistory.equals(that.matchHistory) : that.matchHistory != null) return false;
        if (!player.equals(that.player)) return false;
        return status == that.status;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = player.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + (matchHistory != null ? matchHistory.hashCode() : 0);
        return result;
    }

    //region Builder

    /**
     * State builder.
     */
    public static class Builder {

        /**
         * Player.
         */
        Player player;

        /**
         * Player status.
         */
        PlayerStatus status = PlayerStatus.Active;

        /**
         * Match history.
         */
        List<Match> matchHistory = list();

        /**
         * Returns new instance of Builder.
         */
        public Builder() { }

        /**
         * Returns new instance of Builder initialized with given player state.
         *
         * @param state Player state.
         */
        public Builder(PlayerState state) {
            this.player = state.player;
            this.status = state.status;
            this.matchHistory = state.matchHistory;
        }

        /**
         * Returns new player state value.
         *
         * @return New player state.
         */
        public PlayerState build() {
            return new PlayerState(player, status, matchHistory);
        }

        /**
         * Sets player.
         *
         * @param p Player.
         *
         * @return Self.
         */
        public Builder player(Player p) {
            this.player = p;
            return this;
        }

        /**
         * Sets status.
         *
         * @param s Player status.
         *
         * @return Self.
         */
        public Builder status(PlayerStatus s) {
            this.status = s;
            return this;
        }

        /**
         * Sets match history.
         *
         * @param history Match history.
         *
         * @return Self.
         */
        public Builder matchHistory(List<Match> history) {
            this.matchHistory = history;
            return this;
        }
    }

    //endregion
}
