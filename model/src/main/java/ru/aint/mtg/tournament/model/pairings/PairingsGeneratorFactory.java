package ru.aint.mtg.tournament.model.pairings;

import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;

/**
 * Pairing Generator Factory.
 */
public final class PairingsGeneratorFactory {

    /**
     * Returns pairing generator according to tournament format.
     *
     * @param format Tournament format.
     *
     * @return Pairings generator for given tournament format.
     *
     * @throws IllegalArgumentException In case given format is not supported.
     */
    public static PairingGenerator getPairingGenerator(TournamentFormat format) throws IllegalArgumentException {
        switch (format) {
            case SingleElimination:
                return new SingleEliminationPairingGenerator();
            case Swiss:
                return new SwissPairingGenerator();
            case RoundRobin:
                return new RoundRobinPairingGenerator();
            case DoubleRoundRobin:
                return new DoubleRoundRobinPairingGenerator();
            case DoubleElimination:
                return new DoubleEliminationPairingGenerator();
            default:
                throw new IllegalArgumentException(String.format("Unsupported format: %s", format.toString()));
        }
    }
}
