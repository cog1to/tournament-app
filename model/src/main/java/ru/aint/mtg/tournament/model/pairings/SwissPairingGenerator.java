package ru.aint.mtg.tournament.model.pairings;

import java.util.Date;
import java.util.Random;

import fj.Ord;
import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Swiss tournament pairing generator.
 */
public class SwissPairingGenerator extends PairingGenerator {
    @Override
    public List<Match> getPairings(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }

        TournamentState state = stateOpt.some();
        if (state.getFormat() != TournamentFormat.Swiss) {
            throw new IllegalArgumentException("Tournament format is not Swiss");
        }

        // If last round is not finished, don't generate any new pairs.
        Option<Round> lastRound = tournament.lastEventOfType(Round.class);
        if (lastRound.isSome() && !lastRound.some().isFinished()) {
            return List.list();
        }

        TournamentConfig config = state.getConfig();
        Random random = new Random(new Date().getTime());

        // Sort by match points.
        List<PlayerState> sortedStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).sort(Ord.ord(playerState -> playerState2 -> {
            return Ord.doubleOrd.compare(playerState2.getMatchPoints(config), playerState.getMatchPoints(config));
        }));

        // Create pairs.
        List<List<PlayerState>> pairs = Utils.takeAll(sortedStates, (List<PlayerState> list) -> {
            if (list.isEmpty()) {
                return List.nil();
            }

            // PlayerState to pair.
            PlayerState nextState = null;
            // For first round, pair randomly.
            if (tournament.getEvents().filter(ev -> ev instanceof Round).length() == 0) {
                nextState = list.index(random.nextInt(list.length()));
            } else {
                nextState = list.head();
            }

            // Get match points groups.
            List<Double> matchPointsGroups = list.map(ps -> ps.getMatchPoints(config)).foldLeft((List<Double> groups, Double mp) -> {
                if (!groups.exists(d -> d.equals(mp))) {
                    return groups.cons(mp);
                }

                return groups;
            }, List.list()).sort(Ord.ord(mp1 -> mp2 -> {
                return Ord.doubleOrd.compare(mp2, mp1);
            }));

            // Try to find a pair in each group
            for (Double matchPointsGroup : matchPointsGroups) {
                // Get states with same MP.
                final PlayerState finalNextState = nextState;
                List<PlayerState> sameMatchPointsStates = list.filter(ps -> ps.getMatchPoints(config) == matchPointsGroup && !ps.getPlayer().equals(finalNextState.getPlayer()) && !ps.playedAgainst(finalNextState.getPlayer()));

                // If there are players with given MP score that are not the given player and never played with given player, then use one of those.
                if (!sameMatchPointsStates.isEmpty()) {
                    // If there is a player with a bye, pair with him/her, so he won't get a second bye.
                    if (sameMatchPointsStates.length() > 1 && sameMatchPointsStates.exists(ps -> ps.hasBye())) {
                        List<PlayerState> byes = sameMatchPointsStates.filter(ps -> ps.hasBye());
                        PlayerState pair = byes.index(random.nextInt(byes.length()));
                        return List.list(nextState, pair);
                    }

                    // Otherwise pair randomly with any one player within a group.
                    PlayerState pair = sameMatchPointsStates.index(random.nextInt(sameMatchPointsStates.length()));
                    return List.list(nextState, pair);
                }
            }

            // If there is no pair at all, we have to give it a bye.
            return List.list(nextState);
        });

        // Set byes.
        List<Match> matches = pairs.map(list -> new Match(list.map(ps -> ps.getPlayer())));
        matches.filter(m -> m.getScores().size() == 1).foreach(m -> {
            m.setScore(m.getScores().keys().head(), Option.some(config.getWinsPerMatch()));
            return Unit.unit();
        });

        return matches;
    }
}
