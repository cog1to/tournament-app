package ru.aint.mtg.tournament.model.events;

import fj.data.Option;
import ru.aint.mtg.tournament.model.base.Persistable;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

import java.util.Date;

/**
 * Base event class
 */
public abstract class Event extends Persistable {

    /**
     * Event's timestamp.
     */
    final long timestamp;

    /**
     * Event name.
     */
    String name = null;

    /**
     * Checks if event is finished.
     *
     * @return <b>true</b> if event is finished, <b>false</b> otherwise.
     */
    public boolean isFinished() {
        return true;
    }

    /**
     * Transforms given state.
     *
     * @param state Original state.
     *
     * @return New state transformed by the receiver.
     */
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        return Option.none();
    }

    /**
     * Returns new Event object with given timestamp.
     *
     * @param timestamp Event's timestamp.
     */
    protected Event(long timestamp) {
        super();
        this.timestamp = timestamp;
    }

    /**
     * Returns new Event object with given version and timestamp.
     *
     * @param version Version string.
     * @param timestamp Timestamp.
     */
    protected Event(String version, long timestamp) {
        super();
        this.version = version;
        this.timestamp = timestamp;
    }

    /**
     * Returns new event object created with current time as timestamp.
     */
    protected Event() {
        super();
        this.timestamp = new Date().getTime();
    }

    /**
     * Returns timestamp.
     *
     * @return Timestamp value.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns event name.
     *
     * @return Event name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets event's name.
     *
     * @param name New name.
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;

        Event event = (Event) o;

        if (isFinished() != event.isFinished()) return false;
        if (timestamp != event.timestamp) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (isFinished() ? 1 : 0);
        return result;
    }
}
