package ru.aint.mtg.tournament.model.pairings.promises;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.pairings.PairingGenerator;
import ru.aint.mtg.tournament.model.tournaments.Tournament;

/**
 * Pairing generator for single elimination tournaments with promises.
 */
public class SingleEliminationPairingsGeneratorWithPromises extends PairingGenerator {
    @Override
    public List<Match> getPairings(Tournament tournament) {
        return null;
    }
}
