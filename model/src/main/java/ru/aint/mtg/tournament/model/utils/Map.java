package ru.aint.mtg.tournament.model.utils;

import java.util.*;

import fj.*;
import fj.data.*;
import fj.data.HashMap;
import fj.data.List;
import fj.function.Effect1;
import ru.aint.mtg.tournament.model.players.Player;

/**
 * Created by aint on 4/29/2015.
 */
public class Map<K, V> {
    HashMap<K, V> hashMap;

    private Map() {
        this.hashMap = HashMap.hashMap();
    }

    private Map(HashMap<K,V> map) {
        this.hashMap = map;
    }

    public Map(Equal<K> e, Hash<K> h) {
        this.hashMap = new HashMap<K, V>(e, h);
    }

    public Map(java.util.Map<K, V> map, Equal<K> e, Hash<K> h) {
        this.hashMap = new HashMap<K, V>(map, e, h);
    }

    public Map(Equal<K> e, Hash<K> h, int initialCapacity) {
        this.hashMap = new HashMap<K, V>(e, h, initialCapacity);
    }

    public Map(java.util.Map<K, V> map) {
        this.hashMap = new HashMap<K, V>(map);
    }

    public Map(Equal<K> e, Hash<K> h, int initialCapacity, float loadFactor) {
        this.hashMap = new HashMap<K, V>(e, h, initialCapacity, loadFactor);
    }

    public Map(List<P2<K, V>> list) {
        this();
        Map<K, V> map = Map.map();
        list.foreach(p -> {
            this.set(p._1(), p._2());
            return Unit.unit();
        });
    }

    public void clear() {
        this.hashMap.clear();
    }

    public boolean contains(K k) {
        return this.hashMap.contains(k);
    }

    public void delete(K k) {
        this.hashMap.delete(k);
    }

    public boolean eq(K k1, K k2) {
        return this.hashMap.eq(k1, k2);
    }

    public F<K,Option<V>> get() {
        return this.hashMap.get();
    }

    public Option<V> get(K k) {
        return this.hashMap.get(k);
    }

    public Option<V> getDelete(K k) {
        return this.hashMap.getDelete(k);
    }

    public int hash(K k) {
        return this.hashMap.hash(k);
    }

    public static <K, V> Map<K, V> map() {
        return new Map<K, V>();
    }

    public boolean isEmpty() {
        return this.hashMap.isEmpty();
    }

    public java.util.Iterator<K> iterator() {
        return this.hashMap.iterator();
    }

    public List<K> keys() {
        return this.hashMap.keys().sort(Ord.ord(new F<K, F<K, Ordering>>() {
            @Override
            public F<K, Ordering> f(K k1) {
                return new F<K, Ordering>() {
                    @Override
                    public Ordering f(K k2) {
                        if (k1 instanceof Comparable) {
                            int result = ((Comparable) k1).compareTo(k2);
                            if (result > 0) return Ordering.GT;
                            if (result < 0) return Ordering.LT;
                            if (result == 0) return Ordering.EQ;
                        }

                        if (k1.hashCode() > k2.hashCode()) return Ordering.GT;
                        if (k1.hashCode() < k2.hashCode()) return Ordering.LT;
                        return Ordering.EQ;
                    }
                };
            }
        }));
    }

    public void set(K k, V v) {
        this.hashMap.set(k, v);
    }

    public int size() {
        return this.hashMap.size();
    }

    public java.util.Collection<P2<K,V>> toCollection() {
        return this.hashMap.toCollection();
    }

    public List<V> values() {
        return this.hashMap.values();
    }

    public void foreach(F<P2<K,V>, Unit> function) {
        this.hashMap.foreach(function);
    }

    public void foreachDoEffect(Effect1<P2<K,V>> effect) {
        this.hashMap.foreachDoEffect(effect);
    }

    public static <K, V> Map<K, V> from(java.lang.Iterable<P2<K,V>> entries) {
        return new Map<K, V>(HashMap.from(entries));
    }

    public static <K, V> Map<K, V> from(java.lang.Iterable<P2<K,V>> entries, Equal<K> equal, Hash<K> hash) {
        return new Map<K, V>(HashMap.from(entries, equal, hash));
    }

    public <A,B> Map<A,B> map(F<K,A> keyFunction, F<V,B> valueFunction) {
        return new Map<A, B>(this.hashMap.map(keyFunction, valueFunction));
    }

    public <A,B> Map<A,B> map(F<K,A> keyFunction, F<V,B> valueFunction, Equal<A> equal, Hash<A> hash) {
        return new Map<A, B>(this.hashMap.map(keyFunction, valueFunction, equal, hash));
    }

    public <A,B> Map<A,B> map(F<P2<K,V>,P2<A,B>> function) {
        return new Map<A, B>(this.hashMap.map(function));
    }

    public <A,B> Map<A,B> map(F<P2<K,V>,P2<A,B>> function, Equal<A> equal, Hash<A> hash) {
        return new Map<A, B>(this.hashMap.map(function, equal, hash));
    }

    public <A> Map<A,V> mapKeys(F<K,A> function) {
        return new Map<>(this.hashMap.mapKeys(function));
    }

    public <A> Map<A,V> mapKeys(F<K,A> keyFunction, Equal<A> equal, Hash<A> hash) {
        return new Map<>(this.hashMap.mapKeys(keyFunction, equal, hash));
    }

    public <B> Map<K,B> mapValues(F<V,B> function) {
        return new Map<>(this.hashMap.mapValues(function));
    }

    public Array<P2<K,V>> toArray() {
        return this.hashMap.toArray();
    }

    public List<P2<K,V>> toList() {
        return this.hashMap.toList();
    }

    public java.util.Map<K,V> toMap() {
        return this.hashMap.toMap();
    }

    public Option<P2<K,V>> toOption() {
        return this.hashMap.toOption();
    }

    public Stream<P2<K,V>> toStream() {
        return this.hashMap.toStream();
    }

    public java.util.HashMap<K,V> toJavaHashMap() {
        java.util.HashMap<K,V> hashMap = new java.util.HashMap<>();
        for (K key: this.hashMap.keys()) {
            V value = this.hashMap.get(key).some();
            hashMap.put(key, value);
        }
        return hashMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Map)) return false;

        Map map = (Map) o;

        if (this.size() != map.size()) return false;

        List<K> keys = this.keys();
        for (K key : keys) {
            if (!map.contains(key)) return false;
            V value = this.get(key).some();
            Object mapValue = map.get(key).some();
            if (!value.equals(mapValue)) return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        List<K> sortedKeys = this.keys().sort(Ord.ord(new F<K, F<K, Ordering>>() {
            @Override
            public F<K, Ordering> f(K k1) {
                return new F<K, Ordering>() {
                    @Override
                    public Ordering f(K k2) {
                        if (k1.hashCode() > k2.hashCode()) return Ordering.GT;
                        if (k1.hashCode() < k2.hashCode()) return Ordering.LT;
                        return Ordering.EQ;
                    }
                };
            }
        }));

        for (K key : sortedKeys) {
            result = 31 * result + key.hashCode() + this.get(key).some().hashCode();
        }

        return result;
    }
}
