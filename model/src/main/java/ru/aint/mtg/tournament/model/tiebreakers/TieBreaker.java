package ru.aint.mtg.tournament.model.tiebreakers;

import ru.aint.mtg.tournament.model.base.Persistable;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Base for a tiebreaker.
 */
public abstract class TieBreaker extends Persistable {
    public static final int GREATER_THAN = 1;
    public static final int LESS_THAN = -1;
    public static final int TIED = 0;

    /**
     * Returns new instance of a Tiebreaker object.
     */
    protected TieBreaker() {
        super();
    }

    /**
     * Compares two player states and return comparision result.
     *
     * @param state1 First player state.
     * @param state2 Second player state.
     * @param state Tournament state.
     *
     * @return GREATER_THAN if first state is greater than second, LESS_THAN if first state is less than second, TIED if states are tied.
     */
    public abstract int compare(PlayerState state1, PlayerState state2, TournamentState state);

    /**
     * Returns tie-breaker value for given player state in a tournament state.
     *
     * @param state Player state to calculate tie-breaker value for.
     * @param tournamentState Tournament state.
     *
     * @return Tie-breaker value.
     */
    public abstract double getValue(PlayerState state, TournamentState tournamentState);
}
