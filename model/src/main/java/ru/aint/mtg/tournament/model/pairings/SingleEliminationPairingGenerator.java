package ru.aint.mtg.tournament.model.pairings;

import fj.P2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Pairing generator for Single Elimination tournaments.
 */
public class SingleEliminationPairingGenerator extends PairingGenerator {
    @Override
    public List<Match> getPairings(Tournament tournament) {
        // Check if we're in a valid state.
        Option<TournamentState> stateOpt = tournament.currentState();
        if (stateOpt.isNone()) {
            throw new IllegalArgumentException("Tournament has no valid states for generating pairings");
        }

        TournamentState state = stateOpt.some();
        if (state.getFormat() != TournamentFormat.SingleElimination) {
            throw new IllegalArgumentException("Tournament format is not Single Elimination");
        }

        // If last round is not finished, don't generate any new pairs.
        Option<Round> lastRound = tournament.lastEventOfType(Round.class);
        if (lastRound.isSome() && !lastRound.some().isFinished()) {
            return List.list();
        }

        TournamentConfig config = state.getConfig();

        // Special treatment for the first match. We must calculate byes and pair players without byes.
        if (tournament.lastEventOfType(Round.class).isNone()) {
            // Get all player states.
            List<PlayerState> playerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
            List<Player> players = playerStates.map(ps -> ps.getPlayer());

            long numberOfByes = Utils.nextHighestPowerOf(2, playerStates.length()) - playerStates.length();

            // Split all players into byes and pairs.
            P2<List<Player>, List<Player>> split = Utils.take(players, l -> l.take((int)numberOfByes));

            // Generate bye matches.
            Option<Integer> byeScore = Option.some(state.getConfig().getWinsPerMatch());
            List<Match> byes = split._1().map(p -> new Match(Map.from(((List<Player>) List.list(p)).zip(List.list(Option.some(byeScore))))));

            // Generate paired matches. Pair first with last.
            List<List<Player>> pairs = Utils.takeAll(split._2(), l -> List.list(l.head(), l.last()));
            List<Match> matches = pairs.map(pls -> new Match(pls));

            return byes.append(matches);
        }

        // If first round was preliminary (i.e. included byes), the next round would be the REAL first round
        // so we need to pair players first-to-last again.
        Option<Registration> lastRegistration = tournament.lastEventOfType(Registration.class);
        List<Event> pastRounds = tournament.eventsSince(lastRegistration.some()).filter(e -> e instanceof Round);
        if ((pastRounds.length() == 1) && ((Round)pastRounds.head()).getMatches().filter(m -> m.isBye()).length() > 0) {

            List<PlayerState> playerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
            List<Player> players = playerStates.map(ps -> ps.getPlayer());

            // Generate paired matches. Pair first with last.
            List<List<Player>> pairs = Utils.takeAll(players, l -> List.list(l.head(), l.last()));
            List<Match> matches = pairs.map(pls -> new Match(pls));

            return matches;
        }

        // For all other rounds, pair closest active players.
        List<PlayerState> playerStates = state.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active);
        if ((playerStates.length() % 2) != 0) {
            String message = String.format("Number of active players is not even! Rounds played: %d, Number of players: %d, number of active players: %d", pastRounds.length(), state.getPlayerStates().length(), playerStates.length());
            throw new IllegalStateException(message);
        }

        // Pair adjacent players by 2.
        List<List<Player>> pairs = Utils.takeAll(playerStates.map(ps -> ps.getPlayer()), l -> l.take(2));
        List<Match> matches = pairs.map(p -> new Match(p));

        return matches;
    }
}
