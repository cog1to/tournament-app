package ru.aint.mtg.tournament.model.events;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Registration event holding current format, config and list of participants.
 */
public class Registration extends Event {

    /**
     * Tournament config.
     */
    private TournamentConfig config;

    /**
     * Tournament format.
     */
    private TournamentFormat format;

    /**
     * List of registered players.
     */
    private List<Player> players;

    /**
     * Returns new Registration instance with given values.
     *
     * @param timestamp Timestamp.
     * @param config Tournament config.
     * @param format Tournament format value.
     * @param players List of participants.
     */
    public Registration(long timestamp, TournamentConfig config, TournamentFormat format, List<Player> players) {
        super(timestamp);
        this.config = config;
        this.format = format;
        this.players = players;
    }

    /**
     *  Returns new Registration instance with given values and current timestamp.
     *
     * @param config Tournament config.
     * @param format Tournament format value.
     * @param players List of participants.
     */
    public Registration(TournamentConfig config, TournamentFormat format, List<Player> players) {
        this.config = config;
        this.format = format;
        this.players = players;
    }

    @Override
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (state.isNone()) {
            List<PlayerState> states = this.players.map(PlayerState::new);
            return Option.some(new TournamentState(this.format, states, this.config));
        }

        List<PlayerState> advancing = state.some().getPlayerStates().filter(s -> players.exists(p -> p == s.getPlayer()));
        List<PlayerState> eliminated = state.some().getPlayerStates().filter(s -> (!advancing.exists(as -> as.getPlayer() == s.getPlayer())) && s.getStatus() == PlayerStatus.Active);
        List<PlayerState> other = state.some().getPlayerStates().filter(s -> (!advancing.exists(as -> as.getPlayer() == s.getPlayer())) && (!eliminated.exists(as -> as.getPlayer() == s.getPlayer())));

        List<PlayerState> newActive = advancing.map(s -> {
            PlayerState.Builder builder = new PlayerState.Builder(s);
            builder.status(PlayerStatus.Active);
            return builder.build();
        });

        List<PlayerState> newOther = eliminated.append(other).map(s -> {
            PlayerState.Builder builder = new PlayerState.Builder(s);
            if (eliminated.exists(es -> es == s)) {
                builder.status(PlayerStatus.Eliminated);
            }
            return builder.build();
        });

        return Option.some(new TournamentState(state.some().getFormat(), newActive.append(newOther), state.some().getConfig()));
    }

    @Override
    public String toString() {
        return "Registration{" +
                "config=" + config +
                ", format=" + format +
                ", players=" + players +
                '}';
    }

    /**
     * Returns config.
     *
     * @return Tournament config.
     */
    public TournamentConfig getConfig() {
        return config;
    }

    /**
     * Sets new config.
     *
     * @param config Tournament config.
     */
    public void setConfig(TournamentConfig config) {
        this.config = config;
    }

    /**
     * Returns list of registered participants.
     *
     * @return List of participants.
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * Sets list of participants.
     *
     * @param players List of participants.
     */
    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    /**
     * Returns tournament format.
     *
     * @return {@link ru.aint.mtg.tournament.model.tournaments.TournamentFormat} value.
     */
    public TournamentFormat getFormat() {
        return format;
    }

    /**
     * Sets new format value.
     *
     * @param format {@link ru.aint.mtg.tournament.model.tournaments.TournamentFormat} value.
     */
    public void setFormat(TournamentFormat format) {
        this.format = format;
    }
}
