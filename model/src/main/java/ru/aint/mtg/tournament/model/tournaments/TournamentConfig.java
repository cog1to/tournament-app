package ru.aint.mtg.tournament.model.tournaments;

import fj.data.List;
import ru.aint.mtg.tournament.model.base.Persistable;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerType;

/**
 * Base tournament config.
 */
public class TournamentConfig extends Persistable {
    public final static int DEFAULT_NUMBER_OF_WINS_PER_MATCH = 2;
    public final static int DEFAULT_MATCH_WIN_POINTS = 3;
    public final static int DEFAULT_MATCH_DRAW_POINTS = 1;
    public final static int DEFAULT_GAME_WIN_POINTS = 3;
    public final static int DEFAULT_GAME_DRAW_POINTS = 1;
    public final static List<TieBreakerType> DEFAULT_SWISS_TIEBREAKERS = List.list(TieBreakerType.MatchPoints, TieBreakerType.OpponentsMatchWinPercentage, TieBreakerType.GameWinPercentage, TieBreakerType.OpponentsGameWinPercentage);

    /**
     * Number of game wins required for a match to be considered finished.
     */
    final int winsPerMatch;

    /**
     * Match points assigned to a player for a match win.
     */
    final int pointsPerMatchWin;

    /**
     * Number of points assigned to a player for a match draw.
     */
    final int pointPerMatchDraw;

    /**
     * Number of game points assigned to a player for a game win.
     */
    final int pointsPerGameWin;

    /**
     * Number of game points assigned to a player for a game draw.
     */
    final int pointsPerGameDraw;

    /**
     * List of tie-breakers.
     */
    final List<TieBreakerType> tieBreakers;

    /**
     * Returns new instance of TournamentConfig with given config values.
     *
     * @param winsPerMatch Number of game wins required for a match to be considered finished.
     * @param pointsPerMatchWin Match points assigned to a player for a match win.
     * @param pointPerMatchDraw Number of points assigned to a player for a match draw.
     * @param pointsPerGameWin Number of game points assigned to a player for a game win.
     * @param pointsPerGameDraw Number of game points assigned to a player for a game draw.
     * @param tieBreakers List of tie-breakers.
     */
    public TournamentConfig(int winsPerMatch, int pointsPerMatchWin, int pointPerMatchDraw, int pointsPerGameWin, int pointsPerGameDraw, List<TieBreakerType> tieBreakers) {
        super();
        this.winsPerMatch = winsPerMatch;
        this.pointsPerMatchWin = pointsPerMatchWin;
        this.pointPerMatchDraw = pointPerMatchDraw;
        this.pointsPerGameWin = pointsPerGameWin;
        this.pointsPerGameDraw = pointsPerGameDraw;
        this.tieBreakers = tieBreakers;
    }

    /**
     * Returns new instance of TournamentConfig with given config values.
     *
     * @param winsPerMatch Number of game wins required for a match to be considered finished.
     * @param pointsPerMatchWin Match points assigned to a player for a match win.
     * @param pointPerMatchDraw Number of points assigned to a player for a match draw.
     * @param pointsPerGameWin Number of game points assigned to a player for a game win.
     * @param pointsPerGameDraw Number of game points assigned to a player for a game draw.
     */
    public TournamentConfig(int winsPerMatch, int pointsPerMatchWin, int pointPerMatchDraw, int pointsPerGameWin, int pointsPerGameDraw) {
        super();
        this.winsPerMatch = winsPerMatch;
        this.pointsPerMatchWin = pointsPerMatchWin;
        this.pointPerMatchDraw = pointPerMatchDraw;
        this.pointsPerGameWin = pointsPerGameWin;
        this.pointsPerGameDraw = pointsPerGameDraw;
        this.tieBreakers = List.list();
    }

    /**
     * Returns new instance of TournamentConfig with given config values.
     *
     * @param winsPerMatch Number of game wins required for a match to be considered finished.
     */
    public TournamentConfig(int winsPerMatch) {
        super();
        this.winsPerMatch = winsPerMatch;
        this.pointsPerMatchWin = DEFAULT_MATCH_WIN_POINTS;
        this.pointPerMatchDraw = DEFAULT_MATCH_DRAW_POINTS;
        this.pointsPerGameWin = DEFAULT_GAME_WIN_POINTS;
        this.pointsPerGameDraw = DEFAULT_GAME_DRAW_POINTS;
        this.tieBreakers = List.list();
    }

    /**
     * Returns new instance of TournamentConfig with given config values.
     *
     * @param winsPerMatch Number of game wins required for a match to be considered finished.
     */
    public TournamentConfig(int winsPerMatch, List<TieBreakerType> tieBreakers) {
        super();
        this.winsPerMatch = winsPerMatch;
        this.pointsPerMatchWin = DEFAULT_MATCH_WIN_POINTS;
        this.pointPerMatchDraw = DEFAULT_MATCH_DRAW_POINTS;
        this.pointsPerGameWin = DEFAULT_GAME_WIN_POINTS;
        this.pointsPerGameDraw = DEFAULT_GAME_DRAW_POINTS;
        this.tieBreakers = tieBreakers;
    }

    /**
     * Returns default config.
     *
     * @return New instance of TournamentConfig with default values.
     */
    public static TournamentConfig defaultConfig() {
        return new TournamentConfig(DEFAULT_NUMBER_OF_WINS_PER_MATCH);
    }

    /**
     * Default swiss config.
     *
     * @return New instance of TournamentConfig with default values and tie-breakers list for a swiss tournament.
     */
    public static TournamentConfig defaultSwissConfig() {
        return new TournamentConfig(DEFAULT_NUMBER_OF_WINS_PER_MATCH, DEFAULT_SWISS_TIEBREAKERS);
    }

    /**
     * Default round robin config.
     *
     * @return New instance of TournamentConfig with default values and tie-breakers list for a round robin tournament.
     */
    public static TournamentConfig defaultRoundRobinConfig() {
        return new TournamentConfig(DEFAULT_NUMBER_OF_WINS_PER_MATCH, DEFAULT_SWISS_TIEBREAKERS);
    }

    /**
     * Returns number of wins required for a match.
     *
     * @return Number of wins required for a match.
     */
    public int getWinsPerMatch() {
        return winsPerMatch;
    }

    /**
     * Returns number of match points assigned for a match win.
     *
     * @return Number of match points assigned for a match win.
     */
    public int getPointsPerMatchWin() {
        return pointsPerMatchWin;
    }

    /**
     * Returns number of match points assigned to a player for match draw.
     *
     * @return Number of match points assigned to a player for match draw.
     */
    public int getPointPerMatchDraw() {
        return pointPerMatchDraw;
    }

    /**
     * Returns number of game points assigned to a player for a game win.
     *
     * @return Number of game points assigned to a player for a game win.
     */
    public int getPointsPerGameWin() {
        return pointsPerGameWin;
    }

    /**
     * Returns number of game points assigned to a player for a draw.
     *
     * @return Number of game points assigned to a player for a draw.
     */
    public int getPointsPerGameDraw() {
        return pointsPerGameDraw;
    }

    /**
     * Returns a list of tie-breakers.
     *
     * @return A list of tie-breakers.
     */
    public List<TieBreakerType> getTieBreakers() {
        return tieBreakers;
    }
}
