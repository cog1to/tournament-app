package ru.aint.mtg.tournament.model.tiebreakers;

/**
 * Tie-breaker type
 */
public enum TieBreakerType {
    MatchPoints,
    OpponentsMatchWinPercentage,
    GameWinPercentage,
    OpponentsGameWinPercentage,
}
