package ru.aint.mtg.tournament.model.players;

import java.util.UUID;

/**
 * Dummy player class.
 */
public class DummyPlayer extends Player {

    /**
     * Main constructor.
     */
    public DummyPlayer() {
        super(UUID.randomUUID().toString());
    }

    @Override
    public String toString() {
        return "-------";
    }
}
