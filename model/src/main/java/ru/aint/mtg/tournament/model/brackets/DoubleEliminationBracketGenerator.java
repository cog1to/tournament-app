package ru.aint.mtg.tournament.model.brackets;

import java.util.Date;

import fj.Ord;
import fj.P2;
import fj.data.List;
import ru.aint.mtg.tournament.model.events.Bracket;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.promises.MatchPromise;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Utils;

/**
 * Double elimination bracket generator.
 */
public class DoubleEliminationBracketGenerator implements IBracketGenerator {
    @Override
    public List<Event> getBracket(List<Player> players, IStringsProvider stringsProvider) {
        SingleEliminationBracketGenerator winnersBracketGenerator = new SingleEliminationBracketGenerator();
        List<Round> winnersBracket = winnersBracketGenerator.getBracket(players, stringsProvider).map(ev -> (Round)ev);
        List<Round> losersBracket = List.list();

        // For 2-player grid, just add finals rematch round.
        if (winnersBracket.length() == 1 && winnersBracket.index(0).getMatches().length() == 1) {
            Round firstRound = winnersBracket.index(0);
            Match firstMatch = firstRound.getMatches().index(0);
            Match rematch = new Match(List.list(
                    new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), firstRound.getName(), firstMatch.getName()), firstMatch.getId(), false),
                    new MatchPromise(String.format("%s %s %s", stringsProvider.getLoserOfString(), firstRound.getName(), firstMatch.getName()), firstMatch.getId(), true)
            ));
            rematch.setVersion("2.0");
            rematch.setName(stringsProvider.getMatchString(1));
            Round finalsRematch = new Round(List.list(rematch));
            finalsRematch.setVersion("2.0");
            finalsRematch.setName(stringsProvider.getRematchString());
            return winnersBracket.map(e -> (Event)e).snoc(finalsRematch);
        }

        // Algorithm for N-th round of Losers Bracket:
        List<Match> matchPool = List.list();
        int index = 0;
        do {
            // 1. Add matches to match pool from N-th round of Winner's bracket.
            if (index < winnersBracket.length()) {
                List<Match> lastWinnersBracketMatches = winnersBracket.index(index).getMatches();
                matchPool = matchPool.append(lastWinnersBracketMatches);
            }

            // 2. Calculate byes
            long numberOfByes = Utils.nextHighestPowerOf(2, matchPool.length()) - matchPool.length();

            // 2.1. If there is not enough matches, skip to next.
            if ((matchPool.length() < 2)) {
                index += 1;
                continue;
            }

            // 2.5. Split pool into byes and matches.
            matchPool = matchPool.sort(Ord.booleanOrd.comap(m -> isMatchFromBracket(winnersBracket, m)));
            P2<List<Match>, List<Match>> split = Utils.take(matchPool, l -> l.take((int)numberOfByes));

            if (split._2().length() == 0) {
                System.out.print("error while creating losers bracket match " + index);
            }

            // 3. For non-byes, pair them.
            List<List<Match>> pairs = Utils.takeAll(split._2(), l -> List.list(l.head(), l.tail().head()));

            final List<Round> finalLosersBracket = losersBracket;
            List<Match> matches = pairs.map(pair -> {
                List<Player> promises;
                promises = pair.map(pairMatch -> {
                    boolean isLooser = isMatchFromBracket(winnersBracket, pairMatch);
                    Round round = roundForMatch(List.list(winnersBracket, finalLosersBracket), pairMatch);
                    return new MatchPromise(String.format("%s %s %s", isLooser ? stringsProvider.getLoserOfString() : stringsProvider.getWinnerOfString(), round.getName(), pairMatch.getName()), pairMatch.getId(), isLooser);
                });

                Match match = new Match(promises);
                match.setVersion("2.0");
                return match;
            });

            for (int idx = 0; idx < matches.length(); idx++) {
                matches.index(idx).setName(stringsProvider.getMatchString(idx + 1));
            }

            // 4. Add generated matches to the round.
            Round nextRound = new Round(matches);
            nextRound.setVersion("2.0");
            nextRound.setName(stringsProvider.getLosersRoundString(losersBracket.length() + 1));
            losersBracket = losersBracket.cons(nextRound);

            // 5. Remove 'used' matches from the pool.
            matchPool = matchPool.filter(filteredMatch -> !split._2().exists(bye -> bye.getId().equals(filteredMatch.getId())));

            // 6. Add resulting matches to the match pool for next round.
            matchPool = matchPool.append(matches);

            // 7. Do while pool consists of more than 1 match
            index += 1;
        } while (matchPool.length() > 1 || losersBracket.bind(Round::getMatches).length() < 1 || index < winnersBracket.length());

        losersBracket = losersBracket.reverse();

        // After both brackets are ready, add finals match.
        Round losersBracketFinals = losersBracket.index(losersBracket.length() - 1);
        Match losersFinals = losersBracketFinals.getMatches().index(0);
        MatchPromise losersFinalist = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), losersBracketFinals.getName(), losersFinals.getName()), losersFinals.getId(), false);

        Round winnersBracketFinals = winnersBracket.index(winnersBracket.length() - 1);
        Match winnersFinals = winnersBracketFinals.getMatches().index(0);
        MatchPromise winnersFinalist = new MatchPromise(String.format("%s %s %s", stringsProvider.getWinnerOfString(), winnersBracketFinals.getName(), winnersFinals.getName()), winnersFinals.getId(), false);

        Match finals = new Match(List.list(winnersFinalist, losersFinalist));
        finals.setName(stringsProvider.getMatchString(1));
        finals.setVersion("2.0");

        Round finalsRound = new Round(List.list(finals));
        finalsRound.setName(stringsProvider.getFinalsString());
        finalsRound.setVersion("2.0");

        // Combine Winners, Losers, and Finals Round.
        Bracket winnersBracketEvent = new Bracket(new Date().getTime(), winnersBracket, Bracket.BRACKET_TYPE_WINNERS);
        winnersBracketEvent.setName(stringsProvider.getWinnersBracketString());

        Bracket losersBracketEvent = new Bracket(new Date().getTime(), losersBracket, Bracket.BRACKET_TYPE_LOSERS);
        losersBracketEvent.setName(stringsProvider.getLosersBracketString());

        return List.list(winnersBracketEvent, losersBracketEvent, finalsRound);
    }

    private boolean isMatchFromBracket(List<Round> bracket, Match match) {
        return bracket.bind(Round::getMatches).exists(m -> m.getId().equals(match.getId()));
    }

    private Round roundForMatch(List<List<Round>> brackets, Match targetMatch) {
        for (int bracketIdx = 0; bracketIdx < brackets.length(); bracketIdx++) {
            List<Round> bracket = brackets.index(bracketIdx);
            for (int idx = 0; idx < bracket.length(); idx++) {
                Round round = bracket.index(idx);
                for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                    Match match = round.getMatches().index(matchIdx);
                    if (match.getId().equals(targetMatch.getId())) {
                        return round;
                    }
                }
            }
        }

        return null;
    }
}
