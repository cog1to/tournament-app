package ru.aint.mtg.tournament.model.events;

import fj.data.Option;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Unarchive event.
 */
public class Unarchive extends Event {

    /**
     * Transforms given state.
     *
     * @param state Original state.
     *
     * @return New state transformed by the receiver.
     */
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (state.isNone()) {
            return state;
        }

        TournamentState previousState = state.some();
        return Option.some(new TournamentState(previousState.getFormat(), previousState.getPlayerStates(), previousState.getConfig(), false));
    }
}
