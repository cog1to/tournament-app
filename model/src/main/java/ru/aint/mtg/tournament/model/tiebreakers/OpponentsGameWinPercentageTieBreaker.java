package ru.aint.mtg.tournament.model.tiebreakers;

import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Tie-breaker for opponents game win percentage value.
 */
public class OpponentsGameWinPercentageTieBreaker extends TieBreaker {
    @Override
    public int compare(PlayerState state1, PlayerState state2, TournamentState state) {
        if (state1.getOpponentsGameWinPercentage(state) > state2.getOpponentsGameWinPercentage(state)) {
            return GREATER_THAN;
        } else if (state1.getOpponentsGameWinPercentage(state) < state2.getOpponentsGameWinPercentage(state)) {
            return LESS_THAN;
        }

        return TIED;
    }

    @Override
    public double getValue(PlayerState state, TournamentState tournamentState) {
        return state.getOpponentsGameWinPercentage(tournamentState);
    }
}
