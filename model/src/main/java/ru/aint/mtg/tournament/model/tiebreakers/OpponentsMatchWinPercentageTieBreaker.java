package ru.aint.mtg.tournament.model.tiebreakers;

import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;

/**
 * Tie-breaker for opponent's match win percentage value.
 */
public class OpponentsMatchWinPercentageTieBreaker extends TieBreaker {
    @Override
    public int compare(PlayerState state1, PlayerState state2, TournamentState state) {
        if (state1.getOpponentsMatchWinPercentage(state) > state2.getOpponentsMatchWinPercentage(state)) {
            return GREATER_THAN;
        } else if (state1.getOpponentsMatchWinPercentage(state) < state2.getOpponentsMatchWinPercentage(state)) {
            return LESS_THAN;
        }

        return TIED;
    }

    @Override
    public double getValue(PlayerState state, TournamentState tournamentState) {
        return state.getOpponentsMatchWinPercentage(tournamentState);
    }
}
