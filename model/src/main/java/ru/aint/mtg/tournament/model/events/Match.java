package ru.aint.mtg.tournament.model.events;

import java.util.Date;
import java.util.Set;

import fj.F2;
import fj.Ord;
import fj.P2;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.pairings.promises.MatchPromise;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;

/**
 * Match event holding results of an individual match.
 */
public class Match extends Event {

    /**
     * List of match results.
     */
    private Map<Player, Option<Integer>> scores;

    /**
     * Number of draws.
     */
    private int draws;

    /**
     * Returns new Match instance with given timestamp.
     *
     * @param timestamp Timestamp value.
     */
    private Match(long timestamp) {
        super(timestamp);
    }

    /**
     * Matches that this match is dependant on.
     */
    private List<String> dependencies;

    /**
     * Returns new Match instance with given timestamp and scores.
     *
     * @param timestamp Timestamp.
     * @param scores List of player results.
     */
    public Match(long timestamp, java.util.Map<Player, Option<Integer>> scores) {
        super(timestamp);

        if ((scores == null) || (scores.size() == 0)) {
            throw new IllegalArgumentException("Match must consist of at least one player");
        }

        Set<Player> keys = scores.keySet();
        this.scores = Map.map();
        for (Player key : keys) {
            Option<Integer> value = scores.get(key);
            this.scores.set(key, value);
        }

        updateDependencies();
    }

    /**
     * Returns new Match instance with given timestamp and scores.
     *
     * @param timestamp Timestamp.
     * @param scores List of player results.
     */
    public Match(long timestamp, Map<Player, Option<Integer>> scores) {
        super(timestamp);

        if ((scores == null) || (scores.size() == 0)) {
            throw new IllegalArgumentException("Match must consist of at least one player");
        }

        this.scores = scores;
        updateDependencies();
    }

    /**
     * Returns new Match instance with given timestamp and players.
     *
     * @param timestamp Timestamp.
     * @param players List of players.
     */
    public Match(long timestamp, List<Player> players) {
        super(timestamp);

        if ((players == null) || (players.length() == 0)) {
            throw new IllegalArgumentException("Match must consist of at least one player");
        }

        this.scores = players.foldLeft((m, p) -> {
            Option<Integer> opt = Option.none();
            m.set(p, opt);
            return m;
        }, Map.<Player, Option<Integer>>map());
        updateDependencies();
    }

    /**
     * Returns new Match instance.
     *
     */
    private Match() {
        this(new Date().getTime());
    }

    /**
     * Returns new Match instance with current timestamp and results.
     *
     * @param scores List of player scores.
     */
    public Match(java.util.Map<Player, Option<Integer>> scores) {
        this(new Date().getTime(), scores);
    }

    /**
     * Returns new Match instance with current timestamp and match results.
     *
     * @param scores Scores.
     */
    public Match(Map<Player, Option<Integer>> scores) {
        this(new Date().getTime(), scores);
    }

    /**
     * Returns new Match instance with current timestamp and players.
     *
     * @param players Players.
     */
    public Match(List<Player> players) {
        this(new Date().getTime(), players);
    }

    @Override
    public boolean isFinished() {
        if (this.scores.toList().filter(score -> score._1() instanceof MatchPromise).length() > 0) {
            return false;
        }

        List<Option<Integer>> values = this.scores.values();
        return !values.exists(Option::isNone);
    }

    /**
     * Checks if match is ready to be started.
     *
     * @return <b>true</b> if match is ready to be started, <b>false</b> otherwise.
     */
    public boolean isReady() {
        return this.scores != null && !this.scores.toList().exists(score -> score._1() instanceof MatchPromise);
    }

    /**
     * Checks if match is a draw.
     *
     * @return <b>true</b> if match is a draw, <b>false otherwise</b>.
     */
    public boolean isDraw() {
        if (!isFinished()) {
            return false;
        }

        if (scores.size() <= 1) {
            return false;
        }

        List<Option<Integer>> values = this.scores.values();
        Integer first = values.head().some();
        return !values.exists(o -> !o.some().equals(first));
    }

    /**
     * Returns winning player, if exists.
     *
     * @return Optional value of a winning player, if winner exists in current match.
     */
    public Option<Player> winner() {
        if (!isFinished()) {
            return Option.none();
        }

        if (isDraw()) {
            return Option.none();
        }

        List<P2<Player, Option<Integer>>> list = this.scores.toList();
        Integer maxScore = list.foldLeft((value, score) -> { return score._2().some() > value ? score._2().some() : value; }, 0);
        return Option.some(list.find(score -> score._2().some() == maxScore).some()._1());
    }

    /**
     * Return a list of losers.
     *
     * @return List of losers.
     */
    public List<Player> losers() {
        if (!isFinished()) {
            return List.nil();
        }

        if (isDraw()) {
            return List.nil();
        }

        Player winner = winner().some();
        return  this.scores.toList().map(P2::_1).foldLeft((loosers, player) -> {
            if (player.equals(winner)) {
                return loosers;
            } else {
                return loosers.cons(player);
            }
        }, List.list());
    }

    /**
     * Returns number of games played.
     *
     * @return Number of games played.
     */
    public int gamesPlayed() {
        return this.scores.toList().map(P2::_2).foldLeft((sum, score) -> sum + (score.isSome() ? score.some() : 0), 0) + this.draws;
    }

    /**
     * Checks if match is a Bye.
     *
     * @return <b>true</b> if match is a bye, <b>false</b> otherwise.
     */
    public boolean isBye() {
        return (this.scores.size() == 1);
    }

    /**
     * Returns list of scores.
     *
     * @return List of scores.
     */
    public Map<Player, Option<Integer>> getScores() {
        return scores;
    }

    /**
     * Sets scores.
     *
     * @param scores New scores list.
     */
    public void setScores(Map<Player, Option<Integer>> scores) {
        this.scores = scores;
    }

    /**
     * Sets score for given player.
     *
     * @param p Player.
     * @param score Player's score.
     */
    public void setScore(Player p, Option<Integer> score) {
        if (!this.scores.contains(p)) {
            throw new IllegalArgumentException("Player " + p.toString() + " is not participating in this match");
        }

        this.scores.set(p, score);
    }

    /**
     * Returns number of draws.
     *
     * @return Number of draws.
     */
    public int getDraws() {
        return this.draws;
    }

    /**
     * Sets number of draws.
     *
     * @param draws number of draws.
     */
    public void setDraws(int draws) {
        this.draws = draws;
    }

    /**
     * Returns list of match IDs that this match is dependant on.
     *
     * @return List of match IDs that this match is dependant on.
     */
    public List<String> getDependencies() {
        return this.dependencies;
    }

    /**
     * Recalculates scores for participants in the match. Caller has to adjust
     * OMWP & OGWP after that manually.
     *
     * @param state Input state.
     *
     * @return New tournament state.
     */
    @Override
    public Option<TournamentState> transformState(Option<TournamentState> state) {
        if (!isFinished()) {
            return state;
        }

        if (state.isNone()) {
            return state;
        }

        Match match = this;
        F2<TournamentState, PlayerState, PlayerState> updateState = (tournamentState, playerState) -> {
            if (!match.scores.contains(playerState.getPlayer())) {
                return playerState;
            }

            // Get current score.
            Option<Integer> score = match.scores.get(playerState.getPlayer()).some();

            // Determine if current player is a winner.
            Option<Player> winner = winner();
            boolean win = (winner.isSome() && (winner.some().equals(playerState.getPlayer())));

            // Determine if match is a draw.
            boolean draw = isDraw();

            // Player status. Changes only if current format is single elimination.
            PlayerStatus status = playerState.getStatus();
            if (tournamentState.getFormat() == TournamentFormat.SingleElimination && !win) {
                status = PlayerStatus.Eliminated;
            } else if (tournamentState.getFormat() == TournamentFormat.DoubleElimination && !win) {
                if (status == PlayerStatus.Active) {
                    status = PlayerStatus.Loser;
                } else if (status == PlayerStatus.Loser) {
                    status = PlayerStatus.Eliminated;
                } else {
                    throw new IllegalStateException("Eliminated player was participating in a match.");
                }
            }

            PlayerState.Builder builder = new PlayerState.Builder(playerState);
            builder.status(status)
                   .matchHistory(playerState.getMatchHistory().cons(match));
            return builder.build();
        };

        List<PlayerState> updatedStates = state.some().getPlayerStates().map(s -> updateState.f(state.some(), s));
        return Option.some(new TournamentState(state.some().getFormat(), updatedStates, state.some().getConfig()));
    }

    @Override
    public String toString() {
        String toString = "Match{\n";
        for (P2<Player, Option<Integer>> score: this.scores.toArray()) {
            toString += score._1().getFullName() + ": " + (score._2().isSome() ? score._2().some().toString() : "?") + "\n";
        }
        toString += ", draws=" + this.draws + "}\n";
        return toString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;
        if (!super.equals(o)) return false;

        Match match = (Match) o;

        if (timestamp != match.getTimestamp()) return false;
        if (isBye() != match.isBye()) return false;
        if (scores != null ? !scores.equals(match.scores) : match.scores != null) return false;
        if (draws != match.getDraws()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (isBye() ? 1 : 0);
        result = 31 * result + (scores != null ? scores.hashCode() : 0);
        result = 31 * result + draws;
        return result;
    }

    private void updateDependencies() {
        this.dependencies = this.scores.toList().foldLeft((list, ps) -> {
            if (ps._1() instanceof MatchPromise) {
                return list.cons(((MatchPromise)ps._1()).getMatchId());
            } else {
                return list;
            }
        }, List.list());
    }
}
