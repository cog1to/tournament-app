package ru.aint.mtg.tournament.model.brackets;

import org.junit.Test;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Bracket;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;

import static org.junit.Assert.assertTrue;

public class DoubleEliminationBracketGeneratorTest {

    private static class StringsProvider implements IStringsProvider {
        @Override
        public String getMatchString(int matchIndex) {
            return "";
        }

        @Override
        public String getRoundString(int roundIndex) {
            return "";
        }

        @Override
        public String getLosersRoundString(int roundIndex) {
            return "";
        }

        @Override
        public String getFinalsString() {
            return "";
        }

        @Override
        public String getRematchString() {
            return "";
        }

        @Override
        public String getLoserOfString() {
            return "";
        }

        @Override
        public String getWinnerOfString() {
            return "";
        }

        @Override
        public String getWinnersBracketString() {
            return "";
        }

        @Override
        public String getLosersBracketString() {
            return "";
        }
    }

    @Test
    public void test4Players() throws Exception {
        List<Player> players = List.list(new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"));

        DoubleEliminationBracketGenerator generator = new DoubleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        int numberOfRounds = rounds.foldLeft((sum, event) -> {
            if (event instanceof Round) {
                return sum+1;
            } else if (event instanceof Bracket) {
                return sum+((Bracket)event).getRounds().length();
            } else {
                return sum;
            }
        }, 0);

        assertTrue("Number of rounds is " + rounds.length() + " instead of 5", numberOfRounds == 5);
    }

    @Test
    public void test17Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"), new Player("Amy V"),
                new Player("Jordan L"), new Player("Suzy G"), new Player("Matt J"), new Player("Bill C"),
                new Player("Uma Y"), new Player("Henry R"), new Player("Jane T"), new Player("Ashlynn B"),
                new Player("Max S"));

        DoubleEliminationBracketGenerator generator = new DoubleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        int numberOfRounds = rounds.foldLeft((sum, event) -> {
            if (event instanceof Round) {
                return sum+1;
            } else if (event instanceof Bracket) {
                return sum+((Bracket)event).getRounds().length();
            } else {
                return 0;
            }
        }, 0);

        assertTrue("Number of rounds is " + rounds.length() + " instead of 13", numberOfRounds == 13);
    }

    @Test
    public void test7Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"));

        DoubleEliminationBracketGenerator generator = new DoubleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        int numberOfRounds = rounds.foldLeft((sum, event) -> {
            if (event instanceof Round) {
                return sum+1;
            } else if (event instanceof Bracket) {
                return sum+((Bracket)event).getRounds().length();
            } else {
                return 0;
            }
        }, 0);

        assertTrue("Number of rounds is " + rounds.length() + " instead of 8", numberOfRounds == 8);
    }
}
