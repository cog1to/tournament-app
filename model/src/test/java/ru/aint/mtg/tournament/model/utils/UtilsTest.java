package ru.aint.mtg.tournament.model.utils;

import fj.P;
import fj.P2;
import fj.data.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class UtilsTest {
    static int maxLength = 100;

    Random random = null;

    @Before
    public void setUp() throws Exception {
        random = new Random();
    }

    @Test
    public void testRotateRandom() throws Exception {
        // Generate random list.
        int length = random.nextInt(maxLength) + 1;
        List<Integer> input = List.list();

        for (int idx = 0; idx < length; idx++) {
            input = input.cons(new Integer(length - idx));
        }

        // Apply rotate manually.
        List<Integer> expected = input.take(input.length() - 1).cons(input.last());

        // Apply rotate by function
        List<Integer> result = Utils.rotate(input);

        // Lists must be the same.
        assertEquals(expected, result);
    }

    @Test
    public void testRotateEmpty() throws Exception {
        List<Integer> list = List.list();
        assertEquals(Utils.rotate(list), list);
    }

    @Test
    public void testRotateOne() throws Exception {
        List<Integer> list = List.list(1);
        assertEquals(Utils.rotate(list), list);
    }

    @Test
    public void testRotate() throws Exception {
        List<Integer> list = List.list(1, 2, 3, 4, 5);
        assertEquals(Utils.rotate(list), List.list(5, 1, 2, 3, 4));
    }

    @Test
    public void testRotate1Random() throws Exception {
        // Generate random list.
        int length = random.nextInt(maxLength) + 1;
        List<Integer> input = List.list();

        for (int idx = 0; idx < length; idx++) {
            input = input.cons(new Integer(length - idx));
        }

        // Number of rotations.
        int times = random.nextInt(maxLength) + 1;

        // Apply rotate manually.
        List<Integer> expected = input;
        for (int iteration = 0; iteration < times; iteration++) {
            expected = expected.take(expected.length() - 1).cons(expected.last());
        }

        // Apply rotate by function
        List<Integer> result = Utils.rotate(input, times);

        // Lists must be the same.
        assertEquals(expected, result);
    }

    @Test
    public void testRotate1() throws Exception {
        List<Integer> list = List.list(1, 2, 3, 4, 5);
        assertEquals(Utils.rotate(list, 2), List.list(4, 5, 1, 2, 3));
    }

    @Test
    public void testRotate1Empty() throws Exception {
        List<Integer> list = List.list();
        assertEquals(Utils.rotate(list, 2), list);
    }

    @Test
    public void testRotate1One() throws Exception {
        List<Integer> list = List.list(1);
        assertEquals(Utils.rotate(list, 2), list);
    }

    @Test
    public void testTake() throws Exception {
        List<Integer> input = List.list(1, 2, 3);
        P2<List<Integer>,List<Integer>> expected = P.p(List.list(1), List.list(2, 3));

        assertEquals(expected, Utils.take(input, l -> l.take(1)));
    }

    @Test(expected = IllegalStateException.class)
    public void testTakeGenerator() throws Exception {
        List<Integer> input = List.list(1, 2, 3);
        Utils.take(input, l -> List.list(5));
    }

    @Test
    public void testTakeAll() throws Exception {
        List<Integer> input = List.list(1, 2, 3);
        List<List<Integer>> expected = List.list(List.list(1),List.list(2),List.list(3));

        assertEquals(expected, Utils.takeAll(input, l -> l.take(1)));
    }

    @Test
    public void testTakeAllNoMatch() throws Exception {
        List<Integer> input = List.list(1, 2, 3);

        List<Integer> empty = List.list();
        List<List<Integer>> expected = List.list();
        expected = expected.cons(empty);

        assertEquals(expected, Utils.takeAll(input, l -> l.filter(e -> e < 0).take(1)));
    }

    @Test
    public void testTakeAllEmptyList() throws Exception {
        List<Integer> input = List.list();

        List<Integer> empty = List.list();
        List<List<Integer>> expected = List.list();
        expected = expected.cons(empty);

        assertEquals(expected, Utils.takeAll(input, l -> l.take(1)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextHighestPowerOfNegativeBase() throws Exception {
        Utils.nextHighestPowerOf(-1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextHighestPowerOfZeroBase() throws Exception {
        Utils.nextHighestPowerOf(0, 1);
        Utils.nextHighestPowerOf(1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextHighestPowerOfBaseOne() throws Exception {
        Utils.nextHighestPowerOf(1, 1);
    }

    @Test
    public void testNextHighestPowerOf() throws Exception {
        assertEquals(16, Utils.nextHighestPowerOf(2, 13));
        assertEquals(32, Utils.nextHighestPowerOf(2, 18));
        assertEquals(27, Utils.nextHighestPowerOf(3, 20));
    }

    @Test
    public void testNextHighestPowerOfExactPower() throws Exception {
        assertEquals(16, Utils.nextHighestPowerOf(2, 16));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextHighestPowerOfZeroTarget() throws Exception {
        Utils.nextHighestPowerOf(2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextHighestPowerOfNegativeTarget() throws Exception {
        Utils.nextHighestPowerOf(2, -1);
    }

    @Test
     public void testNextLowestPowerOf() throws Exception {
        assertEquals(8, Utils.nextLowestPowerOf(2, 13));
        assertEquals(16, Utils.nextLowestPowerOf(2, 26));
        assertEquals(64, Utils.nextLowestPowerOf(2, 70));
    }

    @Test
    public void testNextLowestPowerOfExactPower() throws Exception {
        assertEquals(2, Utils.nextLowestPowerOf(2, 4));
        assertEquals(16, Utils.nextLowestPowerOf(2, 32));
        assertEquals(9, Utils.nextLowestPowerOf(3, 27));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextLowestPowerOfZeroTarget() throws Exception {
        Utils.nextLowestPowerOf(2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextLowestPowerOfZeroBase() throws Exception {
        Utils.nextLowestPowerOf(0, 12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNextLowestPowerOfBaseGreaterThanTarget() throws Exception {
        Utils.nextLowestPowerOf(4, 1);
    }
}