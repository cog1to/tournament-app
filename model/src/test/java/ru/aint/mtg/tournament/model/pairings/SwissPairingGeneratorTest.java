package ru.aint.mtg.tournament.model.pairings;

import fj.data.List;
import fj.data.Option;
import org.junit.Test;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.SwissPairingGenerator;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.utils.Map;
import ru.aint.mtg.tournament.model.utils.Utils;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Swiss pairing generator tests.
 */
public class SwissPairingGeneratorTest {

    @Test
    public void testGetPairingsWithBye() throws Exception {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);
        Player player5 = new Player("player5", null);

        TournamentConfig config = new TournamentConfig(2);
        Registration registration = new Registration(config, TournamentFormat.Swiss, List.list(player1, player2, player3, player4, player5));
        Tournament tournament = new Tournament("t1", "t1", new Date().getTime());
        tournament.setEvents(tournament.getEvents().cons(registration));

        SwissPairingGenerator generator = new SwissPairingGenerator();
        List<Match> matches = generator.getPairings(tournament);

        assertTrue(matches.filter(Match::isBye).length() == 1);
        assertTrue(matches.filter(Match::isBye).head().getScores().toList().head()._2().some() == 2);
    }

    @Test
    public void testGetPairingsSecondRound() throws Exception {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);
        Player player5 = new Player("player5", null);
        List<Player> players = List.list(player1, player2, player3, player4, player5);

        TournamentConfig config = new TournamentConfig(2);
        Registration registration = new Registration(config, TournamentFormat.Swiss, players);
        Tournament tournament = new Tournament("t1", "t1", new Date().getTime());
        tournament.setEvents(tournament.getEvents().cons(registration));

        List<List<Player>> pairs = Utils.takeAll(players, list -> {
            if (list.length() >= 2) {
                return List.list(list.head(), list.tail().head());
            } else {
                return List.list(list.head());
            }
        });

        Player byePlayer = null;
        List<Player> wonPlayers = List.list();

        List<Match> matches = pairs.map(pair -> new Match(pair));
        for (Match m : matches) {
            m.setScore(m.getScores().keys().head(), Option.some(config.getWinsPerMatch()));
            if (m.getScores().size() > 1) {
                m.setScore(m.getScores().keys().tail().head(), Option.some(0));
                wonPlayers = wonPlayers.cons(m.getScores().keys().head());
            } else {
                byePlayer = m.getScores().keys().head();
            }
        }

        Round round1 = new Round(new Date().getTime(), matches);
        tournament.setEvents(tournament.getEvents().cons(round1));

        SwissPairingGenerator generator = new SwissPairingGenerator();
        List<Match> round2matches = generator.getPairings(tournament);

        // 1 bye
        assertTrue(round2matches.filter(Match::isBye).length() == 1);
        assertTrue(round2matches.filter(Match::isBye).head().getScores().toList().head()._2().some() == 2);

        // should be a player that is not a byePlayer
        Player byePlayer2 = round2matches.filter(Match::isBye).head().getScores().toList().head()._1();
        assertTrue(!byePlayer2.equals(byePlayer));

        // 2 matches, should be with wonPlayers;
        final List<Player> finalWonPlayers = wonPlayers;
        List<Match> otherMatches = round2matches.filter(m -> !m.isBye());
        assertTrue(otherMatches.forall(m -> m.getScores().keys().exists(p -> finalWonPlayers.exists(wp -> wp.equals(p)))));
    }
}