package ru.aint.mtg.tournament.model.brackets;

import org.junit.Test;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;

import static org.junit.Assert.assertTrue;

public class SingleEliminationBracketGeneratorTest {
    private static class StringsProvider implements IStringsProvider {
        @Override
        public String getMatchString(int matchIndex) {
            return "";
        }

        @Override
        public String getRoundString(int roundIndex) {
            return "";
        }

        @Override
        public String getLosersRoundString(int roundIndex) {
            return "";
        }

        @Override
        public String getFinalsString() {
            return "";
        }

        @Override
        public String getRematchString() {
            return "";
        }

        @Override
        public String getLoserOfString() {
            return "";
        }

        @Override
        public String getWinnerOfString() {
            return "";
        }

        @Override
        public String getWinnersBracketString() {
            return "";
        }

        @Override
        public String getLosersBracketString() {
            return "";
        }
    }

    @Test
    public void test2Players() throws Exception {
        List<Player> players = List.list(new Player("Alex B"), new Player("Molly H"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        assertTrue(rounds.length() == 1);
    }

    @Test
    public void test4Players() throws Exception {
        List<Player> players = List.list(new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 2);
        assertTrue(rounds.index(0).getMatches().length() == 2);
        assertTrue(rounds.index(1).getMatches().length() == 1);
    }

    @Test
    public void test13Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"), new Player("Amy V"),
                new Player("Jordan L"), new Player("Suzy G"), new Player("Matt J"), new Player("Bill C"),
                new Player("Uma Y"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 4);
        assertTrue(rounds.index(0).getMatches().length() == 5);
        assertTrue(rounds.index(1).getMatches().length() == 4);
        assertTrue(rounds.index(2).getMatches().length() == 2);
        assertTrue(rounds.index(3).getMatches().length() == 1);

        for (int roundIdx = 0; roundIdx < rounds.length(); roundIdx++) {
            Round round = rounds.index(roundIdx);
            System.out.println("Round " + roundIdx);
            for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                Match match = round.getMatches().index(matchIdx);
                System.out.println("    Match " + matchIdx + "(" + match.getId() + ")");
                List<Player> matchPlayers = match.getScores().keys();
                for (int playerIdx = 0; playerIdx < match.getScores().size(); playerIdx++) {
                    System.out.println("        " + matchPlayers.index(playerIdx).getFullName());
                }
            }
        }
    }

    @Test
    public void test11Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"), new Player("Amy V"),
                new Player("Jordan L"), new Player("Suzy G"), new Player("Matt J"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 4);
        assertTrue(rounds.index(0).getMatches().length() == 3);
        assertTrue(rounds.index(1).getMatches().length() == 4);
        assertTrue(rounds.index(2).getMatches().length() == 2);
        assertTrue(rounds.index(3).getMatches().length() == 1);

        for (int roundIdx = 0; roundIdx < rounds.length(); roundIdx++) {
            Round round = rounds.index(roundIdx);
            System.out.println("Round " + roundIdx);
            for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                Match match = round.getMatches().index(matchIdx);
                System.out.println("    Match " + matchIdx + "(" + match.getId() + ")");
                List<Player> matchPlayers = match.getScores().keys();
                for (int playerIdx = 0; playerIdx < match.getScores().size(); playerIdx++) {
                    System.out.println("        " + matchPlayers.index(playerIdx).getFullName());
                }
            }
        }
    }

    @Test
    public void test9Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"), new Player("Amy V"),
                new Player("Jordan L"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 4);
        assertTrue(rounds.index(0).getMatches().length() == 1);
        assertTrue(rounds.index(1).getMatches().length() == 4);
        assertTrue(rounds.index(2).getMatches().length() == 2);
        assertTrue(rounds.index(3).getMatches().length() == 1);

        for (int roundIdx = 0; roundIdx < rounds.length(); roundIdx++) {
            Round round = rounds.index(roundIdx);
            System.out.println("Round " + roundIdx);
            for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                Match match = round.getMatches().index(matchIdx);
                System.out.println("    Match " + matchIdx + "(" + match.getId() + ")");
                List<Player> matchPlayers = match.getScores().keys();
                for (int playerIdx = 0; playerIdx < match.getScores().size(); playerIdx++) {
                    System.out.println("        " + matchPlayers.index(playerIdx).getFullName());
                }
            }
        }
    }

    @Test
    public void test7Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 3);
        assertTrue(rounds.index(0).getMatches().length() == 3);
        assertTrue(rounds.index(1).getMatches().length() == 2);
        assertTrue(rounds.index(2).getMatches().length() == 1);

        for (int roundIdx = 0; roundIdx < rounds.length(); roundIdx++) {
            Round round = rounds.index(roundIdx);
            System.out.println("Round " + roundIdx);
            for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                Match match = round.getMatches().index(matchIdx);
                System.out.println("    Match " + matchIdx + "(" + match.getId() + ")");
                List<Player> matchPlayers = match.getScores().keys();
                for (int playerIdx = 0; playerIdx < match.getScores().size(); playerIdx++) {
                    System.out.println("        " + matchPlayers.index(playerIdx).getFullName());
                }
            }
        }
    }

    @Test
    public void test17Players() throws Exception {
        List<Player> players = List.list(
                new Player("Alex B"), new Player("Molly H"), new Player("Kevin R"), new Player("Stanley P"),
                new Player("Leo W"), new Player("Nick P"), new Player("Frank D"), new Player("Amy V"),
                new Player("Jordan L"), new Player("Suzy G"), new Player("Matt J"), new Player("Bill C"),
                new Player("Uma Y"), new Player("Henry R"), new Player("Jane T"), new Player("Ashlynn B"),
                new Player("Max S"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Round> rounds = generator.getBracket(players, new StringsProvider()).map(ev -> (Round)ev);

        assertTrue(rounds.length() == 5);
        assertTrue(rounds.index(0).getMatches().length() == 1);
        assertTrue(rounds.index(1).getMatches().length() == 8);
        assertTrue(rounds.index(2).getMatches().length() == 4);
        assertTrue(rounds.index(3).getMatches().length() == 2);
        assertTrue(rounds.index(4).getMatches().length() == 1);

        for (int roundIdx = 0; roundIdx < rounds.length(); roundIdx++) {
            Round round = rounds.index(roundIdx);
            System.out.println("Round " + roundIdx);
            for (int matchIdx = 0; matchIdx < round.getMatches().length(); matchIdx++) {
                Match match = round.getMatches().index(matchIdx);
                System.out.println("    Match " + matchIdx + "(" + match.getId() + ")");
                List<Player> matchPlayers = match.getScores().keys();
                for (int playerIdx = 0; playerIdx < match.getScores().size(); playerIdx++) {
                    System.out.println("        " + matchPlayers.index(playerIdx).getFullName());
                }
            }
        }
    }
}
