package ru.aint.mtg.tournament.model.pairings;

import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import fj.Ord;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.utils.Map;
import ru.aint.mtg.tournament.model.utils.Utils;

import static org.junit.Assert.assertEquals;

public class SingleEliminationPairingGeneratorTest {

    @Test
    public void testGetFirstPairingsEven() throws Exception {
        int playersCount = 16;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + String.format("%02d", idx), baseSurname + String.format("%02d", idx)));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, players);
        tournament.setEvents(List.list(registration));

        // Generate matches.
        PairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();
        List<Match> matches = pairingGenerator.getPairings(tournament);

        // Create expected list.
        List<List<Player>> expectedPairs = Utils.takeAll(players, l -> List.list(l.head(), l.last()));
        List<List<Player>> pairs = matches.filter(m -> !m.isBye()).map(m -> m.getScores().keys());

        assertEquals(expectedPairs, pairs);
        assertEquals(0, matches.filter(m -> m.isBye()).length());
    }

    @Test
    public void testGetFirstPairingsOdd() throws Exception {
        int playersCount = 13;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName +  String.format("%02d", idx), baseSurname +  String.format("%02d", idx)));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, players);
        tournament.setEvents(List.list(registration));

        // Generate matches.
        PairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();
        List<Match> matches = pairingGenerator.getPairings(tournament);

        List<Player> expectedByes = players.take(3);
        List<Player> byes = matches.filter(m -> m.isBye()).map(m -> m.getScores()).bind(s -> s.keys());
        assertEquals(expectedByes, byes);

        List<List<Player>> expectedPairs = Utils.takeAll(players.drop(3), l -> List.list(l.head(), l.last()));
        List<List<Player>> pairs = matches.filter(m -> !m.isBye()).map(m -> m.getScores().keys());
        assertEquals(expectedPairs, pairs);
    }

    @Test
    public void testGetSecondPairingsEven() throws Exception {
        int playersCount = 16;
        String baseName = "FirstName", baseSurname="LastName";
        List<Option<Integer>> resultsTemplate = List.list(Option.some(TournamentConfig.defaultConfig().getWinsPerMatch()), Option.some(0));

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName +  String.format("%02d", idx), baseSurname +  String.format("%02d", idx)));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        /** Part 1 - Generate and fill first round **/
        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, players);
        tournament.setEvents(List.list(registration));

        // Generate first round matches.
        PairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();
        List<Match> matches = pairingGenerator.getPairings(tournament);

        // Generate results for first round.
        List<Match> finishedMatches = matches.map(m -> {
            Map<Player, Option<Integer>> scores = m.getScores();
            List<Player> matchPlayers = scores.keys();
            Map<Player, Option<Integer>> newScores = Map.from(matchPlayers.zip(resultsTemplate));
            return new Match(m.getTimestamp(), newScores);
        });

        // Generate first round.
        Round roundOne = new Round(1, finishedMatches);
        assert(roundOne.isFinished());

        // Add round event to tournament.
        tournament.setEvents(tournament.getEvents().cons(roundOne));

        /** Part 2 - Check pairings for the second round **/
        List<Match> secondRoundMatches = pairingGenerator.getPairings(tournament);

        List<Player> expectedActivePlayers = players.take(8);
        List<Player> activePlayers = tournament.currentState().some().getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).map(ps -> ps.getPlayer());
        assertEquals(expectedActivePlayers, activePlayers);

        List<List<Player>> expectedPairs = Utils.takeAll(expectedActivePlayers, l -> l.take(2));
        List<List<Player>> pairs = secondRoundMatches.map(m -> m.getScores().keys());
        assertEquals(expectedPairs, pairs);
    }

    @Test
    public void testGetSecondPairingsOdd() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null), new Player("c", null), new Player("d", null), new Player("e", null));

        // Register players.
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, players);
        tournament.setEvents(List.list(registration));

        // Make first round.
        PairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();
        List<Match> matches = pairingGenerator.getPairings(tournament);

        // Check that first round is valid.
        assertEquals(players.take(3), matches.filter(m -> m.isBye()).bind(m -> m.getScores().keys()));

        List<List<Player>> expectedFirstRound = Utils.takeAll(players.drop(3), l -> List.list(l.head(), l.last()));
        List<List<Player>> firstRound = matches.filter(m -> !m.isBye()).map(m -> m.getScores().keys());

        // Check that first round consists of exactly one game and that this game includes only "d" and "e" players.
        assertEquals(1, firstRound.length());
        assert(expectedFirstRound.zip(firstRound).forall(pair -> pair._1().forall(p -> pair._2().exists(p2 -> p2 == p))));
        assert(expectedFirstRound.zip(firstRound).forall(pair -> pair._2().forall(p -> pair._1().exists(p2 -> p2 == p))));
        assertEquals(List.list("d", "e"), firstRound.bind(p -> p).map(p -> p.getFullName()).sort(Ord.<String>comparableOrd()));

        // Populate scores.
        List<Option<Integer>> resultsTemplate = List.list(Option.some(TournamentConfig.defaultConfig().getWinsPerMatch()), Option.some(0));
        matches.filter(m -> !m.isBye()).foreachDoEffect(match -> match.setScores(Map.from(match.getScores().keys().sort(Ord.<Player>hashOrd()).zip(resultsTemplate))));

        // Generate first round with scored matches.
        Round roundOne = new Round(1, matches);
        assert(roundOne.isFinished());

        // Add round event to tournament.
        tournament.setEvents(tournament.getEvents().cons(roundOne));
        matches = pairingGenerator.getPairings(tournament);

        // Check that second round pairings are correct.
        assertEquals(2, matches.length());

        List<List<Player>> expectedSecondRound = List.list(List.list(players.index(0), players.index(3)), List.list(players.index(1), players.index(2)));
        List<List<Player>> secondRound = matches.filter(m -> !m.isBye()).map(m -> m.getScores().keys());
        assert(expectedSecondRound.zip(secondRound).forall(pair -> pair._1().forall(p -> pair._2().exists(p2 -> p2 == p))));
        assert(expectedSecondRound.zip(secondRound).forall(pair -> pair._2().forall(p -> pair._1().exists(p2 -> p2 == p))));
    }
}