package ru.aint.mtg.tournament.model.players;

import fj.data.List;
import fj.data.Option;
import org.junit.Test;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * PlayerState tests.
 */
public class PlayerStateTest {

    private static final double epsilon = 0.01;

    @Test
    public void testGetGamesWonOneMatch() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);

        java.util.Map<Player, Option<Integer>> scores = new HashMap<>();
        scores.put(player1, Option.some(1));
        scores.put(player2, Option.some(2));
        Match match = new Match(1, scores);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match));
        PlayerState state2 = new PlayerState(player2, PlayerStatus.Active, List.list(match));

        assertEquals(state1.getGamesWon(), 1);
        assertEquals(state2.getGamesWon(), 2);
    }

    @Test
    public void testGetGamesWonThreeMatches() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);

        java.util.Map<Player, Option<Integer>> scores1 = new HashMap<>();
        scores1.put(player1, Option.some(1));
        scores1.put(player2, Option.some(2));
        Match match1 = new Match(1, scores1);

        java.util.Map<Player, Option<Integer>> scores2 = new HashMap<>();
        scores2.put(player1, Option.some(1));
        scores2.put(player3, Option.some(0));
        Match match2 = new Match(2, scores2);

        java.util.Map<Player, Option<Integer>> scores3 = new HashMap<>();
        scores3.put(player1, Option.some(2));
        scores3.put(player4, Option.some(0));
        Match match3 = new Match(3, scores3);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match1, match2, match3));

        assertEquals(state1.getGamesWon(), 4);
    }

    @Test
    public void testGetGameWinPercentageThreeMatches() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);

        java.util.Map<Player, Option<Integer>> scores1 = new HashMap<>();
        scores1.put(player1, Option.some(1));
        scores1.put(player2, Option.some(2));
        Match match1 = new Match(1, scores1);

        java.util.Map<Player, Option<Integer>> scores2 = new HashMap<>();
        scores2.put(player1, Option.some(1));
        scores2.put(player3, Option.some(0));
        Match match2 = new Match(2, scores2);

        java.util.Map<Player, Option<Integer>> scores3 = new HashMap<>();
        scores3.put(player1, Option.some(0));
        scores3.put(player4, Option.some(2));
        Match match3 = new Match(3, scores3);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match1, match2, match3));

        assertTrue(Math.abs(state1.getGameWinPercentage(new TournamentConfig(2)) - 0.33) < epsilon);
    }

    @Test
    public void testGetMatchWinPercentageThreeMatches() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);

        java.util.Map<Player, Option<Integer>> scores1 = new HashMap<>();
        scores1.put(player1, Option.some(2));
        scores1.put(player2, Option.some(1));
        Match match1 = new Match(1, scores1);

        java.util.Map<Player, Option<Integer>> scores2 = new HashMap<>();
        scores2.put(player1, Option.some(2));
        scores2.put(player3, Option.some(0));
        Match match2 = new Match(2, scores2);

        java.util.Map<Player, Option<Integer>> scores3 = new HashMap<>();
        scores3.put(player1, Option.some(0));
        scores3.put(player4, Option.some(2));
        Match match3 = new Match(3, scores3);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match1, match2, match3));

        assertTrue(Math.abs(state1.getMatchWinPercentage(new TournamentConfig(2)) - 0.66) < epsilon);
    }

    @Test
    public void testGetMatchWinPercentageMinimum() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);

        java.util.Map<Player, Option<Integer>> scores1 = new HashMap<>();
        scores1.put(player1, Option.some(0));
        scores1.put(player2, Option.some(2));
        Match match1 = new Match(1, scores1);

        java.util.Map<Player, Option<Integer>> scores2 = new HashMap<>();
        scores2.put(player1, Option.some(1));
        scores2.put(player3, Option.some(2));
        Match match2 = new Match(2, scores2);

        java.util.Map<Player, Option<Integer>> scores3 = new HashMap<>();
        scores3.put(player1, Option.some(0));
        scores3.put(player4, Option.some(2));
        Match match3 = new Match(3, scores3);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match1, match2, match3));

        assertTrue((state1.getMatchWinPercentage(new TournamentConfig(2)) - 0.33) < epsilon);
    }

    @Test
    public void testGetGameWinPercentageMinimum() throws Exception
    {
        Player player1 = new Player("player1", null);
        Player player2 = new Player("player2", null);
        Player player3 = new Player("player3", null);
        Player player4 = new Player("player4", null);

        java.util.Map<Player, Option<Integer>> scores1 = new HashMap<>();
        scores1.put(player1, Option.some(0));
        scores1.put(player2, Option.some(2));
        Match match1 = new Match(1, scores1);

        java.util.Map<Player, Option<Integer>> scores2 = new HashMap<>();
        scores2.put(player1, Option.some(1));
        scores2.put(player3, Option.some(2));
        Match match2 = new Match(2, scores2);

        java.util.Map<Player, Option<Integer>> scores3 = new HashMap<>();
        scores3.put(player1, Option.some(0));
        scores3.put(player4, Option.some(2));
        Match match3 = new Match(3, scores3);

        PlayerState state1 = new PlayerState(player1, PlayerStatus.Active, List.list(match1, match2, match3));

        assertTrue((state1.getGameWinPercentage(new TournamentConfig(2)) - 0.33) < epsilon);
    }
}