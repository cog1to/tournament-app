package ru.aint.mtg.tournament.model.brackets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Bracket;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;

import static org.junit.Assert.fail;

/**
 * Tests bracket updater.
 */
@RunWith(Parameterized.class)
public class BracketUpdaterFinishTest {

    private static class StringsProvider implements IStringsProvider {

        @Override
        public String getMatchString(int matchIndex) {
            return String.format("Match %d", matchIndex);
        }

        @Override
        public String getRoundString(int roundIndex) {
            return String.format("Round %d", roundIndex);
        }

        @Override
        public String getLosersRoundString(int roundIndex) {
            return String.format("Losers Round %d", roundIndex);
        }

        @Override
        public String getFinalsString() {
            return null;
        }

        @Override
        public String getRematchString() {
            return null;
        }

        @Override
        public String getLoserOfString() {
            return "Loser of";
        }

        @Override
        public String getWinnerOfString() {
            return "Winner of";
        }

        @Override
        public String getWinnersBracketString() {
            return null;
        }

        @Override
        public String getLosersBracketString() {
            return null;
        }
    }

    @Parameterized.Parameters(name = "{index}: testBracketCompletion({0})")
    public static Iterable<Integer> data() {
        ArrayList<Integer> variants = new ArrayList<>();
        for (int idx = 2; idx <= 64; idx++) {
            variants.add(idx);
        }
        return variants;
    }

    private int numberOfPlayers;

    public BracketUpdaterFinishTest(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    @Test
    public void TestSingleEliminationBracketCompletion() {
        List<Player> players = List.list();
        for (int idx = 0; idx < numberOfPlayers; idx++) {
            players = players.cons(new Player(String.format("Player %d", idx)));
        }

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        BracketUpdater updater = new BracketUpdater();
        while (rounds.exists(ev -> !ev.isFinished())) {
            List<Match> matches = rounds.bind(event -> ((Round)event).getMatches());
            List<Match> notFinishedMatches = matches.filter(match -> !match.isFinished() && match.isReady() && !match.isBye());
            if (notFinishedMatches.length() > 0) {
                Match firstMatch = notFinishedMatches.last();
                Player winner = firstMatch.getScores().keys().head();
                Player loser = firstMatch.getScores().keys().filter(p -> !p.equals(winner)).head();
                firstMatch.setScore(winner, Option.some(2));
                firstMatch.setScore(loser, Option.some(0));
            }

            updater.updateBracket(rounds);
        }
    }

    @Test
    public void TestDoubleEliminationBracketCompletion() {
        List<Player> players = List.list();
        for (int idx = 0; idx < numberOfPlayers; idx++) {
            players = players.cons(new Player(String.format("Player %d", idx)));
        }

        DoubleEliminationBracketGenerator generator = new DoubleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new StringsProvider());

        BracketUpdater updater = new BracketUpdater();
        while (rounds.exists(ev -> !ev.isFinished())) {
            List<Match> matches = rounds.bind(event -> {
                if (event instanceof Bracket) {
                    Bracket bracket = (Bracket)event;
                    return bracket.getRounds().bind(round -> round.getMatches());
                } else {
                    return ((Round)event).getMatches();
                }
            });
            List<Match> notFinishedMatches = matches.filter(match -> !match.isFinished() && match.isReady() && !match.isBye());
            if (notFinishedMatches.length() > 0) {
                Match firstMatch = notFinishedMatches.last();
                Player winner = firstMatch.getScores().keys().head();
                Player loser = firstMatch.getScores().keys().filter(p -> !p.equals(winner)).head();
                firstMatch.setScore(winner, Option.some(2));
                firstMatch.setScore(loser, Option.some(0));
            } else {
                fail(String.format("No matches to finish, but tournament is not finished yet\nBracket state:\n%s", rounds.toString()));
            }

            updater.updateBracket(rounds);
        }
    }
}
