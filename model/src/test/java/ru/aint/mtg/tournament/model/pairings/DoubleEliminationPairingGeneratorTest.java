package ru.aint.mtg.tournament.model.pairings;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;
import static org.junit.Assert.assertTrue;

/**
 * Double elimination pairing generator tests.
 */
public class DoubleEliminationPairingGeneratorTest {

    /**
     * Random number generator.
     */
    Random random;

    @Before
    public void setUp() {
        random = new Random(new Date().getTime());
    }

    @Test
    public void test2Players() throws Exception {
        int playersCount = 2;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 1);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 1);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 1);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);
    }

    @Test
    public void test4Players() throws Exception {
        int playersCount = 4;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 2);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 2);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 3);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);
    }

    @Test
    public void test8Players() throws Exception {
        int playersCount = 8;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 4);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 4);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 7);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);

        List<Event> rounds = tournament.getEvents().filter(e -> e instanceof Round);
        assertTrue(rounds.length() <= 7);
    }

    @Test
    public void test5Players() throws Exception {
        int playersCount = 5;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 1);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 4);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 4);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);
    }

    @Test
    public void test8PlayersRounds() throws Exception {
        int playersCount = 8;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate rounds.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Round> rounds = pairingGenerator.getRounds(tournament);

            // Create expected list.
            rounds.bind(r -> r.getMatches()).foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            List<Event> newEventsList = rounds.foldLeft((list, round) -> list.cons(round), tournament.getEvents());
            tournament.setEvents(newEventsList);
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 4);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 4);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 7);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);

        List<Event> rounds = tournament.getEvents().filter(e -> e instanceof Round);
        assertTrue(rounds.length() <= 9);
    }

    @Test
    public void test16PlayersRounds() throws Exception {
        int playersCount = 16;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleElimination, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate rounds.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleEliminationPairingGenerator();
            List<Round> rounds = pairingGenerator.getRounds(tournament);

            // Create expected list.
            rounds.bind(r -> r.getMatches()).foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            List<Event> newEventsList = rounds.foldLeft((list, round) -> list.cons(round), tournament.getEvents());
            tournament.setEvents(newEventsList);
        }

        Round firstRound = (Round)tournament.getEvents().filter(e -> e instanceof Round).last();
        TournamentState stateAfterFirstRound = tournament.stateAfterEvent(firstRound).some();

        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 8);
        assertTrue(stateAfterFirstRound.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 8);

        TournamentState lastState = tournament.currentState().some();
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Eliminated).length() == 15);
        assertTrue(lastState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1);

        List<Event> rounds = tournament.getEvents().filter(e -> e instanceof Round);
        assertTrue(rounds.length() <= 11);
    }
}