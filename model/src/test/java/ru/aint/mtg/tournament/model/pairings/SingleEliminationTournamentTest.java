package ru.aint.mtg.tournament.model.pairings;

import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;

/**
 * Created by aint on 9/27/2015.
 */
public class SingleEliminationTournamentTest {

    /**
     * Random number generator.
     */
    Random random;

    /**
     * List of first names.
     */
    private List<String> firstNames = List.list(
            "Josie",
            "Little",
            "Meghan",
            "Avila",
            "Erickson",
            "Johnnie",
            "Barron",
            "Elena",
            "Angelique",
            "Sellers",
            "Socorro",
            "Rosario",
            "Judith",
            "Glass",
            "Larsen",
            "Leona",
            "Vicky",
            "Scott",
            "Miranda",
            "Rebecca",
            "Morse",
            "Cummings",
            "Cash",
            "Jimmie",
            "Meyers",
            "Ophelia",
            "Bradford",
            "Brittney",
            "Glenna",
            "Rhoda",
            "Swanson",
            "Winnie",
            "Vinson",
            "Raymond",
            "Holder",
            "Wolfe",
            "Dominique",
            "Raquel",
            "Juanita",
            "Gay",
            "Daisy",
            "Sally",
            "Dean",
            "Gale",
            "Andrews",
            "Kane",
            "Ward",
            "Loraine",
            "Espinoza",
            "Bradshaw");

    /**
     * List of surnames.
     */
    List<String> surnames = List.list("Glenn",
            "Long",
            "Burks",
            "Workman",
            "Key",
            "Dickson",
            "Slater",
            "Bean",
            "Clements",
            "Clayton",
            "Robertson",
            "Norman",
            "Yates",
            "Jensen",
            "Lewis",
            "Lopez",
            "Foreman",
            "Albert",
            "English",
            "Gonzalez",
            "Maddox",
            "Donaldson",
            "Roach",
            "Mcintosh",
            "Harrington",
            "Payne",
            "Holland",
            "Haley",
            "Perez",
            "Kramer",
            "Carey",
            "Goodwin",
            "Estrada",
            "Witt",
            "Ward",
            "Nielsen",
            "Cummings",
            "Avery",
            "Ellison",
            "Ochoa",
            "Baxter",
            "Lyons",
            "Mack",
            "Marquez",
            "Stout",
            "Sutton",
            "Dotson",
            "Trevino",
            "Preston",
            "Henderson");

    @Before
    public void setUp() {
        random = new Random(new Date().getTime());
    }

    @Test
    public void testSingleEliminationTournament() throws Exception {
        int numberOfPlayers = 9; // = random.nextInt(DEFAULT_MAX_NUMBER_OF_PLAYERS - 1) + 2
        Tournament t = randomTournament(numberOfPlayers);
        t.currentState().some();
    }

    private Tournament randomTournament(int numberOfPlayers) {
        Date date = randomDate();

        // Create tournament.
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Tournament " + format.format(date), date.getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, randomPlayerList(numberOfPlayers));
        tournament.setEvents(tournament.getEvents().cons(registration));

        // Generate all rounds.
        while (!tournament.currentState().some().isFinished()) {
            addRandomRound(tournament);
        }

        // Return.
        return tournament;
    }

    private List<Player> randomPlayerList(int numberOfPlayers) {
        List<Player> players = List.list();
        while (players.length() < numberOfPlayers) {
            Player player = randomPlayer();
            if (!players.exists(p -> p.equals(player))) {
                players = players.cons(player);
            }
        }

        return players;
    }

    private Player randomPlayer() {
        String firstName = firstNames.index(random.nextInt(firstNames.length()));
        String lastName = surnames.index(random.nextInt(surnames.length()));

        return new Player(firstName, lastName);
    }

    private void addRandomRound(Tournament tournament) {
        TournamentState lastState = tournament.currentState().some();
        if (lastState.getFormat() == TournamentFormat.SingleElimination) {
            SingleEliminationPairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
            return;
        }

        throw new IllegalStateException("Current format is not supported yet");
    }

    private Date randomDate() {
        int year = random.nextInt(100) + 1900;
        int dayOfYear = random.nextInt(365);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
        return calendar.getTime();
    }
}
