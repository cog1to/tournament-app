package ru.aint.mtg.tournament.model.pairings;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;

/**
 * Double round robin pairing generator test.
 */
public class DoubleRoundRobinPairingGeneratorTest extends TestCase {

    /**
     * Random number generator.
     */
    Random random;

    @Before
    public void setUp() {
        random = new Random(new Date().getTime());
    }


    @Test
    public void testGenerator() throws Exception {
        int playersCount = 4;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleRoundRobin, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            PairingGenerator pairingGenerator = new DoubleRoundRobinPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        assertTrue(tournament.getEvents().filter(e -> e instanceof Round).length() == 6);

        TournamentState lastState = tournament.currentState().some();
        final List<PlayerState> states = lastState.getPlayerStates();

        lastState.getPlayerStates().foreachDoEffect(ps -> {
            assertTrue(ps.getMatchHistory().length() == 6);

            List<Player> otherPlayers = states.filter(s -> !s.getPlayer().equals(ps.getPlayer())).map(s -> s.getPlayer());
            assertTrue(otherPlayers.forall(player -> ps.playedAgainst(player)));
        });
    }

    @Test
    public void testGeneratorWithDrops() throws Exception {
        int playersCount = 4;
        String baseName = "FirstName", baseSurname="LastName";

        // Generate players.
        List<Player> players = List.range(0, playersCount).map(idx -> new Player(baseName + idx, baseSurname + idx));
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Test Tournament", new Date().getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.DoubleRoundRobin, players);
        List<Event> events = List.list();
        tournament.setEvents(events.cons(registration));

        // Generate matches.
        while (!tournament.currentState().some().isFinished()) {
            TournamentState lastState = tournament.currentState().some();

            if (tournament.getEvents().filter(e -> e instanceof Round).length() == 3) {
                tournament.setEvents(tournament.getEvents().cons(new Drop(List.list(players.tail().head(), players.tail().last()))));
            }

            PairingGenerator pairingGenerator = new DoubleRoundRobinPairingGenerator();
            List<Match> matches = pairingGenerator.getPairings(tournament);

            // Create expected list.
            matches.foreach(m -> {
                if (m.isBye()) {
                    return Unit.unit();
                }

                Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                    if (p1._1().equals(winner)) {
                        return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                    } else {
                        return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                    }
                });
                m.setScores(scores);
                return Unit.unit();
            });

            Round round = new Round(matches);
            tournament.setEvents(tournament.getEvents().cons(round));
        }

        assertTrue(tournament.getEvents().filter(e -> e instanceof Round).length() == 6);

        TournamentState lastState = tournament.currentState().some();
        final List<PlayerState> states = lastState.getPlayerStates();

        lastState.getPlayerStates().foreachDoEffect(ps -> {
            List<Player> otherPlayers = states.filter(s -> !s.getPlayer().equals(ps.getPlayer())).map(s -> s.getPlayer());
            assertTrue(otherPlayers.forall(player -> ps.playedAgainst(player)));
        });
    }
}