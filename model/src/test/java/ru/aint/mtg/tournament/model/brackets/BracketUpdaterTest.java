package ru.aint.mtg.tournament.model.brackets;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Random;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;

import static org.junit.Assert.assertTrue;

/**
 * Bracket updater test.
 */
public class BracketUpdaterTest {
    /**
     * Random number generator.
     */
    Random random;

    @Before
    public void setUp() {
        random = new Random(new Date().getTime());
    }

    @Test
    public void test4Players() throws Exception {
        List<Player> players = List.list(new Player("Alex B"), new Player("Molly H"), new Player("John C"), new Player("Ken T"));

        SingleEliminationBracketGenerator generator = new SingleEliminationBracketGenerator();
        List<Event> rounds = generator.getBracket(players, new IStringsProvider() {
            @Override
            public String getMatchString(int matchIndex) {
                return "";
            }

            @Override
            public String getRoundString(int roundIndex) {
                return "";
            }

            @Override
            public String getLosersRoundString(int roundIndex) {
                return "";
            }

            @Override
            public String getFinalsString() {
                return "";
            }

            @Override
            public String getRematchString() {
                return "";
            }

            @Override
            public String getLoserOfString() {
                return "";
            }

            @Override
            public String getWinnerOfString() {
                return "";
            }

            @Override
            public String getWinnersBracketString() {
                return "";
            }

            @Override
            public String getLosersBracketString() {
                return "";
            }
        });

        assertTrue(!((Round)rounds.index(1)).getMatches().index(0).isReady());

        Round firstRound = (Round)rounds.index(0);
        for (Match match :
                firstRound.getMatches()) {
            match.setScore(match.getScores().toList().index(0)._1(), Option.some(2));
            match.setScore(match.getScores().toList().index(1)._1(), Option.some(0));
        }

        BracketUpdater updater = new BracketUpdater();
        updater.updateBracket(rounds);

        Round finals = (Round)rounds.index(1);
        assertTrue(finals.getMatches().index(0).isReady());
    }
}
