package ru.aint.mtg.tournament.model.events;

import fj.P;
import fj.data.List;
import fj.data.Option;
import org.junit.Test;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Map;

import static org.junit.Assert.*;

/**
 * Created by aint on 5/1/2015.
 */
public class MatchTest {

    @Test(expected = IllegalArgumentException.class)
    public void testZeroPlayers() throws Exception {
        new Match(List.list());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroPlayersMap() throws Exception {
        new Match(Map.from(List.list()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroPlayersJavaMap() throws Exception {
        new Match(new java.util.HashMap<Player, Option<Integer>>());
    }

    @Test
    public void testPlayersList() throws Exception {
        List<Player> players = List.list(new Player("a", "a"), new Player("b", "b"));
        Match m = new Match(List.list(new Player("a", "a"), new Player("b", "b")));
        assert(m.getScores().size() == 2);
        assert(players.forall(expectedPlayer -> m.getScores().keys().exists(p -> p.equals(expectedPlayer))));
    }

    @Test
    public void testPlayersMap() throws Exception {
        List<Player> players = List.list(new Player("a", "a"), new Player("b", "b"));
        Map<Player, Option<Integer>> scores = Map.from(players.map(player -> P.p(player, Option.none())));

        Match m = new Match(scores);
        assert(m.getScores().size() == 2);
        assert(players.forall(expectedPlayer -> m.getScores().keys().exists(p -> p.equals(expectedPlayer))));
        assert(m.getScores().values().forall(score -> score.isNone()));
    }

    @Test
    public void testPlayersJavaMap() throws Exception {
        List<Player> players = List.list(new Player("a", "a"), new Player("b", "b"));
        Map<Player, Option<Integer>> scores = Map.from(players.map(player -> P.p(player, Option.none())));
        java.util.HashMap<Player, Option<Integer>> scoresJavaMap = scores.toList().foldLeft((map, score) -> {
            map.put(score._1(), score._2());
            return map;
        }, new java.util.HashMap<Player, Option<Integer>>());

        Match m = new Match(scoresJavaMap);
        assert(m.getScores().size() == 2);
        assert(players.forall(expectedPlayer -> m.getScores().keys().exists(p -> p.equals(expectedPlayer))));
        assert(m.getScores().values().forall(score -> score.isNone()));
    }

    @Test
    public void testIsFinished() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.some(new Integer(1)), Option.some(new Integer(0)));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
    }

    @Test
    public void testIsNotFinished() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.some(new Integer(1)), Option.none());

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(!finishedMatch.isFinished());
    }

    @Test
    public void testIsDraw() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.some(new Integer(1)), Option.some(new Integer(1)));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isDraw());
    }

    @Test
    public void testIsNotDraw() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.some(new Integer(1)), Option.some(new Integer(2)));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(!finishedMatch.isDraw());
    }

    @Test
    public void testByeIsNotDraw() throws Exception {
        List<Player> players = List.list(new Player("a", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>some(0));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
        assert(finishedMatch.isBye());
        assert(!finishedMatch.isDraw());
    }

    @Test
    public void testWinner() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.<Integer>some(0), Option.<Integer>some(2));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
        assert(finishedMatch.winner().isSome());
        assertEquals(players.index(1), finishedMatch.winner().some());
    }

    @Test
    public void testNoWinnerOnDraw() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.<Integer>some(2), Option.<Integer>some(2));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
        assert(finishedMatch.winner().isNone());
    }

    @Test
    public void testByeWinner() throws Exception {
        List<Player> players = List.list(new Player("a", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>some(0));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
        assert(finishedMatch.isBye());
        assert(finishedMatch.winner().isSome());
        assertEquals(players.index(0), finishedMatch.winner().some());
    }

    @Test
    public void testNoWinnerInProgress() throws Exception {
        List<Player> players = List.list(new Player("a", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>none());

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(!finishedMatch.isFinished());
        assert(finishedMatch.winner().isNone());
    }

    @Test
    public void testNoDrawInProgress() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>none()).cons(Option.<Integer>none());

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(!finishedMatch.isFinished());
        assert(!finishedMatch.isDraw());
    }

    @Test
    public void testGamesPlayedNone() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>none()).cons(Option.<Integer>none());

        Match match = new Match(Map.from(players.zip(scores)));
        assertEquals(0, match.gamesPlayed());
    }

    @Test
    public void testGamesPlayedFinished() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>some(3)).cons(Option.<Integer>some(0));

        Match match = new Match(Map.from(players.zip(scores)));
        assertEquals(3, match.gamesPlayed());
    }

    @Test
    public void testIsBye() throws Exception {
        List<Player> players = List.list(new Player("a", null));
        List<Option<Integer>> scores = List.list();
        scores = scores.cons(Option.<Integer>some(2));

        Match finishedMatch = new Match(Map.from(players.zip(scores)));
        assert(finishedMatch.isFinished());
        assert(finishedMatch.isBye());
    }

    @Test
    public void testGetScores() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        List<Option<Integer>> scores = List.list(Option.some(new Integer(1)), Option.none());

        Match m = new Match(Map.from(players.zip(scores)));
        assertEquals(Map.from(players.zip(scores)), m.getScores());
    }

    @Test
    public void testSetScores() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m = new Match(players);

        List<Option<Integer>> scores = List.list(Option.some(new Integer(999)), Option.none());
        m.setScores(Map.from(players.zip(scores)));
        assertEquals(Map.from(players.zip(scores)), m.getScores());
    }

    @Test
    public void testSetScore() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m = new Match(players);
        m.setScore(new Player("a", null), Option.some(5));

        assertEquals(Option.some(5), m.getScores().get(new Player("a", null)).some());
        assertEquals(Option.none(), m.getScores().get(new Player("b", null)).some());
    }

    @Test
    public void testEquals() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(1, players);
        Match m2 = new Match(1, players.reverse());
        assertEquals(m1, m2);
    }

    @Test
    public void testNotEqualsByTimestamp() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(players);
        Thread.sleep(100);
        Match m2 = new Match(players.reverse());
        assertNotEquals(m1, m2);
    }

    @Test
    public void testNotEqualsByPlayers() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(1, players.take(1));
        Match m2 = new Match(1, players.drop(1));
        assertNotEquals(m1, m2);
    }

    @Test
    public void testNotEqualsByType() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(1, players);

        assertNotEquals(m1, new String("sample"));
    }

    @Test
    public void testNotEqualsByBye() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(1, players);
        Match m2 = new Match(1, players.take(1));

        assertNotEquals(m1, m2);
    }

    @Test
    public void testNotEqualsByScore() throws Exception {
        List<Player> players = List.list(new Player("a", null), new Player("b", null));
        Match m1 = new Match(1, players);
        m1.setScore(new Player("a", null), Option.some(5));

        Match m2 = new Match(1, players);
        m2.setScore(new Player("b", null), Option.some(5));

        assertNotEquals(m1, m2);
    }
}