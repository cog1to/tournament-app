# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\aint\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn java.lang.invoke.*
-dontwarn org.apache.**
-dontwarn com.android.org.**
-dontwarn java.net.PlainSocketImpl
-dontwarn java.net.InetAddress.**
-dontwarn crittercism.android.**
-dontwarn android.net.http.**
-dontwarn java.net.**
-dontwarn com.crittercism.internal.**
-keep public class com.crittercism.**
-keep public class com.crittercism.internal.**
-keepclassmembers public class com.crittercism.* { *; }
-keepattributes SourceFile, LineNumberTable, Signature
-keep public class ru.aint.mtg.tournament.model.**
-keepclassmembers public class ru.aint.mtg.tournament.model.** { *; }
-keep public class ru.aint.mtg.tournament.tournamenthelper.data.**
-keepclassmembers public class ru.aint.mtg.tournament.tournamenthelper.data.** { *; }
-keep public class fj.data.**
-printmapping build/outputs/mapping/release/mapping.txt