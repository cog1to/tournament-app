package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.log;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import ru.aint.mtg.tournament.tournamenthelper.data.log.Logger;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log.ILogItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log.ILogPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.log.ILogView;

/**
 * Log presenter.
 */
public class LogPresenter implements ILogPresenter {

    @Override
    public void onCreateView(@NonNull ILogView view) {
        List<String> logEntries = Logger.getLogEntries();
        ArrayList<ILogItemPresenter> presenters = new ArrayList<ILogItemPresenter>();

        for (int idx = 0; idx < logEntries.size(); idx++) {
            presenters.add(new LogItemPresenter(logEntries.get(idx)));
        }

        view.setItems(presenters.toArray(new ILogItemPresenter[0]));
    }

    private class LogItemPresenter implements ILogItemPresenter {

        String mText;

        private LogItemPresenter(String text) {
            mText = text;
        }

        @Override
        public String getText() {
            return mText;
        }
    }
}
