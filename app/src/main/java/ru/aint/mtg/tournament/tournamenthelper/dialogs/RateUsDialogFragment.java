package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * 'Rate us' dialog fragment.
 */
public class RateUsDialogFragment extends DialogFragment {
    /**
     * Confirm action constant.
     */
    public static final int ACTION_OK = 2;

    /**
     * 'No thanks' action constant.
     */
    public static final int ACTION_NO = 1;

    /**
     * 'Already rated' action constant.
     */
    public static final int ACTION_ALREADY = 0;

    /**
     * Dialog event listener interface.
     */
    public interface Listener {

        /**
         * Called when user selects an action.
         *
         * @param action Selected action.
         */
        void onActionSelected(RateUsDialogFragment fragment, int action);
    }

    /**
     * Dialog listener.
     */
    private Listener listener;

    /**
     * Presented dialog instance.
     */
    private AlertDialog dialog;

    /**
     * Sets dialog listener.
     *
     * @param listener Dialog listener.
     */
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getActivity().getResources().getString(R.string.rate_us_text))
                .setPositiveButton(R.string.rate_us, (dialog1, id) -> {
                    if (listener != null) {
                        listener.onActionSelected(this, ACTION_OK);
                    }
                    dismiss();
                })
                .setNegativeButton(R.string.not_now, (dialog1, id) -> {
                    if (listener != null) {
                        listener.onActionSelected(this, ACTION_NO);
                    }
                    dismiss();
                })
                .setNeutralButton(R.string.already_rated, (dialog1, id) -> {
                    if (listener != null) {
                        listener.onActionSelected(this, ACTION_ALREADY);
                    }
                    dismiss();
                })
                .setCancelable(false);

        this.dialog = builder.create();
        return this.dialog;
    }
}
