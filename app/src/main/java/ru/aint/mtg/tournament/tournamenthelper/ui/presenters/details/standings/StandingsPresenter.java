package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.standings;

import org.atteo.evo.inflector.English;

import java.io.IOException;

import fj.Equal;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreaker;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerFactory;
import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerType;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.files.InternalStorageFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.helpers.TieBreakerPresenter;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.JsonPlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsActionPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsHeaderPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsPlayerPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IStandingsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.ITournamentUpdatedCallback;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.DetailsSubPresenter;

/**
 * Standings presenter.
 */
public class StandingsPresenter extends DetailsSubPresenter implements IStandingsPresenter {

    private IStandingsView mView;

    private List<IStandingsItemPresenter> mPresenters;

    private ITournamentUpdatedCallback mUpdateDelegate;

    public StandingsPresenter(Tournament tournament) {
        super(tournament);
    }

    public void setUpdateCallback(ITournamentUpdatedCallback callback) {
        mUpdateDelegate = callback;
    }

    @Override
    public DetailsType getType() {
        return DetailsType.Standings;
    }

    @Override
    public void setView(IDetailsSubView view) {
        mView = (IStandingsView)view;
        updatePresenters();
        mView.showStandings(mPresenters);
    }

    private void updatePresenters() {
        TournamentState state = mTournament.currentState().some();

        StandingsHeaderPresenter headerPresenter = new StandingsHeaderPresenter(mTournament.currentState().some());

        List<PlayerState> playerStates;
        if (!mTournament.isStarted()) {
            playerStates = state.getPlayerStates();
        } else {
            playerStates = state.getSortedStandings();
        }
        mPresenters = playerStates.map(ps -> new StandingsPlayerPresenter(ps, state));

        if (!mTournament.isStarted()) {
            mPresenters = mPresenters.snoc(new IStandingsActionPresenter() {
                @Override
                public String getName() {
                    return MainApplication.getInstance().getString(R.string.add_new_participant);
                }

                @Override
                public ActionType getType() {
                    return ActionType.AddParticipant;
                }
            });

            if (mTournament.currentState().isSome() && mTournament.currentState().some().getPlayerStates().length() > 1) {
                mPresenters = mPresenters.snoc(new IStandingsActionPresenter() {
                    @Override
                    public String getName() {
                        return MainApplication.getInstance().getString(R.string.start_tournament);
                    }

                    @Override
                    public ActionType getType() {
                        return ActionType.StartTournament;
                    }
                });
            }
        }

        mPresenters = mPresenters.cons(headerPresenter);
    }

    @Override
    protected void onTournamentStarted() {
        updatePresenters();
        mView.showStandings(mPresenters);

        if (mUpdateDelegate != null) {
            mUpdateDelegate.onTournamentUpdated(mTournament);
        }
    }

    @Override
    protected IDetailsSubView getView() {
        return mView;
    }

    @Override
    public  void onItemTapped(IStandingsItemPresenter presenter) {
        if (presenter instanceof IStandingsActionPresenter) {
            IStandingsActionPresenter.ActionType type = ((IStandingsActionPresenter) presenter).getType();
            switch (type) {
                case AddParticipant:
                    mView.showNewPlayerDialog();
                    break;
                case StartTournament:
                    mView.showConfirmationDialog(MainApplication.getInstance().getString(R.string.start_tournament_confirm_title), MainApplication.getInstance().getString(R.string.start_tournament_confirm_message), confirmed -> {
                        if (confirmed) {
                            startTournament();
                        }
                    });
                    break;
            }
        } else {
            if (mTournament.isStarted()) {
                StandingsPlayerPresenter playerPresenter = (StandingsPlayerPresenter)presenter;
                MainApplication.getInstance().getRouter().showPlayerDetails(playerPresenter.mPlayerState, playerPresenter.mTournamentState.getConfig());
            } else {
                IStandingsPlayerPresenter playerPresenter = (IStandingsPlayerPresenter)presenter;
                mView.showPlayerEditingOptions(playerPresenter.getName(), playerPresenter, !mTournament.isStarted());
            }
        }
    }

    @Override
    public void onPlayerEdited(IStandingsPlayerPresenter presenter, IStandingsView.EditPlayerModel existingPlayer, IStandingsView.EditPlayerModel player) {
        Registration registration = mTournament.lastEventOfType(Registration.class).some();
        List<Player> players = registration.getPlayers();

        Player existingPlayerObject = players.find(p -> p.getFullName().equals(existingPlayer.getFullName())).some();
        players = players.map(p -> {
            if (p == existingPlayerObject) {
                return new Player(player.getFullName());
            } else {
                return p;
            }
        });
        registration.setPlayers(players);
        saveTournament();

        Option<Integer> index = mPresenters.elementIndex(Equal.anyEqual(), presenter);
        updatePresenters();

        if (index.isSome()) {
            mView.reloadItem(index.some(), mPresenters.index(index.some()));
        } else {
            mView.showStandings(mPresenters);
        }

        // Update suggestions table.
        try {
            JsonPlayerSuggestionsProvider suggestionsProvider = new JsonPlayerSuggestionsProvider(MainApplication.getInstance(), new InternalStorageFileManager(MainApplication.getInstance()));
            suggestionsProvider.replaceSuggestion(existingPlayer.getFullName(), player.getFullName());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPlayerCreated(IStandingsView.EditPlayerModel player) {
        Registration registration = mTournament.lastEventOfType(Registration.class).some();

        int previousCount = registration.getPlayers().length();
        registration.setPlayers(registration.getPlayers().snoc(new Player(player.getFullName())));
        saveTournament();

        updatePresenters();
        mView.addItem(registration.getPlayers().length(), mPresenters.index(registration.getPlayers().length()));

        if (previousCount < 2 && registration.getPlayers().length() >= 2) {
            mView.addItem(mPresenters.length() - 1, mPresenters.index(mPresenters.length() - 1));
        }

        // Update suggestions table.
        try {
            JsonPlayerSuggestionsProvider suggestionsProvider = new JsonPlayerSuggestionsProvider(MainApplication.getInstance(), new InternalStorageFileManager(MainApplication.getInstance()));
            suggestionsProvider.addSuggestion(player.getFullName());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPlayerDeleting(IStandingsPlayerPresenter presenter) {
        Registration registration = mTournament.lastEventOfType(Registration.class).some();

        int previousCount = registration.getPlayers().length();
        Player existingPlayerObject = ((StandingsPlayerPresenter)presenter).mPlayerState.getPlayer();
        registration.setPlayers(registration.getPlayers().filter(p -> !p.equals(existingPlayerObject)));
        saveTournament();

        Option<Integer> index = mPresenters.elementIndex(Equal.anyEqual(), presenter);
        updatePresenters();

        if (index.isSome()) {
            mView.removeItem(index.some());
            if (previousCount >= 2 && registration.getPlayers().length() < 2) {
                mView.removeItem(mPresenters.length() - 1);
            }
        } else {
            mView.showStandings(mPresenters);
        }
    }

    @Override
    public void onPlayerEditing(IStandingsPlayerPresenter presenter) {
        Player existingPlayerObject = ((StandingsPlayerPresenter)presenter).mPlayerState.getPlayer();
        mView.showEditPlayerDialog(presenter, new IStandingsView.EditPlayerModel(existingPlayerObject.getFullName()));
    }

    @Override
    public BasePlayerSuggestionsProvider getSuggestionsProvider() {
        return new JsonPlayerSuggestionsProvider(MainApplication.getInstance(), new InternalStorageFileManager(MainApplication.getInstance()));
    }

    @Override
    public boolean canCreatePlayer(IStandingsView.EditPlayerModel player) {
        Registration registration = mTournament.lastEventOfType(Registration.class).some();
        return !registration.getPlayers().exists(p -> p.getFullName().equals(player.getFullName()));
    }

    /**
     * Player presenter.
     */
    private static class StandingsPlayerPresenter implements IStandingsPlayerPresenter {

        /**
         * Player state.
         */
        PlayerState mPlayerState;

        /**
         * Tournament state.
         */
        TournamentState mTournamentState;

        /**
         * Returns new instance of player state presenter.
         *
         * @param state Player state.
         * @param tournamentState Tournament state.
         */
        StandingsPlayerPresenter(PlayerState state, TournamentState tournamentState) {
            mPlayerState = state;
            mTournamentState = tournamentState;
        }

        @Override
        public String getStatus() {
            if (!mTournamentState.isFinished()) {
                return mPlayerState.getStatus().toString();
            }

            if (mTournamentState.getFormat() == TournamentFormat.DoubleElimination) {
                if ((mPlayerState.getStatus() == PlayerStatus.Active || mPlayerState.getStatus() == PlayerStatus.Loser) && mTournamentState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active || ps.getStatus() == PlayerStatus.Loser).length() == 1) {
                    return MainApplication.getInstance().getString(R.string.winner);
                } else {
                    return mPlayerState.getStatus().toString();
                }
            } else if (mTournamentState.getFormat() == TournamentFormat.SingleElimination) {
                if (mPlayerState.getStatus() == PlayerStatus.Active && mTournamentState.getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 1) {
                    return MainApplication.getInstance().getString(R.string.winner);
                } else {
                    return mPlayerState.getStatus().toString();
                }
            } else {
                if (mTournamentState.getSortedStandings().head().getPlayer().equals(mPlayerState.getPlayer())) {
                    return MainApplication.getInstance().getString(R.string.winner);
                }
            }

            return mPlayerState.getStatus().toString();
        }

        @Override
        public String getName() {
            return mPlayerState.getPlayer().getFullName();
        }

        @Override
        public List<String> getDetailsValues() {
            List<String> values = List.list();
            for (int idx = 0; idx < Math.min(mTournamentState.getConfig().getTieBreakers().length(), StandingsHeaderPresenter.MAX_TIEBRAEKERS_SHOWN); idx++) {
                TieBreakerType tieBreakerType = mTournamentState.getConfig().getTieBreakers().index(idx);
                TieBreaker tieBreaker = TieBreakerFactory.getTieBreaker(tieBreakerType);
                double value = tieBreaker.getValue(mPlayerState, mTournamentState);
                values = values.snoc(TieBreakerPresenter.formatValue(tieBreakerType, value));
            }

            return values;
        }
    }

    /**
     * Standings header presenter.
     */
    private static class StandingsHeaderPresenter implements IStandingsHeaderPresenter {

        /**
         * Max number of tie-breakers to display on screen.
         */
        static final int MAX_TIEBRAEKERS_SHOWN = 4;

        /**
         * Tournament state.
         */
        private TournamentState mTournamentState;

        /**
         * Returns new instance.
         *
         * @param state State.
         */
        StandingsHeaderPresenter(TournamentState state) {
            mTournamentState = state;
        }

        @Override
        public String getTitle() {
            int numberOfPlayers = mTournamentState.getPlayerStates().length();
            return numberOfPlayers + " " + English.plural(MainApplication.getInstance().getResources().getString(R.string.player), numberOfPlayers);
        }

        @Override
        public List<String> getDetailTitles() {

            List<String> titles = List.list();
            for (int idx = 0; idx < Math.min(mTournamentState.getConfig().getTieBreakers().length(), MAX_TIEBRAEKERS_SHOWN); idx++) {
                TieBreakerType tieBreakerType = mTournamentState.getConfig().getTieBreakers().index(idx);
                String title = TieBreakerPresenter.getTieBreakerTitle(tieBreakerType);
                titles = titles.snoc(title);
            }
            return titles;
        }
    }
}
