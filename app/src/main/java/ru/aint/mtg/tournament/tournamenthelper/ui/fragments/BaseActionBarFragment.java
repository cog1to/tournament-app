package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.INavigationActivity;

/**
 * Action bar fragment.
 */
public abstract class BaseActionBarFragment extends Fragment {

    /**
     * Toolbar.
     */
    protected Toolbar mToolbar;

    /**
     * Root view.
     */
    protected View mRootView;

    /**
     * Menu item
     */
    protected int mMenuItem = -1;

    /**
     * Current menu.
     */
    protected List<MenuItem> mCurrentMenuItems = List.list();

    /**
     * Title to set into action bar.
     *
     * @return Title to set into action bar.
     */
    protected abstract String getTitle();

    /**
     * Retrieves layout resource of the view.
     *
     * @return Layout resource of the view.
     */
    protected abstract @LayoutRes int getViewLayout();

    /**
     * Indicates whether shows the back button on action bar.
     *
     * @return Whether to show back button of action bar instead of hamburger menu.
     */
    protected boolean showBackButton() {
        return false;
    }

    /**
     * Sets checked navigation item.
     *
     * @param navigationItem Checked navigation item.
     */
    public void setNavigationItem(@IdRes int navigationItem) {
        mMenuItem = navigationItem;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getViewLayout(), container, false);
        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        updateToolbar();

        return mRootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mCurrentMenuItems = inflateMenuItems(menu);
        mCurrentMenuItems.foreachDoEffect(i -> i.getActionView().setAlpha(0.0f));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        fadeInMenu();
    }

    /**
     * Called when back button is pressed.
     *
     * @return Flag indicating whether back button press was handled.
     */
    public boolean onBackPressed() {
        return false;
    }

    /**
     * Updates toolbar.
     */
    protected void updateToolbar() {
        setTitleValue(getTitle());

        FragmentActivity activity = getActivity();
        if (activity instanceof INavigationActivity) {
            ((INavigationActivity)activity).setActiveToolbar(mToolbar);
            ((INavigationActivity)activity).showBackButton(showBackButton());

            if (!showBackButton()) {
                if (mMenuItem > 0) {
                    ((INavigationActivity) activity).setActiveNavigationItem(mMenuItem);
                }
            }
        }
    }

    /**
     * Sets fragment's title.
     *
     * @param title Title to set.
     */
    protected void setTitleValue(String title) {
        mToolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
        mToolbar.setTitle(title);
    }

    protected fj.data.List<MenuItem> inflateMenuItems(Menu menu) {
        return fj.data.List.list();
    }

    protected void reloadMenu() {
        fadeOutMenu();
    }

    private void fadeOutMenu() {
        ArrayList<Animator> animators = new ArrayList<>();
        this.mCurrentMenuItems.foreachDoEffect(item -> {
            ObjectAnimator animator = ObjectAnimator.ofFloat(item.getActionView(), View.ALPHA, 1.0f, 0.0f);
            animators.add(animator);
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(animators);
        set.setDuration(getActivity().getResources().getInteger(R.integer.half_animation_duration));
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                triggerMenuReload();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        set.start();
    }

    private void fadeInMenu() {
        ArrayList<Animator> animators = new ArrayList<Animator>();
        mCurrentMenuItems.foreachDoEffect(i -> {
            ObjectAnimator animator = ObjectAnimator.ofFloat(i.getActionView(), View.ALPHA, 1.0f);
            animators.add(animator);
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(animators);
        set.setDuration(getActivity().getResources().getInteger(R.integer.animation_duration));
        set.start();
    }

    private void triggerMenuReload() {
        getActivity().supportInvalidateOptionsMenu();
    }
}
