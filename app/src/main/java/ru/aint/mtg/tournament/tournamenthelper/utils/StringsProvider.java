package ru.aint.mtg.tournament.tournamenthelper.utils;

import ru.aint.mtg.tournament.model.brackets.IStringsProvider;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * Strings provider implementation.
 */
public class StringsProvider implements IStringsProvider {
    @Override
    public String getMatchString(int matchIndex) {
        return String.format(MainApplication.getInstance().getString(R.string.match_format), matchIndex);
    }

    @Override
    public String getRoundString(int roundIndex) {
        return String.format(MainApplication.getInstance().getString(R.string.round_format), roundIndex);
    }

    @Override
    public String getLosersRoundString(int roundIndex) {
        return String.format(MainApplication.getInstance().getString(R.string.losers_round_format), roundIndex);
    }

    @Override
    public String getFinalsString() {
        return MainApplication.getInstance().getString(R.string.finals);
    }

    @Override
    public String getRematchString() {
        return MainApplication.getInstance().getString(R.string.finals_rematch);
    }

    @Override
    public String getLoserOfString() {
        return MainApplication.getInstance().getString(R.string.loser_of);
    }

    @Override
    public String getWinnerOfString() {
        return MainApplication.getInstance().getString(R.string.winner_of);
    }

    @Override
    public String getWinnersBracketString() {
        return MainApplication.getInstance().getString(R.string.winners_bracket);
    }

    @Override
    public String getLosersBracketString() {
        return MainApplication.getInstance().getString(R.string.losers_bracket);
    }
}
