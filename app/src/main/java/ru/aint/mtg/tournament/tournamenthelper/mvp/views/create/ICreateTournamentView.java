package ru.aint.mtg.tournament.tournamenthelper.mvp.views.create;

import java.util.HashMap;

import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;

/**
 * Created by Rogachev on 2/24/2017.
 */

public interface ICreateTournamentView {

    String getName();

    void setName(String name);

    TournamentFormat getFormat();

    boolean isFormatSelected();

    void setFormat(TournamentFormat format);

    void setFormats(HashMap<TournamentFormat,String> formats);

    int getWinsPerMatch();

    void setWinsPerMatch(int value);

    int getPointsPerMatchWin();

    void setPointsPerMatchWin(int value);

    int getPointsPerMatchDraw();

    void setPointsPerMatchDraw(int value);

    int getPointsPerGameWin();

    void setPointsPerGameWin(int value);

    int getPointsPerGameDraw();

    void setPointsPerGameDraw(int value);
}
