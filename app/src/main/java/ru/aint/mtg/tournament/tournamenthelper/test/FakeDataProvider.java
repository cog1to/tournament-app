package ru.aint.mtg.tournament.tournamenthelper.test;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.SingleEliminationPairingGenerator;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Map;
import ru.aint.mtg.tournament.tournamenthelper.data.entities.TournamentInfo;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BaseDataProvider;

import static fj.P.p;

/**
 * Fake data provider to test out UI elements.
 */
public class FakeDataProvider extends BaseDataProvider {

    /**
     * Default number of tournaments.
     */
    public final int DEFAULT_NUMBER_OF_TOURNAMENTS = 5;

    /**
     * Default number of tournaments.
     */
    public final int DEFAULT_MAX_NUMBER_OF_PLAYERS = 20;

    /**
     * Random generator.
     */
    private Random random;

    /**
     * List of first names.
     */
    private List<String> firstNames = List.list(
            "Josie",
            "Little",
            "Meghan",
            "Avila",
            "Erickson",
            "Johnnie",
            "Barron",
            "Elena",
            "Angelique",
            "Sellers",
            "Socorro",
            "Rosario",
            "Judith",
            "Glass",
            "Larsen",
            "Leona",
            "Vicky",
            "Scott",
            "Miranda",
            "Rebecca",
            "Morse",
            "Cummings",
            "Cash",
            "Jimmie",
            "Meyers",
            "Ophelia",
            "Bradford",
            "Brittney",
            "Glenna",
            "Rhoda",
            "Swanson",
            "Winnie",
            "Vinson",
            "Raymond",
            "Holder",
            "Wolfe",
            "Dominique",
            "Raquel",
            "Juanita",
            "Gay",
            "Daisy",
            "Sally",
            "Dean",
            "Gale",
            "Andrews",
            "Kane",
            "Ward",
            "Loraine",
            "Espinoza",
            "Bradshaw");

    /**
     * List of surnames.
     */
    List<String> surnames = List.list("Glenn",
            "Long",
            "Burks",
            "Workman",
            "Key",
            "Dickson",
            "Slater",
            "Bean",
            "Clements",
            "Clayton",
            "Robertson",
            "Norman",
            "Yates",
            "Jensen",
            "Lewis",
            "Lopez",
            "Foreman",
            "Albert",
            "English",
            "Gonzalez",
            "Maddox",
            "Donaldson",
            "Roach",
            "Mcintosh",
            "Harrington",
            "Payne",
            "Holland",
            "Haley",
            "Perez",
            "Kramer",
            "Carey",
            "Goodwin",
            "Estrada",
            "Witt",
            "Ward",
            "Nielsen",
            "Cummings",
            "Avery",
            "Ellison",
            "Ochoa",
            "Baxter",
            "Lyons",
            "Mack",
            "Marquez",
            "Stout",
            "Sutton",
            "Dotson",
            "Trevino",
            "Preston",
            "Henderson");

    /**
     * List of tournaments.
     */
    private List<Tournament> tournaments;

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public FakeDataProvider(Context context) {
        super(context);
        random = new Random(new Date().getTime());
        populateData(DEFAULT_NUMBER_OF_TOURNAMENTS);
    }

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public FakeDataProvider(Context context, int numberOfTournaments) {
        super(context);
        random = new Random(new Date().getTime());
        populateData(numberOfTournaments);
    }

    @Override
    public List<TournamentInfo> getTournaments() {
        return this.tournaments.map(t -> new TournamentInfo(t.getId(), t.getName(), t.getTimestamp(), t.currentState().some().isFinished(), t.currentState().some().isArchived(), t.currentState().some().getPlayerStates().length()));

    }

    @Override
    public Tournament getTournament(String id) {
        return this.tournaments.find(t -> t.getId().equals(id)).some();
    }

    @Override
    public void saveTournament(Tournament tournament) {
        if (tournaments.exists(t -> t.getId().equals(tournament.getId()))) {
            tournaments = tournaments.map(t -> {
                if (t.getId().equals(tournament.getId())) {
                    return tournament;
                } else {
                    return t;
                }
            });

            TournamentInfo info = new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), tournament.currentState().some().isFinished(), tournament.currentState().some().isArchived(), tournament.currentState().some().getPlayerStates().length());
        } else {
            tournaments = tournaments.cons(tournament);
        }
    }

    @Override
    public void deleteTournament(Tournament tournament) {
        this.tournaments = this.tournaments.filter(t -> !t.getId().equals(tournament.getId()));
        TournamentInfo info = new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), tournament.currentState().some().isFinished(), tournament.currentState().some().isArchived(), tournament.currentState().some().getPlayerStates().length());
    }

    @Override
    public void deleteTournaments(List<TournamentInfo> tournaments) {
        throw new UnsupportedOperationException("Deleting not implemented yet for fake data provider");
    }

    @Override
    public void archiveTournaments(List<TournamentInfo> tournaments) {
        throw new UnsupportedOperationException("Archiving not implemented yet for fake data provider");
    }

    @Override
    public void unarchiveTournaments(List<TournamentInfo> tournaments) {
        throw new UnsupportedOperationException("Unarchiving not implemented yet for fake data provider");
    }

    @Override
    public void deleteAllData() {
        throw new UnsupportedOperationException("Deleting not implemented yet for fake data provider");
    }

    private void populateData(int numberOfTournaments) {
        this.tournaments = List.range(0, numberOfTournaments).map(i -> randomTournament());
    }

    private Tournament randomTournament() {
        Date date = randomDate();

        // Create tournament.
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), "Tournament " + format.format(date), date.getTime());

        // Register players.
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, randomPlayerList(random.nextInt(DEFAULT_MAX_NUMBER_OF_PLAYERS - 1) + 2));
        tournament.setEvents(tournament.getEvents().cons(registration));

        // Generate all rounds.
        while (!tournament.currentState().some().isFinished()) {
            addRandomRound(tournament);
        }

        // Return.
        return tournament;
    }

    private List<Player> randomPlayerList(int numberOfPlayers) {
        List<Player> players = List.list();
        while (players.length() < numberOfPlayers) {
            Player player = randomPlayer();
            if (!players.exists(p -> p.equals(player))) {
                players = players.cons(player);
            }
        }

        return players;
    }

    private Player randomPlayer() {
        String firstName = firstNames.index(random.nextInt(firstNames.length()));
        String lastName = surnames.index(random.nextInt(surnames.length()));

        return new Player(firstName, lastName);
    }

    private void addRandomRound(Tournament tournament) {
        TournamentState lastState = tournament.currentState().some();
        if (lastState.getFormat() == TournamentFormat.SingleElimination) {
            SingleEliminationPairingGenerator pairingGenerator = new SingleEliminationPairingGenerator();

            try {
                List<Match> matches = pairingGenerator.getPairings(tournament);
                matches.foreach(m -> {
                    if (m.isBye()) {
                        return Unit.unit();
                    }

                    Player winner = m.getScores().keys().index(random.nextInt(m.getScores().size()));
                    Map<Player, Option<Integer>> scores = m.getScores().map(p1 -> {
                        if (p1._1().equals(winner)) {
                            return p(p1._1(), Option.some(lastState.getConfig().getWinsPerMatch()));
                        } else {
                            return p(p1._1(), Option.some(random.nextInt(lastState.getConfig().getWinsPerMatch())));
                        }
                    });
                    m.setScores(scores);
                    return Unit.unit();
                });

                Round round = new Round(matches);
                tournament.setEvents(tournament.getEvents().cons(round));
                return;
            } catch (Exception ex) {
                Log.d("FakeDataProvider", "Caught an expection, dumping tournament history...");
                List<Event> events = tournament.getEvents();
                for (Event ev: events) {
                    Log.d("FakeDataProvider", ev.toString());
                }

                throw ex;
            }
        }

        throw new IllegalStateException("Current format is not supported yet");
    }

    private Date randomDate() {
        int year = random.nextInt(100) + 1900;
        int dayOfYear = random.nextInt(365);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
        return calendar.getTime();
    }
}
