package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events;

import org.atteo.evo.inflector.English;

import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.events.Bracket;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.PairingGenerator;
import ru.aint.mtg.tournament.model.pairings.PairingsGeneratorFactory;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsActionItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsEventPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IEventsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.ITournamentUpdatedCallback;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.DetailsSubPresenter;
import ru.aint.mtg.tournament.tournamenthelper.utils.StringUtils;

import static ru.aint.mtg.tournament.model.events.Bracket.BRACKET_TYPE_WINNERS;

/**
 * Events presenter.
 */
public class EventsPresenter extends DetailsSubPresenter implements IEventsPresenter, ITournamentUpdatedCallback {

    private IEventsView mView;

    private List<IEventsItemPresenter> mPresenters;

    public EventsPresenter(Tournament tournament) {
        super(tournament);
    }

    @Override
    public DetailsType getType() {
        return DetailsType.Events;
    }

    @Override
    public void setView(IDetailsSubView view) {
        mView = (IEventsView)view;
        updatePresenters();
        mView.showEvents(mPresenters);
    }

    private void updatePresenters() {
        List<Event> events = mTournament.getEvents().filter(ev -> !(ev instanceof Registration) && !(ev instanceof Archive));
        TournamentFormat format = mTournament.currentState().some().getFormat();
        mPresenters = List.list();

        if (format == TournamentFormat.DoubleElimination && mTournament.getVersion().equals("2.0")) {
            for (int idx = 0; idx < events.length(); idx++) {
                Event ev = events.index(idx);
                if (ev instanceof Bracket) {
                    Bracket bracket = (Bracket)ev;

                    bracket.getRounds().reverse().foreach(round -> {
                        mPresenters = mPresenters.cons(new EventsEventPresenter(round));
                        return Unit.unit();
                    });

                    mPresenters = mPresenters.cons(new EventsHeaderPresenter(bracket.getType() == BRACKET_TYPE_WINNERS ? MainApplication.getInstance().getString(R.string.winners_bracket) : MainApplication.getInstance().getString(R.string.losers_bracket)));
                } else {
                    mPresenters = mPresenters.cons(new EventsEventPresenter(ev));
                    mPresenters = mPresenters.cons(new EventsHeaderPresenter(ev.getName()));
                }
            }
        } else {
            for (int idx = 0; idx < events.length(); idx++) {
                Event ev = events.index(idx);
                if (mTournament.getVersion().equals("2.0") && (format == TournamentFormat.SingleElimination || format == TournamentFormat.DoubleElimination)) {
                    mPresenters = mPresenters.cons(new EventsEventPresenter(ev));
                } else {
                    String name = StringUtils.nameForEvent(mTournament, ev);
                    mPresenters = mPresenters.cons(new EventsEventPresenter(ev, name));
                }
            }

            mPresenters = mPresenters.cons(new EventsHeaderPresenter(events.length() + " " + English.plural(MainApplication.getInstance().getString(R.string.event), events.length())));
        }

        if (!mTournament.isStarted()) {
            mPresenters = mPresenters.snoc(new IEventsActionItemPresenter() {
                @Override
                public ActionType getType() {
                    return ActionType.Start;
                }

                @Override
                public String getName() {
                    return MainApplication.getInstance().getString(R.string.start_tournament);
                }
            });
        } else if (mTournament.currentState().isSome()) {
            TournamentState state = mTournament.currentState().some();
            if (state.getFormat() == TournamentFormat.Swiss || state.getFormat() == TournamentFormat.RoundRobin || state.getFormat() == TournamentFormat.DoubleRoundRobin) {
                Option<Round> lastRound = mTournament.lastEventOfType(Round.class);
                if (!state.isFinished() && lastRound.isSome() && lastRound.some().isFinished()) {
                    // Add next round button.
                    mPresenters = mPresenters.snoc(new IEventsActionItemPresenter() {
                        @Override
                        public ActionType getType() {
                            return ActionType.NextRound;
                        }

                        @Override
                        public String getName() {
                            return MainApplication.getInstance().getString(R.string.next_round);
                        }
                    });

                    // Add drop button.
                    if (!(mTournament.getEvents().head() instanceof Drop)) {
                        mPresenters = mPresenters.snoc(new IEventsActionItemPresenter() {
                            @Override
                            public ActionType getType() {
                                return ActionType.Drop;
                            }

                            @Override
                            public String getName() {
                                return MainApplication.getInstance().getString(R.string.drop_players);
                            }
                        });
                    }
                }
            } else if (!mTournament.getVersion().equals("2.0")) {
                Option<Round> lastRound = mTournament.lastEventOfType(Round.class);
                if (!state.isFinished() && lastRound.isSome() && lastRound.some().isFinished()) {
                    // Add next round button.
                    mPresenters = mPresenters.snoc(new IEventsActionItemPresenter() {
                        @Override
                        public ActionType getType() {
                            return ActionType.NextRound;
                        }

                        @Override
                        public String getName() {
                            return MainApplication.getInstance().getString(R.string.next_round);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onItemTapped(int position) {
        IEventsItemPresenter presenter = mPresenters.index(position);
        if (presenter instanceof IEventsEventPresenter) {
            Event event = ((EventsEventPresenter) mPresenters.index(position)).getEvent();

            if (event instanceof Round) {
                MainApplication.getInstance().getRouter().showEventDetails(mTournament, event);
            } else if (event instanceof Drop) {
                MainApplication.getInstance().getRouter().showDropEvent(mTournament, (Drop)event);
            }
        } else if (presenter instanceof IEventsActionItemPresenter) {
            IEventsActionItemPresenter actionPresenter = (IEventsActionItemPresenter)presenter;
            switch (actionPresenter.getType()) {
                case Start:
                    Registration registration = mTournament.lastEventOfType(Registration.class).some();
                    if (registration.getPlayers().length() < 2) {
                        getView().showError(MainApplication.getInstance().getString(R.string.not_enough_players_error));
                    } else {
                        mView.showConfirmationDialog(MainApplication.getInstance().getString(R.string.start_tournament_confirm_title), MainApplication.getInstance().getString(R.string.start_tournament_confirm_message), confirmed -> {
                            if (confirmed) {
                                startTournament();
                            }
                        });
                    }
                    break;
                case NextRound:
                    List<Round> rounds = getNextRounds();
                    List<Event> updatedEventsList = rounds.foldLeft((list, round) -> list.cons(round), mTournament.getEvents());
                    mTournament.setEvents(updatedEventsList);
                    saveTournament();

                    updatePresenters();
                    mView.showEvents(mPresenters);
                    break;
                case Drop:
                    MainApplication.getInstance().getRouter().showDropEvent(mTournament, null);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Creates a new round.
     *
     * @return New round in a tournament.
     */
    private List<Round> getNextRounds() {
        PairingGenerator generator = PairingsGeneratorFactory.getPairingGenerator(mTournament.currentState().some().getFormat());
        return generator.getRounds(mTournament);
    }

    @Override
    protected void onTournamentStarted() {
        updatePresenters();
        mView.showEvents(mPresenters);
    }

    @Override
    protected IDetailsSubView getView() {
        return mView;
    }

    @Override
    public void onTournamentUpdated(Tournament tournament) {
        mTournament = tournament;
        updatePresenters();
        mView.showEvents(mPresenters);
    }
}
