package ru.aint.mtg.tournament.tournamenthelper;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;

import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.data.files.InternalStorageFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.log.Logger;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BaseDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.JsonPlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.dialogs.RateUsDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.feedback.FeedbackManager;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.INavigationActivity;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.IRouter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.AboutView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.BaseActionBarFragment;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.CreateTournamentFragmentView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.DropOutsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.HistoryView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.LicensesView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.LogView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.PlayerDetailsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.RoundView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.TournamentDetailsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.fragments.VersionsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.HistoryPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about.AboutPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about.LicensesPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about.VersionsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.create.CreateTournamentPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.TournamentDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events.DropOutsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events.RoundPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.standings.PlayerDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.log.LogPresenter;
import ru.aint.mtg.tournament.tournamenthelper.utils.StringUtils;

/**
 * Main activity.
 */
public class MainActivity extends AppCompatActivity implements IRouter, INavigationActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /**
     * Drawer layout.
     */
    private DrawerLayout drawerLayout;

    /**
     * Action bar toggle for opening/closing drawer.
     */
    private ActionBarDrawerToggle drawerToggle;

    /**
     * Data provider.
     */
    private BaseDataProvider dataProvider;

    /**
     * Suggestions provider.
     */
    private BasePlayerSuggestionsProvider suggestionsProvider;

    /**
     * Previously selected item.
     */
    private MenuItem previousMenuItem;

    /**
     * Navigation view.
     */
    private NavigationView navigationView;

    /**
     * Link to currently displayed toolbar.
     */
    private Toolbar currentToolbar;

    /**
     * Toolbar holder.
     */
    private View toolbarHolder;

    /**
     * Flag indicating whether we should show back button.
     */
    private boolean mShowBackButton = false;

    /**
     * Current fragment.
     */
    BaseActionBarFragment mCurrentActionBarFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log("activity.onCreate(), savedInstanceState = %s", ((savedInstanceState == null) ? "null" : "not null"));

        setContentView(R.layout.activity_main);
        if (!isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        MainApplication.getInstance().reset();
        MainApplication.getInstance().setRouter(this);

        initData();
        initView();

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("savedFragment")) {
                mCurrentActionBarFragment = (BaseActionBarFragment) getSupportFragmentManager().getFragment(savedInstanceState, "savedFragment");
            } else {
                mCurrentActionBarFragment = getActiveFragment();
            }
        } else {
            HistoryView historyView = (HistoryView)Fragment.instantiate(this, HistoryView.class.getName());
            historyView.setNavigationItem(R.id.drawer_menu_item_history);
            historyView.setPresenter(new HistoryPresenter(ITournamentListPresenter.HistoryMode.Current));
            mCurrentActionBarFragment = historyView;
            getSupportFragmentManager().beginTransaction().add(R.id.content_view, mCurrentActionBarFragment, "history").commit();
        }
    }

    @Override
    public void setActiveNavigationItem(@IdRes int item) {
        navigationView.setCheckedItem(item);
    }

    /**
     * Retrieves active fragment.
     * l
     *
     * @return Active fragment.
     */
    private BaseActionBarFragment getActiveFragment() {
        BaseActionBarFragment fragment;
        int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
        if (index < 0) {
            fragment = (BaseActionBarFragment) getSupportFragmentManager().findFragmentByTag("history");
        } else {
            FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(index);
            fragment = (BaseActionBarFragment) getSupportFragmentManager().findFragmentByTag(entry.getName());
        }
        return fragment;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.log("activity.onResume()");
        checkRating();
    }

    @Override
    protected void onPause() {
        Logger.log("activity.onPause()");
        // getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.log("activity.onStart()");
    }

    @Override
    protected void onStop() {
        Logger.log("activity.onStop()");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Logger.log("activity.onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.log("activity.onDestroy()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Logger.log("activity.onSaveInstanceState()");

        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "savedFragment", mCurrentActionBarFragment);
    }

    /**
     * Initializes data providers.
     */
    private void initData() {
        this.dataProvider = MainApplication.getInstance().getDataProvider();
        this.suggestionsProvider = new JsonPlayerSuggestionsProvider(this, new InternalStorageFileManager(this));
    }

    /**
     * Initializes main view.
     */
    @SuppressLint("NewApi")
    private void initView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this::onDrawerItemSelected);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorStatusBar));
        }
    }

    /**
     * Checks if user rated the app.
     */
    private void checkRating() {
        FeedbackManager.incrementLaunchCount(this);
        if (FeedbackManager.showShowRateUsDialog(this)) {
            // Check connection.
            ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            // Check if device can handle market intent.
            PackageManager packageManager = getPackageManager();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=ru.aint.mtg.tournament.tournamenthelper"));

            if ((intent.resolveActivity(packageManager) != null) && isConnected) {
                showRateUsDialog();
                FeedbackManager.incrementLaunchCount(this);
            }
        }
    }

    /**
     * Shows 'Rate us' dialog.
     */
    private void showRateUsDialog() {
        RateUsDialogFragment fragment = new RateUsDialogFragment();
        fragment.setListener((fragment1, action) -> {
            if (action == RateUsDialogFragment.ACTION_OK) {
                FeedbackManager.setRatedFlag(this, true);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=ru.aint.mtg.tournament.tournamenthelper"));
                startActivity(intent);
            } else if (action == RateUsDialogFragment.ACTION_ALREADY) {
                FeedbackManager.setRatedFlag(this, true);
            }
        });
        fragment.show(getFragmentManager(), "RateUsDialog");
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Logger.log("activity.onPostCreate(), savedInstanceState = %s", ((savedInstanceState == null) ? "null" : "not null"));

        if (drawerToggle != null) {
            drawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (drawerToggle != null) {
            drawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        boolean result = mCurrentActionBarFragment != null && mCurrentActionBarFragment.onBackPressed();
        if (!result) {
            result = getSupportFragmentManager().popBackStackImmediate();
            if (result) {
                mCurrentActionBarFragment = getActiveFragment();
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_new_tournament) {
            newTournament();
            return true;
        }
        if (id == R.id.drawer_menu_item_new_tournament) {
            newTournament();
            return true;
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Callback for drawer item selection event.
     *
     * @param menuItem Menu item that was selected.
     *
     * @return true if action was handled, false otherwise.
     */
    public boolean onDrawerItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        boolean handled = false;
        if (id == R.id.drawer_menu_reset) {
            try {
                this.dataProvider.deleteAllData();
                this.suggestionsProvider.deleteAllData();
                Logger.deleteLogFile();
            } catch (IOException e) {
                showIOError(getResources().getString(R.string.delete_error), e);
            }
            handled = true;
        } else if (id == R.id.drawer_menu_log) {
            LogView logView = (LogView)Fragment.instantiate(this, LogView.class.getName());
            logView.setNavigationItem(R.id.drawer_menu_log);
            logView.setPresenter(new LogPresenter());
            mCurrentActionBarFragment = logView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "logview").addToBackStack("logview").commit();
            handled = true;
        } else if (id == R.id.drawer_menu_item_new_tournament) {
            newTournament();
            handled = true;
        } else if (id == R.id.drawer_menu_item_archive) {
            HistoryView historyView = (HistoryView)Fragment.instantiate(this, HistoryView.class.getName());
            historyView.setNavigationItem(R.id.drawer_menu_item_archive);
            historyView.setPresenter(new HistoryPresenter(ITournamentListPresenter.HistoryMode.Archive));
            mCurrentActionBarFragment = historyView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "archive").addToBackStack("archive").commit();
            handled = true;
        } else if (id == R.id.drawer_menu_item_history) {
            HistoryView historyView = (HistoryView)Fragment.instantiate(this, HistoryView.class.getName());
            historyView.setNavigationItem(R.id.drawer_menu_item_history);
            historyView.setPresenter(new HistoryPresenter(ITournamentListPresenter.HistoryMode.Current));
            mCurrentActionBarFragment = historyView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "history").commit();
            handled = true;
        } else if (id == R.id.drawer_menu_about) {
            AboutView aboutView = (AboutView)Fragment.instantiate(this, AboutView.class.getName());
            aboutView.setNavigationItem(R.id.drawer_menu_about);
            aboutView.setPresenter(new AboutPresenter());
            mCurrentActionBarFragment = aboutView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "about").commit();
            handled = true;
        } else if (id == R.id.drawer_menu_donate) {
            // TODO: implement donations.
        }

        if (handled) {
            if (menuItem.isCheckable()) {
                if (previousMenuItem != null) {
                    previousMenuItem.setChecked(false);
                }
                menuItem.setChecked(true);
                previousMenuItem = menuItem;
            }

            drawerLayout.closeDrawers();
        }

        return handled;
    }

    // editing

    @Override
    public void setDrawerState(boolean editing) {
        try {
            mShowBackButton = editing;

            if (currentToolbar.getNavigationIcon() == null) {
                return;
            }

            ObjectAnimator animator = ObjectAnimator.ofFloat(currentToolbar.getNavigationIcon(), "progress", editing ? 1 : 0);
            if (animator == null) {
                return;
            }

            animator.start();
        } catch (Exception ex) {
            Logger.log("Failed to animate drawer icon");
            ex.printStackTrace();
        }
    }

    @Override
    public void showTournamentView(String id) {
        try {
            TournamentDetailsView detailsView = (TournamentDetailsView) Fragment.instantiate(this, TournamentDetailsView.class.getName());
            detailsView.setPresenter(new TournamentDetailsPresenter(id));
            mCurrentActionBarFragment = detailsView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "details").addToBackStack("details").commit();
        } catch (IOException e) {
            showIOError(getResources().getString(R.string.load_error), e);
        }
    }

    @Override
    public void showEventDetails(Tournament tournament, Event event) {
        TournamentState currentState = tournament.currentState().some();
        boolean editable = !currentState.isArchived();

        if (event instanceof Round) {
            if (currentState.getFormat() == TournamentFormat.Swiss || currentState.getFormat() == TournamentFormat.RoundRobin || currentState.getFormat() == TournamentFormat.DoubleRoundRobin) {
                editable = editable && ((tournament.lastEventOfType(Round.class).isNone()) || (tournament.lastEventOfType(Round.class).some() == event));
            } else if (currentState.getFormat() == TournamentFormat.SingleElimination) {
                if (tournament.getVersion().equals("2.0")) {
                    editable = editable && ((Round)event).hasReadyMatches() && !event.isFinished();
                } else {
                    editable = editable && ((tournament.lastEventOfType(Round.class).isNone()) || (tournament.lastEventOfType(Round.class).some() == event)) && !event.isFinished();
                }
            } else if (currentState.getFormat() == TournamentFormat.DoubleElimination) {
                if (tournament.getVersion().equals("2.0")) {
                    editable = editable && ((Round)event).hasReadyMatches()  && !event.isFinished();
                } else {
                    editable = editable && ((tournament.lastEventOfType(Round.class).isNone()) || (tournament.lastEventOfType(Round.class).some() == event))  && !event.isFinished();
                }
            }

            RoundView detailsView = (RoundView) Fragment.instantiate(this, RoundView.class.getName());
            detailsView.setPresenter(new RoundPresenter(tournament, (Round)event, StringUtils.nameForEvent(tournament, event), editable));
            mCurrentActionBarFragment = detailsView;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "rounddetails").addToBackStack("rounddetails").commit();
        }
    }

    @Override
    public void showDropEvent(@NonNull Tournament tournament, @Nullable Drop event) {
        DropOutsView detailsView = (DropOutsView)Fragment.instantiate(this, DropOutsView.class.getName());
        detailsView.setPresenter(new DropOutsPresenter(tournament, event));
        mCurrentActionBarFragment = detailsView;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "dropdetails").addToBackStack("dropdetails").commit();
    }

    @Override
    public void showVersions() {
        VersionsView versionsView = (VersionsView)Fragment.instantiate(this, VersionsView.class.getName());
        versionsView.setPresenter(new VersionsPresenter());
        mCurrentActionBarFragment = versionsView;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "versions").addToBackStack("versions").commit();
    }

    @Override
    public void showPlayerDetails(PlayerState playerState, TournamentConfig config) {
        PlayerDetailsView detailsView = (PlayerDetailsView)Fragment.instantiate(this, PlayerDetailsView.class.getName());
        detailsView.setPresenter(new PlayerDetailsPresenter(playerState, config));
        mCurrentActionBarFragment = detailsView;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "playerstats").addToBackStack("playerstats").commit();
    }

    @Override
    public void showLicenses() {
        LicensesView licensesView = (LicensesView)Fragment.instantiate(this, LicensesView.class.getName());
        licensesView.setPresenter(new LicensesPresenter());
        mCurrentActionBarFragment = licensesView;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "licenses").addToBackStack("licenses").commit();
    }

    @Override
    public void showError(String message, Exception ex) {
        this.showIOError(message, ex);
    }

    @Override
    public void showCreateTournament() {
        newTournament();
    }

    @Override
    public void pop() {
        getSupportFragmentManager().popBackStackImmediate();
    }

    /**
     * Initiates tournament creation.
     */
    protected void newTournament() {
        CreateTournamentFragmentView detailsView = (CreateTournamentFragmentView) Fragment.instantiate(this, CreateTournamentFragmentView.class.getName());
        detailsView.setPresenter(new CreateTournamentPresenter());
        mCurrentActionBarFragment = detailsView;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, mCurrentActionBarFragment, "createtournament").addToBackStack("createtournament").commit();
    }

    /**
     * Shows IO exception that occurred.
     *
     * @param ex underlying exception.
     */
    protected void showIOError(String message, Exception ex) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.error));
        alertDialog.setMessage(String.format("%s: %s", message, ex.getMessage()));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.OK),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    /**
     * Shows error alert.
     *
     * @param message Message.
     */
    protected void showGenericError(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.error));
        alertDialog.setMessage(String.format("%s", message));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.OK),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    /**
     * Shows error alert.
     *
     * @param message Message.
     */
    protected void showMessage(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(String.format("%s", message));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.OK),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    protected boolean isTablet() {
        return getResources().getBoolean(R.bool.isTablet);
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void setActiveToolbar(Toolbar toolbar) {
        currentToolbar = toolbar;
        setSupportActionBar(toolbar);

        if (drawerToggle != null) {
            drawerLayout.removeDrawerListener(drawerToggle);
        }

        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        toolbar.setNavigationOnClickListener(view -> {
            if (mShowBackButton) {
                onBackPressed();
            } else {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    @Override public void showBackButton(boolean show) {
        mShowBackButton = show;
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (show) {
                @SuppressLint("PrivateResource")
                final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material);
                upArrow.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_OVER);
            }

            actionBar.setDisplayHomeAsUpEnabled(show);
            drawerLayout.setDrawerLockMode(show ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
            if (!show) {
                drawerToggle.setDrawerIndicatorEnabled(true);
                drawerToggle.syncState();
            }
        }
    }

    // <editor-fold desc="in-app billing">

    // </editor-fold>
}
