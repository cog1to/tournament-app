package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.ILicensesPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.ILicenseItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.ILicensesView;

/**
 * Licenses presenter implementation.
 */
public class LicensesPresenter implements ILicensesPresenter {

    @Override
    public void onCreateView(@NonNull ILicensesView view) {
        String[] licenseTitles = MainApplication.getInstance().getResources().getStringArray(R.array.licensed_components);
        String[] licenseDisclaimers = MainApplication.getInstance().getResources().getStringArray(R.array.licensed_components_disclaimers);
        String[] licenseLinks = MainApplication.getInstance().getResources().getStringArray(R.array.licensed_components_urls);

        ArrayList<LicenseItemPresenter> presenters = new ArrayList<>();
        for (int idx = 0; idx < licenseTitles.length; idx++) {
            presenters.add(new LicenseItemPresenter(licenseTitles[idx], licenseDisclaimers[idx], licenseLinks[idx]));
        }

        view.setItems(presenters.toArray(new ILicenseItemPresenter[0]));
    }

    private static class LicenseItemPresenter implements ILicenseItemPresenter {

        private String mTitle;

        private String mDisclaimer;

        private String mLink;

        public LicenseItemPresenter(String title, String disclaimer, String link) {
            mTitle = title;
            mDisclaimer = disclaimer;
            mLink = link;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }

        @Override
        public String getDisclaimer() {
            return mDisclaimer;
        }

        @Override
        public String getLink() {
            return mLink;
        }
    }
}
