package ru.aint.mtg.tournament.tournamenthelper.data.providers;

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;

import fj.data.List;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.tournamenthelper.data.entities.TournamentInfo;

/**
 * Base for data provider.
 */
public abstract class BaseDataProvider {

    /**
     * Application context.
     */
    protected Context context;

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public BaseDataProvider(Context context) {
        this.context = context;
    }

    /**
     * Returns a list of tournaments that data store has.
     *
     * @return List of tournaments that data store has.
     */
    public abstract List<TournamentInfo> getTournaments() throws IOException;

    /**
     * Returns a tournament details  for given tournament ID.
     *
     * @param id ID of the tournament.
     *
     * @return Tournament details for given tournament ID.
     */
    public abstract Tournament getTournament(String id) throws IOException;

    /**
     * Saves given tournament to the store.
     *
     * @param tournament A tournament to save.
     */
    public abstract void saveTournament(Tournament tournament) throws IOException;

    /**
     * deletes given tournament from the store.
     *
     * @param tournament A tournament to delete.
     */
    public abstract void deleteTournament(Tournament tournament) throws IOException;

    /**
     * Deletes given list of tournaments.
     *
     * @param tournaments Tournaments to delete.
     */
    public abstract void deleteTournaments(List<TournamentInfo> tournaments) throws IOException;

    /**
     * Archives given list of tournaments.
     *
     * @param tournaments Tournaments to archive.
     */
    public abstract void archiveTournaments(List<TournamentInfo> tournaments) throws IOException;

    /**
     * Unarchives given list of tournaments.
     *
     * @param tournaments Tournaments to unarchive.
     */
    public abstract void unarchiveTournaments(List<TournamentInfo> tournaments) throws IOException;

    /**
     * Deletes all saved data.
     */
    public abstract void deleteAllData() throws IOException;
}