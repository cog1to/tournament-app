package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log;

/**
 * Log item presenter.
 */
public interface ILogItemPresenter {

    /**
     * Return entry's text.
     *
     * @return Entry's text.
     */
    String getText();
}
