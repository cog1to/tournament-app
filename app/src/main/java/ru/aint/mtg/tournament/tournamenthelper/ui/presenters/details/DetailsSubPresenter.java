package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details;

import java.io.IOException;

import fj.data.List;
import ru.aint.mtg.tournament.model.brackets.DoubleEliminationBracketGenerator;
import ru.aint.mtg.tournament.model.brackets.IBracketGenerator;
import ru.aint.mtg.tournament.model.brackets.SingleEliminationBracketGenerator;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.pairings.PairingGenerator;
import ru.aint.mtg.tournament.model.pairings.PairingsGeneratorFactory;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.ui.details.DetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.utils.StringsProvider;

/**
 * Details subview presenter.
 */
public abstract class DetailsSubPresenter {

    protected Tournament mTournament;

    protected DetailsSubPresenter(Tournament tournament) {
        mTournament = tournament;
    }

    protected void startTournament() {
        Registration registration = mTournament.lastEventOfType(Registration.class).some();
        if (registration.getPlayers().length() < 2) {
            getView().showError(MainApplication.getInstance().getString(R.string.not_enough_players_error));
        } else {
            TournamentFormat format = mTournament.currentState().some().getFormat();
            switch (format) {
                case RoundRobin:
                case DoubleRoundRobin:
                case Swiss:
                    List<Round> rounds = getNextRounds();
                    List<Event> updatedEventsList = rounds.foldLeft((list, round) -> list.cons(round), mTournament.getEvents());
                    mTournament.setEvents(updatedEventsList);
                    saveTournament();
                    break;
                default:
                    if (mTournament.getVersion().equals("2.0")) {
                        IBracketGenerator generator = null;
                        if (format == TournamentFormat.SingleElimination) {
                            generator = new SingleEliminationBracketGenerator();
                        } else if (format == TournamentFormat.DoubleElimination) {
                            generator = new DoubleEliminationBracketGenerator();
                        }

                        List<Event> bracket = generator.getBracket(registration.getPlayers(), new StringsProvider());
                        List<Event> updatedBracketEventsList = bracket.foldLeft((list, round) -> list.cons(round), mTournament.getEvents());
                        mTournament.setEvents(updatedBracketEventsList);
                        saveTournament();
                    } else {
                        List<Round> elimRounds = getNextRounds();
                        List<Event> updatedElimEventsList = elimRounds.foldLeft((list, round) -> list.cons(round), mTournament.getEvents());
                        mTournament.setEvents(updatedElimEventsList);
                        saveTournament();
                    }
                    break;
            }

            onTournamentStarted();
        }
    }

    /**
     * Creates a new round.
     *
     * @return New round in a tournament.
     */
    private List<Round> getNextRounds() {
        PairingGenerator generator = PairingsGeneratorFactory.getPairingGenerator(mTournament.currentState().some().getFormat());
        return generator.getRounds(mTournament);
    }

    /**
     * Saves tournament to the store.
     */
    protected void saveTournament() {
        try {
            MainApplication.getInstance().getDataProvider().saveTournament(mTournament);
        } catch (IOException ex) {
            MainApplication.getInstance().getRouter().showError(MainApplication.getInstance().getString(R.string.save_error), ex);
        }
    }

    /**
     * Callback method called when tournament has been started.
     */
    protected abstract void onTournamentStarted();

    /**
     * Returns corresponding view.
     *
     * @return View associated with presenter.
     */
    protected abstract IDetailsSubView getView();
}
