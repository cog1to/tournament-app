package ru.aint.mtg.tournament.tournamenthelper.data.entities;

import ru.aint.mtg.tournament.model.tournaments.Tournament;

/**
 * Tournament info entity.
 */
public class TournamentInfo {

    /**
     * Uniquie ID.
     */
    final String id;

    /**
     * Tournament name;
     */
    final String name;

    /**
     * Timestamp.
     */
    final long timestamp;

    /**
     * Indicates whether tournament is finished.
     */
    final boolean isFinished;

    /**
     * Indicates whether tournament is archived.
     */
    final boolean isArchived;

    /**
     * Initial number of participants.
     */
    final int numberOfPlayers;

    /**
     * Returns new instance of TournamentInfo wih given data.
     *
     * @param id ID.
     * @param name Name.
     * @param timestamp Timestamp.
     * @param isFinished Whether tournament is finished.
     * @param isArchived Whether tournament is archived.
     * @param numberOfPlayers Number of players.
     */
    public TournamentInfo(String id, String name, long timestamp, boolean isFinished, boolean isArchived, int numberOfPlayers) {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.isArchived = isArchived;
        this.isFinished = isFinished;
        this.numberOfPlayers = numberOfPlayers;
    }

    /**
     * Returns tournament's ID.
     *
     * @return Tournament's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns tournament's name.
     *
     * @return Tournament's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns tournament's timestamp.
     *
     * @return Tournament's timestamp.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns a value indicating whether tournament is finished.
     *
     * @return A value indicating whether tournament is finished.
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * Returns a value indicating whether tournament is archived.
     *
     * @return A value indicating whether tournament is archived.
     */
    public boolean isArchived() {
        return isArchived;
    }

    /**
     * Returns number of registered players.
     *
     * @return Number of registered players.
     */
    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }
}
