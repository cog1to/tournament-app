package ru.aint.mtg.tournament.tournamenthelper.mvp.views.log;

import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log.ILogItemPresenter;

/**
 * Log view.
 */
public interface ILogView {
    void setItems(ILogItemPresenter[] presenters);
}
