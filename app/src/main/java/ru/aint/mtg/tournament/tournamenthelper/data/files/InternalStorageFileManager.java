package ru.aint.mtg.tournament.tournamenthelper.data.files;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by aint on 6/10/2015.
 */
public class InternalStorageFileManager implements IFileManager {

    /**
     * Application context.
     */
    private Context context;

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public InternalStorageFileManager(Context context) {
        this.context = context;
    }

    @Override
    public File getDataDir() {
        return context.getFilesDir();
    }

    @Override
    public boolean fileExistsInDataDir(String path) {
        File file = new File(getDataDir(), path);
        return file.exists();
    }

    @Override
    public InputStream getInputStreamFromDataDirPath(String path) throws IOException {
        File file = new File(getDataDir(), path);
        if (file.exists()) {
            return new FileInputStream(file);
        }

        return null;
    }

    @Override
    public OutputStream getOutputStreamFromDataDirPath(String path, boolean overwrite) throws IOException {
        File file = new File(getDataDir(), path);

        if (overwrite) {
            if (file.exists()) {
                file.delete();
            }
        }

        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }

        file.createNewFile();

        if (file.exists()) {
            FileOutputStream outputStream = new FileOutputStream(file.getPath(), !overwrite);
            return outputStream;
        }

        return null;
    }

    @Override
    public void deleteFileFromDataDirPath(String path) {
        File file = new File(getDataDir(), path);

        if (file.exists()) {
            file.delete();
        }
    }
}
