package ru.aint.mtg.tournament.tournamenthelper.data.log;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import ru.aint.mtg.tournament.tournamenthelper.data.files.IFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.files.InternalStorageFileManager;

/**
 * Logger.
 */
public class Logger {

    /**
     * Log tag.
     */
    private static final String sLogTag = "COMPANION";

    /**
     * Log file name.
     */
    private static final String sLogFileName = "log.txt";

    /**
     * Logger instance.
     */
    private static Logger instance;

    /**
     * File manager instance.
     */
    private IFileManager fileManager;

    /**
     * Returns instance of a logger.
     *
     * @param fileManager File manager.
     */
    public Logger(IFileManager fileManager) {
        this.fileManager = fileManager;
    }

    /**
     * Initializes logger.
     *
     * @param context Application context.
     */
    public static void init(Context context) {
        instance = new Logger(new InternalStorageFileManager(context));
    }

    /**
     * Logs new entry.
     *
     * @param entry String to log.
     */
    public static void log(String entry) {
        if (instance != null) {
            instance.addEntry(entry);
        }
        Log.d(sLogTag, entry);
    }

    /**
     * Logs new entry.
     *
     * @param entry String to log.
     */
    public static void log(String entry, String... args) {
        if (instance != null) {
            instance.addEntry(String.format(entry, (Object[])args));
        }
        Log.d(sLogTag, String.format(entry, (Object[])args));
    }

    /**
     * Deletes log file.
     */
    public static void deleteLogFile() {
        if (instance != null) {
            if (instance.fileManager.fileExistsInDataDir(sLogFileName)){
                instance.fileManager.deleteFileFromDataDirPath(sLogFileName);
            }
        }
    }

    /**
     * Returns all log entries.
     *
     * @return List of log entries.
     */
    public static List<String> getLogEntries() {
        if (instance == null) {
            return new ArrayList<>();
        }

        try {
            InputStream inputStream = instance.fileManager.getInputStreamFromDataDirPath(sLogFileName);

            StringBuilder content = new StringBuilder();

            byte[] buffer = new byte[1024];
            int n = inputStream.read(buffer);
            while (n != -1) {
                content.append(new String(buffer, 0, n));
                n = inputStream.read(buffer);
            }
            inputStream.close();

            String[] entries = content.toString().split("\n");

            return Arrays.asList(entries);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * Adds new entry to the log file.
     *
     * @param entry Entry.
     */
    private void addEntry(String entry) {
        try {
            String dateString = DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date()).toString();
            String logEntry = String.format("[%s] - %s\n", dateString, entry);

            OutputStream outputStream = this.fileManager.getOutputStreamFromDataDirPath(sLogFileName, false);
            outputStream.write(logEntry.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
