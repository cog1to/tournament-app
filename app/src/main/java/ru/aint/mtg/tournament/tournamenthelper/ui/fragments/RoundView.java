package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.dialogs.MatchEditingDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchEditingPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IRoundPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IRoundView;

/**
 * Round view.
 */
public class RoundView extends BaseActionBarFragment implements IRoundView {

    private IRoundPresenter mPresenter;

    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.matches_recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(new MatchesAdapter(getActivity(), new IMatchPresenter[0]));
        mPresenter.onCreateView(this);
        return view;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Override
    public void showMatches(IMatchPresenter[] presenters) {
        ((MatchesAdapter)mRecyclerView.getAdapter()).setItems(presenters);
    }

    @Override
    public void reloadMatch(int position, IMatchPresenter presenter) {
        ((MatchesAdapter)mRecyclerView.getAdapter()).reloadItem(position, presenter);
    }

    @Override
    public void showMatchEditing(IMatchEditingPresenter presenter) {
        MatchEditingDialogFragment dialogFragment = (MatchEditingDialogFragment) Fragment.instantiate(getActivity(), MatchEditingDialogFragment.class.getName());
        dialogFragment.setPresenter(presenter);
        dialogFragment.show(getActivity().getSupportFragmentManager(), "EditMatchDialogFragment");
    }

    @Override
    protected String getTitle() {
        return mPresenter.getTitle();
    }

    @Override
    protected int getViewLayout() {
        return R.layout.details_round_view;
    }

    public void setPresenter(IRoundPresenter presenter) {
        mPresenter = presenter;
    }

    private class MatchesAdapter extends RecyclerView.Adapter<MatchViewHolder> {

        Activity mContext;

        IMatchPresenter[] mPresenters;

        MatchesAdapter(Activity context, @NonNull IMatchPresenter[] presenters) {
            mContext = context;
            mPresenters = presenters;
        }

        @Override
        public MatchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MatchViewHolder(mContext.getLayoutInflater().inflate(R.layout.list_match_item, parent, false));
        }

        @Override
        public void onBindViewHolder(MatchViewHolder holder, int position) {
            IMatchPresenter presenter = mPresenters[position];
            holder.setPresenter(presenter);

            if (presenter.isEditable()) {
                holder.itemView.setOnClickListener(view -> mPresenter.onItemTapped(position));
            } else {
                holder.itemView.setOnClickListener(null);
            }
        }

        @Override
        public int getItemCount() {
            return mPresenters.length;
        }

        void reloadItem(int index, IMatchPresenter presenter) {
            mPresenters[index] = presenter;
            notifyItemChanged(index);
        }

        void setItems(IMatchPresenter[] presenters) {
            mPresenters = presenters;
            notifyDataSetChanged();
        }
    }

    private class MatchViewHolder extends RecyclerView.ViewHolder {

        LayoutInflater mInflater = getActivity().getLayoutInflater();

        LinearLayout mContainer;
        TextView mTitleView;
        View mSeparator;

        IMatchPresenter mPresenter;

        MatchViewHolder(View itemView) {
            super(itemView);
            mContainer = (LinearLayout) itemView.findViewById(R.id.match_item_container);
            mTitleView = (TextView) itemView.findViewById(R.id.match_name);
            mSeparator = itemView.findViewById(R.id.match_name_separator);
        }

        void setPresenter(IMatchPresenter presenter) {
            mPresenter = presenter;

            if (presenter.showMatchNames()) {
                mTitleView.setVisibility(View.VISIBLE);
                mSeparator.setVisibility(View.VISIBLE);
                mTitleView.setText(mPresenter.getMatchName());
            } else {
                mTitleView.setVisibility(View.GONE);
                mSeparator.setVisibility(View.GONE);
            }

            // Add scores.
            List<IMatchPresenter.MatchEntryModel> models = mPresenter.getEntries();
            List<RelativeLayout> scoreLayouts = models.map(model -> {
                RelativeLayout scoreView = (RelativeLayout)mInflater.inflate(R.layout.score_view, mContainer, false);

                TextView playerName = (TextView) scoreView.findViewById(R.id.player_name);
                playerName.setTextColor(ContextCompat.getColor(getActivity(), model.getColor()));
                playerName.setText(model.getName());

                TextView score = (TextView) scoreView.findViewById(R.id.player_score);
                score.setTextColor(ContextCompat.getColor(getActivity(),model.getColor()));
                score.setText(model.getScore());

                return scoreView;
            });

            mContainer.removeAllViewsInLayout();
            scoreLayouts.foreach(layout -> {
                mContainer.addView(layout); return Unit.unit();
            });
        }
    }
}
