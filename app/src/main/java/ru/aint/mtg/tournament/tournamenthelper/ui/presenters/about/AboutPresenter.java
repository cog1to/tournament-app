package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.IAboutPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutView;

/**
 * About presenter.
 */
public class AboutPresenter implements IAboutPresenter {

    private enum AboutItemType {
        Versions,
        Licenses
    }

    @Override
    public void onCreateView(@NonNull IAboutView view) {
        AboutItemPresenter[] presenters = {
                new AboutItemPresenter(AboutItemType.Versions),
                new AboutItemPresenter(AboutItemType.Licenses)
        };
        view.setItems(presenters);
    }

    @Override
    public void onItemSelected(IAboutItemPresenter presenter) {
        if (presenter instanceof AboutItemPresenter) {
            switch (((AboutItemPresenter)presenter).getType()) {
                case Licenses:
                    MainApplication.getInstance().getRouter().showLicenses();
                    break;
                case Versions:
                    MainApplication.getInstance().getRouter().showVersions();
                    break;
            }
        }
    }

    private static class AboutItemPresenter implements IAboutItemPresenter {

        private AboutItemType mType;

        private AboutItemPresenter(AboutItemType type) {
            mType = type;
        }

        private AboutItemType getType() {
            return mType;
        }

        @Override
        public String getName() {
            switch (mType) {
                case Licenses:
                    return MainApplication.getInstance().getString(R.string.licenses);
                case Versions:
                    return MainApplication.getInstance().getString(R.string.version);
            }

            return null;
        }
    }
}
