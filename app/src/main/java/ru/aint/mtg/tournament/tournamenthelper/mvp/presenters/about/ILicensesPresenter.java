package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.ILicensesView;

/**
 * Licenses presenter.
 */
public interface ILicensesPresenter {
    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull ILicensesView view);
}
