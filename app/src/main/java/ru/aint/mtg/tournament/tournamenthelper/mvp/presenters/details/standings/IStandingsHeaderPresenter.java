package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings;

import fj.data.List;

/**
 * Standings header presenter.
 */
public interface IStandingsHeaderPresenter extends IStandingsItemPresenter {
    String getTitle();
    List<String> getDetailTitles();
}
