package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;

import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * Dialog for editing existing player entry on registration screen.
 */
public class EditPlayerEntryDialogFragment extends AppCompatDialogFragment {

    /**
     * Edit player action constant.
     */
    public static int CHANGE_PLAYER_EDIT = 0;

    /**
     * Delete player action constant.
     */
    public static int CHANGE_PLAYER_DELETE = 1;

    /**
     * Listener interface.
     */
    public interface Listener {

        /**
         * Called when editing option is selected.
         *
         * @param option Option that was selected.
         * @param player Associated player object.
         */
        void onOptionSelected(int option, Player player);
    }

    /**
     * Dialog listener.
     */
    private Listener listener;

    /**
     * Existing player being edited.
     */
    private Player existingPlayer;

    /**
     * Indicates whether user can delete entry.
     */
    private boolean canDelete;

    /**
     * Sets dialog listener.
     *
     * @param listener Dialog listener object.
     */
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }

    /**
     * Sets existing player.
     *
     * @param player Existing player.
     */
    public void setExistingPlayer(Player player) {
        this.existingPlayer = player;
    }

    /**
     * Sets flag indicating whether user can delete entry.
     *
     * @param canDelete Value for a flag indicating whether user can delete entry.
     */
    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(existingPlayer.getFullName())
               .setItems(this.canDelete ? R.array.change_player : R.array.change_player_without_delete, (dialogInterface, which) -> {
                   if (listener != null) {
                       listener.onOptionSelected(which, existingPlayer);
                   }
               });

        return builder.create();
    }
}
