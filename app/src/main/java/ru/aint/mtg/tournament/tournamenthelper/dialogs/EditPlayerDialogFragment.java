package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import fj.data.List;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * Edit player info dialog fragment.
 */
public class EditPlayerDialogFragment extends AppCompatDialogFragment {

    /**
     * Dialog event listener interface.
     */
    public interface Listener {

        /**
         * Returns new player data from dialog when player confirms editing.
         *
         * @param player New player data.
         * @param existingPlayer Existing player data.
         */
        void onPlayerEdited(Player player, Player existingPlayer);

        /**
         * Asks if current editing should be allowed.
         *
         * @param player New player data.
         * @param existingPlayer Previous player data.
         *
         * @return <b>true</b> if editing should be allowed, <b>false</b> otherwise.
         */
        boolean shouldAllowPlayerEditing(Player player, Player existingPlayer);
    }

    /**
     * Dialog listener.
     */
    private Listener listener;

    /**
     * First name edit.
     */
    private AppCompatAutoCompleteTextView fullNameText;

    /**
     * Instance of the dialog that is presented.
     */
    private AlertDialog dialog;

    /**
     * Existing player (in case dialog is for editing existing entry).
     */
    private Player existingPlayer;

    /**
     * suggestions.
     */
    public String[] suggestions;

    /**
     * Sets dialog listener.
     *
     * @param listener Dialog listener.
     */
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }

    /**
     * Sets suggestions.
     *
     * @param suggestions Suggestions.
     */
    public void setSuggestions(String[] suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * Sets suggestions.
     *
     * @param suggestions Suggestions.
     */
    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions.toJavaList().toArray(new String[suggestions.length()]);
    }

    /**
     * Sets existing player.
     *
     * @param player Existing player to edit.
     */
    public void setExistingPlayer(Player player) {
        this.existingPlayer = player;

        if (player == null) {
            return;
        }

        if (this.fullNameText != null) {
            this.fullNameText.setText(player.getFullName());
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if ((fullNameText.getText().toString().trim().length() > 0)) {
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                } else {
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }

                if (editable.toString().contains("\n")) {
                    int newLine = editable.toString().indexOf('\n');
                    editable.delete(newLine, newLine + 1);
                }
            }
        };

        View content = inflater.inflate(R.layout.dialog_new_player, null);

        this.fullNameText = (AppCompatAutoCompleteTextView)content.findViewById(R.id.dialog_full_name_text);
        this.fullNameText.addTextChangedListener(textWatcher);

        if (this.suggestions != null && this.suggestions.length > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, this.suggestions);
            fullNameText.setAdapter(adapter);
        }

        builder.setView(content)
                .setPositiveButton(R.string.menu_done, null)
                .setNegativeButton(R.string.menu_cancel, null);

        Bundle arguments = getArguments();
        if (arguments != null && arguments.getString("title") != null) {
            builder.setTitle(arguments.getString("title"));
        }

        dialog = builder.create();

        dialog.setOnShowListener(dialogInterface -> {
            Button positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positive.setOnClickListener(view -> {
                if (listener != null) {
                    Player newPlayer = new Player(fullNameText.getText().toString().trim());
                    if (listener.shouldAllowPlayerEditing(newPlayer, existingPlayer)) {
                        listener.onPlayerEdited(newPlayer, existingPlayer);
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), R.string.player_already_registered, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (existingPlayer != null) {
            this.fullNameText.setText(existingPlayer.getFullName());
        } else {
            dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
    }
}
