package ru.aint.mtg.tournament.tournamenthelper.utils;

import android.content.Context;

import java.util.Collection;
import java.util.Iterator;

import fj.Equal;
import fj.data.List;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.helpers.DoubleEliminationUtils;

/**
 * String utility methods.
 */
public class StringUtils {
    /**
     * Converts integer value to ordinal string.
     *
     * @param i Integer to convert.
     *
     * @return Ordinal representation of the value.
     */
    public static String intToOrdinal(int i) {
        String[] sufixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];

        }
    }

    /**
     * Converts tournament format valaue to a string.
     *
     * @param format Value to convert.
     *
     * @return User-friendly name of the format.
     */
    public static String tournamentFormatToString(Context context, TournamentFormat format) {
        switch (format) {
            case SingleElimination:
                return context.getResources().getString(R.string.single_elimination);
            case Swiss:
                return context.getResources().getString(R.string.swiss);
            case RoundRobin:
                return context.getResources().getString(R.string.round_robin);
            case DoubleRoundRobin:
                return context.getResources().getString(R.string.double_round_robin);
            case DoubleElimination:
                return context.getResources().getString(R.string.double_elimination);
            default:
                return null;
        }
    }

    /**
     * Returns name for event.
     *
     * @param tournament Parent tournament.
     * @param event Event.
     *
     * @return Name for event.
     */
    public static String nameForEvent(Tournament tournament, Event event) {
        if (event.getName() != null && event.getName().length() > 0) {
            return event.getName();
        }

        if (event instanceof Round) {
            TournamentState state = tournament.stateBeforeEvent(event).some();
            if (state.getFormat() == TournamentFormat.DoubleElimination) {
                return DoubleEliminationUtils.getRoundName(MainApplication.getInstance(), (Round)event, tournament);
            } else {
                List<Event> rounds = tournament.getEvents().filter(e -> e instanceof Round).reverse();
                int roundIndex = rounds.elementIndex(Equal.anyEqual(), event).orSome(-1);
                String name = MainApplication.getInstance().getResources().getString(R.string.roundCap) + " " + (roundIndex + 1);
                return name;
            }
        } else if (event instanceof Drop) {
            return MainApplication.getInstance().getResources().getString(R.string.drop_outs);
        } else if (event instanceof Registration) {
            return MainApplication.getInstance().getResources().getString(R.string.registration);
        }

        return null;
    }

    /**
     * Joins given collection into one string.
     *
     * @param collection Collection of strings to join.
     * @param delimiter Delimiter string.
     *
     * @return A single string created by joining components of collection with given delimiter.
     */
    public static String join(Collection<String> collection, String delimiter) {
        StringBuilder builder = new StringBuilder();

        Iterator<String> iterator = collection.iterator();
        for (int idx = 0; idx < collection.size(); idx++) {
            String elem = iterator.next();
            builder.append(elem);
            if (iterator.hasNext()) {
                builder.append(delimiter);
            }
        }

        return builder.toString();
    }
}
