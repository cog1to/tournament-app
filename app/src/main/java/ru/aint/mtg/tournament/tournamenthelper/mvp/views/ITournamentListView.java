package ru.aint.mtg.tournament.tournamenthelper.mvp.views;

import android.support.annotation.StringRes;

import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListItemPresenter;

/**
 * Tournament list view.
 */
public interface ITournamentListView {

    /**
     * Shows loading spinner.
     */
    void showLoadingSpinner();

    /**
     * Reloads list with given items.
     */
    void showList(ITournamentListItemPresenter[] items);

    /**
     * Reloads a specific item.
     *
     * @param index Index of item to reload.
     */
    void reloadItemAt(int index);

    /**
     * Toggles edit mode.
     *
     * @param active Edit mode state.
     */
    void toggleEditMode(boolean active);
}
