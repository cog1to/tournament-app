package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events;

import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;

/**
 * Events presenter interface.
 */
public interface IEventsPresenter extends IDetailsSubPresenter {
    /**
     * Called when item is tapped.
     *
     * @param position Item position.
     */
    void onItemTapped(int position);
}
