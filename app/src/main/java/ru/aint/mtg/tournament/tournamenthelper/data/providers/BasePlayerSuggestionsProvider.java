package ru.aint.mtg.tournament.tournamenthelper.data.providers;

import android.content.Context;

import java.io.IOException;

import fj.data.List;

/**
 * Player name suggestions provider.
 */
public abstract class BasePlayerSuggestionsProvider {
    /**
     * Application context.
     */
    protected Context context;

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public BasePlayerSuggestionsProvider(Context context) {
        this.context = context;
    }

    /**
     * Returns list of saved player suggestions.
     *
     * @return List of available player suggestions.
     */
    public abstract List<String> getPlayerSuggestions() throws IOException;

    /**
     * Saves new list of player suggestions.
     *
     * @param suggestions List of player suggestions.
     */
    public abstract void setPlayerSuggestions(List<String> suggestions) throws IOException;

    /**
     * Adds new suggestion to the list of known suggestions.
     *
     * @param suggestion New player name.
     *
     * @return Updated list of suggestions.
     */
    public abstract List<String> addSuggestion(String suggestion) throws IOException;

    /**
     * Replaces one suggestion entry with another.
     *
     * @param old Old suggestion.
     * @param newSuggestion New suggestion.
     *
     * @return Updated list of suggestions.
     */
    public abstract List<String> replaceSuggestion(String old, String newSuggestion) throws IOException;

    /**
     * Deletes all saved data.
     */
    public abstract void deleteAllData() throws IOException;
}
