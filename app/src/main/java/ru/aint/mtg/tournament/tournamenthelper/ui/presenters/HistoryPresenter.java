package ru.aint.mtg.tournament.tournamenthelper.ui.presenters;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.atteo.evo.inflector.English;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fj.Unit;
import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.entities.TournamentInfo;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BaseDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentListView;

/**
 * History list presenter.
 */
public class HistoryPresenter implements ITournamentListPresenter {

    private ITournamentListView mView;

    private HistoryMode mMode;

    private BaseDataProvider mProvider;

    private fj.data.List<HistoryItemPresenter> mItems;

    private boolean mEditMode;

    public HistoryPresenter(HistoryMode mode) {
        this.mMode = mode;
        this.mProvider = MainApplication.getInstance().getDataProvider();
    }

    @Override
    public MenuItemType[] onMenuInflated() {
        if (mMode == HistoryMode.Current) {
            if (mEditMode) {
                return new MenuItemType[]{MenuItemType.Delete};
            } else {
                return new MenuItemType[] { MenuItemType.Create };
            }
        } else {
            if (mEditMode) {
                return new MenuItemType[] { MenuItemType.Delete };
            } else {
                return new MenuItemType[] {};
            }
        }
    }

    @NonNull
    @Override
    public ITournamentListItemPresenter getItemAt(int index) {
        return mItems.index(index);
    }

    @Override
    public void onItemTapped(int itemIndex) {
        if (!mEditMode) {
            MainApplication.getInstance().getRouter().showTournamentView(mItems.index(itemIndex).getItem().getId());
        } else {
            mItems.index(itemIndex).setSelected(!mItems.index(itemIndex).getIsSelected());
            mView.reloadItemAt(itemIndex);

            if (!mItems.exists(HistoryItemPresenter::getIsSelected)) {
                mEditMode = false;
                mView.toggleEditMode(false);
            }
        }
    }

    @Override
    public boolean onItemLongTapped(int itemIndex) {
        if (!mEditMode) {
            // Toggle edit mode.
            mEditMode = true;
            mView.toggleEditMode(true);

            // Select item.
            mItems.index(itemIndex).setSelected(true);
            mView.reloadItemAt(itemIndex);
            return true;
        }

        return false;
    }

    @Override
    public void onDelete() {
        try {
            mProvider.deleteTournaments(mItems.filter(HistoryItemPresenter::getIsSelected).map(p -> p.mInfo));
            mEditMode = false;
            mView.toggleEditMode(false);
            loadItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onArchive() {
        try {
            mProvider.archiveTournaments(mItems.filter(HistoryItemPresenter::getIsSelected).map(p -> p.mInfo));
            mEditMode = false;
            mView.toggleEditMode(false);
            loadItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUnarchive() {
        try {
            mProvider.unarchiveTournaments(mItems.filter(HistoryItemPresenter::getIsSelected).map(p -> p.mInfo));
            mEditMode = false;
            mView.toggleEditMode(false);
            loadItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onBack() {
        if (mEditMode) {
            mEditMode = false;
            mView.toggleEditMode(false);

            for (int index = 0; index < mItems.length(); index++) {
                if (mItems.index(index).getIsSelected()) {
                    mItems.index(index).setSelected(false);
                    mView.reloadItemAt(index);
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public void onCreateTournament() {
        MainApplication.getInstance().getRouter().showCreateTournament();
    }

    @Override
    public void onCreateView(@NonNull ITournamentListView view) {
        mView = view;
        mView.showLoadingSpinner();
        loadItems();
    }

    @Override
    public int getTitle() {
        if (mMode == HistoryMode.Current) {
            return R.string.menu_history;
        } else {
            return R.string.menu_archive;
        }
    }

    @Override
    public void onDestroyView() {}

    private void loadItems() {
        new AsyncTask<Void, Void, fj.data.List<TournamentInfo>>() {

            @Override
            protected fj.data.List<TournamentInfo> doInBackground(Void... voids) {
                try {
                    if (mMode == HistoryMode.Current) {
                        return mProvider.getTournaments().filter(tournamentInfo -> !tournamentInfo.isArchived());
                    } else {
                        return mProvider.getTournaments().filter(TournamentInfo::isArchived);
                    }
                } catch (IOException e) {
                    return fj.data.List.list();
                }
            }

            @Override
            protected void onPostExecute(fj.data.List<TournamentInfo> tournamentInfos) {
                if (tournamentInfos.exists(ti -> ti.isFinished() && !ti.isArchived())) {
                    tournamentInfos.foreach(tournamentInfo -> {
                        try {
                            if (tournamentInfo.isFinished() && !tournamentInfo.isArchived()) {
                                Tournament tournament = mProvider.getTournament(tournamentInfo.getId());
                                tournament.setEvents(tournament.getEvents().cons(new Archive()));
                                mProvider.saveTournament(tournament);
                            }
                        } catch (IOException ex) {
                            MainApplication.getInstance().getRouter().showError("Failed to update tournaments", ex);
                        }

                        loadItems();

                        return Unit.unit();
                    });
                } else {
                    mItems = tournamentInfos.map(HistoryItemPresenter::new);
                    ITournamentListItemPresenter[] array = mItems.toJavaList().toArray(new ITournamentListItemPresenter[]{});
                    mView.showList(array);
                }
            }
        }.execute();
    }

    /**
     * History item presenter.
     */
    private class HistoryItemPresenter implements ITournamentListItemPresenter {

        TournamentInfo mInfo;
        boolean mSelected;

        HistoryItemPresenter(@NonNull TournamentInfo info) {
            mInfo = info;
        }

        private TournamentInfo getItem() {
            return mInfo;
        }

        protected void setSelected(boolean selected) {
            mSelected  = selected;
        }

        @Override
        public IconSymbol getIcon() {
            return IconSymbol.Tournament;
        }

        @Override
        public String getName() {
            return mInfo.getName();
        }

        @Override
        public String getSubtitle() {
            Date date = new Date(mInfo.getTimestamp());
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            String dateString = formatter.format(date);

            @SuppressLint("DefaultLocale") String detailsBuilder = String.format("%d %s", mInfo.getNumberOfPlayers(),
                    English.plural(MainApplication.getInstance().getResources().getString(R.string.player), mInfo.getNumberOfPlayers()));
            return dateString + ", " + detailsBuilder;
        }

        @Override
        public boolean getIsSelected() {
            return mSelected;
        }
    }
}
