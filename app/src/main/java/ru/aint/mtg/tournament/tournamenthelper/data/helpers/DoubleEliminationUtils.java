package ru.aint.mtg.tournament.tournamenthelper.data.helpers;

import android.content.Context;

import fj.Equal;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * Double elimination utils.
 */
public final class DoubleEliminationUtils {

    public static boolean isWinnersRound(Round round, Tournament tournament) {
        TournamentState beforeState = tournament.stateBeforeEvent(round).some();

        return round.getMatches().forall(m -> {
            List<Player> players = m.getScores().keys();
            return players.forall(p -> {
                Option<PlayerState> foundState = beforeState.getPlayerStates().find(ps -> ps.getPlayer().equals(p));
                return foundState.isNone() || (foundState.some().getStatus() == PlayerStatus.Active);
            });
        });
    }

    public static boolean isLastWinnersRound(Round round, Tournament tournament) {
        List<Event> winnerRounds = tournament.getEvents().filter(e -> {
            if (!(e instanceof Round)) {
                return false;
            }

            Round r = (Round) e;
            return isWinnersRound(r, tournament);
        });

        return ((winnerRounds.length() > 0 && winnerRounds.head().equals(round)) || (winnerRounds.length() == 0 && isWinnersRound(round, tournament)));
    }

    public static boolean containsLosers(Round round, Tournament tournament) {
        TournamentState beforeState = tournament.stateBeforeEvent(round).some();

        return round.getMatches().forall(m -> {
            List<Player> players = m.getScores().keys();
            return players.exists(p -> beforeState.getPlayerStates().find(ps -> ps.getPlayer().equals(p)).some().getStatus() == PlayerStatus.Loser);
        });
    }

    public static boolean isLosersRound(Round round, Tournament tournament) {
        TournamentState beforeState = tournament.stateBeforeEvent(round).some();

        return round.getMatches().forall(m -> {
            List<Player> players = m.getScores().keys();
            return players.forall(p -> beforeState.getPlayerStates().find(ps -> ps.getPlayer().equals(p)).some().getStatus() == PlayerStatus.Loser);
        });
    }

    public static boolean isFinalsRound(Round round, Tournament tournament) {
        if (round.getMatches().length() != 1) {
            return false;
        }

        TournamentState beforeState = tournament.stateBeforeEvent(round).some();

        List<Player> players = round.getMatches().index(0).getScores().keys();
        return (players.exists(p -> beforeState.getPlayerStates().find(ps -> ps.getPlayer().equals(p)).some().getStatus() == PlayerStatus.Active) &&
                players.exists(p -> beforeState.getPlayerStates().find(ps -> ps.getPlayer().equals(p)).some().getStatus() == PlayerStatus.Loser));
    }

    public static boolean isLastLosersRound(Round round, Tournament tournament) {
        List<Event> loserRounds = tournament.getEvents().filter(e -> {
            if (!(e instanceof Round)) {
                return false;
            }

            Round r = (Round) e;
            return containsLosers(r, tournament);
        });

        if ((loserRounds.isEmpty() && containsLosers(round, tournament)) || (loserRounds.isNotEmpty() && loserRounds.head().equals(round))) {
            return true;
        }

        return false;
    }

    public static String getRoundName(Context context, Round round, Tournament tournament) {
        TournamentState state = tournament.currentState().some();

        if (isFinalsRound(round, tournament)) {
            return context.getResources().getString(R.string.finals);
        }

        if (isWinnersRound(round, tournament)) {
            List<Event> winnerRounds = tournament.getEvents().filter(e -> e instanceof Round).filter(r -> isWinnersRound((Round)r, tournament));
            Option<Integer> index = winnerRounds.reverse().elementIndex(Equal.anyEqual(), round);
            if (index.isSome()) {
                return String.format(context.getResources().getString(R.string.winners_bracket_template), index.some() + 1);
            } else {
                return "";
            }
        }

        if (isLosersRound(round, tournament)) {
            List<Event> rounds = tournament.getEvents().filter(e -> e instanceof Round);
            if (rounds.head().equals(round) && rounds.length() > 1 && isFinalsRound((Round) rounds.tail().head(), tournament)) {
                return context.getResources().getString(R.string.rematch);
            }

            List<Event> losersRounds = rounds.filter(r -> isLosersRound((Round) r, tournament));
            Option<Integer> index = losersRounds.reverse().elementIndex(Equal.anyEqual(), round);
            if (index.isSome()) {
                return String.format(context.getResources().getString(R.string.losers_bracket_template), index.some() + 1);
            } else {
                return "";
            }
        }

        return null;
    }
}
