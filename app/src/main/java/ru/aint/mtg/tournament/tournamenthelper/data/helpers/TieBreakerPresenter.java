package ru.aint.mtg.tournament.tournamenthelper.data.helpers;

import ru.aint.mtg.tournament.model.tiebreakers.TieBreakerType;

/**
 * Tie-breaker presenter.
 */
public final class TieBreakerPresenter {

    /**
     * Returns header title for given tie-breaker type.
     *
     * @param type Tie-breaker type.
     *
     * @return Title string.
     */
    public static String getTieBreakerTitle(TieBreakerType type) {
        switch (type) {
            case MatchPoints:
                return "MP";
            case GameWinPercentage:
                return "GW%";
            case OpponentsMatchWinPercentage:
                return "OMW%";
            case OpponentsGameWinPercentage:
                return "OGW%";
        }

        throw new IllegalArgumentException("Unknown tie-breaker type");
    }

    /**
     * Returns value format for given tie-breaker type.
     *
     * @param type Tie-breaker type.
     *
     * @return Format string.
     */
    public static String formatValue(TieBreakerType type, double value) {
        switch (type) {
            case MatchPoints:
                return String.format("%.0f", value);
            case GameWinPercentage:
                return String.format("%.2f", value);
            case OpponentsMatchWinPercentage:
                return String.format("%.2f", value);
            case OpponentsGameWinPercentage:
                return String.format("%.2f", value);
        }

        throw new IllegalArgumentException("Unknown tie-breaker type");
    }
}
