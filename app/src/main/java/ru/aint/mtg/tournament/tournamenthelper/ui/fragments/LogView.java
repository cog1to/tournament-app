package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log.ILogItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log.ILogPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.log.ILogView;
import ru.aint.mtg.tournament.tournamenthelper.views.DividerItemDecoration;

/**
 * Log view implementation.
 */
public class LogView extends BaseActionBarFragment implements ILogView {

    private RecyclerView mLogRecyclerView;

    private ILogPresenter mPresenter;

    public void setPresenter(ILogPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setItems(ILogItemPresenter[] presenters) {
        mLogRecyclerView.setAdapter(new LogEntryAdapter(getActivity(), presenters));
    }

    @Override
    protected String getTitle() {
        return MainApplication.getInstance().getString(R.string.log);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_log;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mLogRecyclerView = (RecyclerView) view.findViewById(R.id.log_recycler_view);
        mLogRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mLogRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));

        if (mPresenter != null) {
            mPresenter.onCreateView(this);
        }

        return view;
    }

    private class LogEntryAdapter extends RecyclerView.Adapter<LogEntryViewHolder> {

        Activity mContext;

        ILogItemPresenter[] mPresenters;

        private LogEntryAdapter(@NonNull Activity context, @NonNull ILogItemPresenter[] presenters) {
            mContext = context;
            mPresenters = presenters;
        }

        @Override
        public LogEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new LogEntryViewHolder(mContext.getLayoutInflater().inflate(R.layout.list_log_item, null));
        }

        @Override
        public void onBindViewHolder(LogEntryViewHolder holder, int position) {
            ILogItemPresenter presenter = mPresenters[position];
            holder.setPresenter(presenter);
        }

        @Override
        public int getItemCount() {
            return mPresenters.length;
        }
    }

    private class LogEntryViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        private LogEntryViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.text);
        }

        private void setPresenter(ILogItemPresenter presenter) {
            mTextView.setText(presenter.getText());
        }
    }
}
