package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details;

import android.support.annotation.NonNull;

import java.io.IOException;

import fj.data.List;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentDetailsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events.EventsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.standings.StandingsPresenter;

/**
 * Tournament details presenter implementation.
 */
public class TournamentDetailsPresenter implements ITournamentDetailsPresenter {

    private Tournament mTournament;

    private ITournamentDetailsView mView;

    public TournamentDetailsPresenter(String id) throws IOException {
        mTournament = MainApplication.getInstance().getDataProvider().getTournament(id);
    }

    @Override
    public String getName() {
        if (mTournament != null) {
            return mTournament.getName();
        }

        return null;
    }

    @NonNull
    @Override
    public MenuItemType[] onMenuInflated() {
        return new MenuItemType[0];
    }

    @Override
    public void onCreateView(@NonNull ITournamentDetailsView view) {
        mView = view;
        mView.setTitle(mTournament.getName());
        if (mTournament.currentState().isSome() && mTournament.currentState().some().isArchived()) {
            mView.setSubtitle(String.format("(%s)", MainApplication.getInstance().getString(R.string.archived).toLowerCase()));
        }

        StandingsPresenter standingsPresenter = new StandingsPresenter(mTournament);
        EventsPresenter eventsPresenter = new EventsPresenter(mTournament);
        standingsPresenter.setUpdateCallback(eventsPresenter);
        mView.setSubPresenters(List.list(standingsPresenter, eventsPresenter));
    }

    @Override
    public void onDestroyView() {

    }
}
