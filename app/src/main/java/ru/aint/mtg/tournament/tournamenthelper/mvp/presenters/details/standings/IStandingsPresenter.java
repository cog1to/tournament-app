package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings;

import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IStandingsView;

/**
 * Standings presenter
 */

public interface IStandingsPresenter extends IDetailsSubPresenter {
    /**
     * Called when player was edited.
     *
     * @param presenter Existing presenter that triggered the editing event.
     * @param existingPlayer Existing player mode.
     * @param player New player model.
     */
    void onPlayerEdited(IStandingsPlayerPresenter presenter, IStandingsView.EditPlayerModel existingPlayer, IStandingsView.EditPlayerModel player);

    /**
     * Called when new player was created.
     *
     * @param player New player model.
     */
    void onPlayerCreated(IStandingsView.EditPlayerModel player);

    /**
     * Called when player deletion option was selected.
     *
     * @param presenter Presenter that activated the action.
     */
    void onPlayerDeleting(IStandingsPlayerPresenter presenter);

    /**
     * Called when player editing option was selected.
     *
     * @param presenter Presenter of selected player item.
     */
    void onPlayerEditing(IStandingsPlayerPresenter presenter);

    /**
     * Suggestions provider.
     *
     * @return Player name suggestion provider.
     */
    BasePlayerSuggestionsProvider getSuggestionsProvider();

    /**
     * Checks if player with given name is allowed.
     *
     * @param player Player model to check.
     *
     * @return <b>true</b> if user can add player with given name, <b>false</b> otherwise.
     */
    boolean canCreatePlayer(IStandingsView.EditPlayerModel player);

    /**
     * Called when item is tapped.
     *
     * @param presenter Item presenter.
     */
    void onItemTapped(IStandingsItemPresenter presenter);
}
