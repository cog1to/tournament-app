package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.create;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.create.ICreateTournamentPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.create.ICreateTournamentView;
import ru.aint.mtg.tournament.tournamenthelper.utils.StringUtils;

/**
 * Create tournament presenter implementation.
 */
public class CreateTournamentPresenter implements ICreateTournamentPresenter {

    private ICreateTournamentView mView;

    @Override
    public void onViewCreated(ICreateTournamentView view) {
        mView = view;

        TournamentConfig config = TournamentConfig.defaultConfig();

        HashMap<TournamentFormat,String> values = new HashMap<>();
        for (TournamentFormat f :
                TournamentFormat.values()) {
            values.put(f, StringUtils.tournamentFormatToString(MainApplication.getInstance(), f));
        }

        mView.setFormats(values);
        mView.setWinsPerMatch(config.getWinsPerMatch());
        mView.setPointsPerMatchWin(config.getPointsPerMatchWin());
        mView.setPointsPerMatchDraw(config.getPointPerMatchDraw());
        mView.setPointsPerGameWin(config.getPointsPerGameWin());
        mView.setPointsPerGameDraw(config.getPointsPerGameDraw());
    }

    @Override
    public boolean canCreate() {
        return mView.getName().length() > 0 &&
                mView.getWinsPerMatch() > 0 &&
                mView.getPointsPerMatchWin() > 0 &&
                mView.getPointsPerGameWin() > 0 &&
                mView.isFormatSelected();
    }

    @Override
    public void onCreateConfirmed() {
        String name = mView.getName();
        TournamentFormat format = mView.getFormat();

        TournamentConfig newConfig;
        if (format == TournamentFormat.Swiss || format == TournamentFormat.RoundRobin || format == TournamentFormat.DoubleRoundRobin) {
            newConfig = new TournamentConfig(mView.getWinsPerMatch(), mView.getPointsPerMatchWin(), mView.getPointsPerMatchDraw(), mView.getPointsPerGameWin(), mView.getPointsPerGameDraw(),  TournamentConfig.DEFAULT_SWISS_TIEBREAKERS);
        } else {
            newConfig = new TournamentConfig(mView.getWinsPerMatch(), mView.getPointsPerMatchWin(), mView.getPointsPerMatchDraw(), mView.getPointsPerGameWin(), mView.getPointsPerGameDraw());
        }

        Registration event = new Registration(newConfig, format, List.list());
        Tournament tournament = new Tournament(UUID.randomUUID().toString(), name, (new Date()).getTime(), List.list(event));
        tournament.setVersion("2.0");

        try {
            MainApplication.getInstance().getDataProvider().saveTournament(tournament);
        } catch (IOException ex) {
            MainApplication.getInstance().getRouter().showError(MainApplication.getInstance().getString(R.string.save_error), ex);
        }

        MainApplication.getInstance().getRouter().pop();
    }
}
