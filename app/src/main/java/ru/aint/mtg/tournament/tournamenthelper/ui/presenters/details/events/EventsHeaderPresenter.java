package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events;

import org.atteo.evo.inflector.English;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsHeaderPresenter;

/**
 * Events header presenter implementation.
 */
public class EventsHeaderPresenter implements IEventsHeaderPresenter {

    private String mTitle;

    public EventsHeaderPresenter(String title) {
        mTitle = title;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}
