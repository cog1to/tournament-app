package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentDetailsView;

/**
 * Tournament presenter interface.
 */
public interface ITournamentDetailsPresenter {

    enum MenuItemType {
        Delete,
        Archive,
        Unarchive
    }

    /**
     * Returns tournament name.
     *
     * @return Tournament name.
     */
    String getName();

    /**
     * Returns available menu items.
     *
     * @return Array of available menu items.
     */
    @NonNull
    MenuItemType[] onMenuInflated();

    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull ITournamentDetailsView view);

    /**
     * Called when view is destroyed.
     */
    void onDestroyView();
}
