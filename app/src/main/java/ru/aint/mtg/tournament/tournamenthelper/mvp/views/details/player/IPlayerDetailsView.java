package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.player;

/**
 * Player details view.
 */
public interface IPlayerDetailsView {
    void setMatchStatistics(int total, int won, int lost, int drew);
    void setGameStatistics(int total, int won, int lost, int drew);
    void setMatchPoints(int point, int maxPoints);
    void setMatchWinPercentage(double percentage);
    void setGamePoints(int point, int maxPoints);
    void setGameWinPercentage(double percentage);
    void setTitle(String title);
}
