package ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common;

import android.app.VoiceInteractor;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import fj.data.Option;

/**
 * Option JSON adapter.
 */
public class OptionAdapter implements JsonSerializer<Option>, JsonDeserializer<Option<?>> {
    @Override
    public Option<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Type[] typeParameters = ((ParameterizedType)typeOfT).getActualTypeArguments();
        Type elementsType = typeParameters[0];

        if (json.isJsonNull()) {
            return Option.none();
        } else {
            Object deserialized = GsonFactory.gson().fromJson(json, elementsType);
            return Option.some(deserialized);
        }
    }

    @Override
    public JsonElement serialize(Option src, Type typeOfSrc, JsonSerializationContext context) {
        if (src.isNone()) {
            return JsonNull.INSTANCE;
        }

        Type[] typeParameters = ((ParameterizedType)typeOfSrc).getActualTypeArguments();
        Type elementsType = typeParameters[0];
        Gson gson = GsonFactory.gson();
        TypeAdapter adapter = gson.getAdapter((Class)elementsType);
        return adapter.toJsonTree(src.some());
    }
}
