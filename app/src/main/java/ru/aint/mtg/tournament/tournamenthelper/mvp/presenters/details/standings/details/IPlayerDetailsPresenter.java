package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.details;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.player.IPlayerDetailsView;

/**
 * Player details presenter.
 */
public interface IPlayerDetailsPresenter {
    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull IPlayerDetailsView view);
}
