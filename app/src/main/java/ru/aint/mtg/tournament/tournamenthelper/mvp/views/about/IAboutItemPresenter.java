package ru.aint.mtg.tournament.tournamenthelper.mvp.views.about;

/**
 * About list item presenter.
 */
public interface IAboutItemPresenter {
    String getName();
}
