package ru.aint.mtg.tournament.tournamenthelper.ui.details;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.dialogs.YesNoDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsActionItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsEventPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsHeaderPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IEventsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.IConfirmCallback;

/**
 * Events view.
 */
public class EventsView extends DetailsSubView implements IEventsView {

    private View mView;

    private RecyclerView mRecyclerView;

    private IEventsPresenter mPresenter;

    public EventsView(FragmentActivity context, View view, IDetailsSubPresenter presenter) {
        super(context);
        mView = view;
        mPresenter = (IEventsPresenter)presenter;
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.events_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void showEvents(List<IEventsItemPresenter> items) {
        mRecyclerView.setAdapter(new EventsAdapter(mContext, items));
    }

    @Override
    public void reloadItem(int index, IEventsItemPresenter item) {
        ((EventsAdapter)mRecyclerView.getAdapter()).reloadItem(item, index);
    }

    @Override
    public void showError(String string) {
        Snackbar.make(mView, string, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showConfirmationDialog(String title, String message, @NonNull IConfirmCallback callbackDelegate) {
        YesNoDialogFragment dialogFragment = new YesNoDialogFragment();
        dialogFragment.setMessage(message);
        dialogFragment.setTitle(title);
        dialogFragment.setListener((fragment, action) -> {
            if (action == YesNoDialogFragment.ACTION_YES) {
                callbackDelegate.onActionSelected(true);
            } else {
                callbackDelegate.onActionSelected(false);
            }
        });
        dialogFragment.show(((FragmentActivity)mContext).getSupportFragmentManager(), "confirmdialog");
    }

    private class EventsAdapter extends RecyclerView.Adapter<EventViewHolder> {

        private IEventsItemPresenter[] mPresenters;
        private Activity mContext;

        private EventsAdapter(Activity context, @NonNull List<IEventsItemPresenter> presenters) {
            mPresenters = presenters.toJavaList().toArray(new IEventsItemPresenter[]{});
            mContext = context;
        }

        private void reloadItem(IEventsItemPresenter item, int index) {
            mPresenters[index] = item;
            notifyItemChanged(index);
        }

        @Override
        public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_section_header, parent, false);
                return new EventHeaderViewHolder(mContext, view);
            } else if (viewType == 1) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_event_item, parent, false);
                return new EventEventViewHolder(mContext, view);
            } else if (viewType == 2) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_event_action_item, parent, false);
                return new EventActionViewHolder(mContext, view);
            }

            throw new IllegalArgumentException("Unknown view type");
        }

        @Override
        public void onBindViewHolder(EventViewHolder holder, int position) {
            IEventsItemPresenter presenter = mPresenters[position];
            holder.setPresenter(position, presenter);
            if (holder instanceof EventHeaderViewHolder) {
                holder.mView.setOnClickListener(null);
            } else {
                holder.mView.setOnClickListener(view -> mPresenter.onItemTapped(position));
            }
        }

        @Override
        public int getItemViewType(int position) {
            IEventsItemPresenter presenter = mPresenters[position];

            if (presenter instanceof IEventsHeaderPresenter) {
                return 0;
            } else if (presenter instanceof IEventsEventPresenter) {
                return 1;
            } else if (presenter instanceof IEventsActionItemPresenter) {
                return 2;
            }

            throw new IllegalArgumentException("Unknown presenter type");
        }

        @Override
        public int getItemCount() {
            return mPresenters.length;
        }
    }

    private abstract class EventViewHolder extends RecyclerView.ViewHolder {

        View mView;
        Activity mContext;

        private EventViewHolder(Activity context, View itemView) {
            super(itemView);
            mView = itemView;
            mContext = context;
        }

        abstract void setPresenter(int position, IEventsItemPresenter presenter);
    }

    private class EventActionViewHolder extends EventViewHolder {

        IEventsActionItemPresenter mPresenter;
        TextView mTitleView;

        public EventActionViewHolder(Activity context, View itemView) {
            super(context, itemView);
            mTitleView = (TextView)itemView.findViewById(R.id.title_text_view);
        }

        public void setPresenter(int position, IEventsItemPresenter presenter) {
            mPresenter = (IEventsActionItemPresenter) presenter;
            mTitleView.setText(mPresenter.getName());
        }
    }

    private class EventHeaderViewHolder extends EventViewHolder {

        TextView mTitleView;
        IEventsHeaderPresenter mPresenter;

        public EventHeaderViewHolder(Activity context, View itemView) {
            super(context, itemView);
            mTitleView = (TextView) itemView.findViewById(R.id.header_title);
        }

        public void setPresenter(int position, IEventsItemPresenter presenter) {
            mPresenter = (IEventsHeaderPresenter)presenter;
            mTitleView.setText(mPresenter.getTitle());
        }
    }

    private class EventEventViewHolder extends EventViewHolder {

        IEventsEventPresenter mPresenter;
        ImageView mImageView;
        TextView mTitleView;
        TextView mSubtitleView;

        public EventEventViewHolder(Activity context, View itemView) {
            super(context, itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.event_icon);
            mTitleView = (TextView) itemView.findViewById(R.id.event_item_name_text_view);
            mSubtitleView = (TextView) itemView.findViewById(R.id.event_item_details_text_view);
        }

        public void setPresenter(int position, IEventsItemPresenter presenter) {
            mPresenter = (IEventsEventPresenter)presenter;
            mImageView.setImageResource(mPresenter.getIconResource());
            mTitleView.setText(mPresenter.getTitle());
            mSubtitleView.setText(mPresenter.getSubtitle());
        }
    }
}
