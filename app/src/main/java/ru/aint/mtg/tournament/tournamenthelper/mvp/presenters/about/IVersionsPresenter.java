package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IVersionsView;

/**
 * Version history presenter.
 */
public interface IVersionsPresenter {
    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull IVersionsView view);
}
