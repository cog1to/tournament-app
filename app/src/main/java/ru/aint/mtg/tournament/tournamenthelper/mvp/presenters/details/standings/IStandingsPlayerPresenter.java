package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings;

import fj.data.List;

/**
 * Standings item presenter.
 */
public interface IStandingsPlayerPresenter extends IStandingsItemPresenter {
    String getName();
    List<String> getDetailsValues();
    String getStatus();
}
