package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;

import ru.aint.mtg.tournament.tournamenthelper.R;

/**
 * Generic dialog fragment for 'yes'/'no' type questions.
 */
public class YesNoDialogFragment extends AppCompatDialogFragment {

    /**
     * Confirm action constant.
     */
    public static final int ACTION_YES = 1;

    /**
     * Deny action constant.
     */
    public static final int ACTION_NO = 0;

    /**
     * Dialog event listener interface.
     */
    public interface Listener {

        /**
         * Called when user selects an action.
         *
         * @param action Selected action.
         */
        void onActionSelected(YesNoDialogFragment fragment, int action);
    }

    /**
     * Dialog listener.
     */
    private Listener listener;

    /**
     * Presented dialog instance.
     */
    private AlertDialog dialog;

    /**
     * Sets message to show.
     */
    private String message;

    /**
     * Sets title to show.
     */
    private String title;

    /**
     * Sets a message to show.
     *
     * @param message Message to show.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets title to show.
     *
     * @param title New title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets dialog listener.
     *
     * @param listener Dialog listener.
     */
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(this.message)
                .setTitle(this.title)
                .setPositiveButton(R.string.yes, (dialog1, id) -> {
                    if (listener != null) {
                        listener.onActionSelected(this, ACTION_YES);
                    }
                    dismiss();
                })
                .setNegativeButton(R.string.no, (dialog1, id) -> {
                    if (listener != null) {
                        listener.onActionSelected(this, ACTION_NO);
                    }
                    dismiss();
                })
                .setCancelable(false);

        this.dialog = builder.create();
        return this.dialog;
    }
}
