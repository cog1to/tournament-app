package ru.aint.mtg.tournament.tournamenthelper.data.providers.json;

import android.content.Context;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.model.utils.Utils;
import ru.aint.mtg.tournament.tournamenthelper.data.entities.TournamentInfo;
import ru.aint.mtg.tournament.tournamenthelper.data.files.IFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BaseDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common.GsonFactory;

/**
 * JSON data provider. Stores data in JSON-formatted files.
 */
public class JsonDataProvider extends BaseDataProvider {

    /**
     * Tournaments list file name.
     */
    private final String sTournamentsFileName = "tournaments.json";

    /**
     * Tournaments directory.
     */
    private final String sTournamentsDirectory = "tournaments";

    /**
     * File manager instance.
     */
    private IFileManager fileManager;

    /**
     * Main constructor.
     *
     * @param context Application context.
     */
    public JsonDataProvider(Context context, IFileManager fileManager) {
        super(context);
        this.fileManager = fileManager;
    }

    @Override
    public List<TournamentInfo> getTournaments() throws IOException {
        return loadIndexFile();
    }

    @Override
    public Tournament getTournament(String id) throws IOException {
        return loadTournamentFile(id);
    }

    @Override
    public void saveTournament(Tournament tournament) throws IOException {
        // Update index file.
        TournamentState state = tournament.currentState().some();
        TournamentInfo newInfo = new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), state.isFinished(), state.isArchived(), state.getPlayerStates().length());

        List<TournamentInfo> oldList = loadIndexFile();
        List<TournamentInfo> newList = Utils.addOrReplace(oldList, (t -> t.getId().equals(newInfo.getId())), newInfo);
        saveIndexFile(newList);

        // Save the tournament itself.
        saveTournamentFile(tournament);
    }

    @Override
    public void deleteTournament(Tournament tournament) throws IOException {
        // Update index file.
        List<TournamentInfo> oldList = loadIndexFile();
        TournamentInfo oldInfo = oldList.find(info -> info.getId().equals(tournament.getId())).some();

        List<TournamentInfo> newList = oldList.filter(t -> !t.getId().equals(tournament.getId()));
        saveIndexFile(newList);

        // Delete details file
        deleteTournamentFile(tournament);
    }

    @Override
    public void deleteTournaments(List<TournamentInfo> tournaments) throws IOException {
        // Delete details files
        for (TournamentInfo info : tournaments) {
            Tournament tournament = loadTournamentFile(info.getId());
            deleteTournamentFile(tournament);
        }

        // Update index file.
        List<TournamentInfo> oldList = loadIndexFile();
        List<TournamentInfo> newList = oldList.filter(t -> !tournaments.exists(info -> info.getId().equals(t.getId())));
        saveIndexFile(newList);
    }

    @Override
    public void archiveTournaments(List<TournamentInfo> tournaments) throws IOException {
        List<TournamentInfo> oldList = loadIndexFile();

        // Archive details files
        for (TournamentInfo info : tournaments) {
            Tournament tournament = loadTournamentFile(info.getId());
            tournament.setEvents(tournament.getEvents().cons(new Archive()));
            saveTournamentFile(tournament);

            // Update index entity.
            TournamentState state = tournament.currentState().some();
            TournamentInfo newInfo = new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), state.isFinished(), state.isArchived(), state.getPlayerStates().length());

            List<TournamentInfo> newList = oldList.map(oldInfo -> {
                if (oldInfo.getId().equals(newInfo.getId())) {
                    return newInfo;
                }

                return oldInfo;
            });

            oldList = newList;
        }

        // Update index file.
        saveIndexFile(oldList);
    }

    @Override
    public void unarchiveTournaments(List<TournamentInfo> tournaments) throws IOException {
        List<TournamentInfo> oldList = loadIndexFile();

        // Unarchive details files
        for (TournamentInfo info : tournaments) {
            Tournament tournament = loadTournamentFile(info.getId());
            tournament.setEvents(tournament.getEvents().filter(e -> !(e instanceof Archive)));
            saveTournamentFile(tournament);

            // Update index entity.
            TournamentState state = tournament.currentState().some();
            TournamentInfo newInfo = new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), state.isFinished(), state.isArchived(), state.getPlayerStates().length());

            List<TournamentInfo> newList = oldList.map(oldInfo -> {
                if (oldInfo.getId().equals(newInfo.getId())) {
                    return newInfo;
                }

                return oldInfo;
            });

            oldList = newList;
        }

        // Update index file.
        saveIndexFile(oldList);
    }

    @Override
    public void deleteAllData() throws IOException {
        try {
            if (this.fileManager.fileExistsInDataDir(sTournamentsFileName)) {
                this.fileManager.deleteFileFromDataDirPath(sTournamentsFileName);
            }

            if (this.fileManager.fileExistsInDataDir(sTournamentsDirectory)) {
                this.fileManager.deleteFileFromDataDirPath(sTournamentsDirectory);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private List<TournamentInfo> loadIndexFile() throws IOException {
        try {
            if (this.fileManager.fileExistsInDataDir(sTournamentsFileName)) {
                // Get file content.
                InputStream inputStream = this.fileManager.getInputStreamFromDataDirPath(sTournamentsFileName);
                StringBuilder content = new StringBuilder();

                byte[] buffer = new byte[1024];
                int n = inputStream.read(buffer);
                while (n != -1) {
                    content.append(new String(buffer, 0, n));
                    n = inputStream.read(buffer);
                }
                inputStream.close();

                // Deserialize.
                Type listType = new TypeToken<List<TournamentInfo>>() {}.getType();
                List<TournamentInfo> list = GsonFactory.gson().fromJson(content.toString(), listType);
                return (list != null) ? list : List.list();
            } else {
                return List.list();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void saveIndexFile(List<TournamentInfo> list) throws IOException {
        try {
            // Serialize
            Type listType = new TypeToken<List<TournamentInfo>>(){}.getType();
            String content = GsonFactory.gson().toJson(list, listType);

            // Write to file.
            OutputStream outputStream = this.fileManager.getOutputStreamFromDataDirPath(sTournamentsFileName, true);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void saveTournamentFile(Tournament tournament) throws IOException {
        try {
            // Serialize
            Type tournamentType = new TypeToken<Tournament>(){}.getType();
            String content = GsonFactory.gson().toJson(tournament, tournamentType);

            // Write to file.
            OutputStream outputStream = this.fileManager.getOutputStreamFromDataDirPath(sTournamentsDirectory + "/" + tournament.getId(), true);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void deleteTournamentFile(Tournament tournament) throws IOException {
        try {
            if (this.fileManager.fileExistsInDataDir(sTournamentsDirectory + "/" + tournament.getId())) {
                this.fileManager.deleteFileFromDataDirPath(sTournamentsDirectory + "/" + tournament.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private Tournament loadTournamentFile(String id) throws IOException {
        try {
            if (this.fileManager.fileExistsInDataDir(sTournamentsDirectory + "/" + id)) {
                // Get file content.
                InputStream inputStream = this.fileManager.getInputStreamFromDataDirPath(sTournamentsDirectory + "/" + id);
                StringBuilder content = new StringBuilder();

                byte[] buffer = new byte[1024];
                int n = inputStream.read(buffer);
                while (n != -1) {
                    content.append(new String(buffer, 0, n));
                    n = inputStream.read(buffer);
                }
                inputStream.close();

                // Deserialize.
                Type tournType = new TypeToken<Tournament>() {
                }.getType();
                return GsonFactory.gson().fromJson(content.toString(), tournType);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return null;
    }
}
