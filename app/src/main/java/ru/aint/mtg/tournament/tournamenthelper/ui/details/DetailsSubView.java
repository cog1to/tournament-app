package ru.aint.mtg.tournament.tournamenthelper.ui.details;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import ru.aint.mtg.tournament.tournamenthelper.dialogs.YesNoDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.IConfirmCallback;

/**
 * Details sub view.
 */
public abstract class DetailsSubView implements IDetailsSubView {

    protected FragmentActivity mContext;

    protected DetailsSubView(FragmentActivity context) {
        mContext = context;
    }

    @Override
    public void showConfirmationDialog(String title, String message, @NonNull IConfirmCallback callbackDelegate) {
        YesNoDialogFragment dialogFragment = new YesNoDialogFragment();
        dialogFragment.setMessage(message);
        dialogFragment.setTitle(title);
        dialogFragment.setListener((fragment, action) -> {
            if (action == YesNoDialogFragment.ACTION_YES) {
                callbackDelegate.onActionSelected(true);
            } else {
                callbackDelegate.onActionSelected(false);
            }
        });
        dialogFragment.show(mContext.getSupportFragmentManager(), "confirmdialog");
    }
}
