package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.dialogs.YesNoDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.INavigationActivity;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentListPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentListView;

/**
 * History view.
 */
public class HistoryView extends BaseActionBarFragment implements ITournamentListView, MenuItem.OnMenuItemClickListener, YesNoDialogFragment.Listener {

    private static final String TAG_DELETE = "Delete";
    private static final String TAG_ARCHIVE = "Archive";
    private static final String TAG_UNARCHIVE = "Unarchive";

    private ITournamentListPresenter mPresenter;

    private RecyclerView mRecyclerView;

    private int mActiveColor;

    private int mClearColor;

    /**
     * Pending list changes.
     */
    private ListChanges mPendingChanges = new ListChanges();

    public void setPresenter(ITournamentListPresenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mActiveColor = ContextCompat.getColor(getActivity(), R.color.colorPlayerSelected);
        mClearColor = ContextCompat.getColor(getActivity(), android.R.color.transparent);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.history_list);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setItemAnimator(new HistoryListAnimator());

        HistoryListAdapter adapter = new HistoryListAdapter(new ITournamentListItemPresenter[] {});
        mRecyclerView.setAdapter(adapter);

        if (mPresenter != null) {
            if (mPresenter.onMenuInflated() != null) {
                setHasOptionsMenu(true);
            }
            mPresenter.onCreateView(this);
        }
        return view;
    }

    @Override
    protected List<MenuItem> inflateMenuItems(Menu menu) {
        ITournamentListPresenter.MenuItemType[] menuTypes = mPresenter.onMenuInflated();
        fj.data.List<MenuItem> items = List.list();

        for (ITournamentListPresenter.MenuItemType itemType: menuTypes) {
            MenuItem item = null;
            switch (itemType) {
                case Archive:
                    item = menu.add(Menu.NONE, R.id.action_archive_event, Menu.FIRST, R.string.menu_archive_selected);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_archive_white_24dp);
                    item.setActionView(R.layout.action_view_archive);
                    break;
                case Delete:
                    item = menu.add(Menu.NONE, R.id.action_delete_event, Menu.FIRST, R.string.menu_delete_selected);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_delete_white_24dp);
                    item.setActionView(R.layout.action_view_delete);
                    break;
                case Unarchive:
                    item = menu.add(Menu.NONE, R.id.action_unarchive_event, Menu.FIRST, R.string.menu_unarchive_selected);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_unarchive_white_24dp);
                    item.setActionView(R.layout.action_view_unarchive);
                    break;
                case Create:
                    item = menu.add(Menu.NONE, R.id.action_new_tournament, Menu.FIRST, R.string.menu_new_tournament);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_create_white_24dp);
                    item.setActionView(R.layout.action_view_create);
                    break;
            }
            items = items.cons(item);
        }

        items.foreach(item -> {
            item.getActionView().setOnClickListener(view -> onMenuItemClick(item));
            return Unit.unit();
        });

        return items;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_delete_event) {
            showConfirmDialog(getActivity().getResources().getString(R.string.delete_selected_tournaments), TAG_DELETE);
            return true;
        } else if (item.getItemId() == R.id.action_archive_event) {
            showConfirmDialog(getActivity().getResources().getString(R.string.archive_selected_tournaments), TAG_ARCHIVE);
            return true;
        } else if (item.getItemId() == R.id.action_unarchive_event) {
            showConfirmDialog(getActivity().getResources().getString(R.string.unarchive_selected_tournaments), TAG_UNARCHIVE);
            return true;
        } else if (item.getItemId() == R.id.action_new_tournament) {
            mPresenter.onCreateTournament();
        }

        return false;
    }

    @Override
    public void showLoadingSpinner() {

    }

    @Override
    public void showList(ITournamentListItemPresenter[] items) {
        HistoryListAdapter adapter = (HistoryListAdapter) mRecyclerView.getAdapter();
        adapter.setItems(items);
    }

    @Override
    public void reloadItemAt(int index) {
        ITournamentListItemPresenter item = mPresenter.getItemAt(index);
        HistoryListAdapter adapter = (HistoryListAdapter) mRecyclerView.getAdapter();
        adapter.reloadItem(index, item);
    }

    @Override
    public void toggleEditMode(boolean active) {
        reloadMenu();

        FragmentActivity activity = getActivity();
        if (activity instanceof INavigationActivity) {
            ((INavigationActivity)activity).setDrawerState(active);
        }
    }

    @Override
    public boolean onBackPressed() {
        return mPresenter.onBack();
    }

    @Override
    protected String getTitle() {
        if (mPresenter != null) {
            return getActivity().getResources().getString(mPresenter.getTitle());
        }

        return getActivity().getResources().getString(R.string.menu_history);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_history_list;
    }

    protected class HistoryItemViewHolder extends RecyclerView.ViewHolder {

        ITournamentListItemPresenter mPresenter;
        ImageView mIcon;
        LinearLayout mIconLayout;
        TextView mTitle;
        TextView mSubtitle;

        HistoryItemViewHolder(View itemView) {
            super(itemView);

            mIconLayout = (LinearLayout) itemView.findViewById(R.id.history_item_date_layout);
            mIcon = (ImageView) itemView.findViewById(R.id.history_item_icon);
            mTitle = (TextView) itemView.findViewById(R.id.history_item_name_text_view);
            mSubtitle = (TextView) itemView.findViewById(R.id.history_item_details_text_view);
        }

        void setPresenter(ITournamentListItemPresenter presenter) {
            mPresenter = presenter;
            mTitle.setText(presenter.getName());
            mSubtitle.setText(presenter.getSubtitle());
            mIcon.setBackgroundResource(getBackgroundIcon());
            mIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), getIcon()));
        }

        @DrawableRes int getPreviousIcon() {
            if (mPresenter.getIsSelected()) {
                return R.drawable.ic_trophy_white;
            } else {
                return R.drawable.ic_done_white_24dp;
            }
        }

        @DrawableRes int getIcon() {
            if (mPresenter.getIsSelected()) {
                return R.drawable.ic_done_white_24dp;
            } else {
                return R.drawable.ic_trophy_white;
            }
        }

        @DrawableRes int getBackgroundIcon() {
            if (mPresenter.getIsSelected()) {
                return R.drawable.circle_grey_shape;
            } else {
                return R.drawable.circle_shape;
            }
        }
    }

    protected class HistoryListAdapter extends RecyclerView.Adapter<HistoryItemViewHolder> {

        private ITournamentListItemPresenter[] mItems;

        HistoryListAdapter(@NonNull ITournamentListItemPresenter[] items) {
            mItems = items;
        }

        public void setItems(ITournamentListItemPresenter[] items) {
            mItems = items;
            notifyDataSetChanged();
        }

        void reloadItem(int index, @NonNull ITournamentListItemPresenter presenter) {
            mItems[index] = presenter;
            notifyItemChanged(index);
        }

        @Override
        public HistoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.list_tournament_history_item, parent, false);
            return new HistoryItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(HistoryItemViewHolder holder, int position) {
            ITournamentListItemPresenter presenter = mItems[position];
            holder.setPresenter(presenter);

            View view = holder.itemView;
            view.setOnClickListener(sender -> mPresenter.onItemTapped(holder.getAdapterPosition()));

            view.setOnLongClickListener(sender -> mPresenter.onItemLongTapped(holder.getAdapterPosition()));
        }

        @Override
        public int getItemCount() {
            return mItems.length;
        }
    }

    /**
     * Shows confirmation dialog for selected action.
     *
     * @param message Message to show.
     * @param tag ID tag for dialog fragment.
     */
    public void showConfirmDialog(String message, String tag) {
        YesNoDialogFragment dialogFragment = new YesNoDialogFragment();
        dialogFragment.setMessage(message);
        dialogFragment.setListener(this);
        dialogFragment.show(getActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onActionSelected(YesNoDialogFragment fragment, int action) {
        if (action == YesNoDialogFragment.ACTION_YES) {
            switch (fragment.getTag()) {
                case TAG_DELETE:
                    mPresenter.onDelete();
                    break;
                case TAG_ARCHIVE:
                    mPresenter.onArchive();
                    break;
                case TAG_UNARCHIVE:
                    mPresenter.onUnarchive();
                    break;
            }
        }
    }

    /**
     * Custom animator for icon flipping effect.
     */
    private class HistoryListAnimator extends DefaultItemAnimator {

        public HistoryListAnimator() {
        }

        @Override
        public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
            if (mPendingChanges != null) {
                if (mPendingChanges.getDataChanges().exists(i -> i == viewHolder.getAdapterPosition())) {
                    return false;
                }

                if (mPendingChanges.getHighlightChanges().exists(i -> i == viewHolder.getAdapterPosition())) {
                    return false;
                }

                if (mPendingChanges.getSelectionChanges().exists(i -> i == viewHolder.getAdapterPosition())) {
                    return true;
                }
            }
            return true;
        }

        @Override
        public boolean animateChange(@NonNull final RecyclerView.ViewHolder oldHolder,
                                     @NonNull final RecyclerView.ViewHolder newHolder,
                                     @NonNull ItemHolderInfo preInfo, @NonNull ItemHolderInfo postInfo) {

            if (mPendingChanges != null) {
                if (mPendingChanges.getDataChanges().exists(i -> i == oldHolder.getAdapterPosition())) {
                    mPendingChanges.setDataChanges(mPendingChanges.getDataChanges().filter(i -> i != oldHolder.getAdapterPosition()));
                    return super.animateChange(oldHolder, newHolder, preInfo, postInfo);
                }

                if (mPendingChanges.getHighlightChanges().exists(i -> i == oldHolder.getAdapterPosition())) {
                    mPendingChanges.setHighlightChanges(mPendingChanges.getHighlightChanges().filter(i -> i != oldHolder.getAdapterPosition()));
                    return super.animateChange(oldHolder, newHolder, preInfo, postInfo);
                }

                if (mPendingChanges.getSelectionChanges().exists(i -> i == oldHolder.getAdapterPosition())) {
                    mPendingChanges.setSelectionChanges(mPendingChanges.getSelectionChanges().filter(i -> i != oldHolder.getAdapterPosition()));
                }
            }

            int startColor, endColor, startIconBackground, endIconBackground;
            Drawable startDrawable, endDrawable;
            HistoryItemViewHolder holder = (HistoryItemViewHolder)oldHolder;
            if (mPresenter.getItemAt(oldHolder.getAdapterPosition()).getIsSelected()) {
                startColor = mClearColor;
                endColor = mActiveColor;
                startIconBackground = R.drawable.circle_shape;
                endIconBackground = R.drawable.circle_grey_shape;

                startDrawable = ContextCompat.getDrawable(getActivity(), holder.getPreviousIcon());
                endDrawable = ContextCompat.getDrawable(getActivity(), holder.getIcon());
            } else {
                startColor = mActiveColor;
                endColor = mClearColor;
                startIconBackground = R.drawable.circle_grey_shape;
                endIconBackground = R.drawable.circle_shape;

                startDrawable = ContextCompat.getDrawable(getActivity(), holder.getPreviousIcon());
                endDrawable = ContextCompat.getDrawable(getActivity(), holder.getIcon());
            }

            ObjectAnimator fadeToBlack = ObjectAnimator.ofInt(oldHolder.itemView, "backgroundColor", startColor, endColor);
            fadeToBlack.setDuration(getActivity().getResources().getInteger(R.integer.animation_duration));
            fadeToBlack.setEvaluator(new ArgbEvaluator());
            fadeToBlack.start();

            ObjectAnimator rotation1 = ObjectAnimator.ofFloat(((HistoryItemViewHolder)oldHolder).mIconLayout, View.ROTATION_Y, 0.0f, 90.0f);
            rotation1.setDuration(getActivity().getResources().getInteger(R.integer.half_animation_duration));
            rotation1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    ((HistoryItemViewHolder)oldHolder).mIcon.setBackgroundResource(startIconBackground);
                    ((HistoryItemViewHolder)oldHolder).mIcon.setImageDrawable(startDrawable);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    ((HistoryItemViewHolder)oldHolder).mIcon.setBackgroundResource(endIconBackground);
                    ((HistoryItemViewHolder)oldHolder).mIcon.setImageDrawable(endDrawable);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            ObjectAnimator rotation2 = ObjectAnimator.ofFloat(((HistoryItemViewHolder)oldHolder).mIconLayout, View.ROTATION_Y, 90.0f, 0.0f);
            rotation2.setDuration(getActivity().getResources().getInteger(R.integer.half_animation_duration));

            AnimatorSet set = new AnimatorSet();
            set.playSequentially(rotation1, rotation2);
            set.start();

            return false;
        }
    }

    protected class ListChanges {
        private List<Integer> dataChanges;
        private List<Integer> selectionChanges;
        private List<Integer> highlightChanges;

        ListChanges() {
            dataChanges = List.list();
            selectionChanges = List.list();
            highlightChanges = List.list();
        }

        List<Integer> getHighlightChanges() {
            return highlightChanges;
        }

        void setHighlightChanges(List<Integer> highlightChanges) {
            this.highlightChanges = highlightChanges;
        }

        List<Integer> getDataChanges() {
            return dataChanges;
        }

        void setDataChanges(List<Integer> dataChanges) {
            this.dataChanges = dataChanges;
        }

        List<Integer> getSelectionChanges() {
            return selectionChanges;
        }

        void setSelectionChanges(List<Integer> selectionChanges) {
            this.selectionChanges = selectionChanges;
        }
    }
}
