package ru.aint.mtg.tournament.tournamenthelper.mvp.navigation;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.tournaments.Tournament;

/**
 * Navigation activity interface.
 */
public interface INavigationActivity {
    void setActiveToolbar(Toolbar toolbar);
    void showBackButton(boolean show);
    void setActiveNavigationItem(@IdRes int item);
    void setDrawerState(boolean state);
}
