package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IMatchEditingView;

/**
 * Match editing presenter.
 */

public interface IMatchEditingPresenter {
    void setView(IMatchEditingView view);
    List<IMatchEditingView.MatchEditingEntry> getItems();
    int getMaxScore();
    void updatedItem(int position, IMatchEditingView.MatchEditingEntry entry);
    boolean hasValidValues();
    void editingConfirmed();
}
