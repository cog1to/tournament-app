package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchEditingPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchPresenter;

/**
 * Round view interface.
 */
public interface IRoundView {
    void showMatches(IMatchPresenter[] presenters);
    void reloadMatch(int position, IMatchPresenter presenter);
    void showMatchEditing(IMatchEditingPresenter presenter);
}
