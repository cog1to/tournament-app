package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.INavigationActivity;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.ITournamentDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentDetailsView;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.ui.details.EventsView;
import ru.aint.mtg.tournament.tournamenthelper.ui.details.StandingsView;

/**
 * Tournament details view implementation.
 */
public class TournamentDetailsView extends BaseActionBarFragment implements ITournamentDetailsView {

    private ITournamentDetailsPresenter mPresenter;

    private ViewPager mViewPager;

    private TabLayout mTabLayout;

    private List<IDetailsSubPresenter> mPresenters;

    public void setPresenter(ITournamentDetailsPresenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);

        mTabLayout = (TabLayout)view.findViewById(R.id.sliding_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        FragmentActivity activity = getActivity();
        if (activity instanceof INavigationActivity) {
            ((INavigationActivity)activity).showBackButton(true);
        }

        if (mPresenter != null) {
            if (mPresenter.onMenuInflated() != null) {
                setHasOptionsMenu(true);
            }
            mPresenter.onCreateView(this);
        }

        return view;
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void setTitle(String title) {
        mToolbar.setTitle(title);
    }

    @Override
    public void setSubtitle(String subtitle) {
        mToolbar.setSubtitle(subtitle);
    }

    @Override
    public void setSubPresenters(@NonNull List<IDetailsSubPresenter> presenters) {
        mPresenters = presenters;
        mViewPager.setAdapter(new CustomViewPagerAdapter(getActivity(), mPresenters));
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_tournament_details;
    }

    // <editor-fold desc="View pager">

    private class CustomViewPagerAdapter extends PagerAdapter {

        private List<IDetailsSubPresenter> mPresenters;

        private Context mContext;

        CustomViewPagerAdapter(Context context, @NonNull List<IDetailsSubPresenter> presenters) {
            mContext = context;
            mPresenters = presenters;
        }

        @Override
        public int getCount() {
            return mPresenters.length();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            IDetailsSubPresenter presenter = mPresenters.index(position);
            IDetailsSubView subView = getViewForPresenter(collection, presenter);
            collection.addView(subView.getView());
            presenter.setView(subView);
            return subView.getView();
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        private IDetailsSubView getViewForPresenter(ViewGroup collection, IDetailsSubPresenter presenter) {
            LayoutInflater inflater = LayoutInflater.from(mContext);

            switch (presenter.getType()) {
                case Standings:
                    ViewGroup standingsLayout = (ViewGroup) inflater.inflate(R.layout.details_standings_view, collection, false);
                    StandingsView standingsView = new StandingsView(getActivity(), standingsLayout, presenter);
                    return standingsView;
                case Events:
                    ViewGroup eventsLayout = (ViewGroup) inflater.inflate(R.layout.details_events_view, collection, false);
                    EventsView eventsView = new EventsView(getActivity(), eventsLayout, presenter);
                    return eventsView;
                default:
                    break;
            }

            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            IDetailsSubPresenter presenter = mPresenters.index(position);

            switch (presenter.getType()) {
                case Standings:
                    return mContext.getResources().getString(R.string.summary);
                case Events:
                    return mContext.getResources().getString(R.string.events);
            }

            return null;
        }
    }

    // </editor-fold>
}
