package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.standings;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.details.IPlayerDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.player.IPlayerDetailsView;

/**
 * Player details presenter.
 */
public class PlayerDetailsPresenter implements IPlayerDetailsPresenter {

    private PlayerState mPlayerState;

    private TournamentConfig mConfig;

    public PlayerDetailsPresenter(@NonNull PlayerState state, @NonNull TournamentConfig config) {
        mPlayerState = state;
        mConfig = config;
    }

    @Override
    public void onCreateView(@NonNull IPlayerDetailsView view) {
        if (mPlayerState == null) {
            return;
        }

        view.setTitle(mPlayerState.getPlayer().getFullName());

        int matchCount = mPlayerState.getMatchHistory().length();
        int matchesLost = mPlayerState.getMatchHistory().filter(m -> m.winner().isSome() && !m.winner().some().equals(mPlayerState.getPlayer())).length();
        int matchesDrew = mPlayerState.getMatchHistory().filter(m -> m.winner().isNone()).length();
        view.setMatchStatistics(matchCount, mPlayerState.getMatchesWon(), matchesLost, matchesDrew);

        int gamesWon = mPlayerState.getGamesWon();
        int gamesLost = mPlayerState.getGamesLost();
        int gamesDrew = mPlayerState.getGamesDrew();
        view.setGameStatistics(gamesWon + gamesLost + gamesDrew, gamesWon, gamesLost, gamesDrew);

        double matchPoints = mPlayerState.getMatchPoints(mConfig);
        double maxMatchPoints = mPlayerState.getMatchHistory().length() * mConfig.getPointsPerMatchWin();
        view.setMatchPoints((int)matchPoints, (int)maxMatchPoints);

        double gamePoints = mPlayerState.getGamePoints(mConfig);
        double maxGamePoints = (mPlayerState.getGamesLost() + mPlayerState.getGamesWon() + mPlayerState.getGamesDrew()) * mConfig.getPointsPerGameWin();
        view.setGamePoints((int)gamePoints, (int)maxGamePoints);

        double matchWinPercentage = mPlayerState.getMatchWinPercentage(this.mConfig);
        view.setMatchWinPercentage(matchWinPercentage);

        double gameWinPercentage = mPlayerState.getGameWinPercentage(this.mConfig);
        view.setGameWinPercentage(gameWinPercentage);
    }
}
