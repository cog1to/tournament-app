package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import fj.data.List;

/**
 * Match editing view interface.
 */
public interface IMatchEditingView {

    class MatchEditingEntry {
        private String mName;
        private int mValue;

        public MatchEditingEntry(String name, int value) {
            mName = name;
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        public void setName(String name) {
            this.mName = name;
        }

        public String getName() {
            return mName;
        }

        public void setValue(int value) {
            this.mValue = value;
        }
    }

    void showItems(List<MatchEditingEntry> items);
}
