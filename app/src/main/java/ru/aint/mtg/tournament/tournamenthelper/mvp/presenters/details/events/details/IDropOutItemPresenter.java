package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details;

/**
 * Drop out item presenter.
 */
public interface IDropOutItemPresenter {
    String getName();
    boolean getSelected();
}
