package ru.aint.mtg.tournament.tournamenthelper.mvp.views.about;

/**
 * License list item presenter.
 */
public interface ILicenseItemPresenter {
    String getTitle();
    String getDisclaimer();
    String getLink();
}
