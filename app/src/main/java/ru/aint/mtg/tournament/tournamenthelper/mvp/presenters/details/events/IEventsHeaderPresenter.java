package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events;

/**
 * Events header presenter.
 */
public interface IEventsHeaderPresenter extends IEventsItemPresenter {
    String getTitle();
}
