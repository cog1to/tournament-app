package ru.aint.mtg.tournament.tournamenthelper.feedback;

import android.content.Context;
import android.content.SharedPreferences;

import static android.R.attr.repeatCount;

/**
 * Feedback manager.
 */
public class FeedbackManager {

    /**
     * Feedback preferences file name.
     */
    private static final String SHARED_PREFERENCES_FILE = "FeedbackData";

    /**
     * Launch count key.
     */
    private static final String LAUNCH_COUNT = "LaunchCount";

    /**
     * Rated flag key.
     */
    private static final String RATED_FLAG = "RatedFlag";

    /**
     * Rate us dialog repeat count.
     */
    private static final int sRepeatCount = 20;

    /**
     * Returns launch count.
     *
     * @param context Application context.
     *
     * @return Number of launches.
     */
    public static int getLaunchCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (preferences.contains(LAUNCH_COUNT)) {
            return preferences.getInt(LAUNCH_COUNT, 0);
        }

        return 0;
    }

    /**
     * Increments launch count.
     *
     * @param context Application context.
     */
    public static void incrementLaunchCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        int launchCount = 0;
        if (preferences.contains(LAUNCH_COUNT)) {
            launchCount = preferences.getInt(LAUNCH_COUNT, 0);
        }

        launchCount += 1;
        if (launchCount > sRepeatCount) {
            launchCount = 0;
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(LAUNCH_COUNT, launchCount);
        editor.apply();
    }

    /**
     * Returns flag indicating whether user selected 'No thanks' or 'Rated' option in a dialog.
     *
     * @param context Application context.
     *
     * @return Value indicating whether user selected 'No thanks' or 'Rated' option in a dialog.
     */
    public static boolean getRatedFlag(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (preferences.contains(RATED_FLAG)) {
            return preferences.getBoolean(RATED_FLAG, false);
        }

        return false;
    }

    /**
     * Saves 'Rated' flag value.
     *
     * @param context Application context.
     * @param rated Flag value.
     */
    public static void setRatedFlag(Context context, boolean rated) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(RATED_FLAG, rated);
        editor.apply();
    }

    /**
     * Returns a flag indicating whether app should show 'Rate us' dialog.
     *
     * @return <b>true</b> if app should display 'rate us' dialog, <b>false</b> otherwise.
     */
    public static boolean showShowRateUsDialog(Context context) {
        int launchCount = getLaunchCount(context);
        boolean rated = getRatedFlag(context);

        return (launchCount >= sRepeatCount && !rated);
    }
}
