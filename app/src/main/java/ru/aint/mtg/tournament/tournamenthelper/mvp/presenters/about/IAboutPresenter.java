package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutView;

/**
 * About presenter.
 */
public interface IAboutPresenter {
    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull IAboutView view);

    /**
     * Called when item was selected in about list.
     *
     * @param presenter Selected item presenter.
     */
    void onItemSelected(IAboutItemPresenter presenter);
}
