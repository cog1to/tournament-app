package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDropOutsView;

/**
 * Presenter for drop outs event.
 */
public interface IDropOutsPresenter {

    enum MenuItemType {
        Done,
        Delete
    }

    /**
     * Called when back button is pressed.
     *
     * @return Flag indicating whether tap was handled.
     */
    boolean onBackPressed();

    /**
     * Called when item is tapped.
     *
     * @param index Index of a tapped item.
     */
    void onItemTapped(int index);

    /**
     * Called when done button is tapped.
     *
     * @param type Item type.
     */
    void onMenuItemTapped(MenuItemType type);

    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onViewCreated(IDropOutsView view);

    /**
     * Returns available menu items.
     *
     * @return Array of available menu items.
     */
    MenuItemType[] onMenuInflated();
}
