package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.log;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.log.ILogView;

/**
 * Log presenter.
 */
public interface ILogPresenter {
    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull ILogView view);
}
