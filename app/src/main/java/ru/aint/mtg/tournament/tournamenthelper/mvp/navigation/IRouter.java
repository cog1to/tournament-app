package ru.aint.mtg.tournament.tournamenthelper.mvp.navigation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;

/**
 * Router interface. Used for presenting views.
 */
public interface IRouter {

    /**
     * Shows tournament view.
     *
     * @param id Tournament ID.
     */
    void showTournamentView(String id);

    /**
     * Shows event details.
     *
     * @param tournament Parent tournament.
     * @param event Event to show.
     */
    void showEventDetails(Tournament tournament, Event event);

    /**
     * Shows an error message.
     *
     * @param message Error message.
     * @param ex Exception.
     */
    void showError(String message, Exception ex);

    /**
     * Presents create tournament view.
     */
    void showCreateTournament();

    /**
     * Shows drop event.
     *
     * @param tournament Parent tournament.
     * @param event Event to show.
     */
    void showDropEvent(@NonNull Tournament tournament, @Nullable Drop event);

    /**
     * Pops the stack.
     */
    void pop();

    /**
     * Show licenses screen.
     */
    void showLicenses();

    /**
     * Show versions screen.
     */
    void showVersions();

    /**
     * Shows player statistics in a given tournament.
     *  @param playerState Player state to show.
     * @param config Parent tournament.
     */
    void showPlayerDetails(PlayerState playerState, TournamentConfig config);
}
