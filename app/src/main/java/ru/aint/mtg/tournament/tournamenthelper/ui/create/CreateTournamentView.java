package ru.aint.mtg.tournament.tournamenthelper.ui.create;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import fr.ganfra.materialspinner.MaterialSpinner;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.log.Logger;
import ru.aint.mtg.tournament.tournamenthelper.dialogs.SpinnerDialogFragment;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.create.ICreateTournamentPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.create.ICreateTournamentView;

/**
 * Create tournament view implementation.
 */
public class CreateTournamentView implements ICreateTournamentView {

    /**
     * Presenter.
     */
    private ICreateTournamentPresenter mPresenter;

    /**
     * Context.
     */
    private Activity mContext;

    /**
     * View.
     */
    private View mView;

    /**
     * Format text view.
     */
    private EditText mFormatView;

    /**
     * Format spinner adapter.
     */
    ArrayAdapter<String> mFormatAdapter;

    /**
     * Name view.
     */
    private EditText mNameView;

    /**
     * Wins per match text view.
     */
    private EditText mWinsPerMatchView;

    /**
     * Advanced toggle layout.
     */
    private RelativeLayout mAdvancedToggle;

    /**
     * Advanced toggle icon.
     */
    private ImageView mAdvancedToggleIcon;

    /**
     * Points per match win edit view.
     */
    private EditText mPointsPerMatchWinView;

    /**
     * Points per match draw edit view.
     */
    private EditText mPointsPerMatchDrawView;

    /**
     * Points per game win edit view.
     */
    private EditText mPointsPerGameWinView;

    /**
     * Points per game draw edit view.
     */
    private EditText mPointsPerGameDrawView;

    /**
     * Formats.
     */
    private HashMap<TournamentFormat, String> mFormats;

    /**
     * Layout containing advanced settings.
     */
    private LinearLayout mAdvancedSettingsViews;

    /**
     * Expand state.
     */
    private boolean mAdvancedExpanded;

    /**
     * Selected format.
     */
    private TournamentFormat mSelectedFormat;

    private TournamentFormat[] sortedFormats() {
        TournamentFormat[] formats = mFormats.keySet().toArray(new TournamentFormat[0]);
        Arrays.sort(formats, (o1, o2) -> o1.compareTo(o2));

        return formats;
    }

    @Override
    public String getName() {
        return mNameView.getText().toString();
    }

    @Override
    public void setName(String name) {
        mNameView.setText(name);
    }

    @Override
    public TournamentFormat getFormat() {
        return mSelectedFormat;
    }

    @Override
    public boolean isFormatSelected() {
        return mSelectedFormat != null;
    }

    @Override
    public void setFormat(TournamentFormat format) {
        TournamentFormat[] formats = mFormats.keySet().toArray(new TournamentFormat[0]);
        for (int idx = 0; idx < formats.length; idx++) {
            if (formats[idx] == format) {
                mFormatView.setSelection(idx);
                break;
            }
        }
    }

    @Override
    public void setFormats(HashMap<TournamentFormat, String> formats) {
        mFormats = formats;
        if (mFormats != null && mFormats.size() > 0 && mSelectedFormat == null) {
            mSelectedFormat = sortedFormats()[0];
            if (mFormatView != null) {
                mFormatView.setText(mFormats.get(mSelectedFormat));
            }
        }
    }

    @Override
    public int getWinsPerMatch() {
        try {
            return Integer.parseInt(mWinsPerMatchView.getText().toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setWinsPerMatch(int value) {
        mWinsPerMatchView.setText(String.format("%d", value));
    }

    @Override
    public int getPointsPerMatchWin() {
        try {
            return Integer.parseInt(mPointsPerMatchWinView.getText().toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setPointsPerMatchWin(int value) {
        mPointsPerMatchWinView.setText(String.format("%d", value));
    }

    @Override
    public int getPointsPerMatchDraw() {
        try {
            return Integer.parseInt(mPointsPerMatchDrawView.getText().toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setPointsPerMatchDraw(int value) {
        mPointsPerMatchDrawView.setText(String.format("%d", value));
    }

    @Override
    public int getPointsPerGameWin() {
        try {
            return Integer.parseInt(mPointsPerGameWinView.getText().toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setPointsPerGameWin(int value) {
        mPointsPerGameWinView.setText(String.format("%d", value));
    }

    @Override
    public int getPointsPerGameDraw() {
        try {
            return Integer.parseInt(mPointsPerGameDrawView.getText().toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setPointsPerGameDraw(int value) {
        mPointsPerGameDrawView.setText(String.format("%d", value));
    }

    public CreateTournamentView(Activity context, View view, ICreateTournamentPresenter presenter) {
        mPresenter = presenter;
        mContext = context;
        mView = view;

        mNameView = (EditText) mView.findViewById(R.id.tournament_edit_name_text);
        mFormatView = (EditText) mView.findViewById(R.id.tournament_type);
        mWinsPerMatchView = (EditText) mView.findViewById(R.id.wins_per_match_text);
        mAdvancedToggle = (RelativeLayout) mView.findViewById(R.id.advanced_toggle_layout);
        mAdvancedToggleIcon = (ImageView) mView.findViewById(R.id.advanced_toggle_icon);
        mAdvancedSettingsViews = (LinearLayout) mView.findViewById(R.id.advanced_settings_views);
        mPointsPerMatchWinView = (EditText) mView.findViewById(R.id.points_per_match_win_text);
        mPointsPerMatchDrawView = (EditText) mView.findViewById(R.id.points_per_match_draw_text);
        mPointsPerGameWinView = (EditText) mView.findViewById(R.id.points_per_game_win_text);
        mPointsPerGameDrawView = (EditText) mView.findViewById(R.id.points_per_game_draw_text);

        if (mSelectedFormat != null) {
            mFormatView.setText(mFormats.get(mSelectedFormat));
        }

        mFormatView.setOnClickListener(formatView -> {
            SpinnerDialogFragment spinnerFragment = new SpinnerDialogFragment();

            ArrayList<String> formatValues = new ArrayList<String>();
            TournamentFormat[] sortedFormats = sortedFormats();
            for (int idx = 0; idx < sortedFormats.length; idx++) {
                formatValues.add(mFormats.get(sortedFormats[idx]));
            }
            spinnerFragment.setItems(formatValues.toArray(new String[0]));

            spinnerFragment.setListener(index -> {
                mSelectedFormat = sortedFormats()[index];
                mFormatView.setText(mFormats.get(mSelectedFormat));
            });

            try {
                FragmentTransaction ft = ((FragmentActivity)mContext).getSupportFragmentManager().beginTransaction();
                ft.add(spinnerFragment, "selectformat");
                ft.commitAllowingStateLoss();
            } catch (Exception e) {
                Logger.log("Failed to show Edit Player dialog.");
            }
        });

        TextInputLayout nameLayout = (TextInputLayout)view.findViewById(R.id.input_tournament_name);
        mNameView.setText(mContext.getResources().getString(R.string.menu_new_tournament));
        mNameView.addTextChangedListener(new TextStringVerifier(nameLayout, value -> (value.length() <= 0) ? mContext.getString(R.string.not_empty_error) : null));

        TextInputLayout winsLayout = (TextInputLayout)view.findViewById(R.id.input_wins_per_match_text);
        mWinsPerMatchView.addTextChangedListener(new TextNumberVerifier(winsLayout, value -> (value <= 0) ? mContext.getString(R.string.greater_than_zero_error) : null));

        mAdvancedToggle.setOnClickListener(v -> {
            mAdvancedExpanded = !mAdvancedExpanded;
            float startAlpha = 0.0f, endAlpha = 1.0f;
            float startAngle = 0.0f, endAngle = 180.0f;

            if (!mAdvancedExpanded) {
                startAlpha = 1.0f;
                endAlpha = 0.0f;
                startAngle = 180.0f;
                endAngle = 0.0f;
            }

            mAdvancedSettingsViews.setAlpha(startAlpha);

            ObjectAnimator animator = ObjectAnimator.ofFloat(mAdvancedSettingsViews, View.ALPHA, startAlpha, endAlpha);
            animator.setDuration(mContext.getResources().getInteger(R.integer.animation_duration));
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    if (mAdvancedExpanded) {
                        mAdvancedSettingsViews.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (!mAdvancedExpanded) {
                        mAdvancedSettingsViews.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            ObjectAnimator iconAnimator = ObjectAnimator.ofFloat(mAdvancedToggleIcon, "rotation", startAngle, endAngle);
            iconAnimator.setDuration(mContext.getResources().getInteger(R.integer.animation_duration));

            AnimatorSet set = new AnimatorSet();
            set.playTogether(animator, iconAnimator);
            set.start();
        });

        // Advanced settings.
        TextInputLayout pointsPerMatchWinLayout = (TextInputLayout)view.findViewById(R.id.input_points_per_match_win);
        mPointsPerMatchWinView.addTextChangedListener(new TextNumberVerifier(pointsPerMatchWinLayout, value -> (value <= 0) ? mContext.getString(R.string.greater_than_zero_error) : null));

        TextInputLayout pointsPerMatchDrawLayout = (TextInputLayout)view.findViewById(R.id.input_wins_per_match_draw);
        mPointsPerMatchDrawView.addTextChangedListener(new TextNumberVerifier(pointsPerMatchDrawLayout));

        TextInputLayout pointsPerGameWinLayout = (TextInputLayout)view.findViewById(R.id.input_points_per_game_win);
        mPointsPerGameWinView.addTextChangedListener(new TextNumberVerifier(pointsPerGameWinLayout, value -> (value <= 0) ? mContext.getString(R.string.greater_than_zero_error) : null));

        TextInputLayout pointsPerGameDrawLayout = (TextInputLayout)view.findViewById(R.id.input_points_per_game_draw);
        mPointsPerGameDrawView.addTextChangedListener(new TextNumberVerifier(pointsPerGameDrawLayout));

        mContext.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mPresenter.onViewCreated(this);
    }

    // <editor-fold desc="Number verifier for text fields" >

    private interface IIntegerValueValidator {
        String getErrorMessage(int value);
    }

    private interface IStringValueValidator {
        String getErrorMessage(String value);
    }

    private class TextStringVerifier implements TextWatcher {
        TextInputLayout layout;
        IStringValueValidator validator;

        TextStringVerifier(TextInputLayout layout, IStringValueValidator validator) {
            this.layout = layout;
            this.validator = validator;
        }

        TextStringVerifier(TextInputLayout layout) {
            this.layout = layout;
            this.validator = i -> null;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (this.validator != null) {
                String error = this.validator.getErrorMessage(editable.toString());
                this.layout.setError(error);
            }
        }
    }

    private class TextNumberVerifier implements TextWatcher {
        TextInputLayout layout;
        IIntegerValueValidator validator;

        TextNumberVerifier(TextInputLayout layout, IIntegerValueValidator validator) {
            this.layout = layout;
            this.validator = validator;
        }

        TextNumberVerifier(TextInputLayout layout) {
            this.layout = layout;
            this.validator = i -> null;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            try {
                int value = Integer.parseInt(editable.toString());

                if (this.validator != null) {
                    String error = this.validator.getErrorMessage(value);
                    this.layout.setError(error);
                }
            } catch (Exception e) {
                if (this.validator != null) {
                    String error = this.validator.getErrorMessage(0);
                    this.layout.setError(error);
                }
            }
        }
    }

    // </editor-fold>
}
