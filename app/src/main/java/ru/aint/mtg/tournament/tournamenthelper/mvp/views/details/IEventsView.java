package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import android.support.annotation.NonNull;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.IConfirmCallback;

/**
 * Events view interface.
 */
public interface IEventsView extends IDetailsSubView {
    void showEvents(List<IEventsItemPresenter> items);
    void reloadItem(int index, IEventsItemPresenter item);
}
