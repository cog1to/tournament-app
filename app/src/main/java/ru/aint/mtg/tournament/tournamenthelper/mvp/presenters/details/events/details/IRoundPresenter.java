package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details;

import android.support.annotation.NonNull;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IRoundView;

/**
 * Round presenter.
 */
public interface IRoundPresenter {

    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull IRoundView view);

    /**
     * Called when view is destroyed.
     */
    void onDestroyView();

    /**
     * Returns title.
     *
     * @return title string.
     */
    String getTitle();

    /**
     * Rerurns subtitle.
     *
     * @return Subtitle string.
     */
    String getSubtitle();

    /**
     * Called when user taps on match item.
     *
     * @param index Index of item that was tapped.
     */
    void onItemTapped(int index);
}
