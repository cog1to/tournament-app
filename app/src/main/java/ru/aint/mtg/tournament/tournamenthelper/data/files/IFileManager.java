package ru.aint.mtg.tournament.tournamenthelper.data.files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * File manager interface.
 */
public interface IFileManager {

    /**
     * Returns app's data dir path.
     *
     * @return Application's data dir path.
     */
    public File getDataDir();

    /**
     * Checks whether file exists with given path inside data dir.
     *
     * @param path Relative path to file inside data dir.
     *
     * @return true if file exists, false otherwise.
     */
    public boolean fileExistsInDataDir(String path);

    /**
     * Returns an input stream corresponding to given file path.
     *
     * @param path File path.
     *
     * @return Input stream from a file path.
     */
    public InputStream getInputStreamFromDataDirPath(String path) throws IOException;

    /**
     * Returns an output stream corresponding to given file path.
     *
     * @param path File path.
     * @param overwrite Indicates whether file should be created or overwritten.
     *
     * @return Output stream from a file path.
     */
    public OutputStream getOutputStreamFromDataDirPath(String path, boolean overwrite) throws IOException;

    /**
     * Deletes file from given path.
     *
     * @param path File path to delete.
     */
    public void deleteFileFromDataDirPath(String path);
}
