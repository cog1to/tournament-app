package ru.aint.mtg.tournament.tournamenthelper.ui.details;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.log.Logger;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsActionPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsHeaderPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsPlayerPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IStandingsView;
import ru.aint.mtg.tournament.tournamenthelper.utils.StringUtils;

/**
 * Standings view.
 */
public class StandingsView extends DetailsSubView implements IStandingsView {

    private View mView;

    private RecyclerView mRecyclerView;

    private IStandingsPresenter mPresenter;

    public StandingsView(FragmentActivity context, View view, IDetailsSubPresenter presenter) {
        super(context);
        mView = view;
        mPresenter = (IStandingsPresenter)presenter;
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.summary_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void showError(String string) {
        Snackbar.make(mView, string, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showStandings(List<IStandingsItemPresenter> items) {
        mRecyclerView.setAdapter(new StandingsAdapter(mContext, items));
    }

    @Override
    public void reloadItem(int index, IStandingsItemPresenter item) {
        ((StandingsAdapter)mRecyclerView.getAdapter()).reloadItem(item, index);
    }

    @Override
    public void addItem(int index, IStandingsItemPresenter item) {
        ((StandingsAdapter)mRecyclerView.getAdapter()).addItem(index, item);
    }

    @Override
    public void removeItem(int index) {
        ((StandingsAdapter)mRecyclerView.getAdapter()).removeItem(index);
    }

    @Override
    public void showPlayerEditingOptions(String title, IStandingsPlayerPresenter presenter, boolean canDelete) {
        EditPlayerOptionsDialogFragment dialog = new EditPlayerOptionsDialogFragment();
        dialog.setCanDelete(canDelete);
        dialog.setListener((option) -> {
            if (option == EditPlayerOptionsDialogFragment.CHANGE_PLAYER_DELETE) {
                mPresenter.onPlayerDeleting(presenter);
            } else if (option == EditPlayerOptionsDialogFragment.CHANGE_PLAYER_EDIT) {
                mPresenter.onPlayerEditing(presenter);
            }
        });

        dialog.show(mContext.getSupportFragmentManager(), "ChangePlayerDialogFragment");
    }

    @Override
    public void showNewPlayerDialog() {
        showEditPlayerDialog(null, null);
    }

    @Override
    public void showEditPlayerDialog(IStandingsPlayerPresenter presenter, EditPlayerModel player)
    {
        EditPlayerDialogFragment dialog = new EditPlayerDialogFragment();
        dialog.setExistingPlayer(player);

        Bundle args = new Bundle();
        if (player != null) {
            args.putString("title", mContext.getResources().getString(R.string.edit_player));
        } else {
            args.putString("title", mContext.getResources().getString(R.string.new_player));
        }
        dialog.setArguments(args);
        dialog.setSuggestionsProvider(mPresenter.getSuggestionsProvider());

        dialog.setListener(new EditPlayerDialogFragment.Listener() {
            @Override
            public void onPlayerEdited(EditPlayerModel player, EditPlayerModel existingPlayer) {
                if (existingPlayer != null) {
                    mPresenter.onPlayerEdited(presenter, existingPlayer, player);
                } else {
                    mPresenter.onPlayerCreated(player);
                }
            }

            @Override
            public boolean shouldAllowPlayerEditing(EditPlayerModel player, EditPlayerModel existingPlayer) {
                return mPresenter.canCreatePlayer(player);
            }
        });

        try {
            FragmentTransaction ft = mContext.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, null);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.log("Failed to show Edit Player dialog.");
        }
    }

    private class StandingsAdapter extends RecyclerView.Adapter<StandingsViewHolder> {

        private ArrayList<IStandingsItemPresenter> mPresenters;
        private Activity mContext;

        private StandingsAdapter(Activity context, @NonNull List<IStandingsItemPresenter> presenters) {
            mPresenters = new ArrayList<>(presenters.toJavaList());
            mContext = context;
        }

        private void reloadItem(IStandingsItemPresenter item, int index) {
            mPresenters.set(index, item);
            notifyItemChanged(index);
        }

        private void addItem(int index, IStandingsItemPresenter item) {
            mPresenters.add(index, item);
            notifyItemInserted(index);
        }

        private void removeItem(int index) {
            mPresenters.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public StandingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_section_header, parent, false);
                return new HeaderViewHolder(mContext, view);
            } else if (viewType == 1) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_standings_item, parent, false);
                return new PlayerViewHolder(mContext, view);
            } else if (viewType == 2) {
                View view = mContext.getLayoutInflater().inflate(R.layout.list_event_action_item, parent, false);
                return new NewPlayerViewHolder(mContext, view);
            }

            throw new IllegalArgumentException("Unknown view type");
        }

        @Override
        public void onBindViewHolder(StandingsViewHolder holder, int position) {
            IStandingsItemPresenter presenter = mPresenters.get(position);
            holder.setPresenter(position, presenter);

            IStandingsItemPresenter itemPresenter = mPresenters.get(position);
            if (!(itemPresenter instanceof IStandingsHeaderPresenter)) {
                holder.mView.setOnClickListener(view -> mPresenter.onItemTapped(presenter));
            } else {
                holder.mView.setOnClickListener(null);
            }
        }

        @Override
        public int getItemViewType(int position) {
            IStandingsItemPresenter itemPresenter = mPresenters.get(position);
            if (itemPresenter instanceof IStandingsHeaderPresenter) {
                return 0;
            } else if (itemPresenter instanceof IStandingsPlayerPresenter){
                return 1;
            } else if (itemPresenter instanceof IStandingsActionPresenter) {
                return 2;
            }

            throw new IllegalArgumentException("Unknown presenter type");
        }

        @Override
        public int getItemCount() {
            return mPresenters.size();
        }
    }

    private abstract class StandingsViewHolder extends RecyclerView.ViewHolder {

        Activity mContext;
        View mView;

        StandingsViewHolder(Activity context, View itemView) {
            super(itemView);
            mContext = context;
            mView = itemView;
        }

        abstract void setPresenter(int index, IStandingsItemPresenter presenter);
    }

    private class HeaderViewHolder extends StandingsViewHolder {

        private IStandingsHeaderPresenter mPresenter;
        private TextView mTitleView;
        private LinearLayout mDetailsView;

        HeaderViewHolder(Activity context, View itemView) {
            super(context, itemView);
            mTitleView = (TextView)itemView.findViewById(R.id.header_title);
            mDetailsView = (LinearLayout)itemView.findViewById(R.id.header_details);
        }

        void setPresenter(int index, IStandingsItemPresenter presenter) {
            mPresenter = (IStandingsHeaderPresenter)presenter;
            mTitleView.setText(mPresenter.getTitle());

            LayoutInflater inflater = mContext.getLayoutInflater();
            LinearLayout.LayoutParams detailsParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            detailsParams.gravity = Gravity.CENTER;

            mDetailsView.removeAllViews();
            List<String> titles = mPresenter.getDetailTitles();
            for (int idx = 0; idx < titles.length(); idx++) {
                TextView valueView = (TextView)inflater.inflate(R.layout.player_details_view, null);
                valueView.setText(titles.index(idx));
                valueView.setLayoutParams(detailsParams);
                valueView.setPadding((int)mContext.getResources().getDimension(R.dimen.dp_2),
                        (int)mContext.getResources().getDimension(R.dimen.dp_2),
                        (int)mContext.getResources().getDimension(R.dimen.dp_2),
                        0);
                mDetailsView.addView(valueView);
            }
        }
    }

    private class NewPlayerViewHolder extends StandingsViewHolder {

        private IStandingsActionPresenter mPresenter;

        private TextView mTextView;

        NewPlayerViewHolder(Activity context, View itemView) {
            super(context, itemView);
            mTextView = (TextView)itemView.findViewById(R.id.title_text_view);
        }

        @Override
        void setPresenter(int index, IStandingsItemPresenter presenter) {
            mPresenter = (IStandingsActionPresenter)presenter;
            mTextView.setText(mPresenter.getName());
        }
    }

    private class PlayerViewHolder extends StandingsViewHolder {

        /**
         * Player number after which standings numbers font has to shrink.
         */
        static final int PLAYER_COUNT_FONT_FIRST_THRESHOLD = 8;

        /**
         * Player number after which standings numbers font has to shrink second time.
         */
        static final int PLAYER_COUNT_FONT_SECOND_THRESHOLD = 64;

        /**
         * Font shrink coefficient for first player count threshold.
         */
        static final double PLAYER_COUNT_FONT_FIRST_THRESHOLD_COEFFICIENT = 0.8;

        /**
         * Font shrink coefficient for second player count threshold.
         */
        static final double PLAYER_COUNT_FONT_SECOND_THRESHOLD_COEFFICIENT = 0.6;

        private IStandingsPlayerPresenter mPresenter;
        private TextView mPlaceView;
        private TextView mNameView;
        private TextView mStatusView;
        private LinearLayout mDetailsView;

        PlayerViewHolder(Activity context, View itemView) {
            super(context, itemView);
            this.mPlaceView = (TextView)itemView.findViewById(R.id.place_text_view);
            this.mNameView = (TextView)itemView.findViewById(R.id.player_name_text_view);
            this.mStatusView = (TextView)itemView.findViewById(R.id.player_status_text_view);
            this.mDetailsView = (LinearLayout)itemView.findViewById(R.id.player_details);
        }

        void setPresenter(int index, IStandingsItemPresenter presenter) {
            mPresenter = (IStandingsPlayerPresenter)presenter;

            mNameView.setText(mPresenter.getName());
            mStatusView.setText(mPresenter.getStatus());

            float textSize = mContext.getResources().getDimension(R.dimen.title_text_size);
            if (index > (PLAYER_COUNT_FONT_SECOND_THRESHOLD - 1)) {
                textSize = (float)(textSize * PLAYER_COUNT_FONT_SECOND_THRESHOLD_COEFFICIENT);
            } else if (index > (PLAYER_COUNT_FONT_FIRST_THRESHOLD - 1)) {
                textSize = (float)(textSize * PLAYER_COUNT_FONT_FIRST_THRESHOLD_COEFFICIENT);
            }

            mPlaceView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            mPlaceView.setText(StringUtils.intToOrdinal(index));

            // Fill details.
            LayoutInflater inflater = mContext.getLayoutInflater();
            LinearLayout.LayoutParams detailsParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            detailsParams.gravity = Gravity.CENTER;

            mDetailsView.removeAllViews();
            List<String> detailValues = mPresenter.getDetailsValues();
            for (int idx = 0; idx < detailValues.length(); idx++) {
                TextView valueView = (TextView)inflater.inflate(R.layout.player_details_view, null);
                valueView.setText(detailValues.index(idx));
                valueView.setLayoutParams(detailsParams);
                valueView.setPadding((int)mContext.getResources().getDimension(R.dimen.dp_2),
                        (int)mContext.getResources().getDimension(R.dimen.dp_2),
                        (int)mContext.getResources().getDimension(R.dimen.dp_2),
                        (int)mContext.getResources().getDimension(R.dimen.dp_2));
                mDetailsView.addView(valueView);
            }
        }
    }
}
