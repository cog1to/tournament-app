package ru.aint.mtg.tournament.tournamenthelper.mvp.views.about;

/**
 * Licenses view.
 */
public interface ILicensesView {

    /**
     * Sets list of log entries to show.
     *
     * @param items List of log entries to show.
     */
    void setItems(ILicenseItemPresenter[] items);
}
