package ru.aint.mtg.tournament.tournamenthelper.ui.callbacks;

import ru.aint.mtg.tournament.model.tournaments.Tournament;

/**
 * Tournament updated callback interface. Use for capturing tournament update event between views.
 */
public interface ITournamentUpdatedCallback {

    /**
     * Called when tournament was updated.
     *
     * @param tournament Updated tournament.
     */
    void onTournamentUpdated(Tournament tournament);
}
