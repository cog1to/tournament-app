package ru.aint.mtg.tournament.tournamenthelper.mvp.views;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.IDetailsSubPresenter;

/**
 * Tournament details view interface.
 */
public interface ITournamentDetailsView {

    /**
     * Shows loading view.
     */
    void showLoadingView();

    /**
     * Hides loading view.
     */
    void hideLoadingView();

    /**
     * Sets title.
     *
     * @param title New title.
     */
    void setTitle(String title);

    /**
     * Sets subtitle.
     *
     * @param subtitle New subtitle.
     */
    void setSubtitle(String subtitle);

    /**
     * Sets sub presenters.
     *
     * @param presenters Presenters to show as sub-views.
     */
    void setSubPresenters(List<IDetailsSubPresenter> presenters);
}
