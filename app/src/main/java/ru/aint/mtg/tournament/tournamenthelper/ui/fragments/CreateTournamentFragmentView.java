package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import fj.data.List;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.create.ICreateTournamentPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.create.ICreateTournamentView;
import ru.aint.mtg.tournament.tournamenthelper.ui.create.CreateTournamentView;

/**
 * Fragment decorator for Create tournament view.
 */
public class CreateTournamentFragmentView extends BaseActionBarFragment implements ICreateTournamentView {

    private View mView;

    private CreateTournamentView mCreateView;

    private ICreateTournamentPresenter mPresenter;

    public void setPresenter(ICreateTournamentPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public String getName() {
        return mCreateView.getName();
    }

    @Override
    public void setName(String name) {
        mCreateView.setName(name);
    }

    @Override
    public TournamentFormat getFormat() {
        return mCreateView.getFormat();
    }

    @Override
    public boolean isFormatSelected() {
        return mCreateView.isFormatSelected();
    }

    @Override
    public void setFormat(TournamentFormat format) {
        mCreateView.setFormat(format);
    }

    @Override
    public void setFormats(HashMap<TournamentFormat, String> formats) {
        mCreateView.setFormats(formats);
    }

    @Override
    public int getWinsPerMatch() {
        return mCreateView.getWinsPerMatch();
    }

    @Override
    public void setWinsPerMatch(int value) {
        mCreateView.setWinsPerMatch(value);
    }

    @Override
    public int getPointsPerMatchWin() {
        return mCreateView.getPointsPerMatchWin();
    }

    @Override
    public void setPointsPerMatchWin(int value) {
        mCreateView.setPointsPerMatchWin(value);
    }

    @Override
    public int getPointsPerMatchDraw() {
        return mCreateView.getPointsPerMatchDraw();
    }

    @Override
    public void setPointsPerMatchDraw(int value) {
        mCreateView.setPointsPerMatchDraw(value);
    }

    @Override
    public int getPointsPerGameWin() {
        return mCreateView.getPointsPerGameWin();
    }

    @Override
    public void setPointsPerGameWin(int value) {
        mCreateView.setPointsPerGameWin(value);
    }

    @Override
    public int getPointsPerGameDraw() {
        return mCreateView.getPointsPerGameDraw();
    }

    @Override
    public void setPointsPerGameDraw(int value) {
        mCreateView.setPointsPerGameDraw(value);
    }

    @Override
    protected String getTitle() {
        return getResources().getString(R.string.menu_new_tournament);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_create_view;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Override
    protected List<MenuItem> inflateMenuItems(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, R.id.action_new_tournament, Menu.FIRST, R.string.menu_new_tournament);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setIcon(R.drawable.ic_done_white_24dp);
        item.setActionView(R.layout.action_view_create);

        item.getActionView().setOnClickListener(view -> onMenuItemClick(item));
        return List.list(item);
    }

    protected boolean onMenuItemClick(MenuItem item) {
        if (mPresenter.canCreate()) {
            mPresenter.onCreateConfirmed();
        } else {
            Snackbar.make(mView, R.string.fix_all_errors, Snackbar.LENGTH_LONG)
                    .show();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mView = view;

        View createView = view.findViewById(R.id.create_tournament_view);
        mCreateView = new CreateTournamentView(getActivity(), createView, mPresenter);

        setHasOptionsMenu(true);

        return view;
    }
}
