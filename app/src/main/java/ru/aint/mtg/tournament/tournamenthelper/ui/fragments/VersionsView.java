package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.IVersionsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IVersionsView;

/**
 * Versions view implementation.
 */
public class VersionsView extends BaseActionBarFragment implements IVersionsView {

    IVersionsPresenter mPresenter;

    /**
     * Version text view.
     */
    private TextView mVersionView;

    /**
     * Build text view.
     */
    private TextView mBuildView;

    /**
     * Changelog.
     */
    private TextView mChangelogView;

    /**
     * Sets presenter.
     *
     * @param presenter Presenter.
     */
    public void setPresenter(IVersionsPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setVersion(String version) {
        mVersionView.setText(version);
    }

    @Override
    public void setBuild(String build) {
        mBuildView.setText(build);
    }

    @Override
    public void setChangelog(String changelog) {
        mChangelogView.setText(changelog);
    }

    @Override
    protected String getTitle() {
        return MainApplication.getInstance().getString(R.string.version);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_version;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mChangelogView = (TextView) view.findViewById(R.id.changelog_text);
        mBuildView = (TextView) view.findViewById(R.id.build_text);
        mVersionView = (TextView) view.findViewById(R.id.version_text);

        if (mPresenter != null) {
            mPresenter.onCreateView(this);
        }

        return view;
    }
}
