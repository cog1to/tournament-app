package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details;

import android.view.View;

import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.JsonPlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDetailsSubView;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IStandingsView;

/**
 * Details sub view presenter.
 */
public interface IDetailsSubPresenter {

    enum DetailsType {
        Standings,
        Events
    }

    /**
     * Returns presenter type.
     *
     * @return Presenter type.
     */
    DetailsType getType();

    /**
     * Sets view.
     *
     * @param view View.
     */
    void setView(IDetailsSubView view);
}
