package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDropOutsView;
import ru.aint.mtg.tournament.tournamenthelper.views.DividerItemDecoration;

import static ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutsPresenter.MenuItemType.Delete;
import static ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutsPresenter.MenuItemType.Done;

/**
 * Drop outs fragment view.
 */
public class DropOutsView extends BaseActionBarFragment implements IDropOutsView, MenuItem.OnMenuItemClickListener {

    /**
     * Recycler view.
     */
    RecyclerView mPlayersView;

    /**
     * Adapter.
     */
    DropOutsAdapter mAdapter;

    /**
     * Presenter.
     */
    IDropOutsPresenter mPresenter;

    /**
     * Editable flag.
     */
    boolean mEditable;

    /**
     * Activated cell color.
     */
    private static int sActiveColor;

    /**
     * Default cell color.
     */
    private static int sClearColor;

    /**
     * Selected image.
     */
    private static Drawable sSelectedImage;

    /**
     * Default image.
     */
    private static Drawable sDefaultImage;

    @Override
    public void setItems(IDropOutItemPresenter[] items) {
        if (mAdapter == null) {
            mAdapter = new DropOutsAdapter(getActivity(), mPresenter);
        }
        mPlayersView.setAdapter(mAdapter);
        mAdapter.setPresenters(items, mEditable);
    }

    @Override
    public void reloadItemAt(int index, IDropOutItemPresenter item) {
        mAdapter.reloadPresenter(index, item);
    }

    @Override
    protected String getTitle() {
        return getActivity().getString(R.string.drop_players);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_drops;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Override
    public void setPresenter(IDropOutsPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setEditable(boolean editable) {
        mEditable = editable;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        sActiveColor = ContextCompat.getColor(getActivity(), R.color.colorPlayerSelected);
        sClearColor = ContextCompat.getColor(getActivity(), android.R.color.transparent);
        sSelectedImage = ContextCompat.getDrawable(getActivity(), R.drawable.ic_done_white_24dp);
        sDefaultImage = ContextCompat.getDrawable(getActivity(), R.drawable.ic_account_circle_white_24dp);

        mPlayersView = (RecyclerView)view.findViewById(R.id.players_recycler_view);
        mPlayersView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mPlayersView.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        mPlayersView.setItemAnimator(new MyAnimator(this, getActivity()));

        if (mPresenter != null) {
            if (mPresenter.onMenuInflated() != null) {
                setHasOptionsMenu(true);
            }
            mPresenter.onViewCreated(this);
        }
        return view;
    }

    @Override
    protected List<MenuItem> inflateMenuItems(Menu menu) {
        IDropOutsPresenter.MenuItemType[] menuTypes = mPresenter.onMenuInflated();
        fj.data.List<MenuItem> items = List.list();

        for (IDropOutsPresenter.MenuItemType itemType: menuTypes) {
            MenuItem item = null;
            switch (itemType) {
                case Done:
                    item = menu.add(Menu.NONE, R.id.action_registration_done, Menu.FIRST, R.string.menu_done);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_done_white_24dp);
                    item.setActionView(R.layout.action_view_done);
                    break;
                case Delete:
                    item = menu.add(Menu.NONE, R.id.action_delete_event, Menu.FIRST, R.string.menu_delete_event);
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    item.setIcon(R.drawable.ic_delete_white_24dp);
                    item.setActionView(R.layout.action_view_delete);
                    break;
                default:
                    break;
            }
            items = items.cons(item);
        }

        items.foreach(item -> {
            item.getActionView().setOnClickListener(view -> onMenuItemClick(item));
            return Unit.unit();
        });

        return items;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_registration_done) {
            mPresenter.onMenuItemTapped(Done);
            return true;
        } else if (item.getItemId() == R.id.action_delete_event) {
            mPresenter.onMenuItemTapped(Delete);
            return true;
        }

        return false;
    }

    @Override
    public boolean onBackPressed() {
        return mPresenter.onBackPressed();
    }

    private abstract static class DropOutsViewHolder extends RecyclerView.ViewHolder {

        View mView;

        DropOutsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        abstract void setPresenter(int position, IDropOutItemPresenter presenter);
    }

    private static class PlayerViewHolder extends DropOutsViewHolder {

        IDropOutItemPresenter mPresenter;

        private ImageView mIcon;
        private TextView mPlayerName;
        private LinearLayout mPlayerIconLayout;

        PlayerViewHolder(View itemView) {
            super(itemView);

            mIcon = (ImageView)itemView.findViewById(R.id.player_icon);
            mPlayerName = (TextView)itemView.findViewById(R.id.player_name);
            mPlayerIconLayout = (LinearLayout)itemView.findViewById(R.id.player_icon_layout);
        }

        @Override
        void setPresenter(int position, IDropOutItemPresenter presenter) {
            mPresenter = presenter;

            mPlayerName.setText(mPresenter.getName());

            if (mPresenter.getSelected()) {
                mIcon.setImageDrawable(sSelectedImage);
                mIcon.setBackgroundResource(R.drawable.circle_grey_shape);
                mView.setBackgroundColor(sActiveColor);
            } else {
                mIcon.setImageDrawable(sDefaultImage);
                mIcon.setBackgroundResource(R.drawable.accent_circle_shape);
                mView.setBackgroundColor(sClearColor);
            }
        }
    }

    private static class DropOutsAdapter extends RecyclerView.Adapter<DropOutsViewHolder> {

        IDropOutItemPresenter[] mPresenters;
        Activity mContext;
        boolean mEditable;
        IDropOutsPresenter mPresenter;

        DropOutsAdapter(@NonNull Activity context, @NonNull IDropOutsPresenter presenter) {
            super();
            mContext = context;
            mPresenter = presenter;
        }

        void setPresenters(IDropOutItemPresenter[] presenters, boolean editable) {
            mEditable = editable;
            mPresenters = presenters;
            notifyDataSetChanged();
        }

        void reloadPresenter(int position, IDropOutItemPresenter presenter) {
            mPresenters[position] = presenter;
            notifyItemChanged(position);
        }

        IDropOutItemPresenter getPresenter(int position) {
            return mPresenters[position];
        }

        @Override
        public DropOutsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mContext.getLayoutInflater().inflate(R.layout.list_player_item, parent, false);
            return new PlayerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DropOutsViewHolder holder, int position) {
            IDropOutItemPresenter presenter = mPresenters[position];
            holder.setPresenter(position, presenter);
            if (mEditable) {
                holder.itemView.setOnClickListener(view -> {
                    if (mPresenter != null) {
                        mPresenter.onItemTapped(position);
                    }
                });
            } else {
                holder.itemView.setOnClickListener(null);
            }
        }

        @Override
        public int getItemCount() {
            if (mPresenters == null) {
                return 0;
            }

            return mPresenters.length;
        }
    }

    private static class MyAnimator extends DefaultItemAnimator {

        DropOutsView mProvider;

        Activity mContext;

        public MyAnimator(@NonNull DropOutsView provider, Activity context) {
            mProvider = provider;
            mContext = context;
        }

        @Override
        public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
            // This allows our custom change animation on the contents of the holder instead
            // of the default behavior of replacing the viewHolder entirely
            return true;
        }

        @Override
        public boolean animateChange(@NonNull final RecyclerView.ViewHolder oldHolder,
                                     @NonNull final RecyclerView.ViewHolder newHolder,
                                     @NonNull ItemHolderInfo preInfo, @NonNull ItemHolderInfo postInfo) {

            int startColor, endColor, startIconBackground, endIconBackground;
            Drawable startDrawable, endDrawable;
            if (mProvider.isItemSelected(oldHolder.getAdapterPosition())) {
                startColor = sClearColor;
                endColor = sActiveColor;
                startIconBackground = R.drawable.accent_circle_shape;
                endIconBackground = R.drawable.circle_grey_shape;
                startDrawable = sDefaultImage;
                endDrawable = sSelectedImage;
            } else {
                startColor = sActiveColor;
                endColor = sClearColor;
                startIconBackground = R.drawable.circle_grey_shape;
                endIconBackground = R.drawable.accent_circle_shape;
                startDrawable = sSelectedImage;
                endDrawable = sDefaultImage;
            }

            ObjectAnimator fadeToBlack = ObjectAnimator.ofInt(oldHolder.itemView, "backgroundColor", startColor, endColor);
            fadeToBlack.setDuration(mContext.getResources().getInteger(R.integer.animation_duration));
            fadeToBlack.setEvaluator(new ArgbEvaluator());
            fadeToBlack.start();

            ObjectAnimator rotation1 = ObjectAnimator.ofFloat(((PlayerViewHolder)oldHolder).mPlayerIconLayout, View.ROTATION_Y, 0.0f, 90.0f);
            rotation1.setDuration(mContext.getResources().getInteger(R.integer.half_animation_duration));
            rotation1.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    ((PlayerViewHolder)oldHolder).mIcon.setBackgroundResource(startIconBackground);
                    ((PlayerViewHolder) oldHolder).mIcon.setImageDrawable(startDrawable);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    ((PlayerViewHolder)oldHolder).mIcon.setBackgroundResource(endIconBackground);
                    ((PlayerViewHolder) oldHolder).mIcon.setImageDrawable(endDrawable);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            ObjectAnimator rotation2 = ObjectAnimator.ofFloat(((PlayerViewHolder)oldHolder).mPlayerIconLayout, View.ROTATION_Y, 90.0f, 0.0f);
            rotation2.setDuration(mContext.getResources().getInteger(R.integer.half_animation_duration));

            AnimatorSet set = new AnimatorSet();
            set.playSequentially(rotation1, rotation2);
            set.start();

            return false;
        }
    }

    private boolean isItemSelected(int position) {
        return mAdapter.getPresenter(position).getSelected();
    }
}
