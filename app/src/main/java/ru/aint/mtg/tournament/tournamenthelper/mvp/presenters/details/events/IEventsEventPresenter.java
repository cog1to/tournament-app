package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events;

import android.support.annotation.DrawableRes;

/**
 * Event presenter.
 */
public interface IEventsEventPresenter extends IEventsItemPresenter {
    String getTitle();
    String getSubtitle();
    @DrawableRes int getIconResource();
}
