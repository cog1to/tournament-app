package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events;

import android.support.annotation.NonNull;

import org.atteo.evo.inflector.English;

import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.IEventsEventPresenter;

/**
 * Event list item presenter.
 */
class EventsEventPresenter implements IEventsEventPresenter {

    private Event mEvent;

    private String mEventName;

    EventsEventPresenter(Event event) {
        mEvent = event;
    }

    EventsEventPresenter(@NonNull Event event, String eventName) {
        this(event);
        mEventName = eventName;
    }

    Event getEvent() {
        return mEvent;
    }

    @Override
    public String getTitle() {
        if (mEventName != null && !mEventName.isEmpty()) {
            return mEventName;
        } else {
            String name = mEvent.getName();
            if (name != null && !name.isEmpty()) {
                return name;
            }

            if (mEvent instanceof Registration) {
                return MainApplication.getInstance().getString(R.string.registration);
            } else if (mEvent instanceof Round) {
                return MainApplication.getInstance().getString(R.string.round);
            } else if (mEvent instanceof Archive) {
                return MainApplication.getInstance().getString(R.string.archived);
            } else if (mEvent instanceof Drop) {
                return MainApplication.getInstance().getString(R.string.drop_outs);
            }
        }

        return null;
    }

    @Override
    public String getSubtitle() {
        if (mEvent instanceof Registration) {
            int numberOfPlayers = ((Registration)mEvent).getPlayers().length();
            return String.format("%d %s", numberOfPlayers, English.plural(MainApplication.getInstance().getString(R.string.player), numberOfPlayers));
        } else if (mEvent instanceof Round) {
            int numberOfMatches = ((Round)mEvent).getMatches().length();
            int numberOfMatchesInProgress = ((Round)mEvent).getMatches().filter(m -> !m.isFinished() && m.isReady()).length();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(numberOfMatches + " " + English.plural(MainApplication.getInstance().getString(R.string.match), numberOfMatches));
            if (numberOfMatchesInProgress > 0) {
                stringBuilder.append(" (" + numberOfMatchesInProgress + " " + MainApplication.getInstance().getString(R.string.in_progress) + ")");
            }

            return stringBuilder.toString();
        } else if (mEvent instanceof Drop) {
            int numberOfPlayers = ((Drop)mEvent).getPlayers().length();
            return String.format("%d %s %s", numberOfPlayers, English.plural(MainApplication.getInstance().getString(R.string.player), numberOfPlayers), MainApplication.getInstance().getString(R.string.dropped));
        }

        return null;
    }

    @Override
    public int getIconResource() {
        if (mEvent instanceof Registration) {
            return R.drawable.ic_toc_black_24dp;
        } else if (mEvent instanceof Round) {
            return R.drawable.ic_dice_white;
        } else if (mEvent instanceof Drop) {
            return R.drawable.ic_exit_to_app_white_24dp;
        } else if (mEvent instanceof Archive) {
            return R.drawable.ic_archive_white_24dp;
        }

        return R.drawable.ic_dice_white;
    }
}
