package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events;

/**
 * Action item interface for Events list.
 */
public interface IEventsActionItemPresenter extends IEventsItemPresenter {

    enum ActionType {
        Start,
        End,
        NextRound,
        Drop
    }

    ActionType getType();

    String getName();
}
