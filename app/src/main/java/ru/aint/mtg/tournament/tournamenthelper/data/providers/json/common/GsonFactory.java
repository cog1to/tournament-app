package ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Map;

/**
 * Gson factory class for JSON serialization.
 */
public class GsonFactory {
    public static Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(List.class, new FJListAdapter());
        gsonBuilder.registerTypeAdapter(Event.class, new EventAdapter());
//        gsonBuilder.registerTypeAdapter(Drop.class, new EventAdapter());
//        gsonBuilder.registerTypeAdapter(Registration.class, new EventAdapter());
//        gsonBuilder.registerTypeAdapter(Round.class, new EventAdapter());
//        gsonBuilder.registerTypeAdapter(Archive.class, new EventAdapter());
//        gsonBuilder.registerTypeAdapter(Match.class, new EventAdapter());
        gsonBuilder.registerTypeAdapter(new TypeToken<Map<Player, Option<Integer>>>() {}.getType(), new ScoresAdapter());
        gsonBuilder.registerTypeAdapter(new TypeToken<Map<Player, Boolean>>() {}.getType(), new PlayersMapAdapter());
        gsonBuilder.serializeNulls();
        return gsonBuilder.create();
    }
}
