package ru.aint.mtg.tournament.tournamenthelper.ui.callbacks;

/**
 * Confirm callback interface. Use for capturing confirm/cancel action results.
 */
public interface IConfirmCallback {
    /**
     * Called when some action was confirmed or canceled.
     *
     * @param confirmed Flag indicating actino result.
     */
    void onActionSelected(boolean confirmed);
}
