package ru.aint.mtg.tournament.tournamenthelper;

import android.app.Application;

import com.crittercism.app.Crittercism;
import com.crittercism.app.CrittercismConfig;

import ru.aint.mtg.tournament.tournamenthelper.data.files.InternalStorageFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.log.Logger;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BaseDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.JsonDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.navigation.IRouter;
import ru.aint.mtg.tournament.tournamenthelper.test.FakeDataProvider;

/**
 * Custom application class.
 */
public class MainApplication extends Application {

    /**
     * Static instance of an application.
     */
    private static MainApplication instance;

    /**
     * Data provider.
     */
    private BaseDataProvider dataProvider;

    /**
     * Router.
     */
    private IRouter router;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (!BuildConfig.DEBUG) {
            CrittercismConfig config = new CrittercismConfig();
            config.setVersionCodeToBeIncludedInVersionString(true);
            Crittercism.initialize(getApplicationContext(), "3894e68e574c458fa637e33fcc1fa38d00555300", config);
        } else {
            Logger.init(this);
        }

        reset();
    }

    /**
     * Returns application object instance.
     *
     * @return Instance of Application class.
     */
    public static MainApplication getInstance() {
        return instance;
    }

    /**
     * Gets data provider.
     *
     * @return Data provider.
     */
    public BaseDataProvider getDataProvider() {
        return this.dataProvider;
    }

    /**
     * Sets router.
     *
     * @param newRouter Router instance.
     */
    public void setRouter(IRouter newRouter) {
        router = newRouter;
    }

    /**
     * Returns router.
     *
     * @return Router instance.
     */
    public IRouter getRouter() {
        return router;
    }

    /**
     * Resets all data.
     */
    public void reset() {
        dataProvider = new JsonDataProvider(this, new InternalStorageFileManager(this));
    }
}
