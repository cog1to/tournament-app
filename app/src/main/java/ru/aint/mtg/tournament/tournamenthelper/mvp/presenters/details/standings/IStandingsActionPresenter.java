package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings;

/**
 * Action item presenter.
 */
public interface IStandingsActionPresenter extends IStandingsItemPresenter {

    enum ActionType {
        AddParticipant,
        StartTournament
    }

    String getName();
    ActionType getType();
}
