package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.details.IPlayerDetailsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.player.IPlayerDetailsView;

/**
 * Player details view implementation.
 */
public class PlayerDetailsView extends BaseActionBarFragment implements IPlayerDetailsView {

    String mTitle;

    IPlayerDetailsPresenter mPresenter;

    View mView;

    String X_OUT_OF_Y;
    String X_OUT_OF_Y_NO_ZEROES;
    String PERCENTAGE;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        X_OUT_OF_Y = context.getResources().getString(R.string.x_out_of_y);
        X_OUT_OF_Y_NO_ZEROES = context.getResources().getString(R.string.x_out_of_y_float_no_zeroes);
        PERCENTAGE = "%.2f";
    }

    public void setPresenter(IPlayerDetailsPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setMatchStatistics(int total, int won, int lost, int drew) {
        TextView matchesWonView = (TextView) mView.findViewById(R.id.matches_won_value);
        matchesWonView.setText(String.format(X_OUT_OF_Y, won, total));

        TextView matchesLostView = (TextView) mView.findViewById(R.id.matches_lost_value);
        matchesLostView.setText(String.format(X_OUT_OF_Y, lost, total));

        TextView matchesDrewView = (TextView) mView.findViewById(R.id.matches_drew_value);
        matchesDrewView.setText(String.format(X_OUT_OF_Y, drew, total));
    }

    @Override
    public void setGameStatistics(int total, int won, int lost, int drew) {
        TextView gamesWonView = (TextView) mView.findViewById(R.id.games_won_value);
        gamesWonView.setText(String.format(X_OUT_OF_Y, won, total));

        TextView gamesLostView = (TextView) mView.findViewById(R.id.games_lost_value);
        gamesLostView.setText(String.format(X_OUT_OF_Y, lost, total));

        TextView gamesDrewView = (TextView) mView.findViewById(R.id.games_drew_value);
        gamesDrewView.setText(String.format(X_OUT_OF_Y, drew, total));
    }

    @Override
    public void setMatchPoints(int point, int maxPoints) {
        TextView gamesWonView = (TextView) mView.findViewById(R.id.match_points_value);
        gamesWonView.setText(String.format(X_OUT_OF_Y, point, maxPoints));
    }

    @Override
    public void setMatchWinPercentage(double percentage) {
        TextView gamesWonView = (TextView) mView.findViewById(R.id.match_win_percentage_value);
        gamesWonView.setText(String.format(PERCENTAGE, percentage));
    }

    @Override
    public void setGamePoints(int point, int maxPoints) {
        TextView gamesWonView = (TextView) mView.findViewById(R.id.game_points_value);
        gamesWonView.setText(String.format(X_OUT_OF_Y, point, maxPoints));
    }

    @Override
    public void setGameWinPercentage(double percentage) {
        TextView gamesWonView = (TextView) mView.findViewById(R.id.game_win_percentage_value);
        gamesWonView.setText(String.format(PERCENTAGE, percentage));
    }

    @Override
    protected String getTitle() {
        return mTitle;
    }

    @Override
    public void setTitle(String title) {
        mTitle = title;
        mToolbar.setTitle(mTitle);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_player_details;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mView = view;

        if (mPresenter != null) {
            mPresenter.onCreateView(this);
        }

        return view;
    }
}
