package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.ITournamentListView;

/**
 * Tournament list presenter.
 */
public interface ITournamentListPresenter {

    enum MenuItemType {
        Delete,
        Archive,
        Unarchive,
        Create
    }

    enum HistoryMode {
        Current,
        Archive
    }

    /**
     * Called when view is created.
     *
     * @param view Created view.
     */
    void onCreateView(@NonNull ITournamentListView view);

    /**
     * Called when view is destroyed.
     */
    void onDestroyView();

    /**
     * Returns title resource ID.
     *
     * @return title string ID.
     */
    @StringRes int getTitle();

    /**
     * Returns available menu items.
     *
     * @return Array of available menu items.
     */
    MenuItemType[] onMenuInflated();

    /**
     * Returns a presenter for a given item.
     *
     * @param index Item index.
     *
     * @return Presenter for item at given index.
     */
    @NonNull ITournamentListItemPresenter getItemAt(int index);

    /**
     * Called when item is tapped.
     *
     * @param itemIndex Index of item.
     */
    void onItemTapped(int itemIndex);

    /**
     * Called when item is long tapped.
     *
     * @param itemIndex Index of the tapped item.
     *
     * @return Flag indicating whether long tap was handled.
     */
    boolean onItemLongTapped(int itemIndex);

    /**
     * Called when Delete action is activated.
     */
    void onDelete();

    /**
     * Called when Archive action is activated.
     */
    void onArchive();

    /**
     * Called when unarchive action is activated.
     */
    void onUnarchive();

    /**
     * Called when Back button is pressed.
     */
    boolean onBack();

    /**
     * Called when create tournament menu item is selected.
     */
    void onCreateTournament();
}
