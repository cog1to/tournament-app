package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.standings.IStandingsPlayerPresenter;

/**
 * Standings view interface.
 */
public interface IStandingsView extends IDetailsSubView {

    /**
     * Edit player model.
     */
    class EditPlayerModel {

        /**
         * Full name value.
         */
        private String fullName;

        /**
         * Creates new instance of EditPlayerModel.
         *
         * @param fullName Player's name.
         */
        public EditPlayerModel(String fullName) {
            this.fullName = fullName;
        }

        /**
         * Returns player's name.
         *
         * @return Player's name.
         */
        public String getFullName() {
            return this.fullName;
        }

        /**
         * Sets player's name.
         *
         * @param fullName New name value.
         */
        public void setFullName(String fullName) {
            this.fullName = fullName;
        }
    }

    void showStandings(List<IStandingsItemPresenter> items);
    void showPlayerEditingOptions(String title, IStandingsPlayerPresenter presenter, boolean canDelete);
    void showNewPlayerDialog();
    void showEditPlayerDialog(IStandingsPlayerPresenter presenter, EditPlayerModel model);

    /// List manipulations.

    void reloadItem(int index, IStandingsItemPresenter item);
    void addItem(int index, IStandingsItemPresenter item);
    void removeItem(int index);
}
