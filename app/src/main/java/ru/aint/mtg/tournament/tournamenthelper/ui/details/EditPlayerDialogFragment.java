package ru.aint.mtg.tournament.tournamenthelper.ui.details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import fj.data.List;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IStandingsView;

/**
 * Edit player info dialog fragment.
 */
public class EditPlayerDialogFragment extends AppCompatDialogFragment {

    /**
     * Dialog event listener interface.
     */
    public interface Listener {

        /**
         * Returns new player data from dialog when player confirms editing.
         *
         * @param player New player data.
         * @param existingPlayer Existing player data.
         */
        void onPlayerEdited(IStandingsView.EditPlayerModel player, IStandingsView.EditPlayerModel existingPlayer);

        /**
         * Asks if current editing should be allowed.
         *
         * @param player New player data.
         * @param existingPlayer Previous player data.
         *
         * @return <b>true</b> if editing should be allowed, <b>false</b> otherwise.
         */
        boolean shouldAllowPlayerEditing(IStandingsView.EditPlayerModel player, IStandingsView.EditPlayerModel existingPlayer);
    }

    /**
     * Dialog listener.
     */
    private Listener listener;

    /**
     * First name edit.
     */
    private AppCompatAutoCompleteTextView fullNameText;

    /**
     * Instance of the dialog that is presented.
     */
    private AlertDialog dialog;

    /**
     * Existing player (in case dialog is for editing existing entry).
     */
    private IStandingsView.EditPlayerModel existingPlayer;

    /**
     * Suggestions provider.
     */
    public BasePlayerSuggestionsProvider suggestionsProvider;

    /**
     * Sets dialog listener.
     *
     * @param listener Dialog listener.
     */
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }

    /**
     * Sets suggestions provider.
     *
     * @param provider Suggestions provider.
     */
    public void setSuggestionsProvider(BasePlayerSuggestionsProvider provider) {
        this.suggestionsProvider = provider;
    }

    /**
     * Sets existing player.
     *
     * @param player Existing player to edit.
     */
    public void setExistingPlayer(IStandingsView.EditPlayerModel player) {
        this.existingPlayer = player;

        if (player == null) {
            return;
        }

        if (this.fullNameText != null) {
            this.fullNameText.setText(player.getFullName());
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if ((fullNameText.getText().toString().trim().length() > 0)) {
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                } else {
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }

                if (editable.toString().contains("\n")) {
                    int newLine = editable.toString().indexOf('\n');
                    editable.delete(newLine, newLine + 1);
                }
            }
        };

        View content = inflater.inflate(R.layout.dialog_new_player, null);

        this.fullNameText = (AppCompatAutoCompleteTextView)content.findViewById(R.id.dialog_full_name_text);
        this.fullNameText.addTextChangedListener(textWatcher);

        try {
            if (this.suggestionsProvider != null) {
                List<String> suggestions = this.suggestionsProvider.getPlayerSuggestions();
                if (suggestions.length() > 0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, suggestions.toJavaList().toArray(new String[0]));
                    fullNameText.setAdapter(adapter);
                }
            }
        } catch (IOException exeption) {

        }

        builder.setView(content)
                .setPositiveButton(R.string.menu_done, null)
                .setNegativeButton(R.string.menu_cancel, null);

        Bundle arguments = getArguments();
        if (arguments != null && arguments.getString("title") != null) {
            builder.setTitle(arguments.getString("title"));
        }

        dialog = builder.create();

        dialog.setOnShowListener(dialogInterface -> {
            Button positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positive.setOnClickListener(view -> {
                if (listener != null) {
                    IStandingsView.EditPlayerModel newPlayer = new IStandingsView.EditPlayerModel(fullNameText.getText().toString().trim());
                    if (listener.shouldAllowPlayerEditing(newPlayer, existingPlayer)) {
                        listener.onPlayerEdited(newPlayer, existingPlayer);
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), R.string.player_already_registered, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (existingPlayer != null) {
            this.fullNameText.setText(existingPlayer.getFullName());
        } else {
            dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
    }
}