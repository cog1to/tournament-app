package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.ILicensesPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.ILicenseItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.ILicensesView;

/**
 * Licenses view implementation.
 */
public class LicensesView extends BaseActionBarFragment implements ILicensesView {

    RecyclerView mLicensesRecyclerView;

    ILicensesPresenter mPresenter;

    public void setPresenter(ILicensesPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setItems(@NonNull ILicenseItemPresenter[] items) {
        mLicensesRecyclerView.setAdapter(new LicenseAdapter(getActivity(), items));
    }

    @Override
    protected String getTitle() {
        return MainApplication.getInstance().getString(R.string.licenses);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_licenses;
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mLicensesRecyclerView = (RecyclerView) view.findViewById(R.id.licenses_view);
        mLicensesRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        if (mPresenter != null) {
            mPresenter.onCreateView(this);
        }

        return view;
    }

    private static class LicenseAdapter extends RecyclerView.Adapter<LicenseViewHolder> {

        @NonNull ILicenseItemPresenter[] mPresenters;

        @NonNull Activity mContext;

        private LicenseAdapter(@NonNull Activity context, @NonNull ILicenseItemPresenter[] presenters) {
            mPresenters = presenters;
            mContext = context;
        }

        @Override
        public LicenseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new LicenseViewHolder(mContext.getLayoutInflater().inflate(R.layout.list_title_subtitle_vertical_item, parent, false), mContext);
        }

        @Override
        public void onBindViewHolder(LicenseViewHolder holder, int position) {
            holder.setPresenter(mPresenters[position]);
        }

        @Override
        public int getItemCount() {
            return mPresenters.length;
        }
    }

    private static class LicenseViewHolder extends RecyclerView.ViewHolder {

        ILicenseItemPresenter mPresenter;

        TextView mTitleView;

        TextView mSubtitleView;

        Activity mContext;

        private LicenseViewHolder(View itemView, Activity context) {
            super(itemView);
            mTitleView = (TextView)itemView.findViewById(R.id.title_text_view);
            mTitleView.setMovementMethod(LinkMovementMethod.getInstance());
            mSubtitleView = (TextView)itemView.findViewById(R.id.subtitle_text_view);
            mContext = context;
        }

        public void setPresenter(@NonNull ILicenseItemPresenter presenter) {
            mPresenter = presenter;

            setTextViewHTML(mTitleView, String.format("%s <a href=\"%s\" style=\"text-decoration:none;\">(%s)</a>", presenter.getTitle(), presenter.getLink(), MainApplication.getInstance().getString(R.string.link)));
            mSubtitleView.setText(presenter.getDisclaimer());
        }

        /**
         * Helper method to make all links clickable inside span.
         *
         * @param strBuilder string builder.
         * @param span URL span.
         */
        private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span)
        {
            int start = strBuilder.getSpanStart(span);
            int end = strBuilder.getSpanEnd(span);
            int flags = strBuilder.getSpanFlags(span);
            ClickableSpan clickable = new ClickableSpan() {
                public void onClick(View view) {
                    try {
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(span.getURL()));
                        mContext.startActivity(myIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MainApplication.getInstance(), "No application can handle this request.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            };
            strBuilder.setSpan(clickable, start, end, flags);
            strBuilder.removeSpan(span);
        }

        /**
         * Helper method to linkify text inside text view.
         *
         * @param text TextView to linkify.
         * @param html Text to put inside text view.
         */
        private void setTextViewHTML(TextView text, String html)
        {
            CharSequence sequence = Html.fromHtml(html);
            SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
            URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
            for(URLSpan span : urls) {
                makeLinkClickable(strBuilder, span);
            }
            text.setText(strBuilder);
            text.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }
}
