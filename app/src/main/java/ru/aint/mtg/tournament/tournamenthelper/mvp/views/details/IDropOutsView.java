package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutsPresenter;

/**
 * Drop outs view.
 */
public interface IDropOutsView {
    void setItems(IDropOutItemPresenter[] items);
    void reloadItemAt(int index, IDropOutItemPresenter item);
    void setPresenter(IDropOutsPresenter presenter);
    void setEditable(boolean editable);
}
