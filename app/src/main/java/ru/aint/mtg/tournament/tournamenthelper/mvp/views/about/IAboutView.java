package ru.aint.mtg.tournament.tournamenthelper.mvp.views.about;

/**
 * About view.
 */
public interface IAboutView {
    void setItems(IAboutItemPresenter[] presenters);
}
