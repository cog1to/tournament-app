package ru.aint.mtg.tournament.tournamenthelper.data.providers.json;

import android.content.Context;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.data.files.IFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.BasePlayerSuggestionsProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common.GsonFactory;

/**
 * JSON implementation of player suggestions.
 */
public class JsonPlayerSuggestionsProvider extends BasePlayerSuggestionsProvider {

    /**
     * Suggestions list file name.
     */
    private final String sSuggestionsFileName = "suggestions.json";

    /**
     * File manager instance.
     */
    private IFileManager fileManager;

    /**
     * Returns new instance of JsonPlayerSuggestionsProvider.
     *
     * @param context Application context.
     * @param fileManager File manager.
     */
    public JsonPlayerSuggestionsProvider(Context context, IFileManager fileManager) {
        super(context);
        this.fileManager = fileManager;
    }

    @Override
    public List<String> getPlayerSuggestions() throws IOException {
        try {
            if (this.fileManager.fileExistsInDataDir(sSuggestionsFileName)) {
                // Get file content.
                InputStream inputStream = this.fileManager.getInputStreamFromDataDirPath(sSuggestionsFileName);
                StringBuilder content = new StringBuilder();

                byte[] buffer = new byte[1024];
                int n = inputStream.read(buffer);
                while (n != -1) {
                    content.append(new String(buffer, 0, n));
                    n = inputStream.read(buffer);
                }
                inputStream.close();

                // Deserialize.
                Type listType = new TypeToken<List<String>>() {}.getType();
                List<String> list = GsonFactory.gson().fromJson(content.toString(), listType);
                return (list != null) ? list : List.list();
            } else {
                return List.list();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void setPlayerSuggestions(List<String> suggestions) throws IOException {
        try {
            // Serialize
            Type listType = new TypeToken<List<String>>(){}.getType();
            String content = GsonFactory.gson().toJson(suggestions, listType);

            // Write to file.
            OutputStream outputStream = this.fileManager.getOutputStreamFromDataDirPath(sSuggestionsFileName, true);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<String> addSuggestion(String suggestion) throws IOException {
        List<String> suggestions = getPlayerSuggestions();
        if (!suggestions.exists(s -> s.equals(suggestion))) {
            suggestions = suggestions.cons(suggestion);
        }

        setPlayerSuggestions(suggestions);
        return suggestions;
    }

    @Override
    public List<String> replaceSuggestion(String old, String newSuggestion) throws IOException {
        List<String> suggestions = getPlayerSuggestions();
        suggestions = suggestions.filter(s -> !s.equals(old)).cons(newSuggestion);

        setPlayerSuggestions(suggestions);
        return suggestions;
    }

    @Override
    public void deleteAllData() throws IOException{
        try {
            if (this.fileManager.fileExistsInDataDir(sSuggestionsFileName)) {
                this.fileManager.deleteFileFromDataDirPath(sSuggestionsFileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
