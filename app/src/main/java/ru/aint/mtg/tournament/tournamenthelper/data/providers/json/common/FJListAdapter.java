package ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import fj.Unit;
import fj.data.List;

/**
 * JSON adapter for fj.data.List objects.
 */
public class FJListAdapter implements JsonSerializer<List>, JsonDeserializer<List<?>> {
    @Override
    public List deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Type[] typeParameters = ((ParameterizedType)typeOfT).getActualTypeArguments();
        Type elementsType = typeParameters[0];

        List list = List.list();
        JsonArray array = (JsonArray)json;
        for (JsonElement elem : array) {
            Object deserialized = GsonFactory.gson().fromJson(elem, elementsType);
            list = list.cons(deserialized);
        }

        return list.reverse();
    }

    @Override
    public JsonElement serialize(List src, Type typeOfSrc, JsonSerializationContext context) {
        Type[] typeParameters = ((ParameterizedType)typeOfSrc).getActualTypeArguments();
        Type elementsType = typeParameters[0];

        Gson gson = GsonFactory.gson();
        TypeAdapter adapter = gson.getAdapter((Class)elementsType);

        JsonArray jsonArray = new JsonArray();
        List<JsonElement> elements = src.map(f -> {
            if (adapter != null) {
                return adapter.toJsonTree(f);
            }

            return GsonFactory.gson().toJsonTree(f);
        });
        elements.foreach(f -> {jsonArray.add(f); return Unit.unit();});

        return jsonArray;
    }
}
