package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;

/**
 * Spinner dialog fragment.
 */
public class SpinnerDialogFragment extends AppCompatDialogFragment {

    /**
     * Listener interface.
     */
    public interface Listener {
        void onItemSelected(int index);
    }

    /**
     * Presented dialog instance.
     */
    private AlertDialog dialog;

    /**
     * Items to show.
     */
    private String[] mItems;

    /**
     * Listener.
     */
    private Listener mListener;

    /**
     * Sets items to show.
     *
     * @param items Items to show.
     */
    public void setItems(String[] items) {
        mItems = items;
    }

    /**
     * Sets callback listener.
     *
     * @param listener Listener.
     */
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setItems(mItems, (dialogInterface, index) -> {
            if (mListener != null) {
                mListener.onItemSelected(index);
                dismiss();
            }
        });

        this.dialog = builder.create();
        return this.dialog;
    }
}
