package ru.aint.mtg.tournament.tournamenthelper.mvp.views.details;

import android.support.annotation.NonNull;
import android.view.View;

import ru.aint.mtg.tournament.tournamenthelper.ui.callbacks.IConfirmCallback;

/**
 * Sub view interface.
 */
public interface IDetailsSubView {
    View getView();
    void showConfirmationDialog(String title, String message, @NonNull IConfirmCallback callbackDelegate);
    void showError(String string);
}
