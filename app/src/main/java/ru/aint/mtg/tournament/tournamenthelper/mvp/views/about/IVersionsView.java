package ru.aint.mtg.tournament.tournamenthelper.mvp.views.about;

/**
 * Versions view.
 */
public interface IVersionsView {

    void setVersion(String version);

    void setBuild(String build);

    void setChangelog(String changelog);
}
