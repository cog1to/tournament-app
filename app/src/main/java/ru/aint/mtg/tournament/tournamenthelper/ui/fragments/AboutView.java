package ru.aint.mtg.tournament.tournamenthelper.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.IAboutPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IAboutView;

/**
 * About fragment view.
 */
public class AboutView extends BaseActionBarFragment implements IAboutView {

    RecyclerView mRecyclerView;

    IAboutPresenter mPresenter;

    public void setPresenter(IAboutPresenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.about_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        if (mPresenter != null) {
            mPresenter.onCreateView(this);
        }

        return view;
    }

    @Override
    public void setItems(IAboutItemPresenter[] presenters) {
        AboutAdapter adapter = new AboutAdapter(getActivity(), presenters, presenter -> {
            if (mPresenter != null) {
                mPresenter.onItemSelected(presenter);
            }
        });
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    protected String getTitle() {
        return MainApplication.getInstance().getString(R.string.menu_about);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_about;
    }

    private interface ItemClickListener {
        void onItemClicked(IAboutItemPresenter presenter);
    }

    private static class AboutAdapter extends RecyclerView.Adapter<AboutItemViewHolder> {

        FragmentActivity mContext;

        IAboutItemPresenter[] mPresenters;

        ItemClickListener mListener;

        public AboutAdapter(FragmentActivity activity, IAboutItemPresenter[] presenters, ItemClickListener listener) {
            mContext = activity;
            mPresenters = presenters;
            mListener = listener;
        }

        @Override
        public AboutItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            AboutItemViewHolder holder = new AboutItemViewHolder(mContext.getLayoutInflater().inflate(R.layout.list_title_item, parent, false));
            return holder;
        }

        @Override
        public void onBindViewHolder(AboutItemViewHolder holder, int position) {
            IAboutItemPresenter presenter = mPresenters[position];

            if (mListener != null) {
                holder.itemView.setOnClickListener(view -> {
                    mListener.onItemClicked(presenter);
                });
            }

            holder.setPresenter(presenter);
        }

        @Override
        public int getItemCount() {
            if (mPresenters != null) {
                return mPresenters.length;
            }

            return 0;
        }
    }

    private static class AboutItemViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        IAboutItemPresenter mPresenter;

        public AboutItemViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView)itemView.findViewById(R.id.title_text_view);
        }

        void setPresenter(IAboutItemPresenter presenter) {
            mPresenter = presenter;
            mTextView.setText(presenter.getName());
        }
    }
}
