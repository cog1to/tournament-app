package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.create;

import ru.aint.mtg.tournament.tournamenthelper.mvp.views.create.ICreateTournamentView;

/**
 * Create Tournament presenter.
 */
public interface ICreateTournamentPresenter {

    /**
     * Called when view is created.
     *
     * @param view View.
     */
    void onViewCreated(ICreateTournamentView view);

    /**
     * Returns flag indicating if current info is valid and tournament can be created.
     *
     * @return <b>true</b> if tournament can be created, <b>false</b> otherwise.
     */
    boolean canCreate();

    /**
     * Called when create button is tapped.
     */
    void onCreateConfirmed();
}
