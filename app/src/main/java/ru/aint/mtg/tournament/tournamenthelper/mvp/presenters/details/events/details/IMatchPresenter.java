package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details;

import android.support.annotation.ColorRes;

import fj.data.List;

/**
 * Match presenter.
 */
public interface IMatchPresenter {

    boolean showMatchNames();

    class MatchEntryModel {
        private String mName;
        private String mScore;
        private @ColorRes int mColor;

        public MatchEntryModel(String name, String score, @ColorRes int color) {
            mName = name;
            mScore = score;
            mColor = color;
        }

        public String getName() {
            return mName;
        }

        public String getScore() {
            return mScore;
        }

        public @ColorRes int getColor() {
            return mColor;
        }
    }

    List<MatchEntryModel> getEntries();

    String getMatchName();

    boolean isEditable();
}
