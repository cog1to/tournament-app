package ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

import fj.P2;
import fj.Unit;
import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;

/**
 * Scores map JSON adapter.
 */
public class ScoresAdapter implements JsonSerializer<Map<Player, Option<Integer>>>, JsonDeserializer<Map<Player, Option<Integer>>> {

    @Override
    public Map<Player, Option<Integer>> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = GsonFactory.gson();
        TypeAdapter adapter = gson.getAdapter(new TypeToken<Option<Integer>>() {
        });

        JsonObject jsonObject =  json.getAsJsonObject();

        JsonObject keys = jsonObject.getAsJsonObject("KEYS");
        java.util.Map<String, Player> playersMap = context.deserialize(keys, new TypeToken<HashMap<String, Player>>() {
        }.getType());

        JsonObject values = jsonObject.getAsJsonObject("VALUES");
        java.util.Map<String, Option<Integer>> valuesMap = new HashMap<>();
        for (java.util.Map.Entry<String, JsonElement> entry : values.entrySet()) {
            if (entry.getValue().isJsonNull()) {
                valuesMap.put(entry.getKey(), Option.<Integer>none());
            } else {
                valuesMap.put(entry.getKey(), Option.some(entry.getValue().getAsInt()));
            }
        }

        List<P2<Player, Option<Integer>>> scoresList = List.list();
        for (String key : playersMap.keySet()) {
            Player player = playersMap.get(key);
            Option<Integer> score = valuesMap.get(key);
            scoresList = scoresList.cons(p(player, score));
        }

        return new Map<>(scoresList);
    }

    @Override
    public JsonElement serialize(Map<Player, Option<Integer>> src, Type typeOfSrc, JsonSerializationContext context) {
        java.util.Map<String, Player> playersMap = new HashMap<>();
        src.foreach(p2 -> {
            playersMap.put(p2._1().getFullName(), p2._1());
            return Unit.unit();
        });

        Gson gson = GsonFactory.gson();
        TypeAdapter adapter = gson.getAdapter(new TypeToken<Option<Integer>>(){});

        JsonObject scoresMap = new JsonObject();
        src.foreach(p2 -> {
            scoresMap.add(p2._1().getFullName(), (p2._2().isSome() ? new JsonPrimitive(p2._2().some()) : JsonNull.INSTANCE));
            return Unit.unit();
        });

        JsonObject ret = new JsonObject();
        ret.add("KEYS", context.serialize(playersMap));
        ret.add("VALUES", scoresMap);
        return ret;
    }
}
