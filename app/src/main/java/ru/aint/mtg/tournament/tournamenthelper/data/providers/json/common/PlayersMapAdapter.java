package ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

import fj.P2;
import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.utils.Map;

import static fj.P.p;

/**
 * Players map JSON adapter.
 */
public class PlayersMapAdapter implements JsonSerializer<Map<Player, Boolean>>, JsonDeserializer<Map<Player, Boolean>> {
    @Override
    public Map<Player, Boolean> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject =  json.getAsJsonObject();

        JsonObject keys = jsonObject.getAsJsonObject("KEYS");
        java.util.Map<String, Player> playersMap = context.deserialize(keys, new TypeToken<HashMap<String, Player>>() {}.getType());

        JsonObject values = jsonObject.getAsJsonObject("VALUES");
        java.util.Map<String, Boolean> valuesMap = new HashMap<>();
        for (java.util.Map.Entry<String, JsonElement> entry : values.entrySet()) {
            if (entry.getValue().isJsonNull()) {
                valuesMap.put(entry.getKey(), false);
            } else {
                valuesMap.put(entry.getKey(), entry.getValue().getAsBoolean());
            }
        }

        List<P2<Player, Boolean>> scoresList = List.list();
        for (String key : playersMap.keySet()) {
            Player player = playersMap.get(key);
            Boolean score = valuesMap.get(key);
            scoresList = scoresList.cons(p(player, score));
        }

        return new Map<>(scoresList);
    }

    @Override
    public JsonElement serialize(Map<Player, Boolean> src, Type typeOfSrc, JsonSerializationContext context) {
        java.util.Map<String, Player> playersMap = new HashMap<>();
        src.foreach(p2 -> {
            playersMap.put(p2._1().getFullName(), p2._1());
            return Unit.unit();
        });

        JsonObject scoresMap = new JsonObject();
        src.foreach(p2 -> {
            scoresMap.add(p2._1().getFullName(), new JsonPrimitive(p2._2()));
            return Unit.unit();
        });

        JsonObject ret = new JsonObject();
        ret.add("KEYS", context.serialize(playersMap));
        ret.add("VALUES", scoresMap);
        return ret;
    }
}
