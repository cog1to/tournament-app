package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events;

import android.support.annotation.NonNull;

import java.io.IOException;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Drop;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutItemPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IDropOutsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IDropOutsView;

/**
 * Drop outs event presenter.
 */
public class DropOutsPresenter implements IDropOutsPresenter {

    private Tournament mTournament;

    private Drop mEvent;

    private IDropOutsView mView;

    private boolean mEditable;

    private List<DropOutItem> mPresenters;

    public DropOutsPresenter(@NonNull Tournament tournament, Drop existingEvent) {
        mTournament = tournament;
        mEvent = existingEvent;
        mEditable = (existingEvent == null) || mTournament.getEvents().head().equals(mEvent);
    }

    @Override
    public void onItemTapped(int index) {
        if (mEditable) {
            mPresenters.index(index).setSelected(!mPresenters.index(index).getSelected());
            mView.reloadItemAt(index, mPresenters.index(index));
        }
    }

    @Override
    public void onMenuItemTapped(MenuItemType type) {
       if (type == MenuItemType.Delete) {
            try {
                mTournament.setEvents(mTournament.getEvents().tail());
                MainApplication.getInstance().getDataProvider().saveTournament(mTournament);
                MainApplication.getInstance().getRouter().pop();
            } catch (IOException ex) {
                MainApplication.getInstance().getRouter().showError("Failed to save tournament", ex);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        List<Player> players = mPresenters.filter(DropOutItem::getSelected).map(item -> item.mPlayer);
        if (players.length() == 0) {
            return false;
        }

        if (mEvent == null) {
            mEvent = new Drop();
            mEvent.setPlayers(players);
            mTournament.setEvents(mTournament.getEvents().cons(mEvent));
        } else {
            mEvent.setPlayers(players);
        }

        try {
            MainApplication.getInstance().getDataProvider().saveTournament(mTournament);
            MainApplication.getInstance().getRouter().pop();
        } catch (IOException ex) {
            MainApplication.getInstance().getRouter().showError("Failed to save tournament", ex);
        }

        return true;
    }

    @Override
    public void onViewCreated(IDropOutsView view) {
        mView = view;
        mView.setEditable(mEditable);
        updatePresenters();
        mView.setItems(mPresenters.toJavaList().toArray(new IDropOutItemPresenter[0]));
    }

    @Override
    public MenuItemType[] onMenuInflated() {
        if (mEditable && (mEvent != null)) {
            return new MenuItemType[] { MenuItemType.Delete };
        }

        return null;
    }

    private void updatePresenters() {
        TournamentState targetState;

        if (mEvent == null) {
            targetState = mTournament.currentState().some();
        } else {
            targetState = mTournament.stateBeforeEvent(mEvent).some();
        }

        List<Player> eligiblePlayers = targetState.getPlayerStates().filter(ps -> ps.getStatus() != PlayerStatus.Dropped).map(PlayerState::getPlayer);

        mPresenters = List.list();
        for (int index = 0; index < eligiblePlayers.length(); index++) {
            Player player = eligiblePlayers.index(index);
            mPresenters = mPresenters.cons(new DropOutItem(player, (mEvent != null) && mEvent.getPlayers().exists(p -> p.equals(player))));
        }
    }

    private static class DropOutItem implements IDropOutItemPresenter {

        private Player mPlayer;

        private boolean mIsSelected;

        DropOutItem(Player player, boolean selected) {
            mPlayer = player;
            mIsSelected = selected;
        }

        @Override
        public String getName() {
            return mPlayer.getFullName();
        }

        @Override
        public boolean getSelected() {
            return mIsSelected;
        }

        public void setSelected(boolean selected) {
            mIsSelected = selected;
        }
    }
}
