package ru.aint.mtg.tournament.tournamenthelper.mvp.presenters;

import android.media.Image;

/**
 * Tournament list item presenter.
 */
public interface ITournamentListItemPresenter {

    enum IconSymbol {
        Tournament
    }

    /**
     * Returns an icon type.
     *
     * @return Icon ID.
     */
    IconSymbol getIcon();

    /**
     * Returns item name.
     *
     * @return Item name.
     */
    String getName();

    /**
     * Returns subtitle text.
     *
     * @return Subtitle text.
     */
    String getSubtitle();

    /**
     * Returns flag indicating if item is in selected state.
     *
     * @return Flag indicating if item is in selected state.
     */
    boolean getIsSelected();
}
