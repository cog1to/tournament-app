package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.about;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import java.util.Locale;

import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.about.IVersionsPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.about.IVersionsView;

/**
 * Versions presenter.
 */

public class VersionsPresenter implements IVersionsPresenter {
    @Override
    public void onCreateView(@NonNull IVersionsView view) {
        Context context = MainApplication.getInstance();

        try {
            PackageInfo pInfo = MainApplication.getInstance().getPackageManager().getPackageInfo(context.getPackageName(), 0);
            view.setVersion(String.format("%s: %s", context.getString(R.string.version), pInfo.versionName));
            view.setBuild(String.format(Locale.getDefault(), "%s: %d", context.getString(R.string.build), pInfo.versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            view.setVersion(String.format("%s: %s", context.getString(R.string.version), "?"));
            view.setBuild(String.format("%s: %s", context.getString(R.string.build), "?"));
            e.printStackTrace();
        }
        view.setChangelog(context.getString(R.string.changelog_details));
    }
}
