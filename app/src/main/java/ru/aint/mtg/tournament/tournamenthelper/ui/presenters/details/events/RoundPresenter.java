package ru.aint.mtg.tournament.tournamenthelper.ui.presenters.details.events;

import android.support.annotation.NonNull;

import java.io.IOException;

import fj.data.List;
import fj.data.Option;
import ru.aint.mtg.tournament.model.brackets.BracketUpdater;
import ru.aint.mtg.tournament.model.events.Archive;
import ru.aint.mtg.tournament.model.events.Match;
import ru.aint.mtg.tournament.model.events.Round;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.players.PlayerState;
import ru.aint.mtg.tournament.model.players.PlayerStatus;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.model.tournaments.TournamentState;
import ru.aint.mtg.tournament.tournamenthelper.MainApplication;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchEditingPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IRoundPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IMatchEditingView;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IRoundView;

/**
 * Round presenter.
 */
public class RoundPresenter implements IRoundPresenter {

    private IRoundView mView;

    private Round mRound;

    private Tournament mTournament;

    private String mName;

    private boolean mEditable;

    private List<MatchItemPresenter> mPresenters;

    public RoundPresenter(Tournament tournament, Round round, String name, boolean editable) {
        mTournament = tournament;
        mRound = round;
        mName = name;
        mEditable = editable;
    }

    @Override
    public void onCreateView(@NonNull IRoundView view) {
        mView = view;
        updateList();
    }

    @Override
    public void onDestroyView() {
    }

    @Override
    public String getTitle() {
        return mName;
    }

    @Override
    public String getSubtitle() {
        return mTournament.getName();
    }

    @Override
    public void onItemTapped(int index) {
        Match match = mPresenters.index(index).mMatch;
        if (!match.isFinished()) {
            mView.showMatchEditing(new MatchEditingPresenter(match, mTournament.currentState().some()));
        }
    }

    private void updateList() {
        boolean showTitles = (mTournament.currentState().some().getFormat() == TournamentFormat.SingleElimination || mTournament.currentState().some().getFormat() == TournamentFormat.DoubleElimination) && mTournament.getVersion().equals("2.0");

        mPresenters = List.list();
        for (int idx = 0; idx < mRound.getMatches().length(); idx++) {
            Match match = mRound.getMatches().index(idx);
            mPresenters = mPresenters.cons(new MatchItemPresenter(match, mEditable && !match.isBye(), showTitles ? match.getName() : String.format(MainApplication.getInstance().getString(R.string.match_format), idx + 1)));
        }
        mPresenters = mPresenters.reverse();
        mView.showMatches(mPresenters.toJavaList().toArray(new IMatchPresenter[0]));
    }

    private class MatchItemPresenter implements IMatchPresenter {

        Match mMatch;

        boolean mEditable;

        String mName;

        MatchItemPresenter(@NonNull Match match, boolean editable, String name) {
            mMatch = match;
            mEditable = editable;
            mName = name;
        }

        @Override
        public List<MatchEntryModel> getEntries() {
            List<MatchEntryModel> entries = mMatch.getScores().toList().map(ps -> {
                String name = ps._1().getFullName();
                String score = ps._2().isSome() ? String.format("%d", ps._2().some()) : "";
                int color = android.R.color.black;
                return new MatchEntryModel(name, score, color);
            });

            if (entries.length() == 1) {
                String name = MainApplication.getInstance().getString(R.string.bye);
                String score = "0";
                int color = R.color.disabled_color;

                entries = entries.cons(new MatchEntryModel(name, score, color)).reverse();
            }

            return entries;
        }

        @Override
        public String getMatchName() {
            return mName;
        }

        @Override
        public boolean isEditable() {
            return mEditable;
        }

        @Override
        public boolean showMatchNames() {
            return (mName != null && !mName.isEmpty());
        }
    }

    private class BoundMatchEditingEntry extends IMatchEditingView.MatchEditingEntry {

        Player mPlayer;

        BoundMatchEditingEntry(String name, int value, Player player) {
            super(name, value);
            mPlayer = player;
        }
    }

    private class MatchEditingPresenter implements IMatchEditingPresenter {

        private Match mMatch;

        private TournamentState mTournamentState;

        private IMatchEditingView mView;

        private List<IMatchEditingView.MatchEditingEntry> mEntries;

        private MatchEditingPresenter(Match match, TournamentState state) {
            mTournamentState = state;
            mMatch = match;
            mEntries = mMatch.getScores().toList().map(m ->
                new BoundMatchEditingEntry(m._1().getFullName(), m._2().isSome() ? m._2().some() : 0, m._1())
            );
            mEntries = mEntries.snoc(new BoundMatchEditingEntry(MainApplication.getInstance().getString(R.string.draws), mMatch.getDraws(), null));
        }

        @Override
        public void setView(IMatchEditingView view) {
            mView = view;
        }

        @Override
        public List<IMatchEditingView.MatchEditingEntry> getItems() {
            return mEntries;
        }

        @Override
        public int getMaxScore() {
            return mTournament.currentState().some().getConfig().getWinsPerMatch();
        }

        @Override
        public void updatedItem(int position, IMatchEditingView.MatchEditingEntry entry) {
            mEntries.index(position).setValue(entry.getValue());
        }

        @Override
        public boolean hasValidValues() {
            boolean canBeDone;

            // For single elimination, we cannot record a win unless one player has more game wins than other.
            boolean isElimination = mTournamentState.getFormat() == TournamentFormat.SingleElimination || mTournamentState.getFormat() == TournamentFormat.DoubleElimination;
            if (isElimination) {
                canBeDone = !mEntries.take(mEntries.length() - 1).forall(e -> e.getValue() == mEntries.index(0).getValue());
            } else {
                canBeDone = !mEntries.forall(e -> e.getValue() == getMaxScore()) && mEntries.foldLeft((sum, entry) -> sum+entry.getValue(), 0) > 0;
            }

            // Number of games played cannot be more than number of required wins + 1, including draws.
            canBeDone = canBeDone && (mEntries.foldLeft((sum, entry) -> {
                return sum += entry.getValue();
            }, 0)) <= (getMaxScore() + 1);

            return canBeDone;
        }

        @Override
        public void editingConfirmed() {
            List<Player> players = mMatch.getScores().keys();
            for (int idx = 0; idx < players.length(); idx++) {
                BoundMatchEditingEntry entry = (BoundMatchEditingEntry)mEntries.index(idx);
                Player player = players.find(p -> p.equals(entry.mPlayer)).some();
                mMatch.setScore(player, Option.some(entry.getValue()));
            }
            mMatch.setDraws(mEntries.last().getValue());
            try {
                BracketUpdater updater = new BracketUpdater();
                updater.updateBracket(mTournament.getEvents());

                // Automatically archive tournament if it's finished.
                Option<TournamentState> state = mTournament.currentState();
                if (state.isSome()) {
                    if (state.some().isFinished()) {
                        mTournament.setEvents(mTournament.getEvents().cons(new Archive()));
                    } else if (mTournament.getVersion().equals("2.0") &&
                            state.some().getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).length() == 2 &&
                            state.some().getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Active).length() == 0) {
                        List<Player> rematchPlayers = state.some().getPlayerStates().filter(ps -> ps.getStatus() == PlayerStatus.Loser).map(PlayerState::getPlayer);
                        // If we're here, it means that finalist of the winner's bracket lost to finalist of the losers bracket.
                        // We need to add a rematch round to get a true finalist.
                        Match rematch = new Match(rematchPlayers);
                        rematch.setVersion("2.0");
                        rematch.setName(String.format(MainApplication.getInstance().getString(R.string.match_format), 1));

                        Round rematchRound = new Round(List.list(rematch));
                        rematchRound.setVersion("2.0");
                        rematchRound.setName(MainApplication.getInstance().getString(R.string.finals_rematch));
                        mTournament.setEvents(mTournament.getEvents().cons(rematchRound));
                    }
                }

                MainApplication.getInstance().getDataProvider().saveTournament(mTournament);
                updateList();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
