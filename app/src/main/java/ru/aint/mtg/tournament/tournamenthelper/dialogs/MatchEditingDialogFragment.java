package ru.aint.mtg.tournament.tournamenthelper.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import fj.Equal;
import fj.Unit;
import fj.data.List;
import ru.aint.mtg.tournament.tournamenthelper.R;
import ru.aint.mtg.tournament.tournamenthelper.mvp.presenters.details.events.details.IMatchEditingPresenter;
import ru.aint.mtg.tournament.tournamenthelper.mvp.views.details.IMatchEditingView;

/**
 * Match editing fragment.
 */
public class MatchEditingDialogFragment extends AppCompatDialogFragment implements IMatchEditingView {

    /**
     * Presenter.
     */
    IMatchEditingPresenter mPresenter;

    /**
     * Scores view.
     */
    private LinearLayout scoresLayout;

    /**
     * List of all spinners.
     */
    private List<AppCompatSpinner> spinners = List.list();

    /**
     * Presented dialog instance.
     */
    private AlertDialog dialog;

    /**
     * Entries to show.
     */
    List<MatchEditingEntry> mEntries;

    public void setPresenter(IMatchEditingPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showItems(List<MatchEditingEntry> items) {
        mEntries = items;
        updateLayout();
    }

    private void updateLayout() {
        scoresLayout.removeAllViews();

        List<MatchEditingEntry> entries = mPresenter.getItems();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        entries.foreach(entry -> {
            View scoreView = inflater.inflate(R.layout.list_edit_score_item, null);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins((int)getResources().getDimension(R.dimen.dp_16), (int)getResources().getDimension(R.dimen.dp_8), (int)getResources().getDimension(R.dimen.dp_16), (int)getResources().getDimension(R.dimen.dp_8));

            scoreView.setLayoutParams(lp);

            TextView name = (TextView)scoreView.findViewById(R.id.name);
            name.setText(entry.getName());

            AppCompatSpinner spinner = (AppCompatSpinner)scoreView.findViewById(R.id.spinner);
            this.spinners = this.spinners.cons(spinner);

            Integer[] values = List.range(0, mPresenter.getMaxScore() + 1).array(Integer[].class);
            ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_item, values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(entry.getValue(), false);
            spinner.setOnItemSelectedListener(new SpinnerListener(entries.elementIndex(Equal.anyEqual(), entry).some()));

            scoresLayout.addView(scoreView);
            return Unit.unit();
        });

        // Set spinners
        this.spinners = this.spinners.reverse();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View content = inflater.inflate(R.layout.dialog_edit_match, null);
        scoresLayout = (LinearLayout)content.findViewById(R.id.scores_layout);

        List<MatchEditingEntry> entries = mPresenter.getItems();
        showItems(entries);

        builder.setTitle(getActivity().getResources().getString(R.string.edit_match))
                .setView(content)
                .setPositiveButton(R.string.menu_done, null)
                .setNegativeButton(R.string.menu_cancel, (dialogInterface, i) -> {
                    dismiss();
                });

        this.dialog = builder.create();
        this.dialog.setOnShowListener(dialogInterface -> {
            Button positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positive.setOnClickListener(view -> {
                mPresenter.editingConfirmed();
                dismiss();
            });

            updateDoneButton();
        });

        return this.dialog;
    }

    private class SpinnerListener implements AdapterView.OnItemSelectedListener {

        private int mPosition;

        private SpinnerListener(int position) {
            mPosition = position;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            MatchEditingEntry entry = mEntries.index(mPosition);
            entry.setValue(position);
            mPresenter.updatedItem(mPosition, entry);
            updateDoneButton();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
        }
    }

    private void updateDoneButton() {
        boolean canBeDone = this.spinners.forall(spinner -> spinner.getSelectedItem() != null);
        canBeDone = canBeDone && mPresenter.hasValidValues();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(canBeDone);
    }
}
