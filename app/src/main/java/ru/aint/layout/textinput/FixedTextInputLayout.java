package ru.aint.layout.textinput;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

/**
 * Fixed text input layout.
 */
public class FixedTextInputLayout extends TextInputLayout {
    public FixedTextInputLayout(Context context) {
        super(context);
    }

    public FixedTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setError(@Nullable CharSequence error) {
        if((getEditText()!=null && getEditText().getBackground()!=null) && (Build.VERSION.SDK_INT == 22 || Build.VERSION.SDK_INT == 21)) {
            getEditText().setBackgroundDrawable(getEditText().getBackground().getConstantState().newDrawable());
        }
        super.setError(error);
    }
}
