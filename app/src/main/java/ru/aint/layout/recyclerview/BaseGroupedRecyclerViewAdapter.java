package ru.aint.layout.recyclerview;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import fj.data.List;

import static fj.data.List.range;

/**
 * Grouped recycler view adapter.
 */
public class BaseGroupedRecyclerViewAdapter implements IRecyclerViewDelegate, IRecyclerViewDataSource {
    /**
     * Common row type for group headers.
     */
    public static int ROW_TYPE_GROUP = 0;

    /**
     * Common row type for unknown rows. Should be used only to indicate internal errors.
     */
    public static int ROW_TYPE_UNKNOWN = -1;

    /**
     * Internal constant to mark non-group header rows.
     */
    private static int ROW_TYPE_ROW = 1;

    /**
     * Data source.
     */
    private IGroupedRecyclerViewDataSource dataSource;

    /**
     * Delegate.
     */
    private IGroupedRecyclerViewDelegate delegate;

    /**
     * Parent activity.
     */
    private Activity activity;

    /**
     * Underlying BaseRecyclerViewAdapter.
     */
    private BaseRecyclerViewAdapter adapter;

    /**
     * Main constructor.
     *
     * @param activity Parent activity.
     * @param dataSource Data source.
     * @param delegate Delegate.
     */
    public BaseGroupedRecyclerViewAdapter(Activity activity, IGroupedRecyclerViewDataSource dataSource, IGroupedRecyclerViewDelegate delegate)
    {
        super();
        this.activity = activity;
        this.dataSource = dataSource;
        this.delegate = delegate;
        this.adapter = new BaseRecyclerViewAdapter(activity, this, this);
    }

    /**
     * Returns underlying adapter.
     *
     * @return BaseRecyclerViewAdapter object used to communicate with recycler view.
     */
    public BaseRecyclerViewAdapter getAdapter() {
        return this.adapter;
    }

    /**
     * Reloads the content.
     */
    public void reload()
    {
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public int getNumberOfRows() {
        if (this.dataSource == null) {
            return 0;
        }

        return range(0, this.dataSource.getNumberOfGroups()).foldLeft((acc, idx) -> acc + this.dataSource.getNumberOfRowsInGroup(idx) + 1, 0);
    }

    @Override
    public int getRowTypeForRowAt(int index) {
        RowPosition position = this.getRowPosition(index);
        if (position.type == this.ROW_TYPE_GROUP) {
            return this.ROW_TYPE_GROUP;
        } else {
            return this.dataSource.getTypeOfRowAt(position.getGroup(), position.getIndex());
        }
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int rowType) {
        return this.dataSource.onCreateViewHolder(parent, rowType);
    }

    @Override
    public void onBindViewHolder(int index, @NonNull BaseRecyclerViewHolder holder) {
        RowPosition position = this.getRowPosition(index);
        if (position.type == this.ROW_TYPE_GROUP) {
            this.dataSource.onBindGroupViewHolder(position.getGroup(), holder);
        } else {
            this.dataSource.onBindViewHolder(position.getGroup(), position.getIndex(), holder);
        }
    }

    @Override
    public void OnItemClick(View view, int index) {
        RowPosition position = this.getRowPosition(index);
        if (position.type == ROW_TYPE_ROW) {
            if (this.delegate != null) {
                this.delegate.OnItemClick(view, position.getGroup(), position.getIndex());
            }
        }
    }

    @Override
    public boolean OnItemLongClick(View view, int index) {
        RowPosition position = this.getRowPosition(index);
        if (position.type == ROW_TYPE_ROW) {
            if (this.delegate != null) {
                return this.delegate.OnItemLongClick(view, position.getGroup(), position.getIndex());
            }
        }

        return false;
    }

    public void OnItemInserted(int group, int position) {
        this.adapter.notifyItemInserted(getIndexFromGroupIndex(group, position));
    }

    public void OnItemRemoved(int group, int position) {
        this.adapter.notifyItemRemoved(getIndexFromGroupIndex(group, position));
    }

    public void OnItemChanged(int group, int position) {
        this.adapter.notifyItemChanged(getIndexFromGroupIndex(group, position));
    }

    public void OnGroupHeaderChanged(int group) {
        this.adapter.notifyItemChanged(getIndexFromGroupIndex(group, 0) - 1);
    }

    // <editor-fold desc="Helpers">

    private int getIndexFromGroupIndex(int group, int position) {
        List<GroupConfig> groupConfigs = getGroupConfigs();
        return groupConfigs.index(group).start + position + 1;
    }

    private List<GroupConfig> getGroupConfigs() {
        List<GroupConfig> groups = List.list();

        int numberOfGroups = this.dataSource.getNumberOfGroups();
        int index = 0;

        for (int groupIndex = 0; groupIndex < numberOfGroups; groupIndex++) {
            int numberOfRows = this.dataSource.getNumberOfRowsInGroup(groupIndex);
            GroupConfig config = new GroupConfig(groupIndex, index, numberOfRows);
            groups = groups.cons(config);
            index += config.getTotalNumberOfRows();
        }

        return groups.reverse();
    }

    private RowPosition getRowPosition(int index) {
        List<GroupConfig> groups = this.getGroupConfigs();
        for (GroupConfig group: groups) {
            if (group.isIndexInside(index)) {
                return group.getRowPosition(index);
            }
        }

        return null;
    }

    private static class RowPosition {
        private int type;
        private int group;
        private int index;

        public RowPosition(int group, int index, int type) {
            this.group = group;
            this.index = index;
            this.type = type;
        }

        public int getIndex() {
            return index;
        }

        public int getGroup() {
            return group;
        }

        public int getType() {
            return this.type;
        }
    }

    private static class GroupConfig {
        private int groupIndex;
        private int start;
        private int length;

        public GroupConfig(int groupIndex, int start, int numberOfRows) {
            this.groupIndex = groupIndex;
            this.start = start;
            this.length = numberOfRows + 1;
        }

        public boolean isIndexInside(int idx) {
            if (idx >= start && idx < start + length) {
                return true;
            }

            return false;
        }

        public RowPosition getRowPosition(int index) {
            int rowType = (index == start) ? ROW_TYPE_GROUP : ROW_TYPE_ROW;
            return new RowPosition(this.groupIndex, index - this.start - rowType, rowType);
        }

        public int getTotalNumberOfRows() {
            return this.length;
        }
    }

    // </editor-fold>
}
