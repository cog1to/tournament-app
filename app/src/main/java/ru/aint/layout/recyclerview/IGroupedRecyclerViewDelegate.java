package ru.aint.layout.recyclerview;

import android.view.View;

/**
 * Grouped recycler view delegate.
 */
public interface IGroupedRecyclerViewDelegate {
    /**
     * Called when user clicks on a cell.
     *
     * @param view View that got onClick event.
     * @param group Group index.
     * @param position Position of the view.
     */
    void OnItemClick(View view, int group, int position);

    /**
     * Called when user long clicks on a cell.
     *
     * @param view View that got onLongClick event.
     * @param group Group index.
     * @param position Position of the view.
     */
    boolean OnItemLongClick(View view, int group, int position);
}
