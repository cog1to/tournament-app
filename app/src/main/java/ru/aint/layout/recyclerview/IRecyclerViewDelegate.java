package ru.aint.layout.recyclerview;

import android.view.View;

/**
 * Recycler view delegate.
 */
public interface IRecyclerViewDelegate {
    /**
     * Called when user clicks on a cell.
     *
     * @param view View that got onClick event.
     * @param position Position of the view.
     */
    void OnItemClick(View view, int position);

    /**
     * Called when user long clicks on a cell.
     *
     * @param view View that got onLongClick event.
     * @param position Position of the view.
     */
    boolean OnItemLongClick(View view, int position);
}
