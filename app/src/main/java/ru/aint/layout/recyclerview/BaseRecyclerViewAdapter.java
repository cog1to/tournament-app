package ru.aint.layout.recyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Base recycler view adapter.
 */
public class BaseRecyclerViewAdapter extends RecyclerView.Adapter<BaseRecyclerViewHolder> implements BaseRecyclerViewHolder.OnClickListener {

    /**
     * Data source.
     */
    private IRecyclerViewDataSource dataSource;

    /**
     * Delegate.
     */
    private IRecyclerViewDelegate delegate;

    /**
     * Parent activity.
     */
    private Activity activity;

    /**
     * Returns delegate.
     *
     * @return Delegate.
     */
    public IRecyclerViewDelegate getDelegate() {
        return delegate;
    }

    /**
     * Sets new delegate.
     *
     * @param delegate New delegate.
     */
    public void setDelegate(IRecyclerViewDelegate delegate) {
        this.delegate = delegate;
    }

    /**
     * Returns data source.
     *
     * @return Data source.
     */
    public IRecyclerViewDataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets new data source.
     *
     * @param dataSource New data source.
     */
    public void setDataSource(IRecyclerViewDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Returns new instance of BaseRecyclerViewAdapter.
     *
     * @param activity Parent activity.
     * @param dataSource Data source.
     * @param delegate Delegate.
     */
    public BaseRecyclerViewAdapter(Activity activity, IRecyclerViewDataSource dataSource, IRecyclerViewDelegate delegate)
    {
        super();
        this.activity = activity;
        this.dataSource = dataSource;
        this.delegate = delegate;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.dataSource != null) {
            return this.dataSource.getRowTypeForRowAt(position);
        }

        return 0;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (this.dataSource != null) {
            BaseRecyclerViewHolder holder = this.dataSource.onCreateViewHolder(parent, viewType);
            if (holder.isOnClickEnabled()) {
                holder.setListener(this);
            }
            return holder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (this.dataSource != null) {
            this.dataSource.onBindViewHolder(position, holder);
        }
    }

    @Override
    public int getItemCount() {
        if (this.dataSource != null) {
            return this.dataSource.getNumberOfRows();
        }

        return 0;
    }

    @Override
    public void onClick(View view, int position) {
        if (this.delegate != null) {
            this.delegate.OnItemClick(view, position);
        }
    }

    @Override
    public boolean onLongClick(View view, int position) {
        if (this.delegate != null) {
            return this.delegate.OnItemLongClick(view, position);
        }

        return false;
    }
}
