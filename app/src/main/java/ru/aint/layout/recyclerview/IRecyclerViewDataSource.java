package ru.aint.layout.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Recycler view data source.
 */
public interface IRecyclerViewDataSource {

    /**
     * Returns number of rows in recycler view.
     *
     * @return Number of rows in recycler view.
     */
    public int getNumberOfRows();

    /**
     * Returns row type for row at given index.
     *
     * @param index Row position inside recycler view.
     *
     * @return Row type number.
     */
    public int getRowTypeForRowAt(int index);

    /**
     * Called when recycler view wants to create a new view holder for given row type.
     *
     * @param parent Parent view group to hold row view.
     * @param rowType Row type.
     *
     * @return Created ViewHolder object.
     */
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int rowType);

    /**
     * Called when recycler view wants to bind given view holder to data.
     *
     * @param position cell position.
     * @param holder View holder to fill with data.
     */
    public void onBindViewHolder(int position, @NonNull BaseRecyclerViewHolder holder);
}
