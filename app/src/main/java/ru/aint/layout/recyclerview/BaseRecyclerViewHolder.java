package ru.aint.layout.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Base view holder for recycler view.
 */
public class BaseRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    /**
     * Click events listener interface.
     */
    public interface OnClickListener {

        /**
         * Called when user clicks on item view.
         *
         * @param view View that was clicked on.
         * @param position Item's position inside recycler view.
         */
        void onClick(View view, int position);

        /**
         * Called when user performs long tap on item view.
         *
         * @param view View that was tapped.
         * @param position Item's position inside recycler view.
         *
         * @return <b>true</b> if event was handled, <b>false</b> otherwise.
         */
        boolean onLongClick(View view, int position);
    }

    /**
     * Indicates whether item should respond to a click event.
     */
    private boolean onClickEnabled;

    /**
     * Click events listener.
     */
    private OnClickListener listener;

    /**
     * Returns click events listener.
     *
     * @return Click events listener.
     */
    public OnClickListener getListener() {
        return listener;
    }

    /**
     * Sets new click event listener.
     *
     * @param listener New click event listener.
     */
    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    /**
     * Returns a value indicating whether item is clickable.
     *
     * @return <b>true</b> if item is clickable, <b>false</b> otherwise.
     */
    public boolean isOnClickEnabled() {
        return onClickEnabled;
    }

    /**
     * Sets a value indicating whether item is clickable.
     *
     * @param onClickEnabled New value indicating whether item is clickable.
     */
    public void setOnClickEnabled(boolean onClickEnabled) {
        this.onClickEnabled = onClickEnabled;
        if (onClickEnabled) {
            this.itemView.setOnClickListener(this);
            this.itemView.setOnLongClickListener(this);
        }
    }

    /**
     * Returns new instance of BaseRecyclerViewHolder.
     *
     * @param itemView Item view.
     */
    public BaseRecyclerViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View view) {
        if (this.listener != null && this.onClickEnabled) {
            this.listener.onClick(view, getAdapterPosition());
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (this.listener != null && this.onClickEnabled) {
            return this.listener.onLongClick(view, getAdapterPosition());
        }

        return false;
    }
}
