package ru.aint.layout.recyclerview;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import ru.aint.layout.recyclerview.BaseRecyclerViewHolder;

/**
 * Data source for a recycler view that supports view groups.
 */
public interface IGroupedRecyclerViewDataSource {

    /**
     * Returns number of groups.
     *
     * @return Number of groups inside recycler view.
     */
    public int getNumberOfGroups();

    /**
     * Returns number of rows inside given group.
     *
     * @param index Index of a group.
     *
     * @return Number of rows inside group with given index.
     */
    public int getNumberOfRowsInGroup(int index);

    /**
     * Returns type of row at given index.
     *
     * @param groupIndex Index of a group containing row.
     * @param rowIndex Index of a row.
     *
     * @return Type of row at given position.
     */
    public int getTypeOfRowAt(int groupIndex, int rowIndex);

    /**
     * Called when recycler view wants to create a new view holder for given row type.
     *
     * @param parent Parent view group to hold row view.
     * @param rowType Row type.
     *
     * @return Created ViewHolder object.
     */
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int rowType);

    /**
     * Called when recycler view wants to bind given view holder to data.
     *
     * @param group Group index.
     * @param position Row position inside group.
     * @param holder View holder to fill with data.
     */
    public void onBindViewHolder(int group, int position, @NonNull BaseRecyclerViewHolder holder);

    /**
     * Called when recycler view wants to bind given group view holder to data.
     *
     * @param group Group index.
     * @param holder View holder to fill with data.
     */
    public void onBindGroupViewHolder(int group, @NonNull BaseRecyclerViewHolder holder);
}
