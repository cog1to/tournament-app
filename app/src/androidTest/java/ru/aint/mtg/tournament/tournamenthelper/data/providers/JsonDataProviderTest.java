package ru.aint.mtg.tournament.tournamenthelper.data.providers;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.google.gson.reflect.TypeToken;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.UUID;

import fj.data.List;
import ru.aint.mtg.tournament.model.events.Event;
import ru.aint.mtg.tournament.model.events.Registration;
import ru.aint.mtg.tournament.model.players.Player;
import ru.aint.mtg.tournament.model.tournaments.Tournament;
import ru.aint.mtg.tournament.model.tournaments.TournamentConfig;
import ru.aint.mtg.tournament.model.tournaments.TournamentFormat;
import ru.aint.mtg.tournament.tournamenthelper.data.entities.TournamentInfo;
import ru.aint.mtg.tournament.tournamenthelper.data.files.IFileManager;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.JsonDataProvider;
import ru.aint.mtg.tournament.tournamenthelper.data.providers.json.common.GsonFactory;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit tests for JsonDataProvider class.
 */
public class JsonDataProviderTest extends ApplicationTestCase<Application> {
    public JsonDataProviderTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        createApplication();
        System.setProperty("dexmaker.dexcache", getContext().getCacheDir().getPath());
    }

    public void testGetTournaments() throws Exception {
        String uuid = UUID.randomUUID().toString();

        Tournament tournament = new Tournament(uuid, "test1", new Date().getTime());
        List<Tournament> list = List.list();
        list = list.cons(tournament);

        Type listType = new TypeToken<List<Tournament>>() {}.getType();
        String json = GsonFactory.gson().toJson(list, listType);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(json.getBytes());
        IFileManager mock = mock(IFileManager.class);
        when(mock.fileExistsInDataDir(anyString())).thenReturn(true);
        when(mock.getInputStreamFromDataDirPath(anyString())).thenReturn(inputStream);

        JsonDataProvider dataProvider = new JsonDataProvider(getContext(), mock);

        List<TournamentInfo> tournaments = dataProvider.getTournaments();
        assertEquals(1, tournaments.length());

        TournamentInfo retrievedTournament = tournaments.head();
        assertEquals("test1", retrievedTournament.getName());
        assertEquals(uuid, retrievedTournament.getId());
    }

    public void testGetTournamentsWithOneEvent() throws Exception {
        // Create tournament
        String uuid = UUID.randomUUID().toString();

        Player player1 = new Player("FirstName1", "LastName1");
        Player player2 = new Player("FirstName2", "LastName2");
        Registration registration = new Registration(TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, List.list(player1, player2));

        Tournament tournament = new Tournament(uuid, "test1", new Date().getTime());
        tournament.setEvents(List.list(registration));

        Type tournamentType = new TypeToken<Tournament>() {}.getType();
        String json = GsonFactory.gson().toJson(tournament, tournamentType);

        // Create list
        List<TournamentInfo> list = List.list();
        list = list.cons(new TournamentInfo(tournament.getId(), tournament.getName(), tournament.getTimestamp(), tournament.currentState().some().isFinished(), tournament.currentState().some().isArchived(), tournament.currentState().some().getPlayerStates().length()));

        Type listType = new TypeToken<List<TournamentInfo>>() {}.getType();
        String listJson = GsonFactory.gson().toJson(list, listType);

        // Mock file manager
        ByteArrayInputStream tournamentInputStream = new ByteArrayInputStream(json.getBytes());
        ByteArrayInputStream listInputStream = new ByteArrayInputStream(listJson.getBytes());
        IFileManager mock = mock(IFileManager.class);
        when(mock.fileExistsInDataDir(anyString())).thenReturn(true);
        when(mock.getInputStreamFromDataDirPath(contains("tournaments.json"))).thenReturn(listInputStream);
        when(mock.getInputStreamFromDataDirPath(contains("" + tournament.getId()))).thenReturn(tournamentInputStream);

        JsonDataProvider dataProvider = new JsonDataProvider(getContext(), mock);

        // Test
        List<TournamentInfo> tournaments = dataProvider.getTournaments();
        assertEquals(1, tournaments.length());

        TournamentInfo retrievedTournament = tournaments.head();
        assertEquals("test1", retrievedTournament.getName());
        assertEquals(uuid, retrievedTournament.getId());

        Tournament tournamentDetails = dataProvider.getTournament(retrievedTournament.getId());
        List<Event> events = tournamentDetails.getEvents();
        assertEquals(1, events.length());

        Registration regEvent = (Registration)events.head();
        List<Player> players = regEvent.getPlayers();
        assertEquals(2, players.length());

        Player newPlayer1 = players.head();
        assertEquals("FirstName1 LastName1", newPlayer1.getFullName());

        Player newPlayer2 = players.tail().head();
        assertEquals("FirstName2 LastName2", newPlayer2.getFullName());
    }

    public void testGetTournamentsEmpty() throws Exception {
        try {
            IFileManager mock = mock(IFileManager.class);
            when(mock.fileExistsInDataDir(anyString())).thenReturn(true);
            when(mock.getInputStreamFromDataDirPath(anyString())).thenThrow(IOException.class);

            JsonDataProvider dataProvider = new JsonDataProvider(getContext(), mock);

            List<TournamentInfo> tournaments = dataProvider.getTournaments();
            fail("Expected exception not reached");
        } catch (IOException ex) {
            // All good.
        }
    }

    public void testGetTournamentsException() throws Exception {
        try {
            IFileManager mock = mock(IFileManager.class);
            when(mock.fileExistsInDataDir(anyString())).thenReturn(true);
            when(mock.getInputStreamFromDataDirPath(anyString())).thenThrow(IOException.class);

            JsonDataProvider dataProvider = new JsonDataProvider(getContext(), mock);

            List<TournamentInfo> tournaments = dataProvider.getTournaments();
            fail("Expected exception not reached");
        } catch (IOException ex) {
            // All good.
        }
    }

    public void testSaveTournamentToEmptyList() throws Exception {
        Date now = new Date();
        String uuid = UUID.randomUUID().toString();

        // Create tournament to save.
        Player player1 = new Player("FirstName1", "LastName1");
        Player player2 = new Player("FirstName2", "LastName2");
        Registration registration = new Registration(now.getTime(), TournamentConfig.defaultConfig(), TournamentFormat.SingleElimination, List.list(player1, player2));

        Tournament tournament = new Tournament(uuid, "test1", now.getTime());
        tournament.setEvents(List.list(registration));

        // Create output stream and mocked file manager with that stream.
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream indexOutputStream = new ByteArrayOutputStream();

        IFileManager mock = mock(IFileManager.class);
        when(mock.fileExistsInDataDir(anyString())).thenReturn(false);
        when(mock.getOutputStreamFromDataDirPath(contains("tournaments.json"), anyBoolean())).thenReturn(indexOutputStream);
        when(mock.getOutputStreamFromDataDirPath(contains("" + tournament.getId()), anyBoolean())).thenReturn(outputStream);

        // Create provider with mocked file manager.
        JsonDataProvider dataProvider = new JsonDataProvider(getContext(), mock);
        dataProvider.saveTournament(tournament);

        String output = new String(outputStream.toByteArray());
        assertNotNull(output);
        //assertEquals("{\"events\":[{\"CLASSNAME\":\"ru.aint.mtg.tournament.model.events.Registration\",\"INSTANCE\":{\"config\":{\"pointPerMatchDraw\":1,\"pointsPerGameDraw\":1,\"pointsPerGameWin\":3,\"pointsPerMatchWin\":3,\"tieBreakers\":[],\"winsPerMatch\":2},\"format\":\"SingleElimination\",\"players\":[{\"fullName\":\"FirstName1 LastName1\"},{\"fullName\":\"FirstName2 LastName2\"}],\"timestamp\":" + now.getTime() + "}}],\"id\":\"" + uuid + "\",\"name\":\"test1\",\"timestamp\":" + now.getTime() + "}", output);
    }
}
