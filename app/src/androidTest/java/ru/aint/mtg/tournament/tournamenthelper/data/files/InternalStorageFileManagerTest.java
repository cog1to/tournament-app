package ru.aint.mtg.tournament.tournamenthelper.data.files;

import android.app.Application;
import android.test.ApplicationTestCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * DefaultFileManager test.
 */
public class InternalStorageFileManagerTest extends ApplicationTestCase<Application> {

    public InternalStorageFileManagerTest() {
        super(Application.class);
    }

    final String testFilePath = "testfile";
    final String missingFilePath = "missingfile";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        createApplication();

        // Delete all test files.
        File file1 = new File(getContext().getFilesDir(), missingFilePath);
        if (file1.exists()) {
            file1.delete();
        }

        File file2 = new File(getContext().getFilesDir(), testFilePath);
        if (file2.exists()) {
            file2.delete();
        }
    }

    public void testGetDataDir() throws Exception {
        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        File dataDir = fileManager.getDataDir();

        assertNotNull(dataDir);
    }

    public void testFileExistsInDataDir() throws Exception {
        File file = new File(getContext().getFilesDir(), missingFilePath);
        if (file.exists()) {
            assertTrue(file.delete());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        assertFalse(fileManager.fileExistsInDataDir(missingFilePath));
    }

    public void testFileExistsInDataDirWithFile() throws Exception {
        File file = new File(getContext().getFilesDir(), testFilePath);
        if (!file.exists()) {
            assertTrue(file.createNewFile());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        assertTrue(fileManager.fileExistsInDataDir(testFilePath));

        boolean deleted = file.delete();
        assertTrue(deleted);
    }

    public void testGetInputStreamFromDataDirPath() throws Exception {
        File file = new File(getContext().getFilesDir(), missingFilePath);
        if (file.exists()) {
            assertTrue(file.delete());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        assertNull(fileManager.getInputStreamFromDataDirPath(missingFilePath));
    }

    public void testGetInputStreamFromDataDirPathWithFile() throws Exception {
        File file = new File(getContext().getFilesDir(), testFilePath);
        if (!file.exists()) {
            assertTrue(file.createNewFile());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        InputStream stream = fileManager.getInputStreamFromDataDirPath(testFilePath);
        assertNotNull(stream);

        stream.close();
        boolean deleted = file.delete();
        assertTrue(deleted);
    }

    public void testGetOutputStreamFromDataDirPathWithoutFile() throws Exception {
        File file = new File(getContext().getFilesDir(), missingFilePath);
        if (file.exists()) {
            assertTrue(file.delete());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        OutputStream stream = fileManager.getOutputStreamFromDataDirPath(missingFilePath, false);
        assertNull(stream);
    }

    public void testGetOutputStreamFromDataDirPathWithFile() throws Exception {
        File file = new File(getContext().getFilesDir(), testFilePath);
        if (!file.exists()) {
            assertTrue(file.createNewFile());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        OutputStream stream = fileManager.getOutputStreamFromDataDirPath(testFilePath, false);
        assertNotNull(stream);
        stream.close();

        boolean deleted = file.delete();
        assertTrue(deleted);
    }

    public void testGetOutputStreamFromDataDirPathWithFileOverwrite() throws Exception {
        File file = new File(getContext().getFilesDir(), testFilePath);
        if (file.exists()) {
            assertTrue(file.delete());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        OutputStream stream = fileManager.getOutputStreamFromDataDirPath(testFilePath, true);
        assertNotNull(stream);
        stream.close();

        boolean deleted = file.delete();
        assertTrue(deleted);
    }

    public void testGetOutputStreamFromDataDirPathWithFileOverwriteWithData() throws Exception {
        File file = new File(getContext().getFilesDir(), testFilePath);
        if (file.exists()) {
            assertTrue(file.delete());
        }

        InternalStorageFileManager fileManager = new InternalStorageFileManager(getContext());
        OutputStream stream = fileManager.getOutputStreamFromDataDirPath(testFilePath, true);
        assertNotNull(stream);

        // Write something to a file.
        String sampleString = "sample string";
        stream.write(sampleString.getBytes());
        stream.close();

        // Check that it was written.
        InputStream inputStream = fileManager.getInputStreamFromDataDirPath(testFilePath);
        assertNotNull(inputStream);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        inputStream.close();
        assertEquals(sampleString, out.toString());

        boolean deleted = file.delete();
        assertTrue(deleted);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        // Delete all test files.
        File file1 = new File(getContext().getFilesDir(), missingFilePath);
        if (file1.exists()) {
            file1.delete();
        }

        File file2 = new File(getContext().getFilesDir(), testFilePath);
        if (file2.exists()) {
            file2.delete();
        }
    }
}